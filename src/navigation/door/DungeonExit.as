package navigation.door 
{
	import navigation.Location;
	import navigation.location.Dungeon;
	import navigation.Room;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DungeonExit extends Exit 
	{
		private var _room:Room;
		
		public function DungeonExit(id:String, location:Dungeon, room:Room) 
		{
			super(id, location);
			
			_room = room;
			_room.door = this;
		}
		
		public function get room():Room 
		{
			return _room;
		}
		
		override public function onEnter():void 
		{
			(_location as Dungeon).currentRoom = _room;
			GameEngine.navigationManager.currentLocation = _location;
		}
		
	}

}