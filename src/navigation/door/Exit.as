package navigation.door 
{
	import navigation.IDoor;
	import navigation.Location;
	
	/**
	 * Exits are the other part of the door pairing system, and should be paired with an entrance.
	 * 
	 * @author GnW Team
	 */
	public class Exit implements IDoor 
	{
		protected var _location:Location;
		protected var _otherDoor:Entrance;
		protected var _id:String;
		
		public function Exit(id:String, location:Location) 
		{
			_id = id;
			_location = location;
		}
		
		/**
		 * The entrance paired with this exit.
		 */
		internal function set entrance(entrance:Entrance):void
		{
			if (_otherDoor != null)
				throw new Error("The exit \"" + _id + "\" has already been paired with the door \"" + _otherDoor.id + "\".");
			
			_otherDoor = entrance;
		}
		
		/**
		 * The exit's globally unique ID. Should also be unique among entrances.
		 */
		public function get id():String
		{
			return _id;
		}
		
		/**
		 * The location of the exit. Should not be the same location as the paired entrance.
		 */
		public function get location():Location 
		{
			return _location;
		}
		
		/**
		 * The entrance in the entrance/exit pair.
		 */
		public function get otherDoor():IDoor 
		{
			return _otherDoor;
		}
		
		/**
		 * Runs when the door is entered from the other door.
		 */
		public function onEnter():void 
		{
			GameEngine.navigationManager.currentLocation = _location;
		}
		
	}

}