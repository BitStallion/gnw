package navigation.door 
{
	import navigation.IDoor;
	import navigation.Location;
	
	/**
	 * Entrances are one part of the door pairing system, and should be paired with an exit. Entrances are responsible for binding the pair
	 * together via the 'attachExit' function.
	 * 
	 * @author GnW Team
	 */
	public class Entrance implements IDoor 
	{
		protected var _location:Location;
		protected var _otherDoor:Exit;
		protected var _id:String;
		
		public function Entrance(id:String, location:Location) 
		{
			_id = id;
			_location = location;
		}
		
		/**
		 * Connects this entrance to an exit and visa versa. Entrances can only be paired once.
		 * 
		 * @param	exit	The exit that will be connected to this entrance.
		 */
		public function attachExit(exit:Exit):void {
			if (exit == null)
				throw new Error("The entrance \"" + id + "\" must be paired with a valid exit.");
				
			if (_otherDoor != null)
				throw new Error("The entrance \"" + id + "\" is already paired with the exit \""+ _otherDoor.id + "\".");
			
			_otherDoor = exit;
			exit.entrance = this;
		}
		
		/**
		 * The entrance's globally unique ID. Should also be unique among exits.
		 */
		public function get id():String
		{
			return _id;
		}
		
		/**
		 * The location of the entrance. Should not be the same location as the paired exit.
		 */
		public function get location():Location 
		{
			return _location;
		}
		
		/**
		 * The exit in the entrance/exit pair.
		 */
		public function get otherDoor():IDoor 
		{
			return _otherDoor;
		}
		
		/**
		 * Runs when the door is entered from the other door.
		 */
		public function onEnter():void 
		{
			GameEngine.navigationManager.currentLocation = _location;
		}
		
	}

}