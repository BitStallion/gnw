package navigation
{
	import data.Action;
	import data.IActionInvoker;
	import enums.TriggerType;
	import events.Command;
	
	/**
	 * Rooms are parts of floors and are connected by corridors.
	 *
	 * @author GnW Team
	 */
	public class Room implements IActionInvoker
	{
		private var _col:int;
		private var _row:int;
		private var _colspan:int;
		private var _rowspan:int;
		private var _id:String;
		private var _text:String;
		private var _imageName:String;
		private var _safe:Boolean;
		
		private var _corridors:Vector.<Corridor>;
		private var _actions:Vector.<Action>;
		private var _commands:Vector.<Command>;
		private var _floor:Floor;
		public var door:IDoor;
		
		public function Room(id:String, row:int, col:int, rowspan:int, colspan:int, floor:Floor, text:String, safe:Boolean = false)
		{
			_id = id;
			_col = col;
			_row = row;
			_colspan = colspan;
			_rowspan = rowspan;
			_floor = floor;
			_floor.addRoom(this);
			_text = text;
			_safe = safe;
			
			_corridors = new Vector.<Corridor>();
			_actions = new Vector.<Action>();
			_commands = new Vector.<Command>();
		}
		
		public function addCorridor(corridor:Corridor):void
		{
			_corridors.push(corridor);
		}
		
		public function get corridors():Vector.<Corridor>
		{
			return _corridors.slice();
		}
		
		/**
		 * Adds a new action to the room.
		 * @param	action	The action to add.
		 */
		public function addAction(action:Action):void 
		{
			_actions.push(action);
		}
		
		/**
		 * Invokes all actions of the specified trigger type.
		 * 
		 * @param	type	The trigger type of the actions to invoke.
		 */
		public function invokeActions(type:TriggerType):void 
		{
			for each (var action:Action in _actions)
			{
				if (action.trigger == type )
					GameEngine.actionManager.queueAction(action);
			}
		}
		
		/**
		 * Adds a new Command to the room.
		 * 
		 * @param	command
		 */
		public function addCommand(command:Command):void
		{
			_commands.push(command);
		}
		
		/**
		 * All commands owned by the room.
		 */
		public function get commands():Vector.<Command>
		{
			var commandList:Vector.<Command> = new Vector.<Command>();
			
			for each (var command:Command in _commands)
				commandList.push(command);
			
			return commandList;
		}
		
		public function get floor():Floor 
		{
			return _floor;
		}
		
		public function get col():int 
		{
			return _col;
		}
		
		public function get row():int 
		{
			return _row;
		}
		
		public function get colspan():int 
		{
			return _colspan;
		}
		
		public function get rowspan():int 
		{
			return _rowspan;
		}
		
		public function get isEntrance():Boolean
		{
			return door != null;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get text():String 
		{
			return _text;
		}
		
		public function set imageName(value:String):void 
		{
			_imageName = value;
		}
		
		public function get imageName():String 
		{
			return _imageName;
		}
		
		public function get safe():Boolean 
		{
			return _safe;
		}
	
	}

}