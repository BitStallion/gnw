package navigation 
{
	
	/**
	 * This interface should be used by classes that allow for transportation between locations. Each door should be paired with
	 * another door, and each door in the pair should reside in different location.
	 * 
	 * @author GnW Team
	 */
	public interface IDoor 
	{
		/**
		 * The door's globally unique ID.
		 */
		function get id():String;
		
		/**
		 * The location that the door resides in.
		 */
		function get location():Location;
		
		/**
		 * The other door in the door pair.
		 */
		function get otherDoor():IDoor;
		
		/**
		 * Runs when the door is entered from the other door.
		 */
		function onEnter():void;
	}
	
}