package navigation
{
	import data.Action;
	import data.IXMLDataLoader;
	import enums.StairType;
	import events.Command;
	import enums.TriggerType;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import gui.PropertyChangedEvent;
	import navigation.door.*;
	import navigation.location.Dungeon;
	import enums.Utils;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class NavigationManager extends EventDispatcher implements IXMLDataLoader
	{
		public static const LOCATION_CHANGED_EVENT:String = "Location_changed";
		
		private var _locations:Dictionary;
		private var _doors:Dictionary;
		
		private var _currentLocation:Location;
		
		public function NavigationManager()
		{
		}
		
		public function startLoading():void
		{
			_locations = new Dictionary();
			_doors = new Dictionary();
		}
		
		public function canHandleXMLType(name:String):Boolean
		{
			return name == "dungeons";
		}
		
		public function readXMLData(file:String, xml:XML):void
		{
			if(xml.name() == "dungeons")
			{
				loadDungeonsFromXML(xml);
			}
		}
		
		public function addLocation(location:Location):void
		{
			if(location.id == null || location.id.length == 0)
				throw new Error("Cannot add a location with a null or empty id.");
			
			if(location.id in _locations)
				throw new Error("A location with the id \"" + location.id + "\" has already been added to the location manager.");
			
			_locations[location.id] = location;
		}
		
		public function getLocationById(id:String):Location
		{
			if(id == null)
				return null;
			
			return _locations[id];
		}
		
		public function addDoor(door:IDoor):void
		{
			if(door.id == null || door.id.length == 0)
				throw new Error("Cannot add a door with a null or empty id.");
			
			if(door.id in _doors)
				throw new Error("A door with the id \"" + door.id + "\" has already been added to the location manager.");
			
			_doors[door.id] = door;
		}
		
		public function getDoorById(id:String):Location
		{
			if(id == null)
				return null;
			
			return _doors[id];
		}
		
		public function enterDoor(door:IDoor):void
		{
			door.otherDoor.onEnter();
		}
		
		public function get currentLocation():Location
		{
			return _currentLocation;
		}
		
		public function set currentLocation(value:Location):void
		{
			var event:PropertyChangedEvent = new PropertyChangedEvent(LOCATION_CHANGED_EVENT, _currentLocation, value);
			_currentLocation = value;
			dispatchEvent(event);
		}
		
		private function loadDungeonsFromXML(dungeons:XML):void
		{
			for each(var dungeonData:XML in dungeons.*)
			{
				var dungeon:Dungeon = new Dungeon(dungeonData.@id, dungeonData.@name);
				
				extractFloors(dungeonData, dungeon);
				
				addLocation(dungeon);
			}
		}
		
		private function extractFloors(dungeonData:XML, dungeon:Dungeon):void
		{
			for each(var floorData:XML in dungeonData.floor)
			{
				var floor:Floor = new Floor(floorData.@level, dungeon, floorData.@defaultImage);
				var actionData:XML;
				var action:Action;
				
				for each(var roomData:XML in floorData.room)
				{
					var safe:Boolean = roomData.@safe == "true";
					
					var room:Room = new Room(roomData.@id, roomData.@row, roomData.@col, roomData.@rowspan, roomData.@colspan, floor, roomData.text[0].toString(), safe);
					
					room.imageName = roomData.@image.toXMLString();
					
					for each(actionData in roomData.action)
					{
						action = Action.buildAction(actionData.@type, actionData.@trigger, actionData.value);
						room.addAction(action);
					}
					
					for each(var commandData:XML in roomData.command)
					{
						var command:Command = new Command(commandData.@text);
						
						for each(actionData in commandData.action)
						{
							action = Action.buildAction(actionData.@type, TriggerType.OnInteraction.text, actionData.value);
							command.addAction(action);
						}
						
						room.addCommand(command);
					}
				}
				
				for each(var corridorData:XML in floorData.corridor)
				{
					var entryRoom:Room = floor.getRoom(corridorData.@entryRoom);
					var exitRoom:Room = floor.getRoom(corridorData.@exitRoom);
					
					if(entryRoom == null || exitRoom == null)
						throw new Error("One or both of the rooms that connects the corridor doesn't exist." + " The expected rooms are '" + corridorData.@entryRoom + "' and '" + corridorData.@exitRoom + "'");
					
					var corridor:Corridor = new Corridor(entryRoom, exitRoom, floor);
				}
				
				for each(var entranceData:XML in floorData.entrance)
				{
					var entrance:DungeonEntrance = new DungeonEntrance(entranceData.@id, dungeon, floor.getRoom(entranceData.@room));
					addDoor(entrance);
					if(_doors.hasOwnProperty(entranceData.@exit))
					{
						entrance.attachExit(_doors[entranceData.@exit.toXMLString()])
					}
				}
				
				for each(var exitData:XML in floorData.exit)
				{
					var exit:DungeonExit = new DungeonExit(exitData.@id, dungeon, floor.getRoom(exitData.@room));
					addDoor(exit);
					if(_doors.hasOwnProperty(exitData.@entrance))
					{
						(_doors[exitData.@entrance.toXMLString()] as Entrance).attachExit(exit);
					}
				}
				
				for each(var enemyData:XML in floorData.enemy)
				{
					floor.addEnemy(enemyData.value);
				}
			}
		}
	}

}