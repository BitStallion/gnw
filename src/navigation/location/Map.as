package navigation.location 
{
	import navigation.Location
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Map extends Location 
	{
		private var _places:Vector.<Location>;
		
		public function Map(id:String, name:String) 
		{
			super(id, name);
			
		}
		
		public function get places():Vector.<Location> 
		{
			return _places.slice();
		}
		
	}

}