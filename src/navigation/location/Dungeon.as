package navigation.location 
{
	import data.Action;
	import flash.utils.Dictionary;
	import enums.*;
	import gui.PropertyChangedEvent;
	import navigation.Floor;
	import navigation.Location;
	import navigation.Room;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Dungeon extends Location 
	{
		private var _floors:Dictionary
		private var _currentRoom:Room;
		
		public static const ROOM_CHANGED:String = "Room changed";
		
		public function Dungeon(id:String, name:String) 
		{
			super(id, name);
			
			_floors = new Dictionary();
		}
		
		public function addFloor(floor:Floor):void
		{
			if (floor.level == null || floor.level.length == 0 )
				throw new Error("Cannot add a floor with a null or empty id.");
			
			if (floor.level in _floors)
				throw new Error("A floor with the level name \"" + floor.level + "\" has already been added to the dungeon\"" + _id + "\".");
			
			_floors[floor.level] = floor;
		}
		
		public function getFloorByLevelName(level:String):Floor
		{
			return _floors[level];
		}
		
		public function get currentRoom():Room 
		{
			return _currentRoom;
		}
		
		public function set currentRoom(value:Room):void
		{
			var event:PropertyChangedEvent = new PropertyChangedEvent(ROOM_CHANGED, _currentRoom, value);
			if(_currentRoom != null)
				_currentRoom.invokeActions(TriggerType.OnExit);
				
			_currentRoom = value;
			
			if (_currentRoom != null)
			{
				_currentRoom.invokeActions(TriggerType.OnEnter)
				getNextCombat(value);
			}
				
			dispatchEvent(event);
		}
		
		private function getNextCombat(room:Room):void {
			if (room.safe)
				return;
			
			if (Math.random() > 0.15)
				return;
			
			var list:Vector.<Action> = room.floor.allEnemies();
			
			if (list.length == 0)
				return;
			
			var rand:int = Math.floor(Math.random() * list.length);
			
			GameEngine.actionManager.queueAction(list[rand]);
		}
		
		public function getRoom(level:String, roomId:String ):Room
		{
			var floor:Floor = getFloorByLevelName(level);
			return floor.getRoom(roomId);
		}
		
	}

}