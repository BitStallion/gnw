package navigation 
{
	import enums.*;
	import data.Entity;
	import data.Action;
	import flash.utils.Dictionary;
	import navigation.location.Dungeon;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Floor 
	{
		private var _rooms:Dictionary;
		private var _corridors:Vector.<Corridor>;
		private var _enemyList:Vector.<Action>;
		
		private var _level:String;
		private var _location:Dungeon;
		private var _image:String;
		
		public function Floor(level:String, location:Dungeon, image:String=null) 
		{
			_rooms = new Dictionary();
			_corridors = new Vector.<Corridor>();
			_enemyList = new Vector.<Action>();
			
			_level = level;
			_location = location;
			_location.addFloor(this);
			_image = image;
		}
		
		/**
		 * Adds a new room to the floor. Rooms must have unique col and row coordinates on each floor.
		 * 
		 * @param	room	The room to add to the floor.
		 */
		public function addRoom(room:Room):void
		{
			if (room.id == null || room.id.length == 0 )
				throw new Error("Cannot add a location with a null or empty id.");
			
			if (room.id in _rooms)
				throw new Error("A room with the id \"" + room.id + "\" has already been added to the floor "+ _level +".");
			
			_rooms[room.id] = room;
		}
		
		/**
		 * Returns a room based with the passed ID.
		 * 
		 * @param	id	ID of the room.
		 * @return	The room at those coordinates or null if no room exists.
		 */
		public function getRoom(id:String):Room 
		{
			return _rooms[id];
		}
		
		public function allRooms():Vector.<Room>
		{
			var rooms:Vector.<Room> = new Vector.<Room>();
			for each (var room:Room in _rooms)
			{
				rooms.push(room);
			}
			return rooms;
		}
		
		public function addEnemy(values:XMLList):void
		{
			var valueList:Dictionary = new Dictionary();
			
			for each (var val:XML in values)
			{
				valueList[val.@key.toString()] = val.@value.toString();
			}
			
			_enemyList.push(new Action(ActionType.combat, TriggerType.OnEnter, valueList));
		}
		
		public function allEnemies():Vector.<Action>
		{
			return _enemyList.slice();
		}
		
		public function allCorridors():Vector.<Corridor>
		{
			return _corridors.slice();
		}
		
		public function addCorridor(corridor:Corridor):void
		{
			_corridors.push(corridor);
		}
		
		public function get location():Dungeon
		{
			return _location;
		}
		
		public function get level():String 
		{
			return _level;
		}
		
		public function get image():String 
		{
			return _image;
		}
		
	}

}