package navigation 
{
	/**
	 * Forms a connection between two rooms in a floor. Corriodors can also be locked, preventing access to the other room.
	 * 
	 * @author GnW Team
	 */
	public class Corridor 
	{
		private var _entryRoom:Room;
		private var _exitRoom:Room;
		private var _floor:Floor;
		
		public var locked:Boolean;
		
		public function Corridor(entryRoom:Room, exitRoom:Room, floor:Floor, locked:Boolean = false ) 
		{
			_entryRoom = entryRoom;
			_entryRoom.addCorridor(this);
			_exitRoom = exitRoom;
			_exitRoom.addCorridor(this);
			_floor = floor;
			floor.addCorridor(this);
			locked = locked;
		}
		
		public function get entryRoom():Room 
		{
			return _entryRoom;
		}
		
		public function get exitRoom():Room 
		{
			return _exitRoom;
		}
		
		public function getOtherRoom(room:Room):Room 
		{
			if (room == _entryRoom)
				return _exitRoom;
			else if (room == _exitRoom)
				return _entryRoom
				
			return null;
		}
		
		public function get floor():Floor 
		{
			return _floor;
		}
		
	}

}