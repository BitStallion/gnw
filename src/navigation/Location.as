package navigation 
{
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Location extends EventDispatcher
	{
		protected var _id:String;
		protected var _name:String;
		
		public function Location(id:String, name:String) 
		{
			_id = id;
			_name = name;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get name():String 
		{
			return _name;
		}
		
	}
}