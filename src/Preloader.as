package 
{
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.utils.getDefinitionByName;
	import gui.Game;
	
	/**
	 * Preloader class. Plays a preloader before the game is loaded and indicates the current percentage of the game
	 * that has been loaded.
	 * @author GnW Team
	 */
	public class Preloader extends MovieClip 
	{
		private var loaderBox:Sprite;
		private var loaderBar:Sprite;
		
		private const LOADER_WIDTH:Number = 294;
		
		public function Preloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			
			loaderBox = new Sprite();
			loaderBox.graphics.beginFill(Colors.CARBON, 0);
			loaderBox.graphics.lineStyle(3, Colors.LASER_GREEN);
			loaderBox.graphics.drawRect(0, 0, 300, 30);
			loaderBox.graphics.endFill();
			
			loaderBar = new Sprite();
			loaderBar.graphics.beginFill(Colors.LASER_GREEN);
			loaderBar.graphics.drawRect(3, 3, LOADER_WIDTH, 24);
			loaderBar.graphics.endFill();
			loaderBox.addChild(loaderBar);
			
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			loaderBox.x = (stage.stageWidth - loaderBox.width) / 2;
			loaderBox.y = (stage.stageHeight - loaderBox.height) / 2;
			addChild(loaderBox);
			loaderBar.width = 0;
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			loaderBar.width = (e.bytesLoaded / e.bytesTotal) * LOADER_WIDTH;
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			loaderBox.visible = false;
			
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
	}
	
}