package map
{
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import map.geom.BaseShape;
	import map.geom.CollisionDetection;
	import mx.utils.StringUtil;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapFileData
	{
		public static function getMapLayerData(xmlData:XML):ByteArray
		{
			var result:ByteArray;
			if(xmlData.hasOwnProperty("@encoding"))
			{
				result = MapCoder.getCoder(xmlData.@["encoding"].toString()).decode(xmlData.toString());
			}
			else
			{
				result = MapCoder.getCoder("xml").decode(xmlData.toString());
			}
			if(xmlData.hasOwnProperty("@compression"))
			{
				if(xmlData.@["compression"].toString() == "zlib")
				{
					result.uncompress(CompressionAlgorithm.ZLIB);
				}
			}
			return result;
		}
		
		public var tilesets:Vector.<String>;
		public var layerData:Vector.<String>;
		public var mapData:Vector.<MapPart>;
		public var telDestData:Dictionary;
		
		public function MapFileData()
		{
			tilesets = new Vector.<String>();
			layerData = new Vector.<String>();
			mapData = new Vector.<MapPart>();
			telDestData = new Dictionary();
		}
		
		public function loadMapaData(file:String, data:XML):void
		{
			var tileBB:Rectangle = new Rectangle(0, 0, 1, 1);
			for each(var xmlLayerData:XML in data.layer)
			{
				layerData.push(xmlLayerData.@name);
			}
			for each(var xmlMapPart:XML in data.mapPart) //Read the different map parts
			{
				var currentMapFileData:XML = (new XML(GameEngine.fileResourceManager.getResource(xmlMapPart.@file.toString())));
				var currentMapPart:MapPart = new MapPart(xmlMapPart.@file.toString(), parseInt(currentMapFileData.@width.toString()), parseInt(currentMapFileData.@height.toString()), parseInt(currentMapFileData.@x.toString()), parseInt(currentMapFileData.@y.toString()), parseInt(currentMapFileData.@z.toString()));
				var telDestGroupID:String = xmlMapPart.@gid.toString() || "default";
				if(!telDestData.hasOwnProperty(telDestGroupID)) telDestData[telDestGroupID] = new Dictionary();
				var telDestGroupData:Dictionary = telDestData[telDestGroupID];
				for each(var xmlTileset:XML in currentMapFileData.tileset)
				{
					if(xmlTileset.hasOwnProperty("@source"))
					{
						var path:String = GameEngine.fileResourceManager.processPath(file + "/../" + xmlTileset.@source);
						currentMapPart.tilesetData[parseInt(xmlTileset.@firstgid)] = path;
						if(tilesets.indexOf(path) < 0) tilesets.push(path);
					}
				}
				for each(var xmlObjectGroups:XML in currentMapFileData.objectgroup)
				{
					for each(var objEntry:XML in xmlObjectGroups.object)
					{
						if(objEntry.@type.toString() == "teldest")
						{
							var s:BaseShape = BaseShape.loadShape(objEntry, currentMapPart.rect.x, currentMapPart.rect.y);
							if(s != null)
							{
								if(!telDestGroupData.hasOwnProperty(s.name)) telDestGroupData[s.name] = new Vector.<Point3D>();
								var telDestPoints:Vector.<Point3D> = telDestGroupData[telDestGroupID];
								var bb:Rectangle = s.boundingBox;
								for(tileBB.y = Math.floor(bb.y); tileBB.y <= Math.ceil(bb.height); tileBB.y++)
								{
									for(tileBB.x = Math.floor(bb.x); tileBB.x <= Math.ceil(bb.width); tileBB.x++)
									{
										if(CollisionDetection.Collides(tileBB, s))
										{
											telDestPoints.push(new Point3D(tileBB.x, tileBB.y, currentMapPart.z));
										}
									}
								}
							}
						}
					}
				}
				for each(var compMapPart:MapPart in mapData)
				{
					if(currentMapPart.rect.intersects(compMapPart.rect)) //Check to see if the current map part occupies the same space as another
					{
						var rect:Rectangle = currentMapPart.rect.intersection(compMapPart.rect);
						var compMapFileData:XML = (new XML(GameEngine.fileResourceManager.getResource(compMapPart.file)));
						for each(var xmlCurrentMapLayer:XML in currentMapFileData.layer)
						{
							var currentNameArray:Array = xmlCurrentMapLayer.@name.toString().split("_", 2);
							var currentFloor:int = parseInt(StringUtil.trim(currentNameArray[0])) + currentMapPart.z;
							var currentLayerName:String = StringUtil.trim(currentNameArray[1]);
							for each(var xmlCompMapLayer:XML in compMapFileData.layer)
							{
								var compNameArray:Array = xmlCompMapLayer.@name.toString().split("_", 2);
								var compFloor:int = parseInt(StringUtil.trim(compNameArray[0])) + compMapPart.z;
								var compLayerName:String = StringUtil.trim(compNameArray[1]);
								if(currentFloor == compFloor && currentLayerName == compLayerName) //Check to see if they edit the same layer
								{
									var currentLayerData:ByteArray = getMapLayerData(xmlCurrentMapLayer.data[0]);
									var compLayerData:ByteArray = getMapLayerData(xmlCompMapLayer.data[0]);
									for(var y:int = rect.y; y < rect.bottom; y++) //Do a tile by tile check of the overlapped area
									{
										for(var x:int = rect.x; x < rect.right; x++)
										{
											currentLayerData.position = ((y - currentMapPart.rect.y) * currentMapPart.rect.width + (x - currentMapPart.rect.x)) * 4;
											compLayerData.position = ((y - compMapPart.rect.y) * compMapPart.rect.width + (x - compMapPart.rect.x)) * 4;
											var currentID:uint = currentLayerData.readUnsignedInt();
											var compID:uint = compLayerData.readUnsignedInt();
											if(currentID != 0 && compID != 0)
											{
												throw new GnWError("Map collision detected between \"" + currentMapPart.file + "\"(" + (x - currentMapPart.rect.x) + ", " + (y - currentMapPart.rect.y) + ", " + (currentFloor - currentMapPart.z) + ", \"" + xmlCurrentMapLayer.@name.toString() + "\") and \"" + compMapPart.file + "\"(" + (x - compMapPart.rect.x) + ", " + (y - compMapPart.rect.y) + ", " + (compFloor - compMapPart.z) + ", \"" + xmlCompMapLayer.@name.toString() + "\")");
											}
										}
									}
									break;
								}
							}
						}
					}
				}
				mapData.push(currentMapPart); //Everything checks out so far, add it to the list
			}
		}
	}
}