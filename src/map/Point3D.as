package map 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Point3D extends Point
	{
		private var z1:Number;
		
		public function Point3D(x:Number, y:Number, z:Number) 
		{
			super(x, y);
			this.z1 = z;
		}
		
		public function set z(z:Number):void
		{
			this.z1 = z;
		}
		
		public function get z():Number
		{
			return this.z1;
		}
		
		override public function get length():Number
		{
			return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z1, 2));
		}
	}
}