package map 
{
	import data.Entity;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import map.tiles.*;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Tile 
	{
		/* This allows for these classes to be dynamically loaded in later as that by default AS3 tries to be efficient by not
		 * compiling classes that aren't explicitly mentioned somewhere in the code
		 */
		TileGenericFloor;
		TileSafeFloor;
		TileGenericSign;
		TileGenericWall;
		
		public static const north:uint = 1;
		public static const south:uint = 2;
		public static const east:uint = 4;
		public static const west:uint = 8;
		
		private var _id:int;
		private var _file:String;
		private var _localID:uint;
		private var _image:BitmapData;
		private var _imageRect:Rectangle;
		
		public function Tile(id:int, file:String, localID:uint, tileProperties:XML, image:BitmapData, imageRect:Rectangle) 
		{
			TileGenericFloor
			_id = id;
			_file = file;
			_localID = localID;
			_image = image;
			_imageRect = imageRect;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function get areDirectionsSecondary():Boolean
		{
			return true;
		}
		
		public function get exitDirections():uint
		{
			return 15;
		}
		
		public function get enterDirections():uint
		{
			return 15;
		}
		
		public function entityEnters(ent:Entity):void
		{
		}
		
		public function entityExits(ent:Entity):void
		{
		}
		
		public function actions():Dictionary
		{
			return null;
		}
		
		public function get image():BitmapData
		{
			return _image;
		}
		
		public function get imageRect():Rectangle
		{
			return _imageRect;
		}
		
		public function get isSafe():Boolean
		{
			return false;
		}
	}
}