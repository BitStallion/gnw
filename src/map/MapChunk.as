package map 
{
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import map.geom.CollisionDetection;
	import map.geom.PolygonShape;
	import map.geom.RectangleShape;
	import map.geom.BaseShape;
	import mx.utils.StringUtil;
	import utility.IDestroyable;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapChunk implements IDestroyable
	{
		public static const chunkXShiftBits:uint = 6
		public static const chunkXMaskBits:uint = int.MAX_VALUE >> (31 - chunkXShiftBits);
		public static const chunkYShiftBits:uint = 6
		public static const chunkYMaskBits:uint = int.MAX_VALUE >> (31 - chunkYShiftBits);
		public static const chunkRect:Rectangle = new Rectangle(0, 0, 1 << chunkXShiftBits, 1 << chunkYShiftBits);
		public static const chunkArea:int = chunkRect.width * chunkRect.height;
		public static const chunkImageRect:Rectangle = new Rectangle(0, 0, chunkRect.width * 32, chunkRect.height * 32);
		private var _map:Map;
		private var _rect:Rectangle;
		private var _z:int;
		private var _layers:Dictionary;
		private var _objectLayers:Dictionary;
		private var _chunkImage:BitmapData;
		
		public function MapChunk(map:Map, x:int, y:int, z:int) 
		{
			_map = map;
			_rect = new Rectangle();
			_rect.x = x;
			_rect.y = y;
			_rect.width = chunkRect.width;
			_rect.height = chunkRect.height;
			_z = z;
			_layers = new Dictionary();
			_objectLayers = new Dictionary();
			_chunkImage = null;
		}
		
		public function destroy():void
		{
			_map = null;
			_rect = null;
			_layers = null;
			if(_chunkImage != null) _chunkImage.dispose();
			_chunkImage = null;
		}
		
		public function unloadImages():void
		{
			if(_chunkImage != null) _chunkImage.dispose();
			_chunkImage = null;
		}
		
		public function loadFromMapData(data:Vector.<MapPart>):void
		{
			var mapMan:MapManager = GameEngine.mapManager;
			for each(var part:MapPart in data)
			{
				if(_rect.intersects(part.rect))
				{
					var intRect:Rectangle = _rect.intersection(part.rect);
					var xmlMap:XML = new XML(GameEngine.fileResourceManager.getResource(part.file));
					var xmlLayer:XML;
					var nameArray:Array;
					var floor:int;
					var layerName:String;
					for each(xmlLayer in xmlMap.objectgroup)
					{
						nameArray = xmlLayer.@name.toString().split("_", 2);
						floor = parseInt(StringUtil.trim(nameArray[0])) + part.z;
						layerName = StringUtil.trim(nameArray[1]);
						if(floor == _z)
						{
							var layerShapes:Vector.<BaseShape> = _objectLayers[layerName] as Vector.<BaseShape>;
							if(layerShapes == null)
							{
								_objectLayers[layerName] = layerShapes = new Vector.<BaseShape>();
							}
							for each(var objEntry:XML in xmlLayer.object)
							{
								var s:BaseShape = BaseShape.loadShape(objEntry, part.rect.x, part.rect.y);
								if(s != null)
								{
									if(CollisionDetection.Collides(_rect, s))
									{
										layerShapes.push(s);
									}
								}
							}
						}
					}
					for each(xmlLayer in xmlMap.layer)
					{
						nameArray = xmlLayer.@name.toString().split("_", 2);
						floor = parseInt(StringUtil.trim(nameArray[0])) + part.z;
						layerName = StringUtil.trim(nameArray[1]);
						if(floor == _z)
						{
							var layer:ByteArray = _layers[layerName];
							if(layer == null)
							{
								_layers[layerName] = layer = new ByteArray();
								for(var i:int = 0; i < chunkArea; i++)
								{
									layer.writeUnsignedInt(0);
								}
							}
							var fileLayerData:ByteArray = MapFileData.getMapLayerData(xmlLayer.data[0]);
							for(var y:int = intRect.y; y < intRect.bottom; y++)
							{
								for(var x:int = intRect.x; x < intRect.right; x++)
								{
									fileLayerData.position = ((y - part.rect.y) * part.rect.width + x - part.rect.x) * 4;
									layer.position = ((y - _rect.y) * _rect.width + x - _rect.x) * 4;
									var tileID:uint = fileLayerData.readUnsignedInt();
									if(tileID != 0) tileID = part.gIDToID(tileID) || 0;
									if(tileID != 0) layer.writeUnsignedInt(tileID);
								}
							}
						}
					}
				}
			}
		}
		
		public function drawAll():void
		{
			var mapMan:MapManager = GameEngine.mapManager;
			if(_chunkImage == null) _chunkImage = new BitmapData(chunkImageRect.width, chunkImageRect.height, true, 0);
			_chunkImage.fillRect(chunkImageRect, 0);
			for each(var layer:String in _map.mapData.layerData)
			{
				var layerData:ByteArray = _layers[layer] as ByteArray;
				if(layerData != null)
				{
					for(var y:int = 0; y < chunkRect.height; y++)
					{
						for(var x:int = 0; x < chunkRect.width; x++)
						{
							layerData.position = (y * chunkRect.width + x) * 4;
							var tile:Tile = mapMan.getTile(layerData.readUnsignedInt());
							if(tile != null)
							{
								var tileImage:BitmapData = tile.image;
								if(tileImage != null)
								{
									_chunkImage.copyPixels(tileImage, tile.imageRect, new Point(x * 32, y * 32), null, null, true);
								}
							}
						}
					}
				}
			}
		}
		
		public function drawTile(x:int, y:int):void
		{
			var posX:int = x & chunkXMaskBits;
			var posY:int = y & chunkYMaskBits;
			var mapMan:MapManager = GameEngine.mapManager;
			if(_chunkImage == null)
			{
				drawAll();
				return;
			}
			_chunkImage.fillRect(new Rectangle(posX * 32, posY * 32, 32, 32), 0);
			for each(var layer:String in _map.mapData.layerData)
			{
				var layerData:ByteArray = _layers[layer] as ByteArray;
				if(layerData != null)
				{
					layerData.position = (posY * chunkRect.width + posX) * 4;
					var tile:Tile = mapMan.getTile(layerData.readUnsignedInt());
					if(tile != null)
					{
						var tileImage:BitmapData = tile.image;
						if(tileImage != null)
						{
							_chunkImage.copyPixels(tileImage, tile.imageRect, new Point(posX * 32, posY * 32), null, null, true);
						}
					}
				}
			}
		}
		
		public function get chunkRectangle():Rectangle
		{
			return _rect;
		}
		
		public function get chunkImage():BitmapData
		{
			if(_chunkImage == null) drawAll();
			return _chunkImage;
		}
		
		public function get layers():Vector.<String>
		{
			var results:Vector.<String> = new Vector.<String>();
			for(var name:String in _layers)
			{
				results.push(name);
			}
			return results;
		}
		
		public function setBlockID(layer:String, x:int, y:int, id:uint):void
		{
			var layerData:ByteArray = _layers[layer] as ByteArray;
			if(layerData != null)
			{
				layerData.position = ((y & chunkYMaskBits) * _rect.width + (x & chunkXMaskBits)) * 4;
				layerData.writeUnsignedInt(id);
				drawTile(x, y);
			}
		}
		
		public function getTileID(layer:String, x:int, y:int):uint
		{
			var layerData:ByteArray = _layers[layer] as ByteArray;
			if(layerData != null)
			{
				layerData.position = ((y & chunkYMaskBits) * _rect.width + (x & chunkXMaskBits)) * 4;
				return layerData.readUnsignedInt();
			}
			return 0;
		}
		
		public function getTileProperties(layer:String, x:int, y:int, type:String = null):Dictionary
		{
			var results:Dictionary = new Dictionary();
			var layerShapes:Vector.<BaseShape> = _objectLayers[layer] as Vector.<BaseShape>;
			if(layerShapes != null)
			{
				var rect:Rectangle = new Rectangle(x, y, 1, 1);
				for each(var shapeObj:BaseShape in layerShapes)
				{
					if((type == null || shapeObj.type == type) && CollisionDetection.Collides(rect, shapeObj))
					{
						for(var key:Object in shapeObj.properties)
						{
							results[key] = shapeObj.properties[key];
						}
					}
				}
			}
			return results;
		}
	}
}