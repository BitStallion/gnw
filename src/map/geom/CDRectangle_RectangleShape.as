package map.geom 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import map.Tile;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class CDRectangle_RectangleShape extends CollisionDetection 
	{
		public function CDRectangle_RectangleShape() 
		{
			super();
		}
		
		override public function get aClass():Class 
		{
			return Rectangle;
		}
		
		override public function get bClass():Class 
		{
			return RectangleShape;
		}
		
		override public function checkCollision(a:Object, b:Object):Boolean 
		{
			var aObj:Rectangle = a as Rectangle;
			var bObj:RectangleShape = b as RectangleShape;
			if(aObj.intersects(bObj.boundingBox))
			{
				var points:Vector.<Point> = new Vector.<Point>();
				points.push(new Point(bObj.pos.tx, bObj.pos.ty));
				points.push(bObj.pos.transformPoint(new Point(bObj.width, 0)));
				points.push(bObj.pos.transformPoint(new Point(bObj.width, bObj.height)));
				points.push(bObj.pos.transformPoint(new Point(0, bObj.height)));
				var sideCheck:uint = 0;
				for(var i:uint = 1; i < points.length; i++)
				{
					var p:Point = points[i];
					if(aObj.left <= p.x)
					{
						sideCheck |= Tile.west;
					}
					if(aObj.right > p.x)
					{
						sideCheck |= Tile.east;
					}
					if(aObj.top <= p.y)
					{
						sideCheck |= Tile.north;
					}
					if(aObj.bottom > p.y)
					{
						sideCheck |= Tile.south;
					}
					if(sideCheck == 15) return true; // If every side has at least one point on the correct side then the two boxes are colliding
				}
			}
			return false;
		}
	}
}