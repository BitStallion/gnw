package map.geom 
{
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class BaseShape
	{
		public static function loadShape(objEntry:XML, offsetX:Number, offsetY:Number):BaseShape
		{
			var s:BaseShape = null;
			if(objEntry.polygon.length() > 0)
			{
				s = new PolygonShape();
				(s as PolygonShape).loadPoints(objEntry.polygon[0].@points.toString())
			}
			else if(objEntry.polyline.length() > 0)
			{
			}
			else if(objEntry.ellipse.length() > 0)
			{
			}
			else if(objEntry.hasOwnProperty("@gid"))
			{
				s = new RectangleShape(1.0, 1.0);
			}
			else
			{
				s = new RectangleShape((parseFloat(objEntry.@width) || 0) / 32, (parseFloat(objEntry.@height) || 0) / 32);
			}
			if(s != null)
			{
				var m:Matrix = new Matrix();
				m.tx = (parseFloat(objEntry.@x) || 0) / 32 + offsetX;
				m.ty = (parseFloat(objEntry.@y) || 0) / 32 + offsetY;
				m.rotate(((parseFloat(objEntry.@rotation) || 0) * Math.PI) / 180);
				s.pos = m;
				if(objEntry.hasOwnProperty("@name")) s.name = objEntry.@name.toString();
				if(objEntry.hasOwnProperty("@type")) s.type = objEntry.@type.toString();
				var properties:Dictionary = s.properties;
				if(objEntry.properties.length() > 0)
				{
					for each(var prop:XML in objEntry.properties[0].property)
					{
						properties[prop.@name.toString()] = prop.@value.toString();
					}
				}
			}
			return s;
		}
		
		protected var _boundingBox:Rectangle;
		private var _pos:Matrix;
		private var _name:String;
		private var _type:String;
		private var _properties:Dictionary;
		
		public function BaseShape():void
		{
			_pos = new Matrix();
			_boundingBox = new Rectangle(_pos.tx, _pos.ty);
			_name = "";
			_type = "";
			_properties = new Dictionary();
		}
		
		public function set pos(m:Matrix):void
		{
			_pos = m;
			calBB();
		}
		
		public function get pos():Matrix
		{
			return _pos;
		}
		
		public function set name(name:String):void
		{
			_name = name;
		}
		
		public function get name():String
		{
			return _name
		}
		
		public function set type(type:String):void
		{
			_type = type;
		}
		
		public function get type():String
		{
			return _type
		}
		
		public function get properties():Dictionary
		{
			return _properties
		}
		
		protected function calBB():void
		{
		}
		
		public function get boundingBox():Rectangle
		{
			return _boundingBox;
		}
	}
}