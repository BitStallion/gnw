package map.geom 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class CollisionDetection 
	{
		private static var _CollisionDetectors:Vector.<CollisionDetection> = new Vector.<CollisionDetection>();
		
		public static function Collides(a:Object, b:Object):Boolean
		{
			if(_CollisionDetectors.length == 0)
			{
				_CollisionDetectors.push(new CDRectangle_RectangleShape());
				_CollisionDetectors.push(new CDRectangle_PolygonShape());
			}
			for each(var cd:CollisionDetection in _CollisionDetectors)
			{
				if(a is cd.aClass && b is cd.bClass)
				{
					return cd.checkCollision(a, b);
				}
				else if(b is cd.aClass && a is cd.bClass)
				{
					return cd.checkCollision(b, a);
				}
			}
			return false;
		}
		
		public function CollisionDetection() 
		{
		}
		
		public function get aClass():Class
		{
			return null;
		}
		
		public function get bClass():Class
		{
			return null;
		}
		
		public function checkCollision(a:Object, b:Object):Boolean
		{
			return false;
		}
	}
}