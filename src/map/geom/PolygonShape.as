package map.geom 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class PolygonShape extends BaseShape
	{
		private var _points:Vector.<Point>;
		
		public function PolygonShape(points:Vector.<Point> = null) 
		{
			super();
			_points = points || new Vector.<Point>();
		}
		
		public function loadPoints(str:String):void
		{
			for each(var p:String in str.split(" "))
			{
				var pointParts:Array = p.split(",");
				_points.push(new Point(parseFloat(pointParts[0]) / 32, parseFloat(pointParts[1]) / 32));
			}
			calBB();
		}
		
		public function get points():Vector.<Point>
		{
			return _points;
		}
		
		override protected function calBB():void
		{
			_boundingBox.width = 0;
			_boundingBox.height = 0;
			if(_points.length > 0)
			{
				var p:Point = _points[0];
				_boundingBox.x = p.x;
				_boundingBox.y = p.y;
				for(var i:uint = 1; i < _points.length; i++)
				{
					p = _points[i];
					if(_boundingBox.left > p.x)
					{
						_boundingBox.left = p.x;
					}
					if(_boundingBox.right < p.x)
					{
						_boundingBox.right = p.x;
					}
					if(_boundingBox.top > p.y)
					{
						_boundingBox.top = p.y;
					}
					if(_boundingBox.bottom < p.y)
					{
						_boundingBox.bottom = p.y;
					}
				}
			}
		}
	}
}