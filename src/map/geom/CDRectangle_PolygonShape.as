package map.geom 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import map.Tile;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class CDRectangle_PolygonShape extends CollisionDetection 
	{
		public function CDRectangle_PolygonShape() 
		{
			super();
		}
		
		override public function get aClass():Class 
		{
			return Rectangle;
		}
		
		override public function get bClass():Class 
		{
			return PolygonShape;
		}
		
		override public function checkCollision(a:Object, b:Object):Boolean 
		{
			var aObj:Rectangle = a as Rectangle;
			var bObj:PolygonShape = b as PolygonShape;
			if(aObj.intersects(bObj.boundingBox))
			{
				var points:Vector.<Point> = new Vector.<Point>();
				var p:Point;
				for each(p in bObj.points)
				{
					points.push(bObj.pos.transformPoint(p));
				}
				var sideCheck:uint = 0;
				for(var i:uint = 1; i < points.length; i++)
				{
					p = points[i];
					if(aObj.left <= p.x)
					{
						sideCheck |= Tile.west;
					}
					if(aObj.right >= p.x)
					{
						sideCheck |= Tile.east;
					}
					if(aObj.top <= p.y)
					{
						sideCheck |= Tile.north;
					}
					if(aObj.bottom >= p.y)
					{
						sideCheck |= Tile.south;
					}
					if(sideCheck == 15) return true; // If every side has at least one point on the correct side then the two shapes are colliding
				}
			}
			return false;
		}
	}
}