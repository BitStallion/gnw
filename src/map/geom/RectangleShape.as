package map.geom 
{
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class RectangleShape extends BaseShape 
	{
		private var _width:Number;
		private var _height:Number;
		
		public function RectangleShape(width:Number = 0, height:Number = 0) 
		{
			super();
			_width = width;
			_height = height;
			calBB();
		}
		
		public function set width(num:Number):void
		{
			_width = num;
			calBB();
		}
		
		public function get width():Number
		{
			return _width;
		}
		
		public function set height(num:Number):void
		{
			_height = num;
			calBB();
		}
		
		public function get height():Number
		{
			return _height;
		}
		
		override protected function calBB():void
		{
			var topLeft:Point = new Point(pos.tx, pos.ty);
			var points:Vector.<Point> = new Vector.<Point>();
			points.push(topLeft);
			points.push(pos.transformPoint(new Point(width, 0)));
			points.push(pos.transformPoint(new Point(width, height)));
			points.push(pos.transformPoint(new Point(0, height)));
			_boundingBox.x = topLeft.x;
			_boundingBox.y = _boundingBox.y;
			_boundingBox.width = 0;
			_boundingBox.height = 0;
			for(var i:uint = 1; i < points.length; i++)
			{
				var p:Point = points[i];
				if(_boundingBox.left > p.x)
				{
					_boundingBox.left = p.x;
				}
				if(_boundingBox.right < p.x)
				{
					_boundingBox.right = p.x;
				}
				if(_boundingBox.top > p.y)
				{
					_boundingBox.top = p.y;
				}
				if(_boundingBox.bottom < p.y)
				{
					_boundingBox.bottom = p.y;
				}
			}
		}
	}
}