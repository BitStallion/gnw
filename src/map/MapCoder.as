package map 
{
	import flash.utils.ByteArray;
	import map.coders.MapCoderBase64;
	import map.coders.MapCoderCSV;
	import map.coders.MapCoderXML;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapCoder 
	{
		public static function getCoder(coder:String):MapCoder
		{
			switch(coder)
			{
				case "xml":
				{
					return new MapCoderXML();
					break;
				}
				case "csv":
				{
					return new MapCoderCSV();
					break;
				}
				case "base64":
				{
					return new MapCoderBase64();
					break;
				}
			}
			return null;
		}
		
		public function MapCoder() 
		{
		}
		
		public function encode(data:ByteArray):String
		{
			return null;
		}
		
		public function decode(data:String):ByteArray
		{
			return null;
		}
	}
}