package map 
{
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapPart
	{
		public var file:String;
		public var rect:Rectangle;
		public var z:int;
		public var tilesetData:Object;
		
		public function MapPart(file:String, width:int, height:int, x:int = 0, y:int = 0, z:int = 0)
		{
			rect = new Rectangle();
			this.file = file;
			rect.x = x;
			rect.y = y;
			this.z = z;
			rect.width = width;
			rect.height = height;
			tilesetData = new Object();
		}
		
		public function getTilesetForGID(gid:uint):String
		{
			var currentKey:uint = 0;
			for(var key:String in tilesetData)
			{
				var keyVal:uint = parseInt(key);
				if(keyVal < gid && keyVal > currentKey) currentKey = keyVal;
			}
			return tilesetData[currentKey];
		}
		
		public function gIDToID(gid:uint):uint
		{
			var currentKey:uint = 0;
			for(var key:String in tilesetData)
			{
				var keyVal:uint = parseInt(key);
				if(keyVal < gid && keyVal > currentKey) currentKey = keyVal;
			}
			var tielsetData:Object = GameEngine.mapManager.getTilesetData(tilesetData[currentKey]);
			if(tielsetData == null) return null;
			return tielsetData[gid - currentKey];
		}
	}
}