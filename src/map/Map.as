package map 
{
	import data.Entity;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import mx.utils.StringUtil;
	import utility.GnWError;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Map 
	{
		private var _name:String;
		private var _mapData:MapFileData;
		private var _chunks:Object;
		
		public function Map(name:String) 
		{
			this._name = name;
			_mapData = GameEngine.mapManager.getMapFileData(name);
			_chunks = new Object();
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function get mapData():MapFileData
		{
			return _mapData;
		}
		
		public function loadChunk(chunkX:int, chunkY:int, z:int):void
		{
			if(_chunks[chunkKey] != null)
			{
				(_chunks[chunkKey] as MapChunk).destroy();
				delete _chunks[chunkKey];
			}
			var chunkKey:String = chunkX.toString() + "," + chunkY.toString() + "," + z.toString();
			var rect:Rectangle = new Rectangle(chunkX << MapChunk.chunkXShiftBits, chunkY << MapChunk.chunkYShiftBits, MapChunk.chunkRect.width, MapChunk.chunkRect.height);
			var mapParts:Vector.<MapPart> = new Vector.<MapPart>();
			for each(var part:MapPart in _mapData.mapData)
			{
				if(part.rect.intersects(rect))
				{
					var xmlMap:XML = new XML(GameEngine.fileResourceManager.getResource(part.file));
					for each(var xmlLayer:XML in xmlMap.layer)
					{
						var fameArray:Array = xmlLayer.@name.toString().split("_", 2);
						var floor:int = parseInt(StringUtil.trim(fameArray[0])) + part.z;
						if(floor == z)
						{
							mapParts.push(part);
							break;
						}
					}
				}
			}
			if(mapParts.length > 0)
			{
				var chunk:MapChunk = new MapChunk(this, chunkX << MapChunk.chunkXShiftBits, chunkY << MapChunk.chunkYShiftBits, z);
				_chunks[chunkKey] = chunk;
				chunk.loadFromMapData(mapParts);
			}
			else
			{
				throw new GnWError("No map parts in chunk (" + chunkX + ", " + chunkY + ", " + z + ")");
			}
		}
		
		public function getChunk(chunkX:int, chunkY:int, z:int):MapChunk
		{
			var chunkKey:String = chunkX.toString() + "," + chunkY.toString() + "," + z.toString();
			if(_chunks[chunkKey] == null)
			{
				loadChunk(chunkX, chunkY, z);
			}
			return _chunks[chunkKey];
		}
		
		public function unloadChunksNotIntersectingRect(rectImage:Rectangle = null, rect:Rectangle = null):void
		{
			for(var key:Object in _chunks)
			{
				var chunk:MapChunk = _chunks[key] as MapChunk;
				if(rect != null && !rect.intersects(chunk.chunkRectangle))
				{
					chunk.destroy();
					delete _chunks[key];
				}
				else if(rectImage != null && !rectImage.intersects(chunk.chunkRectangle))
				{
					chunk.unloadImages();
				}
			}
		}
		
		public function canMoveInDirections(x:int, y:int, z:int, directions:uint = 15):uint
		{
			try
			{
				var primaryDirections:uint = directions;
				var secondaryDirections:uint = 15;
				var primaryDirectionsCount:uint = 0;
				var chunk:MapChunk = getChunk(x >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
				var layer:String;
				var tile:Tile;
				for each(layer in chunk.layers)
				{
					tile = GameEngine.mapManager.getTile(chunk.getTileID(layer, x, y));
					primaryDirections &= tile.exitDirections;
				}
				if((primaryDirections & Tile.north) > 0)
				{
					try
					{
						secondaryDirections = Tile.north;
						primaryDirectionsCount = 0;
						chunk = getChunk(x >> MapChunk.chunkXShiftBits, (y - 1) >> MapChunk.chunkYShiftBits, z);
						for each(layer in chunk.layers)
						{
							tile = GameEngine.mapManager.getTile(chunk.getTileID(layer, x, y - 1));
							if(!tile.areDirectionsSecondary) primaryDirectionsCount++;
							secondaryDirections &= tile.enterDirections;
						}
						if(primaryDirectionsCount == 0) secondaryDirections = 0;
						if(secondaryDirections == 0) primaryDirections &= ~Tile.north;
					}
					catch(e:GnWError)
					{
						primaryDirections &= ~Tile.north;
					}
				}
				if((primaryDirections & Tile.south) > 0)
				{
					try
					{
						secondaryDirections = Tile.south;
						primaryDirectionsCount = 0;
						chunk = getChunk(x >> MapChunk.chunkXShiftBits, (y + 1) >> MapChunk.chunkYShiftBits, z);
						for each(layer in chunk.layers)
						{
							tile = GameEngine.mapManager.getTile(chunk.getTileID(layer, x, y + 1));
							if(!tile.areDirectionsSecondary) primaryDirectionsCount++;
							secondaryDirections &= tile.enterDirections;
						}
						if(primaryDirectionsCount == 0) secondaryDirections = 0;
						if(secondaryDirections == 0) primaryDirections &= ~Tile.south;
					}
					catch(e:GnWError)
					{
						primaryDirections &= ~Tile.south;
					}
				}
				if((primaryDirections & Tile.east) > 0)
				{
					try
					{
						secondaryDirections = Tile.east;
						primaryDirectionsCount = 0;
						chunk = getChunk((x + 1) >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
						for each(layer in chunk.layers)
						{
							tile = GameEngine.mapManager.getTile(chunk.getTileID(layer, x + 1, y));
							if(!tile.areDirectionsSecondary) primaryDirectionsCount++;
							secondaryDirections &= tile.enterDirections;
						}
						if(primaryDirectionsCount == 0) secondaryDirections = 0;
						if(secondaryDirections == 0) primaryDirections &= ~Tile.east;
					}
					catch(e:GnWError)
					{
						primaryDirections &= ~Tile.east;
					}
				}
				if((primaryDirections & Tile.west) > 0)
				{
					try
					{
						secondaryDirections = Tile.west;
						primaryDirectionsCount = 0;
						chunk = getChunk((x - 1) >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
						for each(layer in chunk.layers)
						{
							tile = GameEngine.mapManager.getTile(chunk.getTileID(layer, x - 1, y));
							if(!tile.areDirectionsSecondary) primaryDirectionsCount++;
							secondaryDirections &= tile.enterDirections;
						}
						if(primaryDirectionsCount == 0) secondaryDirections = 0;
						if(secondaryDirections == 0) primaryDirections &= ~Tile.west;
					}
					catch(e:GnWError)
					{
						primaryDirections &= ~Tile.west;
					}
				}
				return primaryDirections;
			}
			catch(e:GnWError)
			{
			}
			return 0;
		}
		
		public function getTileIDs(x:int, y:int, z:int):Vector.<uint>
		{
			
			try
			{
				var chunk:MapChunk = getChunk(x >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
				var results:Vector.<uint> = new Vector.<uint>();
				for each(var layer:String in chunk.layers)
				{
					results.push(chunk.getTileID(layer, x, y));
				}
				return results;
			}
			catch(e:GnWError)
			{
			}
			return null;
		}
		
		public function entityEntersTile(ent:Entity, x:int, y:int, z:int):void
		{
			
			try
			{
				var chunk:MapChunk = getChunk(x >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
				for each(var layer:String in chunk.layers)
				{
					GameEngine.mapManager.getTile(chunk.getTileID(layer, x, y)).entityEnters(ent);
				}
			}
			catch(e:GnWError)
			{
			}
		}
		
		public function entityExitsTile(ent:Entity, x:int, y:int, z:int):void
		{
			
			try
			{
				var chunk:MapChunk = getChunk(x >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
				for each(var layer:String in chunk.layers)
				{
					GameEngine.mapManager.getTile(chunk.getTileID(layer, x, y)).entityExits(ent);
				}
			}
			catch(e:GnWError)
			{
			}
		}
		
		public function getTileProperties(layer:String, x:int, y:int, z:int, type:String = null):Dictionary
		{
			try
			{
				var chunk:MapChunk = getChunk(x >> MapChunk.chunkXShiftBits, y >> MapChunk.chunkYShiftBits, z);
				return chunk.getTileProperties(layer, x, y, type);
			}
			catch(e:GnWError)
			{
			}
			return null;
		}
	}
}