package map 
{
	import data.IXMLDataLoader;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapManager implements IXMLDataLoader
	{
		private var _tilesetFileData:Dictionary;
		private var _tiles:Object;
		private var _allMapData:Dictionary;
		private var _nextTileID:uint;
		
		public function MapManager() 
		{
		}
		
		public function startLoading():void 
		{
			_tilesetFileData = new Dictionary();
			_tiles = new Object();
			_tiles[0] = new Tile(0, null, 0, null, null, null);
			_allMapData = new Dictionary();
			_nextTileID = 0;
		}
		
		public function canHandleXMLType(name:String):Boolean 
		{
			return name == "maps";
		}
		
		public function readXMLData(file:String, xml:XML):void 
		{
			if(xml.name() == "maps")
			{
				var tilesetPaths:Vector.<String> = new Vector.<String>();
				var tilesetPath:String;
				for each(var xmlMap:XML in xml.map) //Read the data for the different maps
				{
					var mapData:MapFileData = _allMapData[xmlMap.@name.toString()];
					if(mapData == null)
					{
						_allMapData[xmlMap.@name.toString()] = mapData = new MapFileData();
					}
					mapData.loadMapaData(file, xmlMap);
					for each(tilesetPath in mapData.tilesets)
					{
						if(tilesetPaths.indexOf(tilesetPath) < 0) tilesetPaths.push(tilesetPath);
					}
				}
				for each(tilesetPath in tilesetPaths)
				{
					loadTilesetData(tilesetPath);
				}
			}
		}
		
		private function get nextFreeTileID():uint
		{
			while(_tiles[_nextTileID] != null)
			{
				_nextTileID++;
			}
			return _nextTileID++;
		}
		
		public function loadTilesetData(file:String):void
		{
			var currentData:XML = new XML(GameEngine.fileResourceManager.getResource(file));
			var currentFile:String = file;
			var tilesetMaping:Object = _tilesetFileData[file];
			if(tilesetMaping == null)
			{
				_tilesetFileData[file] = tilesetMaping = new Object();
			}
			var imageSource:Bitmap = GameEngine.fileResourceManager.getImage(file + "/../" + currentData.image[0].@source);
			var imageSourceTileWidth:int = imageSource.width / 32;
			var imageSourceTileHight:int = imageSource.height / 32;
			for(var y:int = 0; y < imageSourceTileHight; y++)
			{
				for(var x:int = 0; x < imageSourceTileWidth; x++)
				{
					var tileLocalID:uint = y * imageSourceTileWidth + x;
					var tileID:uint = nextFreeTileID;
					var tileType:String = "TileGenericWall";
					var tileRect:Rectangle = new Rectangle(x * 32, y * 32, 32, 32);
					tilesetMaping[tileLocalID] = tileID;
					var properties:XML = null;
					for each(var xmlTile:XML in currentData.tile)
					{
						if(parseInt(xmlTile.@id.toString()) == tileLocalID)
						{
							properties = xmlTile.properties[0];
							for each(var property:XML in properties.property)
							{
								if(property.@name.toString() == "TileType")
								{
									tileType = property.@value.toString();
									break;
								}
							}
							break;
						}
					}
					if(_tiles[tileID] == null)
					{
						var tileClass:Class = getDefinitionByName("map.tiles." + tileType) as Class;
						_tiles[tileID] = new tileClass(tileID, currentFile, tileLocalID, properties, imageSource.bitmapData, tileRect);
					}
				}
			}
		}
			
		public function getTilesetData(file:String):Object
		{
			return _tilesetFileData[file];
		}
		
		public function getTile(id:uint):Tile
		{
			return _tiles[id] || _tiles[0];
		}
		
		public function getMapFileData(name:String):MapFileData
		{
			return _allMapData[name];
		}
	}
}