package map.tiles
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import map.Tile;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class TileGenericWall extends TileGenericFloor
	{
		public function TileGenericWall(id:int, file:String, localID:uint, tileProperties:XML, image:BitmapData, imageRect:Rectangle) 
		{
			super(id, file, localID, tileProperties, image, imageRect);
		}
		
		override public function get exitDirections():uint
		{
			return 0;
		}
		
		override public function get enterDirections():uint
		{
			return 0;
		}
	}
}