package map.tiles 
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class TileSafeFloor extends TileGenericFloor 
	{
		
		public function TileSafeFloor(id:int, file:String, localID:uint, tileProperties:XML, image:BitmapData, imageRect:Rectangle) 
		{
			super(id, file, localID, tileProperties, image, imageRect);
		}
		
		override public function get isSafe():Boolean
		{
			return true;
		}
	}
}