package map.tiles
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import map.Tile;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class TileGenericFloor extends Tile
	{
		public function TileGenericFloor(id:int, file:String, localID:uint, tileProperties:XML, image:BitmapData, imageRect:Rectangle)
		{
			super(id, file, localID, tileProperties, image, imageRect);
		}
		
		override public function get areDirectionsSecondary():Boolean
		{
			return false;
		}
	}
}