package map.tiles 
{
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import map.MapChunk;
	import map.Tile;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class TileGenericSign extends Tile 
	{
		private var _actions:Dictionary;
		
		public function TileGenericSign(id:int, file:String, localID:uint, tileProperties:XML, image:BitmapData, imageRect:Rectangle) 
		{
			super(id, file, localID, tileProperties, image, imageRect);
			_actions = new Dictionary();
			_actions["Read"] = read;
		}
		
		private function read(chunk:MapChunk, layer:String, x:int, y:int):void
		{
			var props:Dictionary = chunk.getTileProperties(layer, x, y, "tilemeta");
			GameEngine.startEvent(props["event"].toString());
		}
		
		override public function actions():Dictionary 
		{
			return _actions;
		}
	}
}