package map.coders 
{
	import flash.utils.ByteArray;
	import map.MapCoder;
	import mx.utils.StringUtil;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapCoderXML extends MapCoder 
	{
		
		public function MapCoderXML() 
		{
			super();
		}
		
		override public function encode(data:ByteArray):String
		{
			var xml:XML = new XML("<data></data>");
			data.position = 0;
			while(data.bytesAvailable > 0)
			{
				var tile:XML = new XML("<tile />");
				tile.@gid = data.readUnsignedInt();
				xml.appendChild(tile);
			}
			return xml.toString();
		}
		
		override public function decode(data:String):ByteArray
		{
			var result:ByteArray = new ByteArray();
			var xml:XML = new XML(data);
			for each(var tile:XML in xml.tile)
			{
				if(tile.hasOwnProperty("@gid"))
				{
					result.writeUnsignedInt(parseInt(StringUtil.trim(tile.@gid.toString())));
				}
				else
				{
					result.writeUnsignedInt(0);
				}
			}
			return result;
		}
	}
}