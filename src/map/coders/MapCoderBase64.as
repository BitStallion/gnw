package map.coders 
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import map.MapCoder;
	import mx.utils.Base64Decoder;
	import mx.utils.Base64Encoder;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapCoderBase64 extends MapCoder 
	{
		//public static const codeTable:Array = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "/");
		public function MapCoderBase64() 
		{
			super();
		}
		
		override public function encode(data:ByteArray):String
		{
			var data1:ByteArray = data;
			if(data.endian != Endian.LITTLE_ENDIAN)
			{
				data1 = new ByteArray();
				data1.endian = Endian.LITTLE_ENDIAN;
				data.position = 0;
				for(var i:uint = 0; i < data.length; i++)
				{
					data1.writeUnsignedInt(data.readUnsignedInt());
				}
			}
			var encoder:Base64Encoder = new Base64Encoder();
			encoder.encodeBytes(data1);
			return encoder.toString();
		}
		
		override public function decode(data:String):ByteArray
		{
			var decoder:Base64Decoder = new Base64Decoder();
			decoder.decode(data);
			var result:ByteArray = decoder.toByteArray();
			result.endian = Endian.LITTLE_ENDIAN;
			return result;
		}
	}
}