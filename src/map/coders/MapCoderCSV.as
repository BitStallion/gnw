package map.coders 
{
	import flash.utils.ByteArray;
	import map.MapCoder;
	import mx.utils.StringUtil;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapCoderCSV extends MapCoder
	{
		public function MapCoderCSV()
		{
			super();
		}
		
		override public function encode(data:ByteArray):String
		{
			var result:String = new String();
			data.position = 0;
			while(data.bytesAvailable > 0)
			{
				if(result.length > 0)
				{
					result += ", ";
				}
				result += data.readUnsignedInt().toString();
			}
			return result;
		}
		
		override public function decode(data:String):ByteArray
		{
			var result:ByteArray = new ByteArray();
			for each(var tileData:String in data.split(","))
			{
				result.writeUnsignedInt(parseInt(StringUtil.trim(tileData)));
			}
			return result;
		}
	}
}