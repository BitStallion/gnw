package ai.pathing 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Path
	{
		private var _path:Vector.<IPathNode>;
		private var _metric:Number;
		
		public function Path()
		{
			_path = new Vector.<IPathNode>();
			metic = 0;
		}
		
		public function containsNode(node:IPathNode):Boolean
		{
			return _path.indexOf(node) >= 0;
		}
		
		public function addNode(node:IPathNode):void
		{
			_path.push(node);
			_metric += node.metric;
		}
		
		public function get metric():Number
		{
			return _metric;
		}
		
		public function clone():Path
		{
			var newPath:Path = new Path()
			newPath._path = _path.splice();
			newPath._metric = _metric;
			return newPath;
		}
	}
}