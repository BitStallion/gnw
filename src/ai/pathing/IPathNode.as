package ai.pathing 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public interface IPathNode
	{
		public function get metric():Number;
		public function nextNodes():Vector.<IPathNode>;
	}
}