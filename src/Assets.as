package  
{
	import data.EntityComponent;
	import flash.globalization.DateTimeFormatter;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Assets 
	{
		[Embed(source="font/BOMBARD_.ttf", fontName="Bombard", embedAsCFF="false")]
		public static const BOMBARD_FONT:String;
		
		[Embed(source="font/CREAMPUF.TTF", fontName="Creampuff", embedAsCFF="false")]
		public static const CREAMPUFF_FONT:String;
		
		public static const GNW_DATE_FORMAT:DateTimeFormatter = new DateTimeFormatter("en-UK");
		{GNW_DATE_FORMAT.setDateTimePattern("MMM dd yyyy hh:mm a"); }
		
		// Genders
		public static const MALE_FLAVOUR_TEXT:String = "As a man you have chest hair like a forest with a mustache or beard covering your face. Muscles budge under your shirt or you are wimpy with very few muscles. Most of the time you think with your dick instead of your brain, so often you get into trouble.";
		public static const FEMALE_FLAVOUR_TEXT:String = "As a woman your lithe form allows for quick moments, while your curvy hips and ample breasts attract the attention of many. Your tastes are though very narrow and your sexuality can be your ally or your enemy. People will try to use you, make you theirs, or eat you.";
		public static const HERM_FLAVOUR_TEXT:String = "You've got the best of both worlds. A sensitive and tender pussy, while a juicy cock for hard fucking rests above. You look like a woman and unless they notice your bulge most people think you are just that. When people say 'go fuck yourself', you can do just that.";
		public static const SHEMALE_FLAVOUR_TEXT:String = "You look like a woman and everyone you met thinks you are female. But at birth you must been undecided as you grew a cock and balls instead of a pussy. Your breasts and hips are just as curvy as any woman, while you have your cock to play with.";
		public static const FEMBOY_FLAVOUR_TEXT:String = "You are male, or at least your genitals are. Even then your genitals are girlish looking, perfect for a little femboy like you. You look like a flat chested female, with soft curves and delicate features. Most people can't tell what you are packing until you have sucked them in.";
		public static const CUNTBOY_FLAVOUR_TEXT:String = "You look, smell and feel just like a man. That is until you drop your pants and show your pussy. In all aspects you are a man, just with a snatch. You know that a man is more than a cock, just as a woman is more than a pussy.";
		
		// Races
		public static const HUMAN_FLAVOUR_TEXT:String = "Pure human and nothing more in the mix. With a strong tall body and a healthy libido you have spread far and wide. With brains, brawn and libido to match the clever elves, strong orks and horny goblins you are able to survive and adapt in this dangerous world. ";
		public static const DWARF_FLAVOUR_TEXT:String = "You might be short and a tad wide, but you are strong, tough and proud. Your power to weight ratio is the greatest of all races. While the other taller races try to claim superiority, they end up crawling to you when the real work needs to be done properly.";
		public static const ELF_FLAVOUR_TEXT:String = "You are tall, slender and proud. Some might say arrogant, but they are usually of the lesser beings. When people talk about grace, grandeur and beauty they can only compare to elves and lose. You breeze untouched and unsullied through the world as the other races squabble in the mud.";
		public static const GOBLIN_FLAVOUR_TEXT:String = "Short as a dwarf but not so grouchy, you are out for a good time. With green skin, flappy ears and red eyes people often attack or run on sight. Sex and food are your idea of a good time, even if they are taken unwillingly. Especially when taken unwillingly.";
		public static const ORK_FLAVOUR_TEXT:String = "You are large burly and not too bright, your green skin and red eyes often have people attacking. Your strength and stamina are second to none and your enemies soon learn dying in battle would have been best. Unlike other races the females are just as strong as the males.";
		
		// Body types
		public static const THIN_FLAVOUR_TEXT:String = "Thin as a whip and twice as quick. You might be skinny but you aren't skin and bones. You have the required muscles and no more. With barely any fat on your entire body you weigh barely anything. Some might call you scrawny, but you consider yourself fit and healthy.";
		public static const	NORMAL_FLAVOUR_TEXT:String = "With muscle and fat in equal balance across your body you are what people call normal. Compared to other body types, most beyond imagination you aren't sure what counts for normal in this world. But you aren't fat or skinny, just the bit in between most people are happy with.";
		public static const MUSCULAR_FLAVOUR_TEXT:String = "Your muscles have muscles, you are buff, ripped and cut. What little fat you have is surrounded by muscles. You only have enough fat to survive, everything else is muscled. After all in this kind of place you need muscles to survive and look damn hot at the same time.";
		public static const CHUBBY_FLAVOUR_TEXT:String = "You have a bit extra fat hanging off your body. Your belly pokes out while your trunk is well full of junk. No matter, more cushion for the pushing is your mantra. Skinny people are unhappy people who deny themselves the joys of life. Like cake, chocolate and ice cream. ";
		public static const HUSKY_FLAVOUR_TEXT:String = "Who said having muscles meant you had to be some rock hard muscle head? With a liberal amount of fat you are just as muscled as any gym junky. You have a perfect fusion of fat and muscle to create a strong but realistic body. Built for fun and strength.";
		
		// Stats
		public static const STRENGTH_FLAVOUR_TEXT:String = "Increasing this stat increases melee damage as well as the size of weapons and armor one can wield comfortably.\n\nYour physical strength allows you to beat up your enemies and haul heavier things with greater ease.";
		public static const ENDURANCE_FLAVOUR_TEXT:String = "Increasing this stat increases your health as well as your ability to resist disease.\n\nYour endurance allows you to take a beating, and with your improved immune system you can fight off disease.";
		public static const DEXTERITY_FLAVOUR_TEXT:String = "This stat increases your accuracy with firearms and thrown weapons.\n\nYour hands are quick and steady.  Your rounds fly true, and your hands dance over complex mechanisms.  ";
		public static const AGILITY_FLAVOUR_TEXT:String = "Agility determines your ability to dodge knives and bullets, as well as turn order.  Heavy armor negates this bonus.\n\nYour speed allows you to dodge knives and bullets alike, though heavy armor weighs you down too much to dodge.";
		public static const INTELLIGENCE_FLAVOUR_TEXT:String = "Intelligence augments your magical power, strengthening your spells and increasing the duration of curses.\n\nYour vast intellect allows you to understand magic at a primal level, making you more in tune with the spells you cast.";
		public static const CHARISMA_FLAVOUR_TEXT:String = "Charisma unlocks special conversation options, and increases your lust damage.\n\nYour good looks and quick talking endear people to you, and your flirting can make women swoon and men drool.";
		public static const WILLPOWER_FLAVOUR_TEXT:String = "Willpower protects you from lust and certain forms of magic.\n\nYour willpower keeps you resolute and composed when enemies try to seduce you or use mind-altering magic on you.";
		
		// START SCREEN STUFF
		public static const WARNING_MESSAGE:String = "THIS IS A RESTRICTED AREA\n\nInside lies zones full of monsters, mutants, cyborgs, humanoids, furries and humans, all horny individuals and looking for a good time. This is not a safe zone, not for those sensitive to this or other sexual and adult imagery.\n\nIf you are an legal adult in your country (18+ or 21+), you are allow to pass. Just note that the world you are about to see is full of sexually deviant fetishes, including a ton of leather and BDSM, bestiality, gay and lesbian, rampant use of drugs, writing from the erotically strange minded individuals volunteering to help build this game, and a lot of other nonsensical and fucked-up shit. Oh, and there's bad language, too.\n\nAll of this is entirely fictitious and any resemblance to real people or creatures, living or dead, is purely coincidental. Additionally, all characters portrayed in this game are above the age of consent.\n\nBy clicking the exclamation mark at the end of \"Warning\", you accept these terms and acknowledge that you are comfortable with the content of this game. Now, sit back, enjoy, and have a fun time in Purgatory!";
		public static const RECRUIT_MESSAGE:String = "Gangs N' Whores is a open and free project, run by volunteers who love and have passion for hentai, furry and fetish porn. The future of the project is always in hands of the people's aspiration. If you are a programmer, erotic writer or you want to participate in other things like game balance, quests, or anything you believe can improve the game and you are a cool being, join our little team, and together we can make a bigger and better game!\n\nFeel free to use our forums as much as you want and share your feedback and ideas. The only rule we have is we will not put any under-aged character (we don't accept loli or shota) in the game. Although, we are always open to a variety of fetishes.\n\nHave fun, enjoy the game and its community!"
		
		// Combat Text
		public static const TAKE_JUICE_DAMAGE:String = "[target.name] is now {Juice:[target." + EntityComponent.JUICE + ".value]/[target." + EntityComponent.JUICE + ".max]} now"
		public static const JUICE_OUT:String = "[target.name] is out of Juice."
		public static const TAKE_LUST_DAMAGE:String = "[target.name] gains [effect] points of lust.";
	}
}