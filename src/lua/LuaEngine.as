package lua 
{
	import combat.Allegiance;
	import combat.CombatSystem;
	import data.Entity;
	import enums.*;
	import events.GameEvent;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import gui.Game;
	import gui.menus.auxiliary.ImageDisplayMenu;
	import gui.menus.CombatMenu;
	import gui.menus.EventMenu;
	import luaAlchemy.LuaAlchemy;
	import utility.IDestroyable;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class LuaEngine extends LuaAlchemy implements IDestroyable
	{
		private var _gnwLib:Object;
		
		public function LuaEngine(param0:* = null, param1:Boolean = true) 
		{
			super(param0, param1);
		}
		
		public function destroy():void 
		{
			close();
		}
		
		override public function init(param0:* = null, param1:Boolean = true):void 
		{
			super.init(param0, param1);
			setGlobalLuaValue("doFile", doFile);
			setGlobalLuaValue("include", doFile);
			_gnwLib = {	"version":addVersionLibrary(),
						"state":addStateLibrary(),
						"event":addEventLibrary(),
						"entity":addEntityLibrary(),
						"battle":addBattleLibrary(),
						"gui":addGUILibrary(),
						"parts":addPartsLibrary(),
						"goto":GameEngine.gotoLocation
						}
			setGlobalLuaValue("gnw", _gnwLib);
		}
		
		private function addVersionLibrary():Object
		{
			return {"stage":Version.stage,
					"major":Version.major,
					"minor":Version.minor,
					"build":Version.build,
					"revision":Version.revision,
					"toString":Version.currentVersion
					}
		}
		
		private function addStateLibrary():Object
		{
			var lib:Object = {	"endEvent":(GameEngine.menuManager.getMenuById(EventMenu.ID) as EventMenu).endEvent,
								"setState":GameEngine.setNextState
								}
			for each(var state:GameState in Utils.EnumList(GameState))
			{
				lib[state.text] = state.text;
			}
			return lib;
		}
		
		private function addEventLibrary():Object
		{
			return {"addEvent":function(id:String, name:String, check:Function, event:Function):void
						{
							GameEngine.eventManager.addEvent(new GameEvent(id, name, check, function():void{(GameEngine.menuManager.getMenuById(EventMenu.ID) as EventMenu).runEventFunction(event)}));
						},
					"startEvent":GameEngine.startEvent
					}
		}
		
		private function addEntityLibrary():Object
		{
			return {"spawnEntityFromNPCTemplate":function(id:String):Entity
						{
							return GameEngine.dataManager.createNPCFromTemplate(id);
						}
					}
		}
		
		private function addBattleLibrary():Object
		{
			return {"new":GameEngine.newCombat,
					"addEntity":function(ent:Entity, team:uint):void
						{
							if(ent != null)
							{
								if(team == 0)
								{
									if(ent == GameEngine.world.player)
									{
										GameEngine.currentBattle.addEntity(ent, Allegiance.Player);
									}
									else
									{
										GameEngine.currentBattle.addEntity(ent, Allegiance.Ally);
									}
								}
								else
								{
									GameEngine.currentBattle.addEntity(ent, Allegiance.Enemy);
								}
							}
						},
					"start":GameEngine.startCombat
					}
		}
		
		private function addGUILibrary():Object
		{
			return {"eventMenu":{
						"setText":function(text:String):void
							{
								(GameEngine.menuManager.getMenuById(EventMenu.ID) as EventMenu).text = text;
							},
						"getText":function():String
							{
								return (GameEngine.menuManager.getMenuById(EventMenu.ID) as EventMenu).text;
							}
						},
					"imageDisplay":{
						"setImage":function(imagePath:String):void
							{
								(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = GameEngine.fileResourceManager.getResource(imagePath) as Bitmap;
							}
						},
					"buttons":{
						"COMMON_BUTTON_01_ID":Game.COMMON_BUTTON_01_ID,
						"COMMON_BUTTON_02_ID":Game.COMMON_BUTTON_02_ID,
						"COMMON_BUTTON_03_ID":Game.COMMON_BUTTON_03_ID,
						"COMMON_BUTTON_04_ID":Game.COMMON_BUTTON_04_ID,
						"COMMON_BUTTON_05_ID":Game.COMMON_BUTTON_05_ID,
						"COMMON_BUTTON_06_ID":Game.COMMON_BUTTON_06_ID,
						"COMMON_BUTTON_07_ID":Game.COMMON_BUTTON_07_ID,
						"COMMON_BUTTON_08_ID":Game.COMMON_BUTTON_08_ID,
						"COMMON_BUTTON_09_ID":Game.COMMON_BUTTON_09_ID,
						"COMMON_BUTTON_10_ID":Game.COMMON_BUTTON_10_ID,
						"COMMON_BUTTON_11_ID":Game.COMMON_BUTTON_11_ID,
						"COMMON_BUTTON_12_ID":Game.COMMON_BUTTON_12_ID,
						"SYSTEM_BUTTON_01_ID":Game.SYSTEM_BUTTON_01_ID,
						"SYSTEM_BUTTON_01_ID":Game.SYSTEM_BUTTON_01_ID,
						"SYSTEM_BUTTON_02_ID":Game.SYSTEM_BUTTON_02_ID,
						"SYSTEM_BUTTON_03_ID":Game.SYSTEM_BUTTON_03_ID,
						"SYSTEM_BUTTON_04_ID":Game.SYSTEM_BUTTON_04_ID,
						"SYSTEM_BUTTON_05_ID":Game.SYSTEM_BUTTON_05_ID,
						"YES_BUTTON_ID":Game.YES_BUTTON_ID,
						"NO_BUTTON_ID":Game.NO_BUTTON_ID,
						"hide":function(id:int):void
							{
								GameEngine.gui.hide(id);
							},
						"hideAll":function():void
							{
								GameEngine.gui.hideAll();
							},
						"hideCommonButtons":function():void
							{
								GameEngine.gui.hideCommonButtons();
							},
						"hideSystemButtons":function():void
							{
								GameEngine.gui.hideSystemButtons();
							},
						"hideYesNoButtons":function():void
							{
								GameEngine.gui.hideYesNoButtons();
							},
						"setAndShow":function(id:int, label:String, action:Function, blink:Function = null):void
							{
								GameEngine.gui.setAndShow(id, label, function(e:Event):void{(GameEngine.menuManager.getMenuById(EventMenu.ID) as EventMenu).runEventFunction(action);}, blink);
							}
						}
					}
		}
		
		private function addPartsLibrary():Object
		{
			var parts:Object = new Object();
			for each (var generic:String in GameEngine.descriptor.generics)
			{
				parts[generic] = { toString:function():String { return GameEngine.descriptor.getGeneric(generic); }};
			}
			return parts;
		}
		
		public function get gnwLib():Object
		{
			return _gnwLib;
		}
		
		override public function doFile(param0:String):Array
		{
			var file:ByteArray = GameEngine.fileResourceManager.getResource(param0) as ByteArray;
			return doString(file.readUTFBytes(file.bytesAvailable));
		}
		
		override public function doFileAsync(param0:String, param1:Function):void
		{
			var file:ByteArray = GameEngine.fileResourceManager.getResource(param0) as ByteArray;
			doStringAsync(file.readUTFBytes(file.bytesAvailable), param1);
		}
	}
}