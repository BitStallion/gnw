package helpers
{
	import enums.Gender;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class ContextHelper
	{
		
		public function ContextHelper()
		{
		
		}
		
		public static function addGenderPronouns(gender:Gender, context:Object):void
		{
			context["Gender"] = gender.text;
			context["gender"] = gender.text.toLowerCase();
			
			switch (gender)
			{
				case Gender.Cuntboy: 
				case Gender.Femboy: 
				case Gender.Male: 
					context["heshe"] = "he";
					context["HeShe"] = "He";
					context["himher"] = "him";
					context["HimHer"] = "Him";
					context["hisher"] = "his";
					context["HisHer"] = "His";
					context["hishers"] = "his";
					context["HisHers"] = "His";
					context["himherself"] = "himself";
					context["HimHerself"] = "Himself";
					break;
				
				case Gender.Herm: 
				case Gender.Shemale: 
				case Gender.Female: 
					context["heshe"] = "she";
					context["HeShe"] = "She";
					context["himher"] = "her";
					context["HimHer"] = "Her";
					context["hisher"] = "her";
					context["HisHer"] = "Her";
					context["hishers"] = "hers";
					context["HisHers"] = "Hers";
					context["himherself"] = "herself";
					context["HimHerself"] = "Herself";
					break;
			}
		}	
	}

}