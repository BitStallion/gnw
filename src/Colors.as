package  
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Colors 
	{
		public static const LASER_GREEN:uint = 0x00D100;
		public static const MEDIUM_GREEN:uint = 0x006F00;
		public static const DARK_GREEN:uint = 0x003F00;
		
		public static const LASER_BLUE:uint = 0x00FFFF;
		public static const MEDIUM_BLUE:uint = 0x006060;
		public static const DARK_BLUE:uint = 0x003030;
		
		public static const CARBON:uint = 0x051900;
		
		public static const NEON_RED:uint = 0xFF0000;
	}
}