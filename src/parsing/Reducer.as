package parsing
{
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	internal class Reducer
	{
		
		public function Reducer()
		{
		
		}
		
		public static function find(array:Array, context:Object):Object
		{
			var current:Object = context[array[0]];
			if (current)
			{
				for (var i:int = 1; i < array.length; i++)
				{
					current = current[array[i]];
					if (!current)
					{
						break;
					}
				}
			}
			return current;
		}
		
		public static function reduce(array:Array, context:Object):String
		{
			var result:Array = [];
			for each (var a:Object in array)
			{
				if (a is String)
				{
					result.push(a as String);
				}
				else if (a.type == "tag")
				{
					result.push(reduceTag(a, context));
				}
				else if (a.type == "ifBlock")
				{
					result.push(reduceIfBlock(a, context));
				}
				else if (a.type == "callBlock")
				{
					result.push(reduceCallBlock(a, context));
				}
				else if (a.type == "inBlock")
				{
					result.push(reduceInBlock(a, context));
				}
			}
			return result.join("");
		}
		
		public static function reduceTag(tag:Object, context:Object):Object
		{
			return find(tag.value, context);
		}
		
		public static function reduceExpr(expr:Object, context:Object):Object
		{
			if (expr is String)
			{
				return expr as String;
			}
			else if (expr is int)
			{
				return expr as int;
			}
			else if (expr is Number)
			{
				return expr as Number;
			}
			else if (expr.type == "multiId")
			{
				return find(expr.value, context);
			}
			else if (expr.type == "mulExpr")
			{
				return reduceMulExpr(expr, context);
			}
			else if (expr.type == "addExpr")
			{
				return reduceAddExpr(expr, context);
			}
			else if (expr.type == "cmprExpr")
			{
				return reduceCmprExpr(expr, context);
			}
			else if (expr.type == "boolExpr")
			{
				return reduceBoolExpr(expr, context);
			}
			else if (expr.type == "notExpr")
			{
				return reduceNotExpr(expr, context);
			}
			return null;
		}
		
		public static function reduceMulExpr(expr:Object, context:Object):Object
		{
			var array:Array = expr.value;
			var number:Number = Number(reduceExpr(array[0], context));
			for (var i:int = 1; i < array.length; i += 2)
			{
				if (array[i] == "*")
				{
					number *= Number(reduceExpr(array[i + 1], context));
				}
				else //if (array[i] == "/")
				{
					number /= Number(reduceExpr(array[i + 1], context));
				}
			}
			return number;
		}
		
		public static function reduceAddExpr(expr:Object, context:Object):Object
		{
			var canConcat:Boolean = true;
			
			var array:Array = expr.value;
			var number:Number = Number(reduceExpr(array[0], context));
			for (var i:int = 1; i < array.length; i += 2)
			{
				if (array[i] == "+")
				{
					number += Number(reduceExpr(array[i + 1], context));
				}
				else //if (array[i] == "-")
				{
					canConcat = false;
					number -= Number(reduceExpr(array[i + 1], context));
				}
			}
			if (isNaN(number) && canConcat)
			{
				return reduceConcatExpr(expr, context);
			}
			else
			{
				return number;
			}
		}
		
		public static function reduceConcatExpr(expr:Object, context:Object):Object
		{
			var array:Array = expr.value;
			var string:Object = reduceExpr(array[0], context);
			for (var i:int = 1; i < array.length; i += 2)
			{
				string += reduceExpr(array[i + 1], context);
			}
			return string;
		}
		
		public static function reduceCmprExpr(expr:Object, context:Object):Object
		{
			var array:Array = expr.value;
			var object:Object = reduceExpr(array[0], context);
			var newObject:Object;
			var result:Boolean = true;
			for (var i:int = 1; i < array.length; i += 2)
			{
				newObject = reduceExpr(array[i + 1], context);
				switch (array[i])
				{
					case "==": 
						result = object == newObject;
						break;
					case "!=": 
						result = object != newObject;
						break;
					case "<=": 
						result = object <= newObject;
						break;
					case ">=": 
						result = object >= newObject;
						break;
					case "<": 
						result = object < newObject;
						break;
					case ">": 
						result = object > newObject;
						break;
					default: 
						throw new Error("Invalid cmprOp");
				}
				object = newObject;
			}
			return result ? 1 : 0;
		}
		
		public static function reduceBoolExpr(expr:Object, context:Object):Object
		{
			var array:Array = expr.value;
			var object:Object = reduceExpr(array[0], context);
			for (var i:int = 1; i < array.length; i += 2)
			{
				if (array[i] == "and")
				{
					object = object && reduceExpr(array[i + 1], context);
				}
				else //if (array[i] == "or")
				{
					object = object || reduceExpr(array[i + 1], context);
				}
			}
			return object ? 1 : 0;
		}
		
		public static function reduceNotExpr(expr:Object, context:Object):Object
		{
			return reduceExpr(expr.value, context) == 1 ? 0 : 1;
		}
		
		public static function reduceIfBlock(block:Object, context:Object):Object
		{
			if (reduceExpr(block.expr, context))
			{
				return reduce(block.value, context);
			}
			for each (var elseIfBlock:Object in block.elseIfValues)
			{
				if (reduceExpr(elseIfBlock.expr, context))
				{
					return reduce(elseIfBlock.value, context);
				}
			}
			if (block.elseValue)
			{
				return reduce(block.elseValue, context);
			}
			return "";
		}
		
		public static function reduceCallBlock(block:Object, context:Object):Object
		{
			var method:Object = find(block.id.value, context);
			if (!method is Function)
			{
				throw new Error("function expected.");
			}
			return (method as Function)(reduce(block.value, context));
		}
		
		public static function reduceInBlock(block:Object, context:Object):Object
		{
			var inner:Object = find(block.id.value, context);
			var result:Array = [];
			if (inner is Array)
			{
				for each (var a:Object in(inner as Array))
				{
					result.push(reduce(block.value, a));
				}
				return result.join("");
			}
			return reduce(block.value, inner);
		}
	
	}

}