start = some*

some = freeText / tag / ifBlock / callBlock / inBlock

multiId =
	car:id ("." cdr:multiId) {return {type: "multiId", value:[car].concat(cdr.value)}} /
	id:id {return {type: "multiId", value:[id]}}

id "id" = car:[a-z_]i cdr:[a-z0-9_]i* {return car + cdr.join("")}

number "number" = car:[0-9]+ cdr:("."[0-9])? {return parseFloat(car.join("") + (cdr ? cdr.join(""):""))}

atom = multiId / number / string

mulExpr = 
	car:atom space op:mulOp space cdr:mulExpr {return {type:"mulExpr", value:[car].concat(op).concat(cdr.hasOwnProperty("type") && cdr.type=="mulExpr" ? cdr.value : cdr)}} /
	atom:atom /
	"(" expr:boolExpr ")" {return expr}

mulOp = "*" / "/"

space "space" = [ \n\r\t]* {return;}

addExpr =
	car:mulExpr space op:addOp space cdr:addExpr {return {type:"addExpr", value:[car].concat(op).concat(cdr.hasOwnProperty("type") && cdr.type=="addExpr" ? cdr.value : cdr)}} /
	mulExpr
	
addOp = "+" / "-"

cmprExpr =
	car:addExpr space op:cmprOp space cdr:cmprExpr {return {type:"cmprExpr", value:[car].concat(op).concat(cdr.hasOwnProperty("type") && cdr.type=="cmprExpr" ? cdr.value : cdr)}} /
	addExpr
	
cmprOp = "==" / "!=" / ">=" / "<=" / ">" / "<"

boolExpr "expression" =	
	"not" space expr:boolExpr {return {type:"notExpr", value:expr}} /
	car:cmprExpr space op:boolOp space cdr:boolExpr {return {type:"boolExpr", value:[car].concat(op).concat(cdr.hasOwnProperty("type") && cdr.type=="boolExpr" ? cdr.value : cdr)}} /
	cmprExpr
	
boolOp = "and" / "or"

freeText  "freeText" = letters:[^\[]+ {return letters.join("")}

tag "tag" = "[" id:multiId "]" {return {type:"tag", value:id.value}}

ifBlock =
	"[#if" space expr:boolExpr "]" value:some* elseIfValues:elseIfBlock* elseValue:elseBlock? "[#endif]" {return {type:"ifBlock", expr:expr, value:value, elseIfValues:elseIfValues, elseValue:elseValue}}
	
elseBlock =
	"[#else]" value:some* {return value}

elseIfBlock =
	"[#elseif" space expr:boolExpr "]" value:some* {return {type:"elseIfBlock", expr:expr, value:value}}
	
callBlock =
	"[#call" space id:multiId "]" value:some* "[#endcall]" {return {type:"callBlock", id:id, value:value}}
	
inBlock = 
	"[#in" space id:multiId "]" value:some* "[#endin]" {return {type:"inBlock", id:id, value:value}}

char
  = [^"\\\0-\x1F\x7f]
  / '\\"' { return '"'; }
  / "\\\\" { return "\\"; }
  / "\\/" { return "/"; }
  / "\\b" { return "\b"; }
  / "\\f" { return "\f"; }
  / "\\n" { return "\n"; }
  / "\\r" { return "\r"; }
  / "\\t" { return "\t"; }
  / "\\u" digits:$(hexDigit hexDigit hexDigit hexDigit) {
      return String.fromCharCode(parseInt(digits, 16));
    }

string "string"
  = '"' '"' space { return ""; }
  / '"' chars:chars '"' space { return chars; }

chars
  = chars:char+ { return chars.join(""); }

hexDigit
  = [0-9a-fA-F]