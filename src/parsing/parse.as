package parsing
{
	public function parse(source:String, context:Object):String
	{
		try
		{
			var parseResult:Array = pegParse(source);
			return Reducer.reduce(parseResult, context);
		}
		catch (err:PegSyntaxError)
		{
			return err.message;
		}
		return "";
	}
}