package parsing
{
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class PegSyntaxError extends Error 
	{
		private var expected:*;
		private var found:*;
		private var offset:*;
		private var line:*;
		private var column:*;
		
		public function PegSyntaxError(message:*, expected:*, found:*, offset:*, line:*, column:*) 
		{
			super(message, 0);
			this.message = message;
			this.expected = expected;
			this.found = found;
			this.offset = offset;
			this.line = line;
			this.column = column;
		}
		
	}

}