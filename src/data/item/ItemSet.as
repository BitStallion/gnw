package data.item 
{
	import data.Entity;
	import data.IXMLSerializable;
	import utility.GnWError;
	import utility.Util;
	/**
	 * Used to generate random subsets of items from a parameterised set of items.
	 * 
	 * @author GnW Team
	 */
	public class ItemSet implements IXMLSerializable
	{
		private var _items:Vector.<String>;
		private var _min:int;
		private var _max:int;
		
		public function ItemSet() 
		{
			_items = new Vector.<String>();
		}
		
		/**
		 * Generates a random set of items.
		 * 
		 * @return	A vector containing the items.
		 */
		public function generateItems():Vector.<Entity>
		{
			var itemlist:Vector.<Entity> = new Vector.<Entity>();
			
			var num:int = Util.randomInt(_min, _max);
			var list:Vector.<String> = _items.slice();
			
			for (var i:int = 0; i < num; i++)
			{
				if (list.length == 0)
					break;
				
				var id:String = list.splice(Util.randomInt(0, list.length - 1), 1)[0];
				var item:Entity = GameEngine.dataManager.createItemFromTemplate(id);
				itemlist.push(item);
			}
			
			return itemlist;
		}
		
		/* INTERFACE data.IXMLSerializable */
		
		public function toXML():XML 
		{
			var xml:XML = new XML("<itemset></itemset>");
			xml.@["min"] = _min;
			xml.@["max"] = _max;
			for each(var val:String in _items)
			{
				var valXML:XML = new XML("<item />"); 
				valXML.@["id"] = val;
				xml.appendChild(valXML);
			}
			return xml;
		}
		
		public function fromXML(xml:XML):void 
		{
			_min = xml.attribute("min");
			_max = xml.attribute("max");
			
			if (_min > _max)
				throw new GnWError("Itemset min cannot be more than max. min=" + _min +" max=" + _max);
			
			for each(var itemXML:XML in xml.item)
			{
				_items.push(itemXML.attribute("id").toXMLString());
			}
		}
		
	}

}