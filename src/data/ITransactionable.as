package data 
{
	/**
	 * This interface will allow the game to retain internal consistency in the event of errors
	 * 
	 * @author TheWrongHands
	 */
	public interface ITransactionable 
	{
		function startTransaction():void; //Begin atomic changes
		function commitTransaction():void; //Everything looks good, let's keep the changes
		function rollbackTransaction():void; //Something went wrong, let's undo changes
		function isTransactionOccurring():Boolean;
	}
}