package data 
{
	import data.EntityComponents.ECDynamicNumber;
	import data.EntityComponents.ECAttributes;
	import data.EntityComponents.ECXMLHolder;
	import enums.*;
	import flash.utils.Dictionary;
	import helpers.ContextHelper;
	/**
	 * ...
	 * @author TheWrongHands
	 * 
	 * This is only to add a temporary compatibility layer between the new entity system and the odd context system until i can figure out how to remove the context system
	 */
	public class TempDataContextableCompatibilityLayer
	{
		
		public function TempDataContextableCompatibilityLayer() 
		{
		}
		
		public static function toDataContext(ent:Entity):Object 
		{
			var context:Object = new Object();
			var status:ECAttributes;
			var dynNum:ECDynamicNumber;
			var stat:CharacterStat;
			var part:XML;
			if(ent.hasTag("character"))
			{
				context["toString"] = function():String
				{
					return ent.variables["name"] as String;
				};
				status = ent.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
				for each(stat in Utils.EnumList(CharacterStat))
				{
					context[stat.text] = status[stat.text];
				}
				for each(part in (ent.getComponent(EntityComponent.PARTS) as ECXMLHolder).xml.*) // it's done this way for reasons?
				{
					var partName:String = part.name().toString();
					var partClass:Class = GameEngine.dataManager.getPartClass(partName);
					
					context[partName] = new partClass(part);
				}
				var vars:Object = new Object();
				for(var key:String in GameEngine.world.variables)
				{
					vars[key] = GameEngine.world.variables[key];
				}
				context["vars"] = vars;
				context["gender"] = ent.variables["gender"];
				context["name"] = ent.variables["name"];
				context["id"] = ent.id;
				
				dynNum = ent.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
				context["HP"] = dynNum.value;
				context["maxHP"] = dynNum.max;
				dynNum = ent.getComponent(EntityComponent.LUST) as ECDynamicNumber;
				context["LUST"] = dynNum.value;
				context["maxLust"] = dynNum.max;
				dynNum = ent.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
				context["MP"] = dynNum.value;
				context["maxMP"] = dynNum.max;
			}
			return context;
		}
		
		public static function toParsingContext(ent:Entity):Object
		{
			var context:Object = new Object();
			var status:ECAttributes;
			var stat:CharacterStat;
			var part:XML;
			if(ent.hasTag("character"))
			{
				context["toString"] = function():String
				{
					return ent.variables["name"] as String;
				};
				for each(var gender:Gender in Utils.EnumList(Gender))
				{
					if(gender.text == ent.variables["gender"])
					{
						ContextHelper.addGenderPronouns(gender, context);
						break;
					}
				}
				status = ent.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
				for each(stat in Utils.EnumList(CharacterStat))
				{
					context[stat.text] = status[stat.text];
				}
				try // TODO Remove my retarded quick solution and do this properly - Hands
				{
					for each(part in (ent.getComponent(EntityComponent.PARTS) as ECXMLHolder).xml.*)
					{
						var partName:String = part.name().toString();
						var partClass:Class = GameEngine.dataManager.getPartClass(partName);
						
						context[partName] = new partClass(part);
						context["has" + partName.toUpperCase()] = true;
					}
				}
				catch(e:Error)
				{
				}
			}
			return context;
		}
	}

}