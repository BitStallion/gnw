package data
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Camera extends Rectangle 
	{
		public function Camera(x:Number=0, y:Number=0, width:Number=0, height:Number=0) 
		{
			super(x, y, width, height);
		}
		
		public function set center(point:Point):void
		{
			x = point.x - width / 2;
			y = point.y - height / 2;
		}
		
		public function get center():Point
		{
			return new Point(x + width / 2, y + height / 2);
		}
	}
}