package data 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ActionManager 
	{
		private var _actions:Vector.<Action>;
		
		public function ActionManager() 
		{
			_actions = new Vector.<Action>();
		}
		
		public function queueActions(actions:Vector.<Action>):void
		{
			_actions.concat(actions);
		}
		
		public function queueAction(action:Action):void
		{
			_actions.push(action);
		}
		
		public function invokeActions():void
		{
			if (_actions.length == 0)
				return;
			
			while (_actions.length > 0)
			{
				var action:Action = _actions.shift();
				action.invoke();
			}
		}
		
	}

}