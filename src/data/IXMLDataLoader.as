package data 
{
	
	/**
	 * This interface is used to allow different data managers to read XML data from files in a more efficient and fixable manner
	 * 
	 * @author TheWrongHands
	 */
	public interface IXMLDataLoader 
	{
		function startLoading():void; // Called at the start of loading so that managers can reallocate their variables
		function canHandleXMLType(name:String):Boolean; // Returns true if it can read this XML data category
		function readXMLData(file:String, xml:XML):void; // Reads the XML data set and loads the data from it
	}
}