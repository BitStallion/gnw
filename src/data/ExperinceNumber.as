package data
{
	
	/**
	 * @author BlueLight
	 * 
	 * This class has no limitation on the max value of current. This is so we can use a dynamic number with experince without having to set max value more than the next level.
	 * 
	 */
	public class ExperinceNumber extends DynamicNumber {
		//a variable to wrap my head around this variable
		private var _infinitAbove:Boolean;
		//
		public function ExperinceNumber(min:Number = 0, max:Number = 100, value:Number = 100, high:Boolean = true ){
			super(min, max, value, high);
			this._infinitAbove = !high;
		}
		
		/**
		 * this function has been changed so that you can increase the current value beyond the maximum. 
		 */
		override public function set current(value:Number):void 
		{
			//makes sure that current is above min value but nothing more.
			this._current = this._highest? Math.min(this._maximum,value):Math.max(this._minimum, value);
		}	
	}
}