package data.time 
{
	import data.*;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DateTime implements IDataContextable, IXMLSerializable
	{
		public static const DAYS_IN_YEAR:int = 365;
		public static const MONTHS_IN_YEAR:int = 12;
		public static const DAYS_IN_WEEK:int = 7;
		public static const HOURS_IN_DAY:int = 24;
		public static const MINUTES_IN_HOUR:int = 60;
		public static const SECONDS_IN_MINUTE:int = 60;
		
		public static const MONTH_NAMES:Array = new Array("January", "February", "March", "April", "May", "June", "July",
															"August", "September", "October", "November", "December");
		
		public static const MONTH_LENGTHS:Array = new Array(31, 28, 31, 30, 31, 30, 31,	31, 30, 31, 30, 31);
		
		public static const DAY_NAMES:Array = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		
		private var _year:int;
		private var _dayOfYear:uint;
		private var _hour:uint;
		private var _minute:uint;
		private var _second:uint;
		
		public function DateTime(year:int = 0, month:uint = 1, day:uint = 1, hour:uint = 0, minute:uint = 0, second:uint = 0) {
			_year = year;
			_dayOfYear = dayOfMonthToDayOfYear(month-1, day-1);
			_hour = hour;
			_minute = minute;
			_second = second;
		}
		
		public function copy():DateTime
		{
			return new DateTime(_year, month, dayOfMonth, hour, minute, second);
		}
		
		public function toString():String {
			return dateString + " " + timeString;
		}
		
		public function toDataContext():Object {
			var context:Object = new Object();
			context["toString"] = function ():String { return toString(); }
			context["date"] = dateString;
			context["time"] = timeString;
			
			return context;
		}
		
		public function get dateString():String {
			return monthName + " " + dayOfMonth + ", " + year;
		}
		
		public function get timeString():String {
			return _hour + ":" +
					(_minute > 9 ? _minute : "0" + _minute) + ":" +
					(_second > 9 ? _second : "0"+ _second);
		}
		
		public function get year():int {
			return _year;
		}
		
		public function get dayOfYear():uint {
			return _dayOfYear;
		}
		
		public function get month():uint {
			return dayOfYearToMonth(_dayOfYear) + 1;
		}
		
		public function get monthName():String {
			return MONTH_NAMES[dayOfYearToMonth(_dayOfYear)];
		}
		
		public function get dayOfMonth():uint {
			return dayOfYearToDayOfMonth(_dayOfYear) + 1;
		}
		
		public function get dayOfWeek():uint	{
			return dayOfYearToDayOfMonth(_dayOfYear) + 1;
		}
		
		public function get dayOfWeekName():String {
			return DAY_NAMES[dayOfYearToDayOfMonth(_dayOfYear)];
		}
		
		public function get hour():uint {
			return _hour;
		}
		
		public function get decimalHours():Number {
			return Number(_hour) + _minute / MINUTES_IN_HOUR + _second / (SECONDS_IN_MINUTE * MINUTES_IN_HOUR);
		}
		
		public function get minute():uint {
			return _minute;
		}
		
		public function get second():uint {
			return _second;
		}
		
		public function addYears(value:int):DateTime {
			_year += value;
			return this;
		}
		
		public function addDays(value:int):DateTime {
			var diff:int = value + _dayOfYear;
			
			var years:int = diff / DAYS_IN_YEAR;
			var remainder:int = diff % DAYS_IN_YEAR;
			
			if (remainder < 0)
			{
				years--;
				remainder += DAYS_IN_YEAR;
			}
			
			_dayOfYear = remainder;
			addYears(years);
			
			return this;
		}
		
		public function addHours(value:int):DateTime {
			var diff:int = value + _hour;
			
			var days:int = diff / HOURS_IN_DAY;
			var remainder:int = diff % HOURS_IN_DAY;
			
			if (remainder < 0)
			{
				days--;
				remainder += HOURS_IN_DAY;
			}
			
			_hour = remainder;
			addDays(days);
			
			return this;
		}
		
		public function addMinutes(value:int):DateTime {
			var diff:int = value + _minute;
			
			var hours:int = diff / MINUTES_IN_HOUR;
			var remainder:int = diff % MINUTES_IN_HOUR;
			
			if (remainder < 0)
			{
				hours--;
				remainder += MINUTES_IN_HOUR;
			}
			
			_minute = remainder;
			addHours(hours);
			
			return this;
		}
		
		public function addSeconds(value:int):DateTime {
			var diff:int = value + _second;
			
			var minutes:int = diff / SECONDS_IN_MINUTE;
			var remainder:int = diff % SECONDS_IN_MINUTE;
			
			if (remainder < 0)
			{
				minutes--;
				remainder += SECONDS_IN_MINUTE;
			}
			
			_second = remainder;
			addMinutes(minutes);
			
			return this;
		}
		
		/* INTERFACE data.IXMLSerializable */
		
		public function toXML():XML 
		{
			var xml:XML = new XML("<calendar/>");
			xml.@["year"] = _year;
			xml.@["doy"] = _dayOfYear;
			xml.@["hour"] = _hour;
			xml.@["minute"] = _minute;
			xml.@["second"] = _second;
			return xml;
		}
		
		public function fromXML(xml:XML):void 
		{
			_year = xml.@["year"];
			_dayOfYear = xml.@["doy"];
			_hour = xml.@["hour"];
			_minute = xml.@["minute"];
			_second = xml.@["second"];
		}
		
		private function dayOfYearToDayOfMonth(dayOfYear:int):int {
			var daysSoFar:int = 0;
			var month:int = 0;
			
			for (month = 0; month < MONTH_LENGTHS.length; month++)
			{
				if (dayOfYear < (MONTH_LENGTHS[month] + daysSoFar))
				{
					return dayOfYear - daysSoFar;
				}
				else
					daysSoFar += MONTH_LENGTHS[month];
			}
			
			return MONTH_LENGTHS[MONTHS_IN_YEAR - 1];
		}
		
		private function dayOfYearToMonth(dayOfYear:int):int {
			var daysSoFar:int = 0;
			var month:int = 0;
			
			for (month = 0; month < MONTH_LENGTHS.length; month++)
			{
				if (dayOfYear < (MONTH_LENGTHS[month] + daysSoFar))
				{
					return month;
				}
				else
					daysSoFar += MONTH_LENGTHS[month];
			}
			
			return MONTHS_IN_YEAR - 1;
		}
		
		private function dayOfMonthToDayOfYear(month:int, day:int):int {
			var daysSoFar:int = 0;
			if (month + 1 > MONTHS_IN_YEAR)
				month = MONTHS_IN_YEAR - 1;
				
			if (day > MONTH_LENGTHS[month])
				day = MONTH_LENGTHS[month]-1;
			
			for (var i:int = 0; i < month; i++)
			{	
				daysSoFar += MONTH_LENGTHS[i];
			}
			
			return day + daysSoFar;
		}
		
		public function valueOf():Object
		{
			var value:int = 0;
			
			value += _second;
			value += _minute * SECONDS_IN_MINUTE;
			value += _hour * SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
			
			value += _dayOfYear * SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY;
			value += _year * SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY * DAYS_IN_YEAR;
			
			return value;
		}
		
	}

}