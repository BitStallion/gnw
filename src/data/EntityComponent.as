package data 
{
	import data.EntityComponents.*
	import flash.utils.Dictionary;
	import flash.utils.Proxy;
	import utility.IDestroyable
	
	/**
	 * ...
	 * @author TheWrongHands
	 * 
	 * The base entity component class to act as an interface between the parent entity and the components that make it up
	 * 
	 * might want to actually make this an interface at some point
	 */
	public class EntityComponent extends Proxy implements IDestroyable, IXMLSerializable, ITransactionable
	{
		/* This allows for these classes to be dynamically loaded in later as that by default AS3 tries to be efficient by not
		 * compiling classes that aren't explicitly mentioned somewhere in the code
		 */
		ECArray;
		ECAttributes;
		ECBuffHandler;
		ECCombat;
		ECContainer;
		ECDamageHandler;
		ECDynamicNumber;
		ECExperience;
		ECInventory;
		ECLootTable;
		ECMapData;
		ECOnEvent;
		ECOnUseBuff;
		ECOnUseDelete;
		ECStatusModifier;
		ECStun;
		ECTimeToLive;
		ECXMLHolder;
		
		// Common Component Names
		public static const ATTACKS:String = "attacks";
		public static const ATTRIBUTES:String = "attributes";
		public static const BUFFS:String = "buffs";
		public static const COMBAT:String = "combat";
		public static const DAMAGE:String = "damage";
		public static const EXPERIENCE:String = "experience";
		public static const INVENTORY:String = "inventory";
		public static const JUICE:String = "juice";
		public static const LUST:String = "lust";
		public static const PARTS:String = "parts";
		public static const POSITION:String = "position";
		public static const SPIRIT:String = "spirit";
		public static const STORE:String = "store";
		public static const VIGOR:String = "vigor";
		
		private var _name:String;
		private var _ent:Entity;
		private var _transactionData:Dictionary;
		
		public function EntityComponent() 
		{
			_ent = null;
			_transactionData = null;
		}
		
		public function destroy():void
		{
			_ent = null;
		}
		
		public function onAdd():void
		{
		}
		
		public function onRemove():void
		{
		}
		
		public function set name(name:String):void
		{
			_name = name;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function setEntity(name:String, ent:Entity):void
		{
			_name = name;
			_ent = ent;
		}
		
		public function set entity(ent:Entity):void
		{
			_ent = ent;
		}
		
		public function get entity():Entity
		{
			return _ent;
		}
		
		public function setVariable(path:Array, value:String):void
		{
		}
		
		public function getVariable(path:Array):String
		{
			return null;
		}
		
		public function toXML():XML
		{
			return null;
		}
		
		public function fromXML(xml:XML):void
		{
		}
		
		protected function get transactionData():Dictionary
		{
			return _transactionData;
		}
		
		public function startTransaction():void 
		{
			if(_transactionData != null) return;
			_transactionData = new Dictionary();
			_transactionData["name"] = _name;
			_transactionData["entity"] = _ent;
		}
		
		public function commitTransaction():void 
		{
			if(_transactionData == null) return;
			_transactionData = null;
		}
		
		public function rollbackTransaction():void 
		{
			if(_transactionData == null) return;
			_name = _transactionData["name"] as String;
			_ent = _transactionData["entity"] as Entity;
			_transactionData = null;
		}
		
		public function isTransactionOccurring():Boolean 
		{
			return _transactionData != null;
		}
	}

}