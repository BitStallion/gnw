package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import utility.GnWError;
	import utility.Util;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	
	public class ECExperience extends EntityComponent 
	{
		private var _experience:uint;
		private var _experienceNextLevel:uint;
		
		public function ECExperience() 
		{
			super();
			_experience = 0;
			_experienceNextLevel = GameEngine.dataManager.getExpToNextLevel(GameEngine.dataManager.getLevelStart());
		}
		
		private function checkLevelUp():void
		{
			while(_experience >= _experienceNextLevel && _experienceNextLevel > 0)
			{
				entity.variables["level"]++;
				if(entity.variables["level"] < GameEngine.dataManager.getLevelCap())
				{
					_experience -= _experienceNextLevel;
				}
				else
				{
					_experience = 0;
				}
				_experienceNextLevel = GameEngine.dataManager.getExpToNextLevel(entity.variables["level"] as uint);
				try
				{
					var attrib:ECAttributes = entity.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
					attrib["free"] += GameEngine.dataManager.getThisLevelData("points", entity.variables["level"] as uint);
				}
				catch(e:GnWError)
				{
				}
				entity.dispatchEvent(new EntityEvent(EntityEvent.levelUp));
			}
		}
		
		public function set experience(experience:uint):void
		{
			if(entity.variables["level"] < GameEngine.dataManager.getLevelCap())
			{
				_experience = experience;
			}
			else
			{
				_experience = 0;
			}
			checkLevelUp();
		}
		
		public function get experience():uint
		{
			return _experience;
		}
		
		public function set experienceNextLevel(experienceNextLevel:uint):void
		{
			_experienceNextLevel = experienceNextLevel;
			checkLevelUp();
		}
		
		public function get experienceNextLevel():uint
		{
			return _experienceNextLevel;
		}
		
		public function checkNextLevel():void
		{
			experienceNextLevel = GameEngine.dataManager.getExpToNextLevel(entity.variables["level"]);
		}
		
		public override function setVariable(path:Array, value:String):void
		{
			if(path.length == 0) return;
			var name:String = path.shift() as String;
			var num:Number;
			if(value.substring(0, 1) == "+")
			{
				num = parseFloat(value.substring(1));
				if(!isNaN(num))
				{
					if(name == "experience")
					{
						experience += num;
					}
				}
			}
			else if(value.substring(0, 1) == "-")
			{
				num = parseFloat(value.substring(1));
				if(!isNaN(num))
				{
					if(name == "experience")
					{
						experience -= num;
					}
				}
			}
			else
			{
				if(!isNaN(num))
				{
					if(name == "experience")
					{
						experience = num;
					}
				}
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECExperience";
			xml.@["experience"] = _experience;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			_experience = Number(xml.@["experience"]);
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["experience"] = _experience;
			transactionData["experienceNextLevel"] = _experienceNextLevel;
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_experience = transactionData["experience"] as uint;
			_experienceNextLevel = transactionData["experienceNextLevel"] as uint;
			super.rollbackTransaction();
		}
	}
}