package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import map.Map;
	import map.Tile;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECMapData extends EntityComponent 
	{
		private var _map:Map;
		private var _x:Number;
		private var _y:Number;
		private var _z:Number;
		private var _deltaX:int;
		private var _deltaY:int;
		private var _deltaZ:int;
		private var _icon:BitmapData;
		private var _iconRect:Rectangle;
		private var _targetX:int;
		private var _targetY:int;
		private var _aiPath:Vector.<Point>;
		private var _speed:Number;
		
		public function ECMapData() 
		{
			super();
			_map = new Map("escape");
			_x = 38;
			_y = 14;
			_z = 0;
			_deltaX = 0;
			_deltaY = 0;
			_deltaZ = 0;
			_icon = GameEngine.fileResourceManager.getImage("/images/map/Player.png").bitmapData;
			_iconRect = new Rectangle(0, 0, _icon.width, _icon.height);
			_targetX = _x;
			_targetY = _y;
			_speed = 7;
		}
		
		public override function destroy():void
		{
			_icon.dispose();
			_map = null;
			super.destroy();
		}
		
		public function set map(map:Map):void
		{
			_map = map;
		}
		
		public function get map():Map
		{
			return _map;
		}
		
		public function set x(x:Number):void
		{
			_deltaX = x - _x;
			_x = x;
		}
		
		public function get x():Number
		{
			return _x;
		}
		
		public function set y(y:Number):void
		{
			_deltaY = y - _y;
			_y = y;
		}
		
		public function get y():Number
		{
			return _y;
		}
		
		public function set z(z:Number):void
		{
			_deltaZ = z - _z;
			_z = z;
		}
		
		public function get z():Number
		{
			return _z;
		}
		
		public function get deltaX():int
		{
			return _deltaX;
		}
		
		public function get deltaY():int
		{
			return _deltaY;
		}
		
		public function get deltaZ():int
		{
			return _deltaZ;
		}
		
		public function set icon(icon:BitmapData):void
		{
			_icon = icon;
		}
		
		public function get icon():BitmapData
		{
			return _icon;
		}
		
		public function set iconRect(iconRect:Rectangle):void
		{
			_iconRect = iconRect;
		}
		
		public function get iconRect():Rectangle
		{
			return _iconRect;
		}
		
		public function set targetX(x:int):void
		{
			_targetX = x;
		}
		
		public function get targetX():int
		{
			return _targetX;
		}
		
		public function set targetY(y:int):void
		{
			_targetY = y;
		}
		
		public function get targetY():int
		{
			return _targetY;
		}
		
		public function set aiPath(path:Vector.<Point>):void
		{
			_aiPath = path;
		}
		
		public function get aiPath():Vector.<Point>
		{
			return _aiPath;
		}
		
		public function stepAIPath():void
		{
			if(_aiPath != null)
			{
				if(_aiPath.length > 0)
				{
					var p:Point = _aiPath.shift();
					try
					{
						setTarget(p.x, p.y);
					}
					catch(e:GnWError)
					{
						_aiPath = null;
					}
				}
				else
				{
					_aiPath = null;
				}
			}
		}
		
		public function set speed(speed:Number):void
		{
			_speed = speed;
		}
		
		public function get speed():Number
		{
			return _speed;
		}
		
		public function setTarget(x:int, y:int):void
		{
			if(targetX == this.x && targetY == this.y)
			{
				if(x == this.x && y == this.y) return;
				var directions:uint = _map.canMoveInDirections(this.x, this.y, z, 15);
				if(y < this.y && (directions & Tile.north) > 0)
				{
					targetX = this.x;
					targetY = y;
					_map.entityExitsTile(entity, this.x, this.y, z);
				}
				else if(y > this.y && (directions & Tile.south) > 0)
				{
					targetX = this.x;
					targetY = y;
					_map.entityExitsTile(entity, this.x, this.y, z);
				}
				else if(x > this.x && (directions & Tile.east) > 0)
				{
					targetX = x;
					targetY = this.y;
					_map.entityExitsTile(entity, this.x, this.y, z);
				}
				else if(x < this.x && (directions & Tile.west) > 0)
				{
					targetX = x;
					targetY = this.y;
					_map.entityExitsTile(entity, this.x, this.y, z);
				}
				else
				{
					throw new GnWError("Can't move that way");
				}
			}
		}
		
		public function move(deltaTime:Number):void
		{
			if(targetX != x || targetY != y)
			{
				var vec:Point = new Point(targetX - x, targetY - y);
				if(vec.length <= 0.03125 || vec.length <= speed * deltaTime)
				{
					x = targetX;
					y = targetY;
					_map.entityEntersTile(entity, x, y, z);
					entity.dispatchEvent(new EntityEvent(EntityEvent.enterTile));
					stepAIPath();
				}
				else
				{
					vec.normalize(_speed * deltaTime);
					x += vec.x;
					y += vec.y;
				}
			}
			else if(_aiPath != null && _aiPath.length > 0)
			{
				stepAIPath();
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = super.toXML();
			var slotsXML:XML = new XML("<slots></slots>");
			xml.@["type"] = "ECMapData";
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			super.fromXML(xml);
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			super.rollbackTransaction();
		}
	}
}