package data.EntityComponents 
{
	import combat.Allegiance;
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import data.entityEvents.EntityEventOnDamage;
	import enums.DamageType;
	
	/**
	 * Handles damage received from combat and other sources
	 * 
	 * @author TheWrongHands
	 */
	public class ECDamageHandler extends EntityComponent 
	{
		
		public function ECDamageHandler() 
		{
			super();
		}
		
		public function takeDamage(amount:Number, type:DamageType, attacker:Entity = null, hitMessage:String = null):void
		{
			var combatComp:ECCombat = entity.getComponent(EntityComponent.COMBAT) as ECCombat;
			var health:ECDynamicNumber = entity.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			var lust:ECDynamicNumber = entity.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			var prevallegiance:Allegiance = GameEngine.currentBattle.logCombatHeader.allegiance;
			var event:EntityEventOnDamage = new EntityEventOnDamage();
			event.target = entity;
			event.attacker = attacker;
			event.damage = amount;
			event.damageType = type;
			if(attacker != null) attacker.dispatchEvent(new EntityEventOnDamage(EntityEvent.onDealDamage, event.bubbles, event.cancelable, event));
			entity.dispatchEvent(event);
			var damage:Number = event.damage;
			damage = (event.damage * event.damageMut + event.damageAdd) * (1 - event.groupResistance);
			if(type != type.group)
			{
				damage *= (1 - event.damageResistance);
			}
			damage = Math.max(0, damage);
			var context:Object = {"owner": attacker, "target": entity, "effect": damage};
			GameEngine.logManager.pushContext(context);
			if(hitMessage != null) GameEngine.logManager.appendToLog(hitMessage);
			GameEngine.currentBattle.logCombatHeader.allegiance = combatComp.allegiance;
			switch(type.group)
			{
				case DamageType.lust:
				{
					GameEngine.logManager.appendToLog(Assets.TAKE_LUST_DAMAGE);
					lust.value += damage;
					break;
				}
				case DamageType.physical:
				case DamageType.magic:
				{
					health.value -= damage;
					if(health.value > 0)
					{
						GameEngine.logManager.appendToLog(Assets.TAKE_JUICE_DAMAGE);
					}
					else
					{
						combatComp.alive = false;
						GameEngine.logManager.appendToLog(Assets.JUICE_OUT);
					}
					break;
				}
			}
			GameEngine.currentBattle.logCombatHeader.allegiance = prevallegiance;
			GameEngine.logManager.popContext();
		}
	}
}