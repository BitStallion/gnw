package data.EntityComponents 
{
	import data.EntityComponent;
	
	/**
	 * This component keeps track of the number of items in a stack as well as the number of remaining uses
	 * 
	 * @author TheWrongHands
	 */
	public class ECStackable extends EntityComponent 
	{
		
		public function ECStackable() 
		{
			super();
		}
	}
}