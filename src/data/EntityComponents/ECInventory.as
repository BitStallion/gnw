package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import data.entityEvents.EntityEventGetAttribute;
	import data.entityEvents.EntityEventOnDamage;
	import enums.DamageType;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import utility.GnWError;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECInventory extends ECContainer 
	{
		// TODO Move slots into it's own component that isn't a child of ECContainer - hands
		private var _slots:Dictionary;
		private var _armorClass:String;
		private var _absoluteArmor:uint;
		private var _relativeArmor:uint;
		
		public function ECInventory() 
		{
			super();
			_slots = new Dictionary();
			_absoluteArmor = 0;
			_relativeArmor = 0;
		}
		
		public override function destroy():void
		{
			_slots = null;
			super.destroy();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.resetToBase, resetToBase);
			entity.addEventListener(EntityEvent.applyEffects, applyEffects);
			entity.addEventListener(EntityEvent.getAttribute, getAttribute);
			entity.addEventListener(EntityEvent.onReceiveDamage, onReceiveDamage);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.resetToBase, resetToBase);
			entity.removeEventListener(EntityEvent.applyEffects, applyEffects);
			entity.removeEventListener(EntityEvent.getAttribute, getAttribute);
			entity.removeEventListener(EntityEvent.onReceiveDamage, onReceiveDamage);
		}
		
		override public function clear():void 
		{
			super.clear();
			_slots = new Dictionary();
		}
		
		public function allTradableEntities():Vector.<Entity>
		{
			var ents:Vector.<Entity> = new Vector.<Entity>();
			for each(var ent:Entity in _entities)
			{
				if(!ent.hasTag("questItem") && ent.variables["equipedSlot"] == null)
				{
					ents.push(ent);
				}
			}
			return ents;
		}
		
		override public function removeEntity(ent:Entity):void
		{
			if(!containsEntity(ent)) return;
			unequipEntity(ent);
			super.removeEntity(ent);
		}
		
		public function equipEntity(ent:Entity):void
		{
			var slot:String = ent.variables["slot"];
			if(_slots[slot] != null)
			{
				var ent1:Entity = _entities[_slots[slot] as String] as Entity;
				if(ent1 != null)
				{
					entity.removeEventDispatcher(ent1);
					delete ent1.variables["equipedSlot"];
				}
			}
			ent.variables["equipedSlot"] = slot;
			_slots[slot] = ent.uuid;
			entity.addEventDispatcher(ent);
			if(slot == "armor") _armorClass = entity.variables["armorClass"] || "naked";
		}
		
		public function equipEntityByUUID(slot:String, uuid:String):void
		{
			var ent:Entity = _entities[uuid] as Entity;
			if(ent == null) throw new GnWError("No entity with the uuid \"" + uuid + "\" within container");
			if(_slots[slot] != null)
			{
				var ent1:Entity = _entities[_slots[slot] as String] as Entity;
				if(ent1 != null)
				{
					entity.removeEventDispatcher(ent1);
					delete ent1.variables["equipedSlot"];
				}
			}
			ent.variables["equipedSlot"] = slot;
			_slots[slot] = uuid;
			entity.addEventDispatcher(ent);
			if(slot == "armor") _armorClass = entity.variables["armorClass"] || "naked";
		}
		
		/*public function equipEntityByID(slot:String, id:String):void
		{
			for each(var ent:Entity in _entities)
			{
				if(ent.id == id)
				{
					_slots[slot] = _entities[uuid];
					return;
				}
			}
			throw new GnWError("No entity with the id \"" + id + "\" within container");
		}*/
		
		public function unequipEntity(ent:Entity):void
		{
			if(ent.variables["equipedSlot"] != null)
			{
				entity.removeEventDispatcher(ent);
				var slot:String = ent.variables["equipedSlot"];
				delete ent.variables["equipedSlot"];
				delete _slots[slot];
				if(slot == "armor") _armorClass = "naked";
			}
		}
		
		public function getEntityInSlot(slot:String):Entity
		{
			return _entities[_slots[slot]];
		}
		
		public function resetToBase(event:Event):void
		{
			_absoluteArmor = 0;
			_relativeArmor = 0;
		}
		
		public function applyEffects(event:Event):void
		{
			var a:ECArray = entity.getComponent(EntityComponent.ATTACKS) as ECArray;
			for each(var uuid:String in _slots)
			{
				var ent:Entity = _entities[uuid];
				if(ent.hasTag("weapon"))
				{
					a.data.push(ent.variables["attack"]);
				}
				else if(ent.hasTag("armor"))
				{
					_absoluteArmor += ent.variables["absoluteArmor"] || 0;
					_relativeArmor += ent.variables["relativeArmor"] || 0;
				}
			}
			if(a.data.length == 0) a.data.push("Punch");
		}
		
		public function getAttribute(event:Event):void
		{
			if(event is EntityEventGetAttribute)
			{
				var getAttr:EntityEventGetAttribute = event as EntityEventGetAttribute;
				if(getAttr.attribute == "doge")
				{
					switch(_armorClass)
					{
						case "naked":
						{
							break;
						}
						case "cloth":
						{
							break;
						}
						case "light":
						{
							getAttr.value -= 0.1;
							break;
						}
						case "heavy":
						{
							getAttr.value -= 0.25;
							break;
						}
					}
				}
			}
		}
		
		public function onReceiveDamage(event:Event):void
		{
			if(event is EntityEventOnDamage)
			{
				var damageEvent:EntityEventOnDamage = event as EntityEventOnDamage;
				if(damageEvent.damageType.group == DamageType.physical)
				{
					damageEvent.damageAdd -= _absoluteArmor;
					damageEvent.groupResistance += _relativeArmor / 100;
				}
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = super.toXML();
			var slotsXML:XML = new XML("<slots></slots>");
			xml.@["type"] = "ECInventory";
			for(var slot:String in _slots)
			{
				var slotXML:XML = new XML("<slot />");
				slotXML.@["slotID"] = slot;
				slotXML.@["uuid"] = _slots[slot] as String;
				slotsXML.appendChild(slotXML);
			}
			xml.appendChild(slotsXML);
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			super.fromXML(xml);
			var groupXML:XML = xml.child("slots")[0];
			var entryXML:XML;
			if(groupXML != null)
			{
				for each(entryXML in groupXML.child("slot"))
				{
					equipEntityByUUID(entryXML.@["slotID"].toXMLString(), entryXML.@["uuid"].toXMLString());
				}
			}
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			var dic:Dictionary = new Dictionary();
			var key:Object;
			for(key in _slots)
			{
				dic[key] = _slots[key];
			}
			transactionData["slots"] = dic;
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_slots = transactionData["slots"] as Dictionary;
			super.rollbackTransaction();
		}
	}
}