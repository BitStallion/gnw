package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import data.entityEvents.DeltaTime;
	import flash.events.Event;
	import flash.utils.flash_proxy;
	
	/**
	 * This will be used to keep track of player and character health, lust, item durability, consumable usage, ect.
	 * 
	 * Kind of a remake of the DynamicNumber class written by BlueLight
	 * 
	 * @author TheWrongHands
	 */
	use namespace flash_proxy;
	public class ECDynamicNumber extends EntityComponent
	{
		private var _min:Number // the lowest the value can be, base is always = 0
		private var _maxBase:Number; // keeps track of the max value without any modifications from items, spells, etc.
		private var _max:Number; // the highest the value can be
		private var _value:Number; // the current value
		private var _regenBase:Number // the base regen without any modifications
		private var _regen:Number // the current regen rate
		
		public function ECDynamicNumber(maxBase:Number = 50, value:Number = 0, regenBase:Number = 0)
		{
			super();
			_min = 0;
			_max = _maxBase = Math.max(0, maxBase);
			_value = Math.max(Math.min(value, _max), 0);
			_regen = _regenBase = regenBase;
		}
		
		override flash_proxy function getProperty(name:*):*
		{
			var nameStr:String;
			if(name is QName || name is String)
			{
				if(name is QName)
				{
					nameStr = (name as QName).localName; 
				}
				else
				{
					nameStr = name as String; 
				}
				switch(nameStr)
				{
					case "min":
					{
						return min;
						break;
					}
					case "maxBase":
					{
						return maxBase;
						break;
					}
					case "max":
					{
						return max;
						break;
					}
					case "value":
					{
						return value;
						break;
					}
					case "regenBase":
					{
						return regenBase;
						break;
					}
					case "regen":
					{
						return regen;
						break;
					}
				}
			}
			return null;
		}
		
		override flash_proxy function setProperty(name:*, value:*):void
		{
			var nameStr:String;
			var num:Number = parseFloat(value);
			if((name is QName || name is String) && !isNaN(num))
			{
				if(name is QName)
				{
					nameStr = (name as QName).localName; 
				}
				else
				{
					nameStr = name as String; 
				}
				switch(nameStr)
				{
					case "min":
					{
						min = num;
						break;
					}
					case "maxBase":
					{
						maxBase = num;
						break;
					}
					case "max":
					{
						max = num;
						break;
					}
					case "value":
					{
						value = num;
						break;
					}
					case "regenBase":
					{
						regenBase = num;
						break;
					}
					case "regen":
					{
						regen = num;
						break;
					}
				}
			}
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.resetToBase, resetToBase);
			entity.addEventListener(EntityEvent.deltaTime, deltaTime);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.resetToBase, resetToBase);
			entity.removeEventListener(EntityEvent.deltaTime, deltaTime);
		}
		
		public function set min(min:Number):void
		{
			_min = Math.max(Math.min(min, _max), 0);
		}
		
		public function get min():Number
		{
			return _min;
		}
		
		public function set maxBase(maxBase:Number):void
		{
			_maxBase = Math.max(0, maxBase);
		}
		
		public function get maxBase():Number
		{
			return _maxBase;
		}
		
		public function set max(max:Number):void
		{
			_max = Math.max(_min, max);
		}
		
		public function get max():Number
		{
			return _max;
		}
		
		public function set value(value:Number):void
		{
			_value = Math.max(Math.min(value, _max), min);
		}
		
		public function get value():Number
		{
			_value = Math.max(Math.min(_value, _max), _min);
			return _value;
		}
		
		public function set regenBase(regenBase:Number):void
		{
			_regenBase = regenBase;
		}
		
		public function get regenBase():Number
		{
			return _regenBase;
		}
		
		public function set regen(regen:Number):void
		{
			_regen = regen;
		}
		
		public function get regen():Number
		{
			return _regen;
		}
		
		public override function setVariable(path:Array, value:String):void
		{
			if(path.length == 0) return;
			var name:String = path.shift() as String;
			var num:Number;
			if(value.substring(0, 1) == "+")
			{
				num = parseFloat(value.substring(1));
				if(!isNaN(num))
				{
					if(name == "min")
					{
						min += num;
					}
					else if(name == "maxBase")
					{
						maxBase += num;
					}
					else if(name == "max")
					{
						max += num;
					}
					else if(name == "value")
					{
						this.value += num;
					}
					else if(name == "regenBase")
					{
						regenBase += num;
					}
					else if(name == "regen")
					{
						regen += num;
					}
				}
			}
			else if(value.substring(0, 1) == "-")
			{
				num = parseFloat(value.substring(1));
				if(!isNaN(num))
				{
					if(name == "min")
					{
						min -= num;
					}
					else if(name == "maxBase")
					{
						maxBase -= num;
					}
					if(name == "max")
					{
						max -= num;
					}
					else if(name == "value")
					{
						this.value -= num;
					}
					else if(name == "regenBase")
					{
						regenBase -= num;
					}
					else if(name == "regen")
					{
						regen -= num;
					}
				}
			}
			else
			{
				if(value.toLowerCase() == "min")
				{
					num = min;
				}
				else if(value.toLowerCase() == "maxBase")
				{
					num = maxBase;
				}
				else if(value.toLowerCase() == "max")
				{
					num = max;
				}
				else if(value.toLowerCase() == "regenBase")
				{
					num = regenBase;
				}
				else if(value.toLowerCase() == "regen")
				{
					num = regen;
				}
				else
				{
					num = parseFloat(value);
				}
				if(!isNaN(num))
				{
					if(name == "min")
					{
						min = num;
					}
					else if(name == "maxBase")
					{
						maxBase = num;
					}
					if(name == "max")
					{
						max = num;
					}
					else if(name == "value")
					{
						this.value = num;
					}
					else if(name == "regenBase")
					{
						regenBase = num;
					}
					else if(name == "regen")
					{
						regen = num;
					}
				}
			}
		}
		
		public override function getVariable(path:Array):String
		{
			if(path.length == 0) return null;
			var name:String = path.shift() as String;
			if(name == "min")
			{
				return min.toString();
			}
			else if(name == "maxBase")
			{
				return maxBase.toString();
			}
			else if(name == "maxBase")
			{
				return max.toString();
			}
			else if(name == "value")
			{
				return value.toString();
			}
			else if(name == "regenBase")
			{
				return regenBase.toString();
			}
			else if(name == "regen")
			{
				return regen.toString();
			}
			return null;
		}
		
		public function resetToBase(event:Event):void
		{
			min = 0;
			max = maxBase;
			regen = regenBase;
		}
		
		public function deltaTime(event:Event):void
		{
			var delta:DeltaTime = event as DeltaTime;
			var genTotal:Number = delta.deltaTime * regen;
			var gen:Number = genTotal;
			if(gen > 0) gen = Math.min(max - value, gen);
			if(gen < 0) gen = Math.max(min - value, gen);
			if(regen != 0)
			{
				value += gen;
				GameEngine.logManager.appendToLog("Regenerated " + gen + "/" + genTotal + " " + name);
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECDynamicNumber";
			xml.@["max"] = maxBase;
			xml.@["value"] = value;
			xml.@["regen"] = regenBase;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			max = maxBase = Number(xml.@["max"]);
			value = Number(xml.@["value"]);
			regen = regenBase = Number(xml.@["regen"]);
		}
		
		override public function startTransaction():void
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["maxBase"] = maxBase;
			transactionData["max"] = max;
			transactionData["value"] = value;
			transactionData["regenBase"] = regenBase;
			transactionData["regen"] = regen;
		}
		
		override public function rollbackTransaction():void
		{
			if(transactionData == null) return;
			maxBase = transactionData["maxBase"] as Number;
			max = transactionData["max"] as Number;
			value = transactionData["value"] as Number;
			regenBase = transactionData["regenBase"] as Number;
			regen = transactionData["regen"] as Number;
			super.rollbackTransaction();
		}
	}
}