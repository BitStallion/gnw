package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECStun extends EntityComponent 
	{
		private var _stunMessage:String;
		
		public function ECStun() 
		{
			super();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.applyEffects, applyEffects);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.applyEffects, applyEffects);
		}
		
		public function applyEffects(event:Event):void
		{
			if(event is EntityEvent)
			{
				var entEvent:EntityEvent = event as EntityEvent;
				try
				{
					var combatComp:ECCombat = (entEvent.target as Entity).getComponent(EntityComponent.COMBAT) as ECCombat;
					combatComp.stunMessage = _stunMessage;
				}
				catch(e:GnWError)
				{
				}
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECStun";
			xml.@["stunMessage"] = _stunMessage;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			_stunMessage = xml.@["stunMessage"].toString();
		}
		
		override public function startTransaction():void
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["stunMessage"] = _stunMessage;
		}
		
		override public function rollbackTransaction():void
		{
			if(transactionData == null) return;
			_stunMessage = transactionData["stunMessage"] as String;
			super.rollbackTransaction();
		}
	}
}