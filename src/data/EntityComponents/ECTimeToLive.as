package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import data.entityEvents.DeltaTime;
	import flash.events.Event;
	
	/**
	 * This component will allow buffs to expire after a time
	 * 
	 * @author TheWrongHands
	 */
	public class ECTimeToLive extends EntityComponent 
	{
		public var timeAlive:Number; // How long this has been going for
		public var timeToLive:Number; // When this dies
		
		public function ECTimeToLive() 
		{
			super();
			timeAlive = 0;
			timeToLive = 0;
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.deltaTime, deltaTime, false, int.MIN_VALUE);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.deltaTime, deltaTime);
		}
		
		public function deltaTime(event:Event):void
		{
			var delta:DeltaTime = event as DeltaTime;
			timeAlive += delta.deltaTime;
			if(timeAlive >= timeToLive)
			{
				if(entity != null)
				{
					entity.destroy();
					if(event.target is Entity)
					{
						var ent:Entity = event.target as Entity;
						ent.markDirty();
					}
				}
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECTimeToLive";
			xml.@["alive"] = timeAlive;
			xml.@["ttl"] = timeToLive;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			timeAlive = parseFloat(xml.@["alive"].toXMLString()) || 0;
			timeToLive = parseFloat(xml.@["ttl"].toXMLString()) || 0;
		}
	}
}