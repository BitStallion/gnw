package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	/**
	 * This component is responsible for keeping track of the applied buffs and maintaining their effects
	 * 
	 * @author TheWrongHands
	 */
	public class ECBuffHandler extends ECContainer 
	{
		public function ECBuffHandler()
		{
			super();
		}
		
		override public function addEntity(ent:Entity):void
		{
			if(containsEntity(ent)) return;
			var buffGroupData:XML = GameEngine.dataManager.getBufferGroupData(ent.variables["buffGroup"] as String);
			if(buffGroupData != null)
			{
				var buffList:Vector.<Entity> = allEntities();
				var buff:Entity;
				var buffGroupID:String;
				var entry:XML;
				for each(buff in buffList)
				{
					buffGroupID = buff.variables["buffGroup"] as String;
					if(buffGroupID != null)
					{
						for each(entry in buffGroupData.conflicts.buffGroup)
						{
							if(buffGroupID == entry.@["id"].toString())
							{
								ent.destroy();
								return;
							}
						}
					}
				}
				for each(buff in buffList)
				{
					buffGroupID = buff.variables["buffGroup"] as String;
					if(buffGroupID != null)
					{
						for each(entry in buffGroupData.replaces.buffGroup)
						{
							if(buffGroupID == entry.@["id"].toString())
							{
								removeEntity(buff);
								buff.destroy();
								break;
							}
						}
					}
				}
			}
			super.addEntity(ent);
			entity.addEventDispatcher(ent);
		}
		
		override public function removeEntity(ent:Entity):void
		{
			if(!containsEntity(ent)) return;
			entity.removeEventDispatcher(ent);
			super.removeEntity(ent);
		}
		
		public override function toXML():XML
		{
			var xml:XML = super.toXML();
			xml.@["type"] = "ECBuffHandler";
			return xml;
		}
	}
}