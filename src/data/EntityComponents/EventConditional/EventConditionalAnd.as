package data.EntityComponents.EventConditional
{
	import data.EventConditional;
	import flash.events.Event;
	import flash.utils.getDefinitionByName
	
	/**
	 * All conditions within this must be true for this to succeed
	 * 
	 * @author TheWrongHands
	 */
	public class EventConditionalAnd extends EventConditional
	{
		protected var _conditions:Vector.<EventConditional>;
		
		public function EventConditionalAnd()
		{
			super();
			_conditions = new Vector.<EventConditional>();
		}
		
		override public function destroy():void
		{
			for each(var cond:EventConditional in _conditions)
			{
				cond.destroy();
			}
			_conditions = null;
			super.destroy();
		}
		
		override public function eventMeetsCondition(event:Event):Boolean
		{
			for each(var cond:EventConditional in _conditions)
			{
				if(!cond.eventMeetsCondition(event))
					return false;
			}
			return true;
		}
		
		override public function toXML():XML
		{
			var xml:XML = new XML("<And></And>");
			for each(var cond:EventConditional in _conditions)
			{
				xml.appendChild(cond.toXML())
			}
			return xml;
		}
		
		override public function fromXML(xml:XML):void
		{
			for each(var entryXML:XML in xml.children())
			{
				var cls:Class = getDefinitionByName("data.EntityComponents.EventConditional.EventConditional" + entryXML.name()) as Class;
				var act:EventConditional = new cls();
				act.fromXML(entryXML);
				_conditions.push(act);
			}
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			for each(var cond:EventConditional in _conditions)
			{
				cond.startTransaction();
			}
			transactionData["conditions"] = _conditions.slice();
		}
		
		override public function commitTransaction():void 
		{
			if(transactionData == null) return;
			for each(var cond:EventConditional in _conditions)
			{
				cond.commitTransaction();
			}
			super.commitTransaction();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			var conds:Vector.<EventConditional> = transactionData["actions"] as Vector.<EventConditional>;
			var cond:EventConditional;
			for each(cond in _conditions)
			{
				if(conds.indexOf(cond) < 0)
				{
					cond.destroy();
				}
			}
			_conditions = conds;
			for each(cond in _conditions)
			{
				cond.rollbackTransaction();
			}
			super.rollbackTransaction();
		}
	}
}