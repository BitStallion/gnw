package data.EntityComponents.EventConditional
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECBuffHandler;
	import data.EventConditional;
	import flash.events.Event;
	
	/**
	 * This returns succeeds only when the target has a buff with the matching id
	 * 
	 * @author TheWrongHands
	 */
	public class EventConditionalTargetHasBuff extends EventConditional
	{
		private var _buffID:String;
		
		public function EventConditionalTargetHasBuff()
		{
			super();
		}
		
		override public function eventMeetsCondition(event:Event):Boolean
		{
			if(event.target is Entity)
			{
				return((event.target as Entity).getComponent(EntityComponent.BUFFS) as ECBuffHandler).containsEntityWithID(_buffID);
			}
			return false;
		}
		
		override public function toXML():XML
		{
			var xml:XML = new XML("<TargetHasBuff />");
			xml.@["buff"] = _buffID;
			return null;
		}
		
		override public function fromXML(xml:XML):void
		{
			_buffID = xml.@["buff"].toXMLString();
		}
		
		override public function startTransaction():void 
		{
			if(transactionData == null) return;
			super.startTransaction();
			transactionData["buffID"] = _buffID;
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_buffID = transactionData["buffID"] as String;
			super.rollbackTransaction();
		}
	}
}