package data.EntityComponents.EventConditional 
{
	import data.EventConditional;
	import flash.events.Event;
	/**
	 * All conditions within this must be false for this to succeed
	 * 
	 * @author TheWrongHands
	 */
	public class EventConditionalNot extends EventConditionalAnd 
	{
		public function EventConditionalNot() 
		{
			super();
			
		}
		
		override public function eventMeetsCondition(event:Event):Boolean
		{
			for each(var cond:EventConditional in _conditions)
			{
				if(cond.eventMeetsCondition(event))
					return false;
			}
			return true;
		}
		
		override public function toXML():XML
		{
			var xml:XML = super.toXML();
			xml.setName("Not");
			return xml;
		}
	}
}