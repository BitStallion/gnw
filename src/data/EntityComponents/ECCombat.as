package data.EntityComponents 
{
	import combat.Ability;
	import combat.ability.special.OrgasmAbility;
	import combat.ability.special.StunnedAbility;
	import combat.ability.special.WonderingHandsAbility;
	import combat.ability.XmlBasicAttack;
	import combat.Allegiance;
	import combat.BasicAI;
	import combat.Brain;
	import combat.brain.ComputerBrain;
	import combat.brain.HumanBrain;
	import combat.CombatEvent;
	import combat.CombatSystem;
	import combat.IDescriptable;
	import combat.RandomAI;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import enums.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import utility.GnWError;
	
	/**
	 * This class contains the combat related systems and variables needed by an entity
	 * 
	 * @author TheWrongHands
	 */
	public class ECCombat extends EntityComponent implements IDescriptable
	{
		private var _active:Boolean;
		private var _alive:Boolean;
		private var _basickAttack:Ability;
		private var _brain:Brain;
		private var _defended:Boolean;
		private var _buttonId:int;
		
		public var abilities:Vector.<Ability>;
		public var ai:BasicAI;
		public var allegiance:Allegiance;
		public var initiative:int;
		public var dispatcher:EventDispatcher;
		public var stunMessage:String;
		
		public function ECCombat() 
		{
			super();
		}
		
		public override function destroy():void
		{
			_basickAttack = null;
			_brain = null;
			abilities = null;
			ai = null;
			dispatcher = null;
			super.destroy();
		}
		
		public override function onAdd():void
		{
			reset();
			entity.addEventListener(EntityEvent.resetToBase, resetToBase);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.resetToBase, resetToBase);
		}
		
		public function reset():void
		{
			_active = true;
			_alive = true;
			_brain = null;
			_defended == false;
			_buttonId = -1;
			abilities = new Vector.<Ability>();
			allegiance = Allegiance.Noncombatant;
			initiative = 0;
			dispatcher = new EventDispatcher();
			stunMessage = null;
			//dispatcher.addEventListener(CombatEvent.ACT, actEventHandler);
			var attacks:ECArray = entity.getComponent(EntityComponent.ATTACKS) as ECArray;
			for each(var attackID:String in attacks.data)
			{
				var attack:XmlBasicAttack = GameEngine.combatDataManager.getAttack(attackID);
				registerAbility(attack, attack.MPCost <= 0);
			}
			registerAI(new RandomAI());
		}
		
		public function determineAbility():Boolean
		{
			if(stunMessage != null)
			{
				brain.ability = new StunnedAbility(entity, stunMessage);
				return true;
			}
			var lust:ECDynamicNumber = entity.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			if(lust.value >= 25)
			{
				if(lust.value >= 100)
				{
					brain.ability = new OrgasmAbility(entity);
					try
					{
						var buffs:ECBuffHandler = entity.getComponent(EntityComponent.BUFFS) as ECBuffHandler;
						buffs.addEntity(GameEngine.dataManager.createBuffFromTemplate("Orgasm"));
						entity.applyEffects();
					}
					catch(e:GnWError)
					{
					}
					return true;
				}
				else
				{
					if(lust.value / 100 > Math.random())
					{
						if(lust.value < 50)
						{
							brain.ability = new StunnedAbility(entity, "[owner.name] is distracted by lusty impure thoughts.");
							return true;
						}
						else if(lust.value < 75)
						{
							if(Math.random() < 0.5)
							{
								brain.ability = new WonderingHandsAbility(entity);
							}
							else
							{
								brain.ability = new StunnedAbility(entity, "[owner.name] is distracted by lusty impure thoughts.");
							}
							return true;
						}
						else
						{
							if(Math.random() < 0.75)
							{
								brain.ability = new WonderingHandsAbility(entity);
							}
							else
							{
								brain.ability = new StunnedAbility(entity, "[owner.name] is distracted by lusty impure thoughts.");
							}
							return true;
						}
					}
				}
			}
			return brain.operate();
		}
		
		public function preRound():void
		{
			_defended = false;
			if(brain.ability) brain.ability.preRound();
		}
		
		public function round():void
		{
			if(brain.ability) brain.ability.useOn(brain.target);
		}
		
		public function postRound():void
		{
			brain.clearAbility();
		}
		
		public function get description():String
		{
			return entity.variables["description"] || entity.variables["name"] || "Unknown Entity";
		}
		
		public function get shortDescription():String
		{
			return entity.variables["shortDescription"] || entity.variables["name"] || "Unknown Entity";
		}
		
		public function get buttonDescription():String
		{
			return entity.variables["name"] || shortDescription;
		}
		
		public function get buttonId():int
		{
			return _buttonId;
		}
		
		public function get gender():Gender
		{
			return Utils.Parse(Gender, entity.variables["gender"]) as Gender || Gender.Male;
		}
		
		public function set alive(alive:Boolean):void
		{
			_alive = alive;
		}
		
		public function set active(active:Boolean):void
		{
			_active = active;
		}
		
		public function get active():Boolean
		{
			return _active && _alive;
		}
		
		public function isStunned():Boolean
		{
			return stunMessage != null;
		}
		
		public function defend():void
		{
			_defended = true;
		}
		
		public function get basickAttack():Ability
		{
			return _basickAttack;
		}
		
		public function surrender():void
		{
			_active = false;
		}
		
		public function rollForInitiative():int
		{
			//return initiative;
			return GameEngine.currentBattle.random.getNextInt(20);
		}
		
		public function registerAbility(ability:Ability, isBasicAttack:Boolean = false):void
		{
			ability.owner = entity;
			abilities.push(ability);
			if(isBasicAttack)
			{
				_basickAttack = ability;
			}
		}
		
		public function registerAI(anAI:BasicAI):void
		{
			ai = anAI;
			anAI.owner = entity;
		}
		
		public function get brain():Brain
		{
			if(_brain ==  null)
			{
				if(allegiance == Allegiance.Player)
				{
					_brain = new HumanBrain(entity);
				}
				else
				{
					_brain = new ComputerBrain(entity);
				}
			}
			return _brain;
		}
		
		public function getHealthDescription():String
		{
			var health:ECDynamicNumber = entity.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			return "HP:" + health.value + "/" + health.max;
		}
		
		public function resetToBase(event:Event):void
		{
			stunMessage = null;
		}
	}
}