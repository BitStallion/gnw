package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.item.ItemSet;
	import data.time.DateTime;
	import utility.GnWError;
	import utility.Util;
	/**
	 * Used to generate sets of items for use in lootable containers, merchant inventories and combat loot.
	 * 
	 * @author GnW Team
	 */
	public class ECLootTable extends EntityComponent
	{
		private var _lootSets:Vector.<ItemSet>;
		private var _lastGenerated:DateTime;
		private var _moneyMax:int;
		private var _moneyMin:int;
		
		public function ECLootTable() 
		{
			super();
			_lootSets = new Vector.<ItemSet>();
		}
		
		/**
		 * Generates a set of items based on the lootsets in the loot table.
		 * @return	A vector of items
		 */
		public function generateItems():Vector.<Entity>
		{
			if (GameEngine.world != null)
				_lastGenerated = GameEngine.world.gameTime.copy();
			
			var items:Vector.<Entity> = new Vector.<Entity>();
			
			for (var i:int = 0; i < _lootSets.length; i++)
			{
				items = items.concat(_lootSets[i].generateItems());
			}
			
			return items;
		}
		
		public function generateMoney():int 
		{
			return Util.randomInt(_moneyMin, _moneyMax);
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component></component>");
			xml.@["type"] = "ECLootTable";
			for each(var val:ItemSet in _lootSets)
			{
				xml.appendChild(val.toXML());
			}
			
			if (_lastGenerated != null)
				xml.appendChild(_lastGenerated.toXML());
			
			if (!(_moneyMax == 0 && _moneyMin == 0))
			{
				var money:XML = new XML("<money />");
				money.@["min"] = _moneyMin;
				money.@["max"] = _moneyMax;
			}
			
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			for each(var itemsetXML:XML in xml.itemset)
			{
				var itemSet:ItemSet = new ItemSet();
				itemSet.fromXML(itemsetXML);
				_lootSets.push(itemSet);
			}
			
			var date:XML = xml.calendar[0]
			if (date != null)
			{
				_lastGenerated = new DateTime();
				_lastGenerated.fromXML(date);
			}
			
			var money:XML = xml.money[0]
			if (money != null)
			{
				_moneyMin = money.@["min"];
				_moneyMax = money.@["max"];
				
				if (_moneyMin > _moneyMax)
					throw new GnWError("Loot Table money min cannot be more than money max. min=" + _moneyMin +" max=" + _moneyMax);
			}
		}
		
		/**
		 * The last game time that the loot table generated a set of items.
		 */
		public function get lastGenerated():DateTime 
		{
			return _lastGenerated;
		}
	}
}