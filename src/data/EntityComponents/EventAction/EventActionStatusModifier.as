package data.EntityComponents.EventAction
{
	import data.Entity;
	import data.EventAction;
	import flash.events.Event;
	
	/**
	 * This action modifies one of the target's stats when an event occurs
	 * 
	 * @author TheWrongHands
	 */
	public class EventActionStatusModifier extends EventAction 
	{
		public var variable:String;
		public var value:String;
		
		public function EventActionStatusModifier() 
		{
			super();
		}
		
		override public function action(event:Event):void
		{
			var ent:Entity = event.target as Entity;
			ent.setVariable(variable, value);
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<StatusModifier />");
			xml.@["variable"] = variable;
			xml.@["value"] = value;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			variable = xml.@["variable"].toXMLString()
			value = xml.@["value"].toXMLString();
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["variable"] = variable;
			transactionData["value"] = value;
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			variable = transactionData["variable"] as String;
			value = transactionData["value"] as String;
			super.rollbackTransaction();
		}
	}
}