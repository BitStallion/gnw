package data.EntityComponents.EventAction 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECBuffHandler;
	import data.EventAction;
	import flash.events.Event;
	
	/**
	 * This action adds a buff to the target entity when an event occurs
	 * 
	 * @author TheWrongHands
	 */
	public class EventActionAddBuffToTarget extends EventAction 
	{
		private var _buff:String;
		
		public function EventActionAddBuffToTarget() 
		{
			super();
		}
		
		override public function action(event:Event):void
		{
			var ent:Entity = event.target as Entity;
			(ent.getComponent(EntityComponent.BUFFS) as ECBuffHandler).addEntity(GameEngine.dataManager.createBuffFromTemplate(_buff));
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<AddBuffToTarget />");
			xml.@["id"] = _buff;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			_buff = xml.@["id"].toXMLString()
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["buff"] = _buff;
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_buff = transactionData["buff"] as String;
			super.rollbackTransaction();
		}
	}
}