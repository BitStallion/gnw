package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEventGetAttribute;
	import data.entityEvents.EntityEventOnDamage;
	import enums.*;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import flash.utils.flash_proxy;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	use namespace flash_proxy;
	public class ECAttributes extends EntityComponent 
	{
		private var _statsBase:Dictionary;
		private var _stats:Dictionary;
		private var _free:Number;
		
		public function ECAttributes() 
		{
			super();
			_statsBase = new Dictionary();
			_stats = new Dictionary();
			_free = 0;
			for each(var stat:CharacterStat in Utils.EnumList(CharacterStat))
			{
				_statsBase[stat.text] = 0;
			}
			for each(var dt:DamageType in Utils.EnumList(DamageType))
			{
				_statsBase[dt.text + "Resistance"] = 0;
			}
			for(var key:Object in _statsBase)
			{
				_stats[key] = _statsBase[key];
			}
		}
		
		override flash_proxy function callProperty(name:*, ...args):* 
		{
		}
		
		override flash_proxy function getProperty(name:*):*
		{
			var nameStr:String;
			if(name is QName || name is String)
			{
				if(name is QName)
				{
					nameStr = (name as QName).localName;
				}
				else
				{
					nameStr = name as String;
				}
				if(nameStr == "free") return _free;
				if(nameStr.substr(-4) == "Base")
				{
					nameStr = nameStr.substr(0, nameStr.length - 4);
					return _statsBase[nameStr];
				}
				else if(_stats[nameStr] != null)
				{
					var attr:EntityEventGetAttribute = new EntityEventGetAttribute();
					attr.attribute = nameStr;
					entity.dispatchEvent(attr);
					return attr.result;
				}
			}
			return null;
		}
		
		override flash_proxy function setProperty(name:*, value:*):void
		{
			var nameStr:String;
			var num:Number = parseFloat(value);
			if((name is QName || name is String) && !isNaN(num))
			{
				if(name is QName)
				{
					nameStr = (name as QName).localName; 
				}
				else
				{
					nameStr = name as String; 
				}
				if(nameStr == "free")
				{
					_free = num;
					return;
				}
				var dic:Dictionary;
				if(nameStr.substr(-4) == "Base")
				{
					dic = _statsBase;
					nameStr = nameStr.substr(0, nameStr.length - 4);
				}
				else
				{
					dic = _stats;
				}
				dic[nameStr] = num;
			}
		}
		
		public override function destroy():void
		{
			super.destroy();
			_stats = null;
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.resetToBase, resetToBase);
			entity.addEventListener(EntityEvent.applyEffects, applyEffects, false, -1);
			entity.addEventListener(EntityEvent.getAttribute, getAttribute);
			entity.addEventListener(EntityEvent.onReceiveDamage, onReceiveDamage);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.resetToBase, resetToBase);
			entity.removeEventListener(EntityEvent.applyEffects, applyEffects);
			entity.removeEventListener(EntityEvent.getAttribute, getAttribute);
			entity.removeEventListener(EntityEvent.onReceiveDamage, onReceiveDamage);
		}
		
		/*public function setStat(name:String, value:Number):void
		{
			_stats[name] = value;
		}
		
		public function addStat(name:String, value:Number):void
		{
			_stats[name] += value;
		}
		
		public function getStat(name:String):Number
		{
			return _stats[name];
		}*/
		
		public override function setVariable(path:Array, value:String):void
		{
			if(path.length == 0) return;
			var name:String = path.shift() as String;
			
		}
		
		public override function getVariable(path:Array):String
		{
			return null;
		}
		
		public function resetToBase(event:Event):void
		{
			_stats = new Dictionary();
			for(var key:Object in _statsBase)
			{
				_stats[key] = _statsBase[key];
			}
		}
		
		public function applyEffects(event:Event):void
		{
			var end:EntityEventGetAttribute = new EntityEventGetAttribute();
			end.attribute = CharacterStat.endurance.text;
			entity.dispatchEvent(end);
			try
			{
				var juice:ECDynamicNumber = entity.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
				juice.max += Math.floor(end.result * 5);
			}
			catch(e:GnWError)
			{
			}
			try
			{
				var vigor:ECDynamicNumber = entity.getComponent(EntityComponent.VIGOR) as ECDynamicNumber;
				vigor.max += Math.floor(end.result * 2.5);
			}
			catch(e:GnWError)
			{
			}
		}
		
		public function getAttribute(event:Event):void
		{
			if(event is EntityEventGetAttribute)
			{
				var getAttr:EntityEventGetAttribute = event as EntityEventGetAttribute;
				getAttr.value += _stats[getAttr.attribute] || 0;
				switch(getAttr.attribute)
				{
					case CharacterStat.strength.text:
					{
						switch(entity.variables["bodyType"])
						{
							case BodyType.Thin.text:
							{
								getAttr.multiplier -= 0.05;
								break;
							}
							case BodyType.Muscular.text:
							case BodyType.Husky.text:
							{
								getAttr.multiplier += 0.05;
								break;
							}
						}
						break;
					}
					case CharacterStat.endurance.text:
					{
						switch(entity.variables["bodyType"])
						{
							case BodyType.Chubby.text:
							case BodyType.Husky.text:
							{
								getAttr.multiplier += 0.05;
								break;
							}
						}
						break;
					}
					case CharacterStat.agility.text:
					{
						switch(entity.variables["bodyType"])
						{
							case BodyType.Thin.text:
							{
								getAttr.multiplier += 0.05;
								break;
							}
							case BodyType.Muscular.text:
							{
								getAttr.multiplier -= 0.05;
								break;
							}
							case BodyType.Husky.text:
							{
								getAttr.multiplier -= 0.1;
								break;
							}
						}
						break;
					}
				}
			}
		}
		
		public function onReceiveDamage(event:Event):void
		{
			if(event is EntityEventOnDamage)
			{
				var damageEvent:EntityEventOnDamage = event as EntityEventOnDamage;
				if(damageEvent.damageType != damageEvent.damageType.group) damageEvent.damageResistance += _stats[damageEvent.damageType.text + "Resistance"] || 0;
				damageEvent.groupResistance += _stats[damageEvent.damageType.group.text + "Resistance"] || 0;
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			var statXML:XML;
			xml.@["type"] = "ECAttributes";
			xml.@["free"] = _free;
			for each(var stat:CharacterStat in Utils.EnumList(CharacterStat))
			{
				statXML = new XML("<" + stat.text + " />");
				statXML.@["value"] = _statsBase[stat.text];
				xml.appendChild(statXML);
			}
			for each(var dt:DamageType in Utils.EnumList(DamageType))
			{
				statXML = new XML("<" + dt.text + "Resistance" + " />");
				statXML.@["value"] = _statsBase[dt.text + "Resistance"];
				xml.appendChild(statXML);
			}
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			_free = parseFloat(xml.@["free"].toXMLString()) || 0;
			for each(var stat:CharacterStat in Utils.EnumList(CharacterStat))
			{
				_stats[stat.text] = _statsBase[stat.text] = parseFloat(xml[stat.text].@["value"]) || 0;
			}
			for each(var dt:DamageType in Utils.EnumList(DamageType))
			{
				_stats[dt.text + "Resistance"] = _statsBase[dt.text + "Resistance"] = parseFloat(xml[dt.text + "Resistance"].@["value"]) || 0;
			}
		}
		
		override public function startTransaction():void
		{
			if(transactionData != null) return;
			super.startTransaction();
			var dic:Dictionary = new Dictionary();
			var key:Object;
			for(key in _statsBase)
			{
				dic[key] = _statsBase[key];
			}
			transactionData["statsBase"] = dic;
			dic = new Dictionary();
			for(key in _stats)
			{
				dic[key] = _stats[key];
			}
			transactionData["stats"] = dic;
			transactionData["free"] = _free;
		}
		
		override public function rollbackTransaction():void
		{
			if(transactionData == null) return;
			_statsBase = transactionData["statsBase"] as Dictionary;
			_stats = transactionData["stats"] as Dictionary;
			_free = transactionData["free"] as Number;
			super.rollbackTransaction();
		}
	}
}