package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	
	/**
	 * Used to store data in an array for an entity
	 * 
	 * @author TheWrongHands
	 */
	public class ECArray extends EntityComponent 
	{
		private var _dataBase:Array;
		private var _data:Array;
		
		public function ECArray() 
		{
			super();
			_data = new Array();
			_dataBase = new Array();
		}
		
		public override function destroy():void
		{
			super.destroy();
			_dataBase = null;
			_data = null;
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.resetToBase, resetToBase);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.resetToBase, resetToBase);
		}
		
		public function get dataBase():Array
		{
			return _dataBase;
		}
		
		public function get data():Array
		{
			return _data;
		}
		
		public function resetToBase(event:Event):void
		{
			_data = new Array();
			for each(var val:String in _dataBase)
			{
				_data.push(val);
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component></component>");
			xml.@["type"] = "ECArray";
			for each(var val:String in _dataBase)
			{
				var valXML:XML = new XML("<base />"); 
				valXML.@["value"] = val;
				xml.appendChild(valXML);
			}
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			for each(var baseXML:XML in xml.base)
			{
				if(baseXML.attribute("value").toXMLString().length > 0)
				{
					_dataBase.push(baseXML.attribute("value").toXMLString());
				}
			}
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["dataBase"] = _dataBase.slice();
			transactionData["data"] = _data.slice();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_dataBase = transactionData["dataBase"] as Array;
			_data = transactionData["data"] as Array;
			super.rollbackTransaction();
		}
	}
}