package data.EntityComponents 
{
	import data.EntityComponent;
	import data.EntityComponents.EventConditional.EventConditionalAnd;
	import data.EventAction;
	import flash.events.Event;
	import flash.utils.getDefinitionByName
	
	/**
	 * This is a flexible component that will allow a set of actions to occur if certain conditions are met during an event
	 * 
	 * @author TheWrongHands
	 */
	public class ECOnEvent extends EntityComponent 
	{
		private var _eventID:String;
		private var _priority:int;
		private var _and:EventConditionalAnd;
		private var _actions:Vector.<EventAction>
		
		public function ECOnEvent() 
		{
			super();
			_priority = 0;
			_and = new EventConditionalAnd();
			_actions = new Vector.<EventAction>();
		}
		
		override public function destroy():void
		{
			_and.destroy();
			_and = null;
			super.destroy();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(_eventID, onEvent, false, _priority);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(_eventID, onEvent);
		}
		
		public function onEvent(event:Event):void
		{
			if(_and.eventMeetsCondition(event))
			{
				for each(var action:EventAction in _actions)
				{
					action.action(event);
				}
			}
		}
		
		override public function toXML():XML
		{
			var xml:XML = new XML("<component></component>");
			xml.@["type"] = "ECOnEvent";
			xml.@["event"] = _eventID;
			if(_priority != 0)
			{
				if(_priority == int.MAX_VALUE)
				{
					xml.@["priority"] = "first";
				}
				else if(_priority == int.MIN_VALUE)
				{
					xml.@["priority"] = "last";
				}
				else
				{
					xml.@["priority"] = _priority.toString();
				}
			}
			var groupXML:XML = _and.toXML();
			groupXML.setName("conditions");
			xml.appendChild(groupXML);
			groupXML = new XML("<actions></actions>");
			for each(var act:EventAction in _actions)
			{
				groupXML.appendChild(act.toXML());
			}
			xml.appendChild(groupXML);
			return xml;
		}
		
		override public function fromXML(xml:XML):void
		{
			_eventID = xml.@["event"];
			var priority:String = xml.@["priority"];
			if(priority.length > 0)
			{
				if(priority == "first")
				{
					_priority = int.MAX_VALUE;
				}
				else if(priority == "last")
				{
					_priority = int.MIN_VALUE;
				}
				else
				{
					_priority = parseInt(priority) || 0;
				}
			}
			else
			{
				_priority = 0;
			}
			var groupXML:XML = xml["conditions"][0];
			var entryXML:XML;
			if(groupXML != null)
			{
				_and.fromXML(groupXML);
			}
			groupXML = xml.child("actions")[0];
			if(groupXML != null)
			{
				for each(entryXML in groupXML.children())
				{
					var cls:Class = getDefinitionByName("data.EntityComponents.EventAction.EventAction" + entryXML.name()) as Class;
					var act:EventAction = new cls();
					act.fromXML(entryXML);
					_actions.push(act);
				}
			}
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["eventID"] = _eventID;
			transactionData["priority"] = _priority;
			_and.startTransaction();
			transactionData["and"] = _and;
			for each(var act:EventAction in _actions)
			{
				act.startTransaction();
			}
			transactionData["actions"] = _actions.slice();
		}
		
		override public function commitTransaction():void 
		{
			if(transactionData == null) return;
			_and.commitTransaction();
			for each(var act:EventAction in _actions)
			{
				act.commitTransaction();
			}
			super.commitTransaction();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_eventID = transactionData["eventID"] as String;
			_priority = transactionData["priority"] as int;
			_and = transactionData["and"] as EventConditionalAnd;
			_and.rollbackTransaction();
			var acts:Vector.<EventAction> = transactionData["actions"] as Vector.<EventAction>;
			var act:EventAction;
			for each(act in _actions)
			{
				if(acts.indexOf(act) < 0)
				{
					act.destroy();
				}
			}
			_actions = acts;
			for each(act in _actions)
			{
				act.rollbackTransaction();
			}
			super.rollbackTransaction();
		}
	}
}