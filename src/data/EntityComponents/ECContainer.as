package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import utility.GnWError;
	
	/**
	 * This class gives entities an inventory that can be used to store other entities such as items
	 * 
	 * @author TheWrongHands
	 */
	public class ECContainer extends EntityComponent 
	{
		protected var _entities:Dictionary;
		
		public function ECContainer() 
		{
			super();
			_entities = new Dictionary();
		}
		
		public override function destroy():void
		{
			for each(var ent:Entity in _entities)
			{
				ent.destroy();
			}
			_entities = null;
			super.destroy();
		}
		
		public function clear():void 
		{
			for each(var ent:Entity in _entities)
			{
				removeEntity(ent);
				ent.destroy();
			}
		}
		
		public function addEntity(ent:Entity):void
		{
			if(containsEntity(ent)) return;
			ent.container = this;
			_entities[ent.uuid] = ent;
			var event:EntityEvent = new EntityEvent(EntityEvent.addEntity, false);
			event.target = entity;
			ent.dispatchEvent(event);
		}
		
		public function addEntities(list:Vector.<Entity>):void
		{
			for (var i:int = 0; i < list.length; i++)
			{
				addEntity(list[i]);
			}
		}
		
		public function getEntityByUUID(uuid:String):Entity
		{
			return _entities[uuid];
			//throw new GnWError("No entity with the UUID: " + uuid);
		}
		
		public function containsEntity(ent:Entity):Boolean
		{
			return _entities[ent.uuid] != null;
		}
		
		public function containsEntityWithID(id:String):Boolean
		{
			for each(var ent:Entity in _entities)
			{
				if(ent.id == id) return true;
			}
			return false;
		}
		
		public function removeEntity(ent:Entity):void
		{
			if(!containsEntity(ent)) return;
			var event:EntityEvent = new EntityEvent(EntityEvent.removeEntity, false);
			event.target = entity;
			ent.dispatchEvent(event);
			ent.container = null;
			delete _entities[ent.uuid];
		}
		
		public function allEntities():Vector.<Entity>
		{
			var ents:Vector.<Entity> = new Vector.<Entity>();
			for each(var ent:Entity in _entities)
			{
				ents.push(ent);
			}
			return ents;
		}
		
		public function allEntitiesWithTag(tag:String):Vector.<Entity>
		{
			var ents:Vector.<Entity> = new Vector.<Entity>();
			for each(var ent:Entity in _entities)
			{
				if(ent.hasTag(tag))
				{
					ents.push(ent);
				}
			}
			return ents;
		}
		
		public function allEntitiesWithoutTag(tag:String):Vector.<Entity>
		{
			var ents:Vector.<Entity> = new Vector.<Entity>();
			for each(var ent:Entity in _entities)
			{
				if(!ent.hasTag(tag))
				{
					ents.push(ent);
				}
			}
			return ents;
		}
		
		public function allEntitiesWithTags(tags:Object):Vector.<Entity>
		{
			var ents:Vector.<Entity> = new Vector.<Entity>();
			var shouldAdd:Boolean;
			for each(var ent:Entity in _entities)
			{
				shouldAdd = true;
				for each(var tag:String in tags)
				{
					if(!ent.hasTag(tag))
					{
						shouldAdd = false;
						break;
					}
				}
				if(shouldAdd) ents.push(ent);
			}
			return ents;
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component></component>");
			var entitiesXML:XML = new XML("<entities></entities>");
			xml.@["type"] = "ECContainer";
			for each(var ent:Entity in _entities)
			{
				entitiesXML.appendChild(ent.toXML());
			}
			xml.appendChild(entitiesXML);
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			var groupXML:XML = xml.child("entities")[0];
			var entryXML:XML;
			if(groupXML != null)
			{
				for each(entryXML in groupXML.child("entity"))
				{
					var ent:Entity = new Entity(entryXML.@["id"].toXMLString());
					ent.fromXML(entryXML);
					addEntity(ent);
				}
			}
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			var dic:Dictionary = new Dictionary();
			var key:Object;
			for(key in _entities)
			{
				var ent:Entity = _entities[key];
				ent.startTransaction();
				dic[key] = ent;
			}
			transactionData["entities"] = dic;
		}
		
		override public function commitTransaction():void 
		{
			if(transactionData == null) return;
			for each(var ent:Entity in _entities)
			{
				ent.commitTransaction();
			}
			super.commitTransaction();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			var dic:Dictionary = transactionData["entities"] as Dictionary;
			var ent:Entity;
			for(var key:Object in _entities)
			{
				ent = _entities[key];
				var deleteComp:Boolean = true;
				for each(var ent1:Entity in dic)
				{
					if(ent == ent1)
					{
						deleteComp = false;
						break;
					}
				}
				if(deleteComp)
				{
					removeEntity(ent);
					ent.destroy();
				}
			}
			_entities = dic;
			for each(ent in _entities)
			{
				ent.rollbackTransaction();
			}
			super.rollbackTransaction();
		}
	}
}