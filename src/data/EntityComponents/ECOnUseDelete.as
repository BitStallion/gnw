package data.EntityComponents 
{
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECOnUseDelete extends EntityComponent 
	{
		public function ECOnUseDelete() 
		{
			super();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.onUse, onUse, false, int.MIN_VALUE);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.onUse, onUse);
		}
		
		public function onUse(event:Event):void
		{
			if(entity != null)
			{
				entity.destroy();
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECOnUseDelete";
			return xml;
		}
	}
}