package data.EntityComponents 
{
	import data.EntityComponent;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECXMLHolder extends EntityComponent 
	{
		private var _xml:XML;
		
		public function ECXMLHolder() 
		{
			super();
		}
		
		public override function destroy():void
		{
			super.destroy();
			_xml = null;
		}
		
		public function set xml(xml:XML):void
		{
			_xml = xml;
		}
		
		public function get xml():XML
		{
			return _xml;
		}
		
		public override function toXML():XML
		{
			if(_xml != null) return _xml.copy();
			var xml:XML = new XML("<component></component>");
			xml.@["type"] = "ECXMLHolder";
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			_xml = xml.copy();
		}
		
		override public function startTransaction():void 
		{
			if(transactionData != null) return;
			super.startTransaction();
			transactionData["xml"] = _xml.copy();
		}
		
		override public function rollbackTransaction():void 
		{
			if(transactionData == null) return;
			_xml = transactionData["xml"] as XML;
			super.rollbackTransaction();
		}
	}

}