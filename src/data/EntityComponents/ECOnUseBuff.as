package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECOnUseBuff extends EntityComponent 
	{
		private var _buffIDs:Vector.<Object>;
		
		public function ECOnUseBuff() 
		{
			super();
			_buffIDs = new Vector.<Object>();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.onUse, onUse);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.onUse, onUse);
		}
		
		public function onUse(event:Event):void
		{
			if(event.target is Entity)
			{
				var ent:Entity = event.target as Entity;
				var buffs:ECBuffHandler = ent.getComponent(EntityComponent.BUFFS) as ECBuffHandler;
				for each(var buff:Object in _buffIDs)
				{
					if(Math.random() < (parseFloat(buff["chance"]) || 1.0)) buffs.addEntity(GameEngine.dataManager.createBuffFromTemplate(buff["id"]));
				}
			}
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component></component>");
			xml.@["type"] = "ECBuffOnUse";
			for each(var buff:Object in _buffIDs)
			{
				var entry:XML = new XML("<buff />");
				entry.@["id"] = buff["id"];
				xml.appendChild(entry);
			}
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			for each(var entryXML:XML in xml.child("buff"))
			{
				if(entryXML.attribute("id").toXMLString().length > 0)
				{
					var buff:Object = {};
					buff["id"] = entryXML.attribute("id").toXMLString();
					buff["chance"] = entryXML.attribute("chance").toXMLString();
					_buffIDs.push(buff);
				}
			}
		}
	}
}