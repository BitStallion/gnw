package data.EntityComponents 
{
	import data.Entity;
	import data.EntityComponent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class ECStatusModifier extends EntityComponent 
	{
		public var variable:String;
		public var value:String;
		
		public function ECStatusModifier() 
		{
			super();
		}
		
		public override function onAdd():void
		{
			entity.addEventListener(EntityEvent.applyEffects, applyEffects);
		}
		
		public override function onRemove():void
		{
			entity.removeEventListener(EntityEvent.applyEffects, applyEffects);
		}
		
		public function applyEffects(event:Event):void
		{
			var ent:Entity = event.target as Entity;
			ent.setVariable(variable, value);
		}
		
		public override function toXML():XML
		{
			var xml:XML = new XML("<component />");
			xml.@["type"] = "ECStatusModifier";
			xml.@["variable"] = variable;
			xml.@["value"] = value;
			return xml;
		}
		
		public override function fromXML(xml:XML):void
		{
			variable = xml.@["variable"].toXMLString()
			value = xml.@["value"].toXMLString();
		}
	}
}