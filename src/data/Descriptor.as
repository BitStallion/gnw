package data
{
	import mx.utils.StringUtil;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Descriptor implements IXMLDataLoader
	{
		private var _parts:Object;
		private var _generics:Object;
		
		function Descriptor()
		{
		}
		
		public function startLoading():void
		{
			_parts = new Object();
			_generics = new Object();
		}
		
		public function canHandleXMLType(name:String):Boolean
		{
			return name == "descriptors";
		}
		
		public function readXMLData(file:String, xml:XML):void
		{
			if(xml.name() == "descriptors")
			{
				loadXMLDescriptors(xml);
			}
		}
		
		/**
		 * Parses the descriptions XML document and populates the _parts and _generics objects with the extracted values.
		 */
		private function loadXMLDescriptors(descSet:XML):void
		{
			for each(var part:XML in descSet.*)
			{
				var partName:String = part.name().toString();
				var values:Array;
				var i:int;
				
				if(partName == "generic")
				{
					for each(var generic:XML in part.*)
					{
						var genericName:String = generic.name().toString();
						values = generic.*.toString().split(",");
						for(i = 0; i < values.length; i++)
							values[i] = StringUtil.trim(values[i]);
						_generics[genericName] = values;
					}
					
					continue;
				}
				
				_parts[partName] = new Object();
				for each(var subPart:XML in part.*)
				{
					var subPartName:String = subPart.name().toString();
					_parts[partName][subPartName] = new Object();
					
					for each(var type:XML in subPart.*)
					{
						var typeName:String = type.name();
						_parts[partName][subPartName]["type"] = typeName;
						
						if(typeName == "size" || typeName == "type")
						{
							values = type.*.toString().split(",");
							for(i = 0; i < values.length; i++)
								values[i] = StringUtil.trim(values[i]);
							
							_parts[partName][subPartName][type.@value] = values;
						}
						else if(typeName == "set")
						{
							
							_parts[partName][subPartName][type.@key] = new Object();
							for each(var value:XML in type.*)
							{
								values = value.*.toString().split(",");
								for(i = 0; i < values.length; i++)
									values[i] = StringUtil.trim(values[i]);
								
								_parts[partName][subPartName][type.@key][value.@key] = values;
							}
						}
					}
				}
			}
		}
		
		/**
		 * Gets a word or set of words that are randomly selected from the set of descriptions specified by the parameters.
		 *
		 * @param	part	The body part name.
		 * @param	subPart	The subpart name.
		 * @param	type	The type to get the word for.
		 * @return	A word or set of words describing the type of body part.
		 */
		public function getTypeDescription(part:String, subPart:String, type:String):String
		{
			var list:Array = (_parts[part][subPart][type] as Array);
			
			return returnRandom(list);
		}
		
		public function getNumericDescription(part:String, subPart:String, value:Number):String
		{
			var obj:Object = _parts[part][subPart];
			
			if(obj == null)
				return "";
			
			var min:int = int.MAX_VALUE;
			var max:int = 0;
			
			for(var key:String in obj)
			{
				var keyVal:int = parseFloat(key);
				
				if(isNaN(keyVal))
					continue;
				
				if(keyVal > max)
					max = keyVal;
				
				if(keyVal < min && keyVal >= value)
					min = keyVal;
			}
			
			if(min == int.MAX_VALUE)
				min = max;
			
			var list:Array = (_parts[part][subPart][min.toString()] as Array);
			
			return returnRandom(list);
		}
		
		public function getPairedValueDescription(part:String, subPart:String, key1:String, key2:String):String
		{
			var list:Array = (_parts[part][subPart][key1][key2] as Array);
			
			return returnRandom(list);
		}
		
		/**
		 * Gets a random word for a given type of body part.
		 *
		 * @return	A single word.
		 */
		public function getGeneric(type:String):String
		{
			var list:Array = (_generics[type] as Array);
			
			return returnRandom(list);
		}
		
		/**
		 * Gets a list containing the names of all generic word sets.
		 */
		public function get generics():Array
		{
			var list:Array = new Array();
			
			for(var key:String in _generics)
			{
				list.push(key);
			}
			
			return list;
		}
		
		private function returnRandom(list:Array):String
		{
			var val:int = Math.floor(Math.random() * list.length);
			return list[val] as String;
		}
	}

}