package data 
{
	import events.Actor;
	import flash.utils.Dictionary;
	import system.GameWorld;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ParsingDataContextBuilder 
	{
		private var _world:GameWorld;
		private var _characters:Object;
		
		public function ParsingDataContextBuilder(world:GameWorld) 
		{
			_world = world;
			_characters = new Object();
		}
		
		/**
		 * Adds a new non-player character to the data context.
		 * 
		 * @param	character	The character data.
		 * @param	label		The label the character is refered to by in the parser.
		 */
		public function addCharacter(character:Entity, label:String):void
		{
			_characters[label] = character;
		}
		
		public function buildContext():Object
		{
			var context:Object = new Object();
			
			if (_world != null)
			{
				context["player"] = _world.player.toParsingContext();
				context["calendar"] = _world.gameTime.toDataContext();
				context["vars"] = _world.variables;
			}
			
			for (var key:String in _characters)
			{
				context[key] = (_characters[key] as Entity).toDataContext();
			}
			
			for each (var generic:String in GameEngine.descriptor.generics)
			{
				context[generic] = { toString: buildGenericFunction(generic) };
			}
			
			return context;
		}
		
		/**
		 * This function returns a function that gets a random word for the passed generic body part type.
		 * (Used to deal with the fact that as3 doesn't do closures)
		 * 
		 * @param	type	The body part name
		 * @return	A function that returns a random word for the passed type	
		 */
		private function buildGenericFunction(type:String):Function
		{
			return function():String { return GameEngine.descriptor.getGeneric(type); }
		}
		
	}

}