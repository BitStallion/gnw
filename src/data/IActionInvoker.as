package data 
{
	import enums.TriggerType;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public interface IActionInvoker 
	{
		function addAction(action:Action):void;
		
		function invokeActions(type:TriggerType):void;
		
	}
	
}