package data 
{
	import data.EntityComponents.EventConditional.*;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import utility.IDestroyable;
	
	/**
	 * This is the base class for all conditions to be checked when and entity event occurs
	 * 
	 * @author TheWrongHands
	 */
	public class EventConditional implements IDestroyable, IXMLSerializable, ITransactionable
	{
		/* This allows for these classes to be dynamically loaded in later as that by default AS3 tries to be efficient by not
		 * compiling classes that aren't explicitly mentioned somewhere in the code
		 */
		EventConditionalAnd;
		EventConditionalNot;
		EventConditionalTargetHasBuff;
		
		private var _transactionData:Dictionary;
		
		public function EventConditional()
		{
			_transactionData = null;
		}
		
		public function destroy():void
		{
		}
		
		public function eventMeetsCondition(event:Event):Boolean
		{
			return true;
		}
		
		public function toXML():XML 
		{
			return null;
		}
		
		public function fromXML(xml:XML):void 
		{
		}
		
		protected function get transactionData():Dictionary
		{
			return _transactionData;
		}
		
		public function startTransaction():void 
		{
			if(_transactionData != null) return;
			_transactionData = new Dictionary();
		}
		
		public function commitTransaction():void 
		{
			if(_transactionData == null) return;
			_transactionData = null;
		}
		
		public function rollbackTransaction():void 
		{
			if(_transactionData == null) return;
			_transactionData = null;
		}
		
		public function isTransactionOccurring():Boolean 
		{
			return _transactionData != null;
		}
	}
}