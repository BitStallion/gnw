package data 
{
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public interface IParsingContextable 
	{
		function toParsingContext():Object;
	}
	
}