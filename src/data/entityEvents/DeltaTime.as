package data.entityEvents
{
	import data.BaseEvent;
	import data.entityEvents.EntityEvent;
	import flash.events.Event;
	
	/**
	 * This event occurs when time advances
	 *
	 * @author TheWrongHands
	 */
	public class DeltaTime extends EntityEvent
	{
		private var _deltaTime:Number;
		
		public function DeltaTime(type:String = EntityEvent.deltaTime, bubbles:Boolean = true, cancelable:Boolean = false, delta:DeltaTime = null)
		{
			super(type, bubbles, cancelable, delta);
			if(delta == null)
			{
				deltaTime = 1;
			}
		}
		
		public function get deltaTime():Number
		{
			if(_original != null)
			{
				return(_original as DeltaTime).deltaTime;
			}
			return _deltaTime;
		}
		
		public function set deltaTime(num:Number):void
		{
			if(_original != null)
			{
				(_original as DeltaTime).deltaTime = num;
			}
			else
			{
				_deltaTime = num;
			}
		}
		
		override public function clone():Event
		{
			return new DeltaTime(type, bubbles, cancelable, _original as DeltaTime || this);
		}
	}
}