package data.entityEvents 
{
	import data.Entity;
	import enums.DamageType;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class EntityEventOnDamage extends EntityEvent 
	{
		private var _attacker:Entity;
		private var _damage:Number
		private var _damageType:DamageType;
		private var _damageMut:Number;
		private var _damageAdd:Number;
		private var _groupResistance:Number;
		private var _damageResistance:Number;
		
		public function EntityEventOnDamage(type:String = EntityEvent.onReceiveDamage, bubbles:Boolean = true, cancelable:Boolean = false, origin:EntityEventOnDamage = null)
		{
			super(type, bubbles, cancelable, origin);
			_attacker = null;
			_damage = 0;
			_damageType = null;
			_damageMut = 1;
			_damageAdd = 0;
			_groupResistance = 0;
			_damageResistance = 0;
		}
		
		public function set attacker(value:Entity):void
		{
			if(_original != null)
			{
				(_original as EntityEventOnDamage).attacker = value;
				return;
			}
			_attacker = value;
		}
		
		public function get attacker():Entity
		{
			if(_original != null) return (_original as EntityEventOnDamage).attacker;
			return _attacker;
		}
		
		public function set damage(value:Number):void
		{
			if(isNaN(value)) return;
			if(_original != null)
			{
				(_original as EntityEventOnDamage).damage = value;
				return;
			}
			_damage = value;
		}
		
		public function get damage():Number
		{
			if(_original != null) return (_original as EntityEventOnDamage).damage;
			return _damage;
		}
		
		public function set damageType(type:DamageType):void
		{
			if(_original != null)
			{
				(_original as EntityEventOnDamage).damageType = type;
				return;
			}
			_damageType = type;
		}
		
		public function get damageType():DamageType
		{
			if(_original != null) return (_original as EntityEventOnDamage).damageType;
			return _damageType;
		}
		
		public function set damageMut(value:Number):void
		{
			if(isNaN(value)) return;
			if(_original != null)
			{
				(_original as EntityEventOnDamage).damageMut = value;
				return;
			}
			_damageMut = value;
		}
		
		public function get damageMut():Number
		{
			if(_original != null) return (_original as EntityEventOnDamage).damageMut;
			return _damageMut;
		}
		
		public function set damageAdd(value:Number):void
		{
			if(isNaN(value)) return;
			if(_original != null)
			{
				(_original as EntityEventOnDamage).damageAdd = value;
				return;
			}
			_damageAdd = value;
		}
		
		public function get damageAdd():Number
		{
			if(_original != null) return (_original as EntityEventOnDamage).damageAdd;
			return _damageAdd;
		}
		
		public function set groupResistance(value:Number):void
		{
			if(isNaN(value))
			{
				return;
			}
			if(_original != null)
			{
				(_original as EntityEventOnDamage).groupResistance = value;
				return;
			}
			_groupResistance = value;
		}
		
		public function get groupResistance():Number
		{
			if(_original != null) return (_original as EntityEventOnDamage).groupResistance;
			return _groupResistance;
		}
		
		public function set damageResistance(value:Number):void
		{
			if(isNaN(value)) return;
			if(_original != null)
			{
				(_original as EntityEventOnDamage).damageResistance = value;
				return;
			}
			_damageResistance = value;
		}
		
		public function get damageResistance():Number
		{
			if(_original != null) return (_original as EntityEventOnDamage).damageResistance;
			return _damageResistance;
		}
		
		override public function clone():Event
		{
			return new EntityEventOnDamage(type, bubbles, cancelable, _original as EntityEventOnDamage || this);
		}
	}
}