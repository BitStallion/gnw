package data.entityEvents
{
	import data.BaseEvent;
	import flash.events.Event;
	
	/**
	 * This is an event specifically for entities
	 * It was originally meant to store flags to signal additional operations to be preformed after the event finished, but found a better way to do them
	 * 
	 * @author TheWrongHands
	 */
	public class EntityEvent extends BaseEvent
	{
		public static const addEntity:String = "EntityEvent:addEntity";				// Called when this entity is added to target
		public static const enterTile:String = "EntityEvent:enterTile";				// Called moves to a new tile
		public static const removeEntity:String = "EntityEvent:removeEntity";		// Called when this entity is removed from target
		public static const resetToBase:String = "EntityEvent:resetToBase";			// Called to reset all temporary stats
		public static const applyEffects:String = "EntityEvent:applyEffects";		// Called to apply temporary stats
		public static const deltaTime:String = "EntityEvent:deltaTime";				// Called to when time changes/increments
		public static const onUse:String = "EntityEvent:onUse"						// Called when this entity is used on target
		public static const levelUp:String = "EntityEvent:levelUp"					// Called when target levels up
		public static const onAttack:String = "EntityEvent:onAttack"				// Called when this entity is involved in attacking target in some way
		public static const onDealDamage:String = "EntityEvent:onDealDamage"		// Called when damage is being dealt to target (attackers prospective)
		public static const onReceiveDamage:String = "EntityEvent:onReceiveDamage"	// Called when damage is being dealt to target (defenders prospective)
		public static const getAttribute:String = "EntityEvent:getAttribute"		// Called when an attribute is needed
		
		public function EntityEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, origin:EntityEvent = null)
		{
			super(type, bubbles, cancelable, origin);
		}
		
		override public function clone():Event
		{
			return new EntityEvent(type, bubbles, cancelable, _original as EntityEvent || this);
		}
	}
}