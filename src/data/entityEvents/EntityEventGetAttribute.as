package data.entityEvents 
{
	import flash.events.Event;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class EntityEventGetAttribute extends EntityEvent 
	{
		private var _attribute:String;
		private var _value:Number;
		private var _mul:Number;
		private var _add:Number;
		
		public function EntityEventGetAttribute(type:String = EntityEvent.getAttribute, bubbles:Boolean = true, cancelable:Boolean = false, origin:EntityEventGetAttribute = null) 
		{
			super(type, bubbles, cancelable, origin);
			_attribute = null;
			_value = 0;
			_mul = 1;
			_add = 0;
		}
		
		public function set attribute(attr:String):void
		{
			if(_original != null)
			{
				(_original as EntityEventGetAttribute).attribute = attr;
				return;
			}
			_attribute = attr;
		}
		
		public function get attribute():String
		{
			if(_original != null) return (_original as EntityEventGetAttribute).attribute;
			return _attribute;
		}
		
		public function set value(value:Number):void
		{
			if(_original != null)
			{
				(_original as EntityEventGetAttribute).value = value;
				return;
			}
			_value = value;
		}
		
		public function get value():Number
		{
			if(_original != null) return (_original as EntityEventGetAttribute).value;
			return _value;
		}
		
		public function set multiplier(value:Number):void
		{
			if(_original != null)
			{
				(_original as EntityEventGetAttribute).multiplier = value;
				return;
			}
			_mul = value;
		}
		
		public function get multiplier():Number
		{
			if(_original != null) return (_original as EntityEventGetAttribute).multiplier;
			return _mul;
		}
		
		public function set additive(value:Number):void
		{
			if(_original != null)
			{
				(_original as EntityEventGetAttribute).additive = value;
				return;
			}
			_add = value;
		}
		
		public function get additive():Number
		{
			if(_original != null) return (_original as EntityEventGetAttribute).additive;
			return _add;
		}
		
		public function get result():Number
		{
			return _value * _mul + _add;
		}
		
		override public function clone():Event
		{
			return new EntityEvent(type, bubbles, cancelable, _original as EntityEventGetAttribute || this);
		}
	}
}