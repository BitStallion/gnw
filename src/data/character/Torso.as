package data.character 
{
	import data.Descriptor;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Torso
	{
		private var _height:Number;
		private var _fatness:Number;
		private var _muscularity:Number;
		
		public function Torso(xmlData:XML) 
		{
			_height = parseFloat(xmlData.@height);
			_fatness = parseFloat(xmlData.@fatness);
			_muscularity = parseFloat(xmlData.@muscularity);
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function get fatness():Number 
		{
			return _fatness;
		}
		
		public function get muscularity():Number 
		{
			return _muscularity;
		}
		
		public function get tone():String
		{
			return GameEngine.descriptor.getPairedValueDescription("torso", "tone", _muscularity.toString(), _fatness.toString());
		}
		
	}

}