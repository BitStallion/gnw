package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class LowerBody
	{
		private var _type:String;
		private var _fatness:Number;
		private var _muscularity:Number;
		private var _height:Number;
		
		public function LowerBody(xmlData:XML) 
		{
			_type = xmlData.@type;
			_height = parseFloat(xmlData.@height);
			_fatness = parseFloat(xmlData.@fatness);
			_muscularity = parseFloat(xmlData.@muscularity);
		}
		
		public function get type():String 
		{
			return GameEngine.descriptor.getTypeDescription("lowerbody", "type", _type );
		}
		
		public function get fatness():Number 
		{
			return _fatness;
		}
		
		public function get muscularity():Number 
		{
			return _muscularity;
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function get tone():String
		{
			return GameEngine.descriptor.getPairedValueDescription("lowerbody", "tone", _muscularity.toString(), _fatness.toString());
		}
		
	}

}