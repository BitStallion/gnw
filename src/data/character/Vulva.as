package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Vulva
	{
		private var _type:String;
		private var _clitSize:Number;
		private var _vaginaSize:Number;
			
		public function Vulva(xmlData:XML) 
		{
			_type = xmlData.@type;
			_clitSize = parseFloat(xmlData.@clitSize);
			_vaginaSize = parseFloat(xmlData.@vaginaSize);
		}
		
		public function get vaginaSize():String
		{
			return GameEngine.descriptor.getNumericDescription("vulva", "vaginaSize", _vaginaSize);
		}
		
		public function get vaginaSizeValue():Number
		{
			return _vaginaSize;
		}
		
		public function get clitSize():String 
		{
			return GameEngine.descriptor.getNumericDescription("vulva", "clitSize", _clitSize);
		}
		
		public function get type():String
		{
			return _type;
		}
		
	}

}