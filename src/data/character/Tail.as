package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Tail
	{
		private var _type:String;
		private var _length:Number;
		
		public function Tail(xmlData:XML) 
		{
			_type = xmlData.@type;
			_length = parseFloat(xmlData.@length);
		}
		
		public function get type():String 
		{
			return GameEngine.descriptor.getTypeDescription("tail", "type", _type);
		}
		
		public function get length():Number 
		{
			return _length;
		}
		
	}

}