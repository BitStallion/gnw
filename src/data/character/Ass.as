package data.character 
{
	import data.Descriptor;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Ass
	{
		private var _anusSize:Number;
		private var _fatness:Number;
		private var _muscularity:Number;
		
		public function Ass(xmlData:XML) 
		{
			_anusSize = parseFloat(xmlData.@anusSize);
			_fatness = parseFloat(xmlData.@fatness);
			_muscularity = parseFloat(xmlData.@muscularity);
		}
		
		public function get anusSize():String
		{
			return GameEngine.descriptor.getNumericDescription("ass","anusSize", _anusSize);
		}
		
		public function get anusSizeValue():Number
		{
			return _anusSize;
		}
		
		public function get fatness():Number 
		{
			return _fatness;
		}
		
		public function get muscularity():Number 
		{
			return _muscularity;
		}
		
		public function get tone():String
		{
			return GameEngine.descriptor.getPairedValueDescription("ass", "tone", _muscularity.toString(), _fatness.toString());
		}
		
		public function get anus():String
		{
			return anusSize + " " + GameEngine.descriptor.getGeneric("anus");
		}
		
		public function toString():String
		{
			return tone + " " + GameEngine.descriptor.getGeneric("ass");
		}
		
	}

}