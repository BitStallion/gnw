package data.character 
{
	import data.Descriptor;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Breasts
	{
		private var _size:Number;
		private var _number:Number;
		private var _nippleSize:Number;
		private var _nippleType:String;
		private var _nippleColor:int;
		
		public function Breasts(xmlData:XML) 
		{
			_size = parseFloat(xmlData.@size);
			_number = parseFloat(xmlData.@number);
			_nippleSize = parseFloat(xmlData.@nippleSize);
			_nippleType = xmlData.@nippleType;
			_nippleColor = parseInt(xmlData.@nippleColor);
		}
		
		public function get size():String 
		{
			return GameEngine.descriptor.getNumericDescription("breasts", "size", _size);
		}
		
		public function get sizeValue():Number
		{
			return _size;
		}
		
		public function get number():Number 
		{
			return _number;
		}
		
		public function get nippleSize():String
		{
			return GameEngine.descriptor.getNumericDescription("breasts", "nippleSize",_nippleSize);
		}
		
		public function get nippleType():String 
		{
			return GameEngine.descriptor.getTypeDescription("breasts", "nippleType", _nippleType);
		}
		
		public function toString():String
		{
			return size + " " + GameEngine.descriptor.getGeneric("breasts");
		}
	}

}