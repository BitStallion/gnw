package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Wings
	{
		private var _type:String;
		private var _color:int;
		private var _size:Number;
		
		public function Wings(xmlData:XML) 
		{
			_type = xmlData.@type;
			_color = parseInt(xmlData.@color);
			_size = parseFloat(xmlData.@size);
		}
		
		public function get type():String 
		{
			return GameEngine.descriptor.getTypeDescription("wings", "type", _type);
		}
		
		public function get color():int 
		{
			return _color;
		}
		
		public function get size():String
		{
			return GameEngine.descriptor.getNumericDescription("wings", "size", _size);
		}
		
	}
}