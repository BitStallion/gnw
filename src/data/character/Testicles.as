package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Testicles
	{
		private var _number:Number;
		private var _size:Number;
		
		public function Testicles(xmlData:XML) 
		{
			_number = parseFloat(xmlData.@number);
			_size = parseFloat(xmlData.@size);
		}
		
		public function get number():Number 
		{
			return _number;
		}
		
		public function get size():String 
		{
			return GameEngine.descriptor.getNumericDescription("testicles", "size", _size);
		}
		
		public function get sizeValue():Number
		{
			return _size;
		}
		
	}

}