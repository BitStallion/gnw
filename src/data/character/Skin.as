package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Skin
	{
		private var _type:String;
		private var _color:int;
		
		public function Skin(xmlData:XML) 
		{
			_type = xmlData.@type;
			_color = parseInt(xmlData.@color);
		}
		
		public function get type():String 
		{
			return GameEngine.descriptor.getTypeDescription("skin", "type", _type);
		}
		
		public function get color():int 
		{
			return _color;
		}
		
	}

}