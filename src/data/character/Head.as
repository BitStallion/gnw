package data.character 
{
	import data.Descriptor;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Head
	{
		private var _eyeColor:int;
		private var _faceType:String;
		private var _earType:String;
		private var _height:Number;
		
		public function Head(xmlData:XML) 
		{
			_eyeColor = parseInt(xmlData.@eyeColor);
			_height = parseFloat(xmlData.@height);
			_faceType = xmlData.@faceType;
			_earType = xmlData.@earType;
		}
		
		public function get faceType():String 
		{
			return GameEngine.descriptor.getTypeDescription("head", "faceType", _faceType);
		}
		
		public function get earType():String 
		{
			return GameEngine.descriptor.getTypeDescription("head", "earType", _earType);
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
	}

}