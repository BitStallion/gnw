package data.character 
{
	import data.Descriptor;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Arms
	{
		private var _handType:String;
		private var _fatness:Number;
		private var _muscularity:Number;
		
		public function Arms(xmlData:XML) 
		{
			_handType = xmlData.@handType;
			_fatness = parseFloat(xmlData.@fatness);
			_muscularity = parseFloat(xmlData.@muscularity);
		}
		
		public function get handType():String 
		{
			return GameEngine.descriptor.getTypeDescription("arms","handtype", _handType );
		}
		
		public function get fatness():Number 
		{
			return _fatness;
		}
		
		public function get muscularity():Number 
		{
			return _muscularity;
		}
		
		public function get tone():String
		{
			return GameEngine.descriptor.getPairedValueDescription("arms", "tone", _muscularity.toString(), _fatness.toString());
		}
		
		public function toString():String
		{
			return tone + " " + "arms";
		}
		
	}

}