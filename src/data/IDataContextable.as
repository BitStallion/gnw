package data 
{
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public interface IDataContextable 
	{
		function toDataContext():Object;
	}
	
}