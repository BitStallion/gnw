package data 
{
	import data.EntityComponents.EventAction.*
	import flash.events.Event;
	import flash.utils.Dictionary;
	import utility.IDestroyable;
	
	/**
	 * This is the base class for actions that need to be done when and entity event occurs
	 * 
	 * @author TheWrongHands
	 */
	public class EventAction implements IDestroyable, IXMLSerializable, ITransactionable
	{
		/* This allows for these classes to be dynamically loaded in later as that by default AS3 tries to be efficient by not
		 * compiling classes that aren't explicitly mentioned somewhere in the code
		 */
		EventActionAddBuffToTarget;
		EventActionStatusModifier;
		
		private var _transactionData:Dictionary;
		
		public function EventAction()
		{
			_transactionData = null;
		}
		
		public function destroy():void
		{
		}
		
		public function action(event:Event):void
		{
		}
		
		public function toXML():XML 
		{
			return null;
		}
		
		public function fromXML(xml:XML):void 
		{
		}
		
		protected function get transactionData():Dictionary
		{
			return _transactionData;
		}
		
		public function startTransaction():void 
		{
			if(_transactionData != null) return;
			_transactionData = new Dictionary();
		}
		
		public function commitTransaction():void 
		{
			if(_transactionData == null) return;
			_transactionData = null;
		}
		
		public function rollbackTransaction():void 
		{
			if(_transactionData == null) return;
			_transactionData = null;
		}
		
		public function isTransactionOccurring():Boolean 
		{
			return _transactionData != null;
		}
	}
}