package data
{
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	
	/**
	 * This gets around some EventDispatcher weirdness and allow for a prosudo bubbling effect when the event is redispatched
	 *
	 * @author TheWrongHands
	 */
	public class BaseEvent extends Event
	{
		private var _target:Object;
		private var _currentTarget:Object;
		protected var _original:BaseEvent;
		
		public function BaseEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false, original:BaseEvent = null)
		{
			super(type, bubbles, cancelable);
			_target = null;
			_original = original;
		}
		
		public function set target(target:Object):void
		{
			_target = target;
		}
		
		override public function get target():Object
		{
			if(_target != null) return _target;
			if(_original != null) return _original.target;
			return super.target;
		}
		
		public function set currentTarget(currentTarget:Object):void
		{
			_currentTarget = currentTarget;
		}
		
		override public function get currentTarget():Object 
		{
			if(_currentTarget != null) return _currentTarget;
			if(_original != null) return _original.currentTarget;
			return super.currentTarget;
		}
		
		override public function clone():Event
		{
			return new BaseEvent(type, bubbles, cancelable, _original || this);
		}
	}
}