package data
{
	import flash.display.Bitmap;
	import flash.media.Sound;
	import flash.utils.Dictionary;
	import utility.GnWError;
	
	/**
	 * This is a centralized resource management system that will keep track of almost all external files in a virtual file path.
	 * This will make it much easier to incorporate a dynamic file loading system later.
	 *
	 * @author TheWrongHands
	 */
	public class FileResourceManager
	{
		private var _resources:Dictionary;
		private var _xmlDataLoaders:Array;
		
		public function FileResourceManager()
		{
			_resources = new Dictionary();
			_xmlDataLoaders = new Array();
			ResourceFile.loadResources(_resources);
		}
		
		public function addXMLDataLoader(loader:IXMLDataLoader):void
		{
			_xmlDataLoaders.push(loader);
		}
		
		public function loadXMLData():void
		{
			var loader:IXMLDataLoader;
			for each(loader in _xmlDataLoaders)
			{
				loader.startLoading();
			}
			for each(var file:String in getAllStartingWith("xml/"))
			{
				if(file.substring(file.length - 4) == ".xml")
				{
					var xmlFile:XML = new XML(getResource(file));
					for each(var xmlData:XML in xmlFile.*)
					{
						for each(loader in _xmlDataLoaders)
						{
							if(loader.canHandleXMLType(xmlData.name()))
							{
								loader.readXMLData(file, xmlData);
								break;
							}
						}
					}
				}
			}
		}
		
		/**
		 * Gets a new instance of a resource and returns it as an object.
		 * @param	path	The relative path to the resourse from the root resource folder.
		 * @return	A new instance of the resource.
		 */
		public function getResource(path:String):Object
		{
			var pathLower:String = processPath(path);
			if(_resources[pathLower] == null) throw new GnWError("File Not Found: \"" + path + "\"");
			var c:Class = (_resources[pathLower] as FileResource).c;
			return new c();
		}
		
		/**
		 * A helper method that returns the resource as a Bitmap.
		 * @param	path	The relative path to the image from the root resource folder.
		 * @return	The resource as a Bitmap.
		 */
		public function getImage(path:String):Bitmap
		{
			return getResource(path) as Bitmap;
		}
		
		/**
		 * A helper method that returns the resource as a Sound.
		 * @param	path	The relative path to the sound from the root resource folder.
		 * @return	The resource as a Sound.
		 */
		public function getSound(path:String):Sound
		{
			return getResource(path) as Sound;
		}
		
		/**
		 * Gets all resources whose path begin with the specified string.
		 * 
		 * @param	path	The search string.
		 * @param	type	The type of resources returned. Default is any.
		 * @return	An array containing all resources that meet the requirements. 
		 * 			Returns an empty array if no results are found.
		 */
		public function getAllStartingWith(path:String, type:String = "any"):Array
		{
			var pathLower:String = processPath(path);
			var list:Array = new Array();
			for(var key:String in _resources)
			{
				if(key.length >= pathLower.length)
				{
					var resource:FileResource = _resources[key] as FileResource;
					if((type == "any" || type == resource.type) && key.substring(0, pathLower.length) == pathLower)
					{
						list.push(key);
					}
				}
			}
			return list;
		}
		
		public function processPath(path:String):String
		{
			var pathArray:Array = path.toLowerCase().replace(new RegExp("\\", "g"), "/").split("/");
			var pathLower:String = new String();
			var i:int;
			for(i = 0; i < pathArray.length; i++)
			{
				if(pathArray[i] == "..")
				{
					if(i > 0)
					{
						pathArray.splice(i - 1, 2);
						i -= 2;
					}
					else
					{
						pathArray.splice(i, 1);
						i--;
					}
				}
			}
			for(i = 0; i < pathArray.length; i++)
			{
				if((pathArray[i] as String).length > 0)
				{
					if(pathLower.length > 0)
					{
						pathLower += "/";
					}
					pathLower += pathArray[i];
				}
			}
			return pathLower;
		}
	}
}