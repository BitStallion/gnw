package data 
{
	import enums.ActionType;
	import enums.TriggerType;
	import enums.Utils;
	import flash.utils.Dictionary;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import mx.utils.StringUtil;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Action 
	{
		private var _type:ActionType
		private var _trigger:TriggerType;
		private var _values:Object;
		
		public function Action(type:ActionType, trigger:TriggerType, values:Object) 
		{
			_type = type;
			_trigger = trigger;
			_values = values;
		}
		
		public static function buildAction(type:String, trigger:String, values:XMLList):Action
		{
			var aType:ActionType = Utils.Parse(ActionType, type) as ActionType;
			var trig:TriggerType = Utils.Parse(TriggerType, trigger) as TriggerType;
			var valueList:Dictionary = new Dictionary();
			
			for each (var value:XML in values)
			{
				valueList[value.@key.toString()] = value.@value.toString();
			}
			
			return new Action(aType, trig, valueList);
		}
		
		public function get type():ActionType 
		{
			return _type;
		}
		
		public function get values():Object
		{
			return _values;
		}
		
		public function get trigger():TriggerType
		{
			return _trigger;
		}
		
		internal function invoke():void
		{
			switch(_type)
			{
				case ActionType.event:
					GameEngine.startEvent(_values["event"]);
					break;
					
				case ActionType.setVar:
					var name:String = (_values["name"]as String).toLowerCase();
					if(name.substring(0, 7) == "player.")
					{
						var path:Array = name.split(".");
						path.shift();
						GameEngine.world.player.setVariable(path, _values["value"] || "true");
						GameEngine.world.player.applyEffects();
						if(GameEngine.menuManager.bottomAuxMenu == AuxiliaryStatMenu.ID)
						{
							(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
						}
					}
					else
					{
						GameEngine.world.variables[_values["name"]] = _values["value"] || true;
					}
					break;
					
				case ActionType.removeVar:
					delete GameEngine.world.variables[_values["name"]];
					break;
					
				case ActionType.goTo:
					GameEngine.gotoLocation(_values["location"], _values["floor"], _values["room"]);
					break;
				
				case ActionType.state:
					GameEngine.setNextState(_values["state"]);
					break;
					
				case ActionType.trade:
					GameEngine.startTrade(_values["character"]);
					break;
					
				case ActionType.combat:
					GameEngine.oldStartCombat(csvToVector(_values["combatants"]), csvToVector(_values["intros"]), csvToVector(_values["wins"]), csvToVector(_values["losses"]));
					break;
			}
		}
		
		private function csvToVector(csv:String):Vector.<String>
		{
			if (csv == null || csv.length == 0)
				return new Vector.<String>();
			
			var values:Array = csv.split(",");
			for (var i:int = 0; i < values.length; i++ )
				values[i] = StringUtil.trim(values[i]);
			
			return Vector.<String>(values);
		}
		
	}

}