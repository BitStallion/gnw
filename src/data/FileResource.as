package data 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class FileResource 
	{
		public var c:Class;
		public var type:String;
		public var virtualPath:String;
		
		public function FileResource(c:Class, type:String, virtualPath:String) 
		{
			this.c = c;
			this.type = type;
			this.virtualPath = virtualPath;
		}
	}
}