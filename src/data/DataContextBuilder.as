package data 
{
	import data.Entity;
	import system.GameWorld;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DataContextBuilder 
	{
		private var _world:GameWorld;
		private var _characters:Object;
		
		public function DataContextBuilder(world:GameWorld) 
		{
			_world = world;
			_characters = new Object();
		}
		
		public function addCharacter(character:Entity, name:String):void
		{
			_characters[name] = character;
		}
		
		public function buildContext():Object
		{
			var context:Object = new Object();
			
			if (_world != null)
			{
				context["player"] = _world.player.toDataContext();
				context["calendar"] = _world.gameTime.toDataContext();
			}
			
			for (var key:String in _characters)
			{
				context[key] = (_characters[key] as Entity).toDataContext();
			}
			
			return context;
		}
		
	}

}