package data {
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author BlueLight
	 * 
	 * This class is designed to minimize the amount of code is required to maintain most stat variables.
	 * We will store 4 variables, a min, current and max; along with a boolean called goHigh. (Might want a better name for that variable. 
	 * The current can not be below the min, or above the max.
	 * 
	 * It will have at least 8 functions. 
	 * Get: min, current & max. set:min current & max. change current, restore current 
	 * 
	 */
	public class DynamicNumber extends EventDispatcher
	{
		public static const CHANGED:String = "changed";
		public static const RESTORED:String = "restored";
		public static const MINNED:String = "minned";
		public static const MAXED:String = "maxed";
		
		protected var _minimum:Number; // the min number that current can be.
		protected var _maximum:Number;//the max number that current can be.
		protected var _current:Number;//current or index number.
		protected var _highest:Boolean;//if you restore method, this decides if you set current to maximum or minimum.
		
		
	
		
		public function DynamicNumber( min:Number = 0, max:Number = 100, value:Number = 100, high:Boolean = true ) 
		{
			
			// this will prevent a bug if the user input a off value where min is more than max.
			_minimum = Math.min(min,max);
			_maximum = Math.max(max, min);
			
			//use the setters since they have safety checks built in.
			minimum = minimum;
			maximum = maximum;
			current = value;
			highest = high;
		}
		
		//public function toString():String {
		//	return super.toString();// "Foo";
		//}
		
		
		
		//public function toString():String {
		//	return "foo";	//return "TEST";//""_minimum: " + this._minimum +". _maximum: " + this._maximum + "._current: " + this._current + ". _highest: "+this._highest;
		//}
		
		/**
		 * Minimum value of the dynamic number
		 */
		public function get minimum ():Number 
		{
			return _minimum;
		}
		
		/**
		 * Maxiumum value of the dynamic number
		 */
		public function get maximum ():Number 
		{
			return _maximum;
		}
		
		/**
		 * Current value of the dynamic number
		 */
		public function get current ():Number 
		{
			return _current;
		}
		
		//returns boolean, highest.
		public function get highest():Boolean
		{
			return _highest;
		}
		
		/**
		 * Minimum value of the dynamic number
		 */
		public function set minimum (min:Number):void 
		{
			//checks to make sure min is more than Maximum. 
			this._minimum = min <= this._maximum? min:this._minimum;
		}
		
		/**
		 * maximum value of the dynamic number
		 */
		public function set maximum (max:Number):void 
		{
			//checks to make sure max is more than minimum. 
			this._maximum = max >= this._minimum? max:this._maximum;
		}
		
		/**
		 * Sets the current variable to a new value so unless it's above the max or below the min.
		 * If it's below the min, it will set the value to the min, or if it's above the max, it will set the value to the max.
		 */
		public function set current(value:Number):void 
		{
			this._current = Math.max(this._minimum, Math.min(this._maximum, value));
		}
		
		//just a simple setter for changing highest.
		public function set highest(high:Boolean):void
		{
			this._highest = high;
		}
		
		/**
		 * Adds an amount to the current value.
		 * @param	value
		 */
		public function add(amount:Number):void
		{
			this.current = this._current + amount;
		}
		
		/**
		 * Restors the current value to either the max value if highest is true, or the min value if not.
		 */
		public function restore():void
		{
			this.current = highest ? maximum : minimum;
		}
	}

}