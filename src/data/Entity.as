package data 
{
	import data.entityEvents.EntityEvent;
	import data.entityEvents.DeltaTime;
	import data.entityEvents.EntityEventGetAttribute;
	import enums.CharacterStat;
	import enums.Utils;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.net.NetStreamMulticastInfo;
	import flash.utils.Dictionary;
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;
	import utility.GnWError;
	import utility.IDestroyable
	import utility.ModularEventDispatcher;
	import utility.UUID
	import data.EntityComponents.*
	import flash.utils.getDefinitionByName
	
	/**
	 * The base entity class to be used for the player, characters, items, weapons, etc.
	 * 
	 * @author TheWrongHands
	 */
	public class Entity extends Proxy implements IEventDispatcher, IDestroyable, IXMLSerializable, IDataContextable, IParsingContextable, ITransactionable
	{
		private var _components:Dictionary;
		private var _tags:Vector.<String>;
		private var _variables:Dictionary;
		private var _temporaryVariables:Dictionary;
		private var _dispatcher:ModularEventDispatcher;
		private var _id:String;
		private var _uuid:UUID;
		private var _parentContainer:ECContainer;
		private var _transactionData:Dictionary;
		
		public function Entity(id:String) 
		{
			_components = new Dictionary();
			_variables = new Dictionary();
			_tags = new Vector.<String>();
			_dispatcher = new ModularEventDispatcher(this);
			_id = id;
			_uuid = new UUID();
			_parentContainer = null;
			_transactionData = null;
		}
		
		public function destroy():void
		{
			if(_parentContainer != null) _parentContainer.removeEntity(this);
			for each(var comp:EntityComponent in _components)
			{
				comp.onRemove();
				comp.destroy();
			}
			_dispatcher = null;
			_components = null;
			_variables = null;
			_tags = null;
			_uuid = null;
			_parentContainer = null;
		}
		
		override flash_proxy function callProperty(name:*, ...args):* 
		{
			trace("tersting");
		}
		
		override flash_proxy function getProperty(name:*):*
		{
			var nameStr:String;
			if(name is QName || name is String)
			{
				if(name is QName)
				{
					nameStr = (name as QName).localName; 
				}
				else
				{
					nameStr = name as String; 
				}
				//For compatibility with the parser
				try
				{
					return getComponent(nameStr);
				}
				catch(e:GnWError)
				{
				}
				try
				{
					switch(nameStr)
					{
						case "vars":
						{
							return GameEngine.world.variables;
						}
						case "gender":
						{
							return variables["gender"];
						}
						case "simplegender":
						{
							switch(variables["gender"])
							{
								case "Cuntboy":
								case "Female":
								{
									return "female";
								}
								case "Femboy":
								case "Male":
								case "Shemale":
								{
									return "male";
									break;
								}
								case "Herm":
								{
									return "herm";
								}
							}
							break;
						}
						case "perceivedsimplegender":
						{
							switch(variables["gender"])
							{
								case "Female":
								case "Herm":
								case "Shemale":
								{
									return "female";
								}
								case "Cuntboy":
								case "Male":
								case "Femboy":
								{
									return "male";
								}
							}
							break;
						}
						case "hisher":
						{
							switch(variables["gender"])
							{
								case "Female":
								case "Herm":
								case "Shemale":
								{
									return "her";
								}
								case "Cuntboy":
								case "Male":
								case "Femboy":
								{
									return "his";
								}
							}
							return "his/her";
							break;
						}
						case "heshe":
						{
							switch(variables["gender"])
							{
								case "Female":
								case "Herm":
								case "Shemale":
								{
									return "she";
								}
								case "Cuntboy":
								case "Male":
								case "Femboy":
								{
									return "he";
								}
							}
							return "his/her";
						}
						case "name":
						{
							return variables["name"] || variables["shortDescription"];
						}
						case "id":
						{
							return id;
						}
						case "uuid":
						{
							return uuid;
						}
						case "HP":
						{
							return (getComponent(EntityComponent.JUICE) as ECDynamicNumber).value;
						}
						case "maxHP":
						{
							return (getComponent(EntityComponent.JUICE) as ECDynamicNumber).max;
						}
						case "LUST":
						{
							return (getComponent(EntityComponent.LUST) as ECDynamicNumber).value;
						}
						case "maxLust":
						{
							return (getComponent(EntityComponent.LUST) as ECDynamicNumber).max;
						}
						case "MP":
						{
							return (getComponent(EntityComponent.SPIRIT) as ECDynamicNumber).value;
						}
						case "maxMP":
						{
							return (getComponent(EntityComponent.SPIRIT) as ECDynamicNumber).max;
						}
					}
					for each(var stat:CharacterStat in Utils.EnumList(CharacterStat))
					{
						if(stat.text == nameStr)
						{
							var attr:EntityEventGetAttribute = new EntityEventGetAttribute();
							attr.attribute = nameStr;
							dispatchEvent(attr);
							return attr.result;
						}
					}
					var xml:XML = (getComponent(EntityComponent.PARTS) as ECXMLHolder).xml;
					if(xml[nameStr].length() > 0)
					{
						var cls:Class = GameEngine.dataManager.getPartClass(nameStr);
						return new cls(xml[nameStr][0]);
					}
					if(nameStr.substr(0, 3) == "has")
					{
						return xml[nameStr.substr(3)].length() > 0;
					}
				}
				catch(e:GnWError)
				{
				}
				return null;
			}
		}
		
		override flash_proxy function setProperty(name:*, value:*):void
		{
			if(name is QName && value is EntityComponent) return setComponent((name as QName).localName, value as EntityComponent);
			if(name is String && value is EntityComponent) return setComponent(name as String, value as EntityComponent);
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void 
		{
			_dispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void 
		{
			_dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function dispatchEvent(event:Event):Boolean 
		{
			if(_dispatcher == null) return true;
			var result:Boolean = _dispatcher.dispatchEvent(event);
			if(hasTag("dirty")) applyEffects();
			return result;
		}
		
		public function hasEventListener(type:String):Boolean 
		{
			return _dispatcher.hasEventListener(type);
		}
		
		public function willTrigger(type:String):Boolean 
		{
			return _dispatcher.willTrigger(type);
		}
		
		public function addEventDispatcher(dispatcher:IEventDispatcher):void
		{
			_dispatcher.addEventDispatcher(dispatcher);
		}
		
		public function removeEventDispatcher(dispatcher:IEventDispatcher):void
		{
			_dispatcher.removeEventDispatcher(dispatcher);
		}
		
		public function addTag(tag:String):void
		{
			if(tag != null && tag.length > 0) _tags.push(tag)
		}
		
		public function hasTag(tag:String):Boolean
		{
			if(_tags == null) return false;
			for(var i:int = 0; i < _tags.length; i++)
			{
				if(_tags[i] == tag) return true;
			}
			return false;
		}
		
		public function removeTag(tag:String):void
		{
			for(var i:int = 0; i < _tags.length; i++)
			{
				if(_tags[i] == tag)
				{
					_tags.splice(i, 1);
					return;
				}
			}
		}
		
		public function getTags():Vector.<String>
		{
			return _tags;
		}
		
		public function markDirty():void
		{
			if(!hasTag("dirty")) addTag("dirty");
		}
		
		public function get variables():Dictionary
		{
			return _variables;
		}
		
		public function setComponent(name:String, component:EntityComponent):void
		{
			if(name == "uuid")
			{
				var uuid:UUID = new UUID();
				uuid.genID();
				name = uuid.id;
			}
			var comp:EntityComponent = _components[name] as EntityComponent;
			if(comp != null)
			{
				comp.onRemove();
				comp.destroy();
			}
			component.setEntity(name, this);
			_components[name] = component;
			component.onAdd();
		}
		
		public function getComponent(name:String):EntityComponent
		{
			var ec:EntityComponent = _components[name];
			if(ec == null) throw new GnWError("Entity \"" + _id || _uuid.id + "\" does not have component \""  + name + "\"");
			return ec;
		}
		
		public function removeComponent(name:String):void
		{
			var comp:EntityComponent = _components[name] as EntityComponent;
			if(comp != null)
			{
				comp.onRemove();
				comp.entity = null;
				delete _components[name];
			}
		}
		
		public function deleteComponent(name:String):void
		{
			var comp:EntityComponent = _components[name] as EntityComponent;
			if(comp != null)
			{
				comp.onRemove();
				comp.entity = null;
				comp.destroy();
				delete _components[name];
			}
		}
		
		public function set id(id:String):void
		{
			_id = id;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function set uuid(str:String):void
		{
			_uuid.id = str;
		}
		
		public function get uuid():String
		{
			if(_uuid == null) return UUID.nilID;
			if(_uuid.id == UUID.nilID) _uuid.genID();
			return _uuid.id;
		}
		
		public function set container(container:ECContainer):void
		{
			_parentContainer = container;
		}
		
		public function get container():ECContainer
		{
			return _parentContainer;
		}
		
		public function setVariable(path:Object, value:String):void
		{
			var curPath:Array;
			if(path is Array)
			{
				curPath = path as Array;
			}
			else
			{
				curPath = (path as String).toLowerCase().split(".");
			}
			if(curPath.length == 0) return;
			var name:String = curPath.shift() as String;
			if(name == "tag")
			{
				_tags.push(value);
			}
			else if(name == "attribute" || name == "attr")
			{
				name = curPath.shift() as String;
				_variables[name] = value;
			}
			else if(name == "id")
			{
				_id = value;
			}
			else
			{
				if(_components.hasOwnProperty(name)) (_components[name] as EntityComponent).setVariable(curPath, value);
			}
		}
		
		public function getVariable(path:Object):String
		{
			var curPath:Array;
			if(path is Array)
			{
				curPath = path as Array;
			}
			else
			{
				curPath = (path as String).toLowerCase().split(".");
			}
			var name:String = curPath.shift() as String;
			if(name == "tag")
			{
				return hasTag(name).toString();
			}
			else if(name == "attribute" || name == "attr")
			{
				name = curPath.shift() as String;
				return _variables[name];
			}
			else if(name == "id")
			{
				return _id;
			}
			else
			{
				if(_components.hasOwnProperty(name)) return (_components[name] as EntityComponent).getVariable(curPath);
			}
			return null;
		}
		
		public function advanceTurn():void
		{
			var delta:DeltaTime = new DeltaTime();
			dispatchEvent(delta);
		}
		
		public function applyEffects():void
		{
			var component:EntityComponent;
			removeTag("dirty");
			dispatchEvent(new EntityEvent(EntityEvent.resetToBase));
			dispatchEvent(new EntityEvent(EntityEvent.applyEffects));
		}
		
		/*
		 * This saves the entity to XML
		 */
		public function toXML():XML
		{
			var xml:XML = new XML("<entity></entity>");
			var groupXML:XML = new XML("<tags></tags>");
			var entryXML:XML;
			var name:String;
			xml.@["id"] = _id;
			xml.@["uuid"] = uuid;
			for(var i:int; i < _tags.length; i++)
			{
				entryXML = new XML("<tag />");
				entryXML.@["value"] = (_tags[i] as String);
				groupXML.appendChild(entryXML);
			}
			xml.appendChild(groupXML);
			groupXML = new XML("<variables></variables>");
			for(name in _variables)
			{
				entryXML = new XML("<variable></variable>");
				entryXML.@["name"] = name;
				if(_variables[name] is String)
				{
					entryXML.@["type"] = "String";
				}
				else if(_variables[name] is int)
				{
					entryXML.@["type"] = "int";
				}
				else if(_variables[name] is uint)
				{
					entryXML.@["type"] = "uint";
				}
				else if(_variables[name] is Number)
				{
					entryXML.@["type"] = "Number";
				}
				entryXML.setChildren(_variables[name]);
				groupXML.appendChild(entryXML);
			}
			xml.appendChild(groupXML);
			groupXML = new XML("<components></components>");
			for(name in _components)
			{
				entryXML = (_components[name] as EntityComponent).toXML(); // Get the xml data from the component, relying on it to also supply the type attribute
				if(entryXML != null) // Some components might not need to be saved, they won't return anything
				{
					entryXML.@["name"] = name;
					groupXML.appendChild(entryXML);
				}
			}
			xml.appendChild(groupXML);
			return xml;
		}
		
		/*
		 * The entity needs to be able to construct it's self and its components from xml data but it can't edit the any of it's components directly or it would risk
		 * becoming as inflexible as the system it's replacing. so instead the xml data must contain all of the information needed for the entity class to construct
		 * the component and then pass on the xml data to the component to finish the more specific aspects of it's own construction.
		 */
		public function fromXML(xml:XML):void
		{
			var groupXML:XML = xml.child("tags")[0];
			var entryXML:XML;
			var typeName:String;
			var name:String;
			if(xml.hasOwnProperty("@id")) _id = xml.@["id"].toXMLString();
			try
			{
				if(xml.hasOwnProperty("@uuid")) _uuid.id = xml.@["id"].toXMLString();
			}
			catch(e:GnWError)
			{
			}
			if(groupXML != null)
			{
				for each(entryXML in groupXML.child("tag"))
				{
					if(entryXML.attribute("value").toXMLString().length > 0)
					{
						_tags.push(entryXML.attribute("value").toXMLString());
					}
				}
			}
			groupXML = xml.child("variables")[0];
			if(groupXML != null)
			{
				for each(entryXML in groupXML.child("variable"))
				{
					name = entryXML.@["name"].toXMLString();
					if(name.length > 0)
					{
						typeName = entryXML.@["type"];
						if(typeName.length == 0) typeName = "String";
						var value:String = entryXML.text().toXMLString();
						switch(typeName)
						{
							case "String":
							{
								_variables[name] = value;
								break;
							}
							case "Number":
							{
								_variables[name] = parseFloat(value) || 0;
								break;
							}
							case "int":
							{
								_variables[name] = parseInt(value) || 0;
								break;
							}
							case "uint":
							{
								_variables[name] = new uint(value) || 0;
								break;
							}
						}
						
					}
				}
			}
			groupXML = xml.child("components")[0];
			if(groupXML != null)
			{
				for each(entryXML in groupXML.child("component"))
				{
					if(entryXML.hasOwnProperty("@name") && entryXML.@["name"].toXMLString().length > 0) //make sure there is a valid name and type attribute
					{
						typeName = "data.EntityComponents.";
						typeName += entryXML.@["type"].toXMLString();
						var componentClass:Class = getDefinitionByName(typeName) as Class; //dymanicly create the class based on type name
						var comp:EntityComponent = new componentClass();
						comp.fromXML(entryXML);
						setComponent(entryXML.@["name"].toXMLString(), comp);
					}
				}
			}
		}
		
		public function appendToLog(str:String):void
		{
		}
		
		public function toDataContext():Object
		{
			return this;// TempDataContextableCompatibilityLayer.toDataContext(this);
		}
		
		public function toParsingContext():Object
		{
			return this;// TempDataContextableCompatibilityLayer.toParsingContext(this);
		}
		
		public function toString():String
		{
			return variables["name"] || id || uuid;
		}
		
		public function startTransaction():void 
		{
			if(_transactionData != null) return;
			_transactionData = new Dictionary();
			var dic:Dictionary = new Dictionary();
			var key:Object;
			for(key in _components)
			{
				var comp:EntityComponent = _components[key];
				comp.startTransaction();
				dic[key] = comp;
			}
			_transactionData["components"] = dic;
			_transactionData["tags"] = _tags.slice();
			dic = new Dictionary();
			for(key in _variables)
			{
				dic[key] = _variables[key];
			}
			_transactionData["variables"] = dic;
			_dispatcher.startTransaction();
			_transactionData["id"] = _id;
			_transactionData["uuid"] = _uuid.id;
			_transactionData["parentContainer"] = _parentContainer;
		}
		
		public function commitTransaction():void 
		{
			if(_transactionData == null) return;
			for each(var comp:EntityComponent in _components)
			{
				comp.commitTransaction()
			}
			_dispatcher.commitTransaction();
			_transactionData = null;
		}
		
		public function rollbackTransaction():void 
		{
			if(_transactionData == null) return;
			var dic:Dictionary = _transactionData["components"] as Dictionary;
			var comp:EntityComponent;
			for(var name:String in _components)
			{
				comp = _components[name];
				var deleteComp:Boolean = true;
				for each(var comp1:EntityComponent in dic)
				{
					if(comp == comp1)
					{
						deleteComp = false;
						break;
					}
				}
				if(deleteComp)
				{
					removeComponent(name);
					comp.destroy();
				}
			}
			_components = dic;
			for each(comp in _components)
			{
				comp.rollbackTransaction();
			}
			_tags = _transactionData["tags"] as Vector.<String>;
			_variables = _transactionData["variables"] as Dictionary;
			_dispatcher.rollbackTransaction();
			_id = _transactionData["id"] as String;
			if(_uuid == null) _uuid = new UUID();
			_uuid.id = _transactionData["uuid"] as String;
			_parentContainer = _transactionData["parentContainer"] as ECContainer;
			_transactionData = null;
		}
		
		public function isTransactionOccurring():Boolean 
		{
			return _transactionData != null;
		}
	}
}