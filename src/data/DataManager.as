package data
{
	import data.character.*;
	import data.EntityComponents.ECArray;
	import data.EntityComponents.ECBuffHandler;
	import data.EntityComponents.ECInventory;
	import data.EntityComponents.ECDamageHandler;
	import data.EntityComponents.ECDynamicNumber;
	import data.EntityComponents.ECExperience;
	import data.EntityComponents.ECAttributes;
	import data.EntityComponents.ECMapData;
	import data.EntityComponents.ECXMLHolder;
	import enums.BodyType;
	import enums.Gender;
	import enums.Race;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import mx.utils.StringUtil;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DataManager implements IXMLDataLoader
	{
		private var _characters:Dictionary;
		private var _characterTemplates:Dictionary;
		private var _itemTemplates:Dictionary;
		private var _buffTemplates:Dictionary;
		private var _raceTemplates:Dictionary;
		private var _bodyParts:Dictionary;
		private var _buffGroups:Dictionary;
		private var _levelData:Dictionary;
		
		private var _bodyMaster:XML;
		private var _bodyTypes:XML;
		
		public function DataManager()
		{
			// These are here to allow getDefinitionByName to find the class names for _bodyParts.
			Arms;
			Ass;
			Breasts;
			Head;
			LowerBody;
			Penis;
			Skin;
			Tail;
			Testicles;
			Torso;
			Vulva;
			Wings;
		}
		
		public function startLoading():void
		{
			_characters = new Dictionary();
			_characterTemplates = new Dictionary()
			_itemTemplates = new Dictionary()
			_buffTemplates = new Dictionary()
			_raceTemplates = new Dictionary();
			_bodyParts = new Dictionary();
			_buffGroups = new Dictionary();
			_bodyMaster = new XML("<master></master>")
			_bodyTypes = new XML("<bodytypes></bodytypes>")
			_levelData = new Dictionary();
		}
		
		public function canHandleXMLType(name:String):Boolean
		{
			return name == "master" || name == "races" || name == "bodytypes" || name == "characters" || name == "characterTemplates" || name == "itemTemplates" || name == "buffTemplates" || name == "buffGroups" || name == "levelData";
		}
		
		public function readXMLData(file:String, xml:XML):void
		{
			if(xml.name() == "master")
			{
				loadMasterBodyTemplate(xml);
			}
			else if(xml.name() == "races")
			{
				loadRaceTemplates(xml);
			}
			else if(xml.name() == "bodytypes")
			{
				loadBodyTypes(xml);
			}
			else if(xml.name() == "characters")
			{
				loadCharactersFromXml(xml);
			}
			else if(xml.name() == "characterTemplates")
			{
				loadCharacterTemplates(xml);
			}
			else if(xml.name() == "itemTemplates")
			{
				loadItemTemplates(xml);
			}
			else if(xml.name() == "buffTemplates")
			{
				loadBuffTemplates(xml);
			}
			else if(xml.name() == "buffGroups")
			{
				loadBuffGroups(xml);
			}
			else if(xml.name() == "levelData")
			{
				loadLevelData(xml);
			}
		}
		
		/**
		 * Adds a new character to the manager character library. All characters added must have unique IDs.
		 *
		 * @param	character	The character to add.
		 */
		public function addCharacter(character:Entity):void
		{
			if(character.id == null || character.id.length == 0)
				throw new Error("Cannot add a character with a null or empty id.");
			
			if(character.id in _characters)
				throw new Error("A character with the id \"" + character.id + "\" has already been added to the data manager.");
			
			_characters[character.id] = character;
		}
		
		/**
		 * Either adds a new character to the character manager or overwrites the existing character if they share ids.
		 *
		 * @param	character	The character to add.
		 */
		public function overrideCharacter(character:Entity):void
		{
			_characters[character.id] = character;
		}
		
		/**
		 * Returns a list containing all characters in the game.
		 *
		 * @return
		 */
		public function getAllCharacters():Vector.<Entity>
		{
			var characters:Vector.<Entity> = new Vector.<Entity>();
			
			for each(var char:Entity in _characters)
			{
				characters.push(char);
			}
			
			return characters;
		}
		
		/**
		 * Returns a charcter with the matching id.
		 *
		 * @param	id	The id of the character to be returned.
		 * @return	A character with the matching id or null if no character has that id.
		 */
		public function getCharacterById(id:String):Entity
		{
			if(id == null || id.length == 0)
				return null;
			
			return _characters[id];
		}
		
		/**
		 * Given a race and a gender, a blank character based of a template is returned.
		 *
		 * @param	race		The race of the new character.
		 * @param	gender		The gender of the new character.
		 * @param	bodyType	The body type of the new character.
		 * @return	A character based off a template.
		 */
		public function createCharacterFromTemplate(id:String, race:Race, gender:Gender, bodyType:BodyType):Entity
		{
			var char:Entity = new Entity(id);
			
			char.addTag("character");
			char.variables["level"] = getLevelStart();
			char.variables["bodyType"] = bodyType.text;
			char.setComponent(EntityComponent.ATTRIBUTES, new ECAttributes());
			char.setComponent(EntityComponent.INVENTORY, new ECInventory());
			char.setComponent(EntityComponent.EXPERIENCE, new ECExperience());
			char.setComponent(EntityComponent.JUICE, new ECDynamicNumber());
			char.setComponent(EntityComponent.LUST, new ECDynamicNumber());
			char.setComponent(EntityComponent.VIGOR, new ECDynamicNumber());
			char.setComponent(EntityComponent.SPIRIT, new ECDynamicNumber());
			char.setComponent(EntityComponent.ATTACKS, new ECArray());
			char.setComponent(EntityComponent.DAMAGE, new ECDamageHandler());
			char.setComponent(EntityComponent.BUFFS, new ECBuffHandler());
			char.setComponent(EntityComponent.POSITION, new ECMapData());
			char.fromXML(_raceTemplates[race.text]);
			applyGender(char, gender);
			applyBodyType(char, bodyType);
			
			return char;
		}
		
		public function createNPCFromTemplate(id:String):Entity
		{
			var char:Entity = new Entity(id);
			char.setComponent(EntityComponent.DAMAGE, new ECDamageHandler());
			char.setComponent(EntityComponent.BUFFS, new ECBuffHandler());
			char.fromXML(_characterTemplates[id]);
			char.applyEffects();
			return char;
		}
		
		public function createItemFromTemplate(id:String):Entity
		{
			if (_itemTemplates[id] == null)
				throw new GnWError("No item with id \"" + id+ "\" found");
			
			var item:Entity = new Entity(id);
			item.fromXML(_itemTemplates[id]);
			item.applyEffects();
			return item;
		}
		
		public function createBuffFromTemplate(id:String):Entity
		{
			if (_buffTemplates[id] == null)
				throw new GnWError("No buff with id \"" + id+ "\" found");
			
			var buff:Entity = new Entity(id);
			buff.fromXML(_buffTemplates[id]);
			return buff;
		}
		
		public function getPartClass(partname:String):Class
		{
			return _bodyParts[partname] as Class;
		}
		
		private function applyGender(char:Entity, gender:Gender):void
		{
			char.variables["gender"] = gender.text;
			
			var compXML:ECXMLHolder = char.getComponent(EntityComponent.PARTS) as ECXMLHolder;
			
			for each(var part:XML in compXML.xml.*)
			{
				var masterPart:XML = _bodyMaster[part.name()][0];
				
				var types:String = masterPart.@genders.toString();
				
				if(types == "all")
					continue;
				
				if(!matchGender(types, gender))
					delete compXML.xml.child(part.name())[0];
			}
		}
		
		private function matchGender(types:String, gender:Gender):Boolean
		{
			var genders:Array = types.split(",");
			
			for each(var genderString:String in genders)
			{
				if(gender.text == StringUtil.trim(genderString))
					return true;
			}
			
			return false;
		}
		
		private function applyBodyType(char:Entity, bodyType:BodyType):void
		{
			var bodytype:XML = _bodyTypes.bodytype.(@name == bodyType.text)[0];
			var fatness:int = bodytype.@fatness;
			var muscularity:int = bodytype.@muscularity;
			
			var compXML:ECXMLHolder = char.getComponent(EntityComponent.PARTS) as ECXMLHolder;
			
			for each(var part:XML in compXML.xml.*)
			{
				var masterPart:XML = _bodyMaster[part.name()][0];
				
				var fatAttr:XML = masterPart.attr.(@type == "fatness")[0];
				var muscAttr:XML = masterPart.attr.(@type == "muscularity")[0];
				
				if(fatAttr != null)
					compXML.xml[part.name()].@fatness = fatness;
				
				if(muscAttr != null)
					compXML.xml[part.name()].@muscularity = muscularity;
			}
		}
		
		public function getBufferGroupData(id:String):XML
		{
			if(id == null) return null;
			return _buffGroups[id];
		}
		
		public function getLevelStart():uint
		{
			return _levelData["start"];
		}
		
		public function getLevelCap():uint
		{
			return _levelData["cap"];
		}
		
		public function getExpToNextLevel(level:uint):uint
		{
			var exp:uint = 0;
			var levelData:Object = null;
			var levelExp:uint = 0;
			for(var i:uint = getLevelStart(); i <= level; i++)
			{
				levelData = _levelData[i];
				if(levelData != null && !isNaN(levelData["experience"])) levelExp = levelData["experience"];
				exp += levelExp;
			}
			return exp;
		}
		
		public function getThisLevelData(name:String, level:uint):Number
		{
			var levelData:Object = null;
			var points:Number = 0;
			for(var i:uint = getLevelStart(); i <= level; i++)
			{
				levelData = _levelData[i];
				if(levelData != null && !isNaN(levelData[name])) points = levelData[name];
			}
			return points;
		}
		
		private function loadMasterBodyTemplate(templateSet:XML):void
		{
			for each(var part:XML in templateSet.*)
			{
				var classname:String = "data.character." + part.@["class"].toString();
				var bodyClass:Class = getDefinitionByName(classname) as Class;
				_bodyParts[part.name().toString()] = bodyClass;
				_bodyMaster.appendChild(part);
			}
		}
		
		private function loadRaceTemplates(templateSet:XML):void
		{
			for each(var race:XML in templateSet.*)
			{
				_raceTemplates[race.@name.toString()] = race;
			}
		}
		
		private function loadBodyTypes(templateSet:XML):void
		{
			for each(var body:XML in templateSet.*)
			{
				_bodyTypes.appendChild(body);
			}
		}
		
		private function loadCharactersFromXml(characterSet:XML):void
		{
			/*for each(var character:XML in characterSet.*)
			{
			}*/
		}
		
		private function loadCharacterTemplates(templateSet:XML):void
		{
			for each(var template:XML in templateSet.characterTemplate)
			{
				_characterTemplates[template.@["id"].toString()] = template;
			}
		}
		
		private function loadItemTemplates(templateSet:XML):void
		{
			for each(var template:XML in templateSet.itemTemplate)
			{
				_itemTemplates[template.@["id"].toString()] = template;
			}
		}
		
		private function loadBuffTemplates(templateSet:XML):void
		{
			for each(var template:XML in templateSet.buffTemplate)
			{
				_buffTemplates[template.@["id"].toString()] = template;
			}
		}
		
		private function loadBuffGroups(templateSet:XML):void
		{
			for each(var template:XML in templateSet.buffGroup)
			{
				_buffGroups[template.@["id"].toString()] = template;
			}
		}
		
		private function loadLevelData(xml:XML):void
		{
			_levelData["start"] = parseInt(xml.@["start"].toString()) || _levelData["start"] || 1;
			_levelData["cap"] = parseInt(xml.@["cap"].toString()) || _levelData["cap"] || 100;
			for each(var xmlData:XML in xml.level)
			{
				var level:uint = parseInt(xmlData.@["level"]);
				if(!isNaN(level))
				{
					var levelData:Object = new Object();
					for each(var attrib:XML in xmlData.@*)
					{
						var str:String = attrib.name();
						var num:Number = parseFloat(attrib.valueOf());
						if(!isNaN(num)) levelData[str] = num;
					}
					_levelData[level] = levelData;
				}
			}
		}
	}

}