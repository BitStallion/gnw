package data 
{
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public interface IXMLSerializable 
	{
		/*
		 * TheWrongHands
		 * todo: these functions should probably be renamed to readXML and writeXML
		 */
		function toXML():XML;
		
		function fromXML(xml:XML):void;
	}
	
}