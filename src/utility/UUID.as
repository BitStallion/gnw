package utility 
{
	/**
	 * ...
	 * @author TheWrongHands
	 * 
	 * Not quite a true UUID since we don't have access to a 64 bit time stamp but should be good enough
	 */
	public class UUID 
	{
		public static const nilID:String = "00000000-0000-0000-0000-000000000000";
		private var _id:String;
		
		public function UUID() 
		{
			_id = nilID;
		}
		
		public function set id(id:String):void
		{
			if(id.length != 36 || id.charAt(8) != "-" || id.charAt(13) != "-" || id.charAt(18) != "-" || id.charAt(23) != "-") throw new GnWError("Improperly formed UUID", 0);
			_id = id;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function genID():void
		{
			var newID:String = "";
			var date:Date = new Date();
			var nums:Array = new Array();
			var p:uint;
			var i:int;
			var n:int;
			nums[0] = date.time;
			for(n = 1; n < 4; n++)
			{
				nums[n] = Math.round(uint.MAX_VALUE * Math.random());
			}
			nums[1] = (uint(nums[1]) & 0xFFFF0FFF) | 0x4000;
			nums[2] = (uint(nums[2]) & 0x3FFFFFFF) | 0x80000000;
			for(n = 0; n < 4; n++)
			{
				for(i = 7; i >= 0; i--)
				{
					p = (uint(nums[n]) >> i) & 0xF;
					newID += p.toString(16);
					if(newID.length == 8 || newID.length == 13 || newID.length == 18 || newID.length == 23) newID += "-";
				}
			}
			_id = newID;
		}
		
		public function toString():String
		{
			return _id;
		}
	}

}