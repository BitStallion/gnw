package utility 
{
	import data.BaseEvent;
	import data.ITransactionable;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	
	/**
	 * This is a custom EventDispatcher to get around some of the odd choices adobe made for the regular EventDispatcher.
	 * 
	 * Note that this is limited to only working with Events that extend data.BaseEvent
	 * 
	 * @author TheWrongHands
	 */
	public class ModularEventDispatcher implements IEventDispatcher, ITransactionable
	{
		private var _eventDispatchers:Vector.<IEventDispatcher>;
		private var _eventListeners:Dictionary;
		private var _target:IEventDispatcher;
		private var _eventCount:uint;
		private var _postEvents:Vector.<Function>;
		private var _transactionData:Dictionary;
		
		public function ModularEventDispatcher(target:IEventDispatcher = null) 
		{
			_eventDispatchers = new Vector.<IEventDispatcher>();
			_eventListeners = new Dictionary();
			_target = target;
			_eventCount = 0;
			_postEvents = new Vector.<Function>();
		}
		
		private function get eventCount():uint
		{
			return _eventCount;
		}
		
		private function set eventCount(num:uint):void
		{
			_eventCount = num;
			if(_eventCount == 0)
			{
				while(_postEvents.length > 0)
				{
					var fun:Function = _postEvents.shift();
					fun.call();
				}
			}
		}
		
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void 
		{
			if(type == null || listener == null) return;
			if(_eventCount == 0)
			{
				var typeGroup:Vector.<EventListenerData> = _eventListeners[type] as Vector.<EventListenerData>;
				if(typeGroup == null)
				{
					typeGroup = new Vector.<EventListenerData>();
					_eventListeners[type] = typeGroup;
				}
				var lisData1:EventListenerData = new EventListenerData(listener, priority);
				var index:int = typeGroup.length - 1;
				for(index = typeGroup.length - 1; index >= 0; index--)
				{
					var lisData2:EventListenerData = typeGroup[index];
					if(lisData2.priority > lisData1.priority)
					{
						index++;
						break;
					}
				}
				if(index < 0)
				{
					index = 0;
				}
				typeGroup.splice(index, 0, lisData1);
			}
			else
			{
				_postEvents.push(function():void
				{
					addEventListener(type, listener, useCapture, priority, useWeakReference);
				});
			}
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void 
		{
			if(_eventCount == 0)
			{
				var typeGroup:Vector.<EventListenerData> = _eventListeners[type] as Vector.<EventListenerData>;
				if(typeGroup != null)
				{
					for(var index:int = 0; index < typeGroup.length; index++)
					{
						var lisData:EventListenerData = typeGroup[index];
						if(lisData.fun == listener)
						{
							typeGroup.splice(index, 1);
							break;
						}
					}
					if(typeGroup.length == 0) delete _eventListeners[type];
				}
			}
			else
			{
				_postEvents.push(function():void
				{
					removeEventListener(type, listener, useCapture);
				});
			}
		}
		
		public function dispatchEvent(event:Event):Boolean
		{
			eventCount++;
			if(event is BaseEvent)
			{
				var bEvent:BaseEvent = event as BaseEvent;
				if(bEvent.target == null) bEvent.target = _target || this;
				bEvent.currentTarget = _target || this;
			}
			var typeGroup:Vector.<EventListenerData> = _eventListeners[event.type] as Vector.<EventListenerData>;
			if(typeGroup != null)
			{
				for each(var lData:EventListenerData in typeGroup)
				{
					lData.fun.call(null, event);
				}
			}
			/*if(!super.dispatchEvent(event))
			{
				eventCount--;
				return false;
			}*/
			if(event.bubbles)
			{
				for each(var dispatcher:IEventDispatcher in _eventDispatchers)
				{
					if(!dispatcher.dispatchEvent(event.clone()))
					{
						eventCount--;
						return false;
					}
				}
			}
			eventCount--;
			return true;
		}
		
		public function hasEventListener(type:String):Boolean
		{
			if(_eventListeners[type] != null) return true;
			for each(var dispatcher:IEventDispatcher in _eventDispatchers)
			{
				if(dispatcher.hasEventListener(type)) return true;
			}
			return false;
		}
		
		public function willTrigger(type:String):Boolean
		{
			if(_eventListeners[type] != null) return true;
			for each(var dispatcher:IEventDispatcher in _eventDispatchers)
			{
				if(dispatcher.willTrigger(type)) return true;
			}
			return false;
		}
		
		public function addEventDispatcher(dispatcher:IEventDispatcher):void
		{
			if(_eventCount == 0)
			{
				if(_eventDispatchers.indexOf(dispatcher) < 0)
				{
					_eventDispatchers.push(dispatcher);
				}
			}
			else
			{
				_postEvents.push(function():void
				{
					if(_eventDispatchers.indexOf(dispatcher) < 0)
					{
						_eventDispatchers.push(dispatcher);
					}
				});
			}
		}
		
		public function removeEventDispatcher(dispatcher:IEventDispatcher):void
		{
			var index:int = _eventDispatchers.indexOf(dispatcher)
			if(index >= 0)
			{
				_eventDispatchers.splice(index, 1);
			}
		}
		
		public function startTransaction():void 
		{
			if(_transactionData != null) return;
			_transactionData = new Dictionary();
			_transactionData["eventDispatchers"] = _eventDispatchers.slice();
			var dic:Dictionary = new Dictionary();
			var key:Object;
			for(key in _eventListeners)
			{
				dic[key] = (_eventListeners[key] as Vector.<EventListenerData>).slice();
			}
			_transactionData["eventListeners"] = dic;
		}
		
		public function commitTransaction():void 
		{
			_transactionData = null;
		}
		
		public function rollbackTransaction():void 
		{
			if(_transactionData == null) return;
			_eventDispatchers = _transactionData["eventDispatchers"] as Vector.<IEventDispatcher>;
			_eventListeners = _transactionData["eventListeners"] as Dictionary;
			_transactionData = null;
		}
		
		public function isTransactionOccurring():Boolean 
		{
			return _transactionData != null;
		}
	}
}

class EventListenerData
{
	public var fun:Function;
	public var priority:int;
	
	public function EventListenerData(fun:Function = null, priority:int = 0):void
	{
		this.fun = fun;
		this.priority = priority;
	}
}