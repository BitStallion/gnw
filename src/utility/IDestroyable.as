package utility 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public interface IDestroyable
	{
		
		// Let's make things a little easier on the garbage collection system by making sure all links are removed when this gets called and then call it when this class is no longer needed
		function destroy():void;
		
	}

}