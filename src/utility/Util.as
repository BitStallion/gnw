package utility 
{
	/**
	 * Utility class.
	 * 
	 * @author GNW
	 */
	public class Util 
	{
		
		public function Util() {}
		
		/**
		 * Returns a value in between a specified range.
		 * 
		 * @param	num1 the first range condition 
		 * @param	num2 the second range condition
		 * @param	value the value we want inbetween the range.
		 * @return the value unless it was not between num1, num2 in which case it returns the closest number in that domain.
		 */
		public static function clampNumber( num1:Number, num2:Number, value:Number):Number {
			value = Math.min(value, num1 > num2?num1:num2);// sets value to the right bound if value is more than both num# values.
			value = Math.max(value, num1 > num2?num2:num1);// sets value to the left bound if value is less than both num# values.
			
			return value;
		}
		
		/**
		 * Returns a value in between a specified range.
		 * 
		 * @param	num1 the first range condition 
		 * @param	num2 the second range condition
		 * @param	value the value we want inbetween the range.
		 * @return the value unless it was not between num1, num2 in which case it returns the closest number in that domain.
		 */
		public static function clampInt( num1:Number, num2:Number, value:Number):Number {
			value = Math.min(value, num1 > num2?num1:num2);// sets value to the right bound if value is more than both num# values.
			value = Math.max(value, num1 > num2?num2:num1);// sets value to the left bound if value is less than both num# values.
			
			return value;
		}
		
		
		/**
		 * Returns a random integer from the specifed range.
		 * 
		 * @param	from	The lower bound of the range.
		 * @param	to		The upper bound of the range.
		 * @return
		 */
		public static function randomInt(from:int, to:int):int {
			if (from > to)
				throw new Error("From cannont be less then to");
			
			var n:int = to - from;
			return int(Math.round(Math.random() * n) + from);
		}
	}

}