package utility 
{
	/**
	 * This error class will allow us to quickly identify errors thrown by our game verses errors thrown by other things
	 * 
	 * @author TheWrongHands
	 */
	public class GnWError extends Error
	{
		public function GnWError(message:String, errorID:int = 0)
		{
			super(message, errorID);
		}
	}
}