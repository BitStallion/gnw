package gui
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display3D.Context3DClearMask;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.*;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import system.GlobalConstants;
	import gui.controls.ConfigurableButton;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class Game extends Sprite
	{
		public static const MAIN_WINDOW_WIDTH:int = 672;
		public static const MAIN_WINDOW_HEIGHT:int = 544;
		public static const MAIN_WINDOW_X:int = 320;
		public static const MAIN_WINDOW_Y:int = 64;
		
		public static const TOP_AUX_WINDOW_WIDTH:int = 256;
		public static const TOP_AUX_WINDOW_HEIGHT:int = 232;
		public static const TOP_AUX_WINDOW_X:int = 32;
		public static const TOP_AUX_WINDOW_Y:int = 64;
		
		public static const BOTTOM_AUX_WINDOW_WIDTH:int = 256;
		public static const BOTTOM_AUX_WINDOW_HEIGHT:int = 296;
		public static const BOTTOM_AUX_WINDOW_X:int = 32;
		public static const BOTTOM_AUX_WINDOW_Y:int = 312;
		
		public static const TOP_PANEL_WIDTH:int = 362;
		public static const TOP_PANEL_HEIGHT:int = 52;
		public static const TOP_PANEL_X:int = 316;
		public static const TOP_PANEL_Y:int = 0;
		
		public static const HIDDEN_PANEL_WIDTH:int = 362;
		public static const HIDDEN_PANEL_HEIGHT:int = 52;
		public static const HIDDEN_PANEL_X:int = 32;
		public static const HIDDEN_PANEL_Y:int = 628;
		
		public static const COMMON_BUTTON_01_ID:int = 1;
		public static const COMMON_BUTTON_02_ID:int = 2;
		public static const COMMON_BUTTON_03_ID:int = 3;
		public static const COMMON_BUTTON_04_ID:int = 4;
		public static const COMMON_BUTTON_05_ID:int = 5;
		public static const COMMON_BUTTON_06_ID:int = 6;
		public static const COMMON_BUTTON_07_ID:int = 7;
		public static const COMMON_BUTTON_08_ID:int = 8;
		public static const COMMON_BUTTON_09_ID:int = 9;
		public static const COMMON_BUTTON_10_ID:int = 10;
		public static const COMMON_BUTTON_11_ID:int = 11;
		public static const COMMON_BUTTON_12_ID:int = 12;
		
		public static const SYSTEM_BUTTON_01_ID:int = 13;
		public static const SYSTEM_BUTTON_02_ID:int = 14;
		public static const SYSTEM_BUTTON_03_ID:int = 15;
		public static const SYSTEM_BUTTON_04_ID:int = 16;
		public static const SYSTEM_BUTTON_05_ID:int = 17;
		
		public static const YES_BUTTON_ID:int = 18;
		public static const NO_BUTTON_ID:int = 19;
		
		public static const SCREEN_BUTTON_PRESSED_IMAGE:String = "images/gui/ScreenButtonPressed.png";
		public static const SCREEN_BUTTON_RELEASED_IMAGE:String = "images/gui/ScreenButtonReleased.png";
		
		private var yesButton:ConfigurableButton = new ConfigurableButton();
		private var noButton:ConfigurableButton = new ConfigurableButton();
		
		private var commonButton01:ConfigurableButton = new ConfigurableButton();
		private var commonButton02:ConfigurableButton = new ConfigurableButton();
		private var commonButton03:ConfigurableButton = new ConfigurableButton();
		private var commonButton04:ConfigurableButton = new ConfigurableButton();
		private var commonButton05:ConfigurableButton = new ConfigurableButton();
		private var commonButton06:ConfigurableButton = new ConfigurableButton();
		private var commonButton07:ConfigurableButton = new ConfigurableButton();
		private var commonButton08:ConfigurableButton = new ConfigurableButton();
		private var commonButton09:ConfigurableButton = new ConfigurableButton();
		private var commonButton10:ConfigurableButton = new ConfigurableButton();
		private var commonButton11:ConfigurableButton = new ConfigurableButton();
		private var commonButton12:ConfigurableButton = new ConfigurableButton();
		
		private var systemButton01:ConfigurableButton = new ConfigurableButton();
		private var systemButton02:ConfigurableButton = new ConfigurableButton();
		private var systemButton03:ConfigurableButton = new ConfigurableButton();
		private var systemButton04:ConfigurableButton = new ConfigurableButton();
		private var systemButton05:ConfigurableButton = new ConfigurableButton();
		
		//  Doing some precoding to make the button work
		private var zoomButton:ConfigurableButton = new ConfigurableButton();
		private var zoomMask:ConfigurableButton = new ConfigurableButton();
		public var _zoomAmount:int;
		private var _scaleX:int;
		private var _scaleY:int;
		
		private var buttons:Dictionary = new Dictionary();
		
		private var _mainMenu:Bitmap;
		private var _blinkTimer:Timer;
		private var _blinkState:Boolean;
		public function Game()
		{
			super();
			graphics.beginFill(Colors.CARBON);
			graphics.drawRect(0, 0, 1024, 768);
			graphics.endFill();
			
			_mainMenu = GameEngine.fileResourceManager.getResource("images/gui/MainWindow.png") as Bitmap;
			addChild(_mainMenu);
			
			setupButtons();
			_blinkTimer = new Timer(1000);
			_blinkState = false;
			_blinkTimer.addEventListener(TimerEvent.TIMER, blink);
			_blinkTimer.start();
		}
		
		public function setMenuTime(hours:Number):void
		{
			var image:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/MainWindow.png") as Bitmap;
			var mask:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/MainWindowMask.png") as Bitmap;
			image.cacheAsBitmap = true;
			mask.cacheAsBitmap = true;
			image.mask = mask;
			
			addChild(mask);
			
			var colors:Object = hoursToColors(hours);
			var transform:ColorTransform = new ColorTransform(colors["r"], colors["g"], colors["b"]);
			_mainMenu.bitmapData.draw(image, null, transform);
			
			removeChild(mask);
		}
		
		private function hoursToColors(hours:Number):Object
		{
			var red:Number;
			var green:Number;
			var blue:Number;
			var diff:Number;
			
			if (hours >= GlobalConstants.DayStart && hours <= GlobalConstants.DayEnd) 	// Day
				red = green = blue = 1.0;
			else if (hours >= 0 && hours <= GlobalConstants.NightEnd || hours >= GlobalConstants.NightStart && hours < 24) 	// Night
			{
				red = green = 0.45
				blue = 0.55;
			}
			else if (hours > GlobalConstants.DayEnd) 	// Sunset
			{
				diff = GlobalConstants.NightStart - hours;
				red = green = 0.45 - 0.14* Math.pow(diff, 2) + 0.56 * diff;
				blue = 0.55 - 0.056 * Math.pow(diff, 4) + 0.147*Math.pow(diff, 3) + 0.078*diff;
			} else {	//Sunrise
				diff = hours - GlobalConstants.NightEnd
				red = green = 0.45 - 0.14* Math.pow(diff, 2) + 0.56 * diff;
				blue = 0.55 - 0.056 * Math.pow(diff, 4) + 0.147*Math.pow(diff, 3) + 0.078*diff;
			}
			
			
			return { r:  red, g: green, b: blue };
		}
		
		/**
		 * Hides all buttons.
		 */
		public function hideAll():void
		{
			for each (var button:ConfigurableButton in buttons)
			{
				button.visible = false;
			}
		}
		
		/**
		 * Hides only the system buttons.
		 */
		public function hideSystemButtons():void
		{
			hide(SYSTEM_BUTTON_01_ID);
			hide(SYSTEM_BUTTON_02_ID);
			hide(SYSTEM_BUTTON_03_ID);
			hide(SYSTEM_BUTTON_04_ID);
			hide(SYSTEM_BUTTON_05_ID);
		}
		
		/**
		 * Hides only the common buttons.
		 */
		public function hideCommonButtons():void
		{
			hide(COMMON_BUTTON_01_ID);
			hide(COMMON_BUTTON_02_ID);
			hide(COMMON_BUTTON_03_ID);
			hide(COMMON_BUTTON_04_ID);
			hide(COMMON_BUTTON_05_ID);
			hide(COMMON_BUTTON_06_ID);
			hide(COMMON_BUTTON_07_ID);
			hide(COMMON_BUTTON_08_ID);
			hide(COMMON_BUTTON_09_ID);
			hide(COMMON_BUTTON_10_ID);
			hide(COMMON_BUTTON_11_ID);
			hide(COMMON_BUTTON_12_ID);
		}
		
		/**
		 * Hides only the yes and no buttons.
		 */
		public function hideYesNoButtons():void
		{
			hide(YES_BUTTON_ID);
			hide(NO_BUTTON_ID);
		}
		
		/**
		 * Sets the label and action for a button, and shows it.
		 * 
		 * @param	id		The id of the button to set and show.
		 * @param	label	The label displayed on the button.
		 * @param	action	The function to call when the button is clicked.
		 */
		public function setAndShow(id:int, label:String, action:Function, blink:Function = null):void
		{
			buttons[id].visible = true;
			buttons[id].label = label;
			buttons[id].action = action;
			buttons[id].blink = blink;
		}
		
		/**
		 * Hides the button with the given id.
		 * 
		 * @param	id	The id of the button to hide.
		 */
		public function hide(id:int):void
		{
			buttons[id].visible = false;
		}
		
		/**
		 * Shows the button with the given id.
		 * 
		 * @param	id	The id of the button to show.
		 */
		public function show(id:int):void
		{
			buttons[id].visible = true;
		}
		
		public function isVisible(id:int):Boolean
		{
			return buttons[id].visible;
		}
		
		private function setupButtons():void
		{
			yesButton.released = GameEngine.fileResourceManager.getResource("images/gui/GreenButtonReleased.png") as Bitmap;
			yesButton.pressed = GameEngine.fileResourceManager.getResource("images/gui/GreenButtonPressed.png") as Bitmap;
			yesButton.soundPath = "sounds/ui/BigClick.mp3";
			yesButton.hotkeyText = "(Space)\n";
			yesButton.addHotkey(Keyboard.SPACE);
			yesButton.addHotkey(Keyboard.ENTER);
			yesButton.label = "Yes";
			yesButton.x = 900;
			yesButton.y = 624;
			addChild(yesButton);
			
			noButton.released = GameEngine.fileResourceManager.getResource("images/gui/RedButtonReleased.png") as Bitmap;
			noButton.pressed = GameEngine.fileResourceManager.getResource("images/gui/RedButtonPressed.png") as Bitmap;
			noButton.soundPath = "sounds/ui/BigClick.mp3";
			noButton.hotkeyText = "(Shift)\n";
			noButton.addHotkey(Keyboard.SHIFT);
			noButton.addHotkey(Keyboard.BACKSPACE);
			noButton.label = "No";
			noButton.x = 900;
			noButton.y = 692;
			addChild(noButton);
			
			commonButton01.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton01.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton01.soundPath = "sounds/ui/Click.mp3";
			commonButton01.hotkeyText = "(Q)";
			commonButton01.addHotkey(Keyboard.Q);
			commonButton01.label = "Common 1";
			commonButton01.x = 312;
			commonButton01.y = 624;
			addChild(commonButton01);
			
			commonButton02.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton02.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton02.soundPath = "sounds/ui/Click.mp3";
			commonButton02.hotkeyText = "(W)";
			commonButton02.addHotkey(Keyboard.W);
			commonButton02.label = "Common 2";
			commonButton02.x = 456;
			commonButton02.y = 624;
			addChild(commonButton02);
			
			commonButton03.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton03.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton03.soundPath = "sounds/ui/Click.mp3";
			commonButton03.hotkeyText = "(E)";
			commonButton03.addHotkey(Keyboard.E);
			commonButton03.label = "Common 3";
			commonButton03.x = 598;
			commonButton03.y = 624;
			addChild(commonButton03);
			
			commonButton04.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton04.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton04.soundPath = "sounds/ui/Click.mp3";
			commonButton04.hotkeyText = "(R)";
			commonButton04.addHotkey(Keyboard.R);
			commonButton04.label = "Common 4";
			commonButton04.x = 742;
			commonButton04.y = 624;
			addChild(commonButton04);
			
			commonButton05.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton05.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton05.soundPath = "sounds/ui/Click.mp3";
			commonButton05.hotkeyText = "(A)";
			commonButton05.addHotkey(Keyboard.A);
			commonButton05.label = "Common 5";
			commonButton05.x = 312;
			commonButton05.y = 666;
			addChild(commonButton05);
			
			commonButton06.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton06.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton06.soundPath = "sounds/ui/Click.mp3";
			commonButton06.hotkeyText = "(S)";
			commonButton06.addHotkey(Keyboard.S);
			commonButton06.label = "Common 6";
			commonButton06.x = 456;
			commonButton06.y = 666;
			addChild(commonButton06);
			
			commonButton07.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton07.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton07.soundPath = "sounds/ui/Click.mp3";
			commonButton07.hotkeyText = "(D)";
			commonButton07.addHotkey(Keyboard.D);
			commonButton07.label = "Common 7";
			commonButton07.x = 598;
			commonButton07.y = 666;
			addChild(commonButton07);
			
			commonButton08.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton08.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton08.soundPath = "sounds/ui/Click.mp3";
			commonButton08.hotkeyText = "(F)";
			commonButton08.addHotkey(Keyboard.F);
			commonButton08.label = "Common 8";
			commonButton08.x = 742;
			commonButton08.y = 666;
			addChild(commonButton08);
			
			commonButton09.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton09.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton09.soundPath = "sounds/ui/Click.mp3";
			commonButton09.hotkeyText = "(Z)";
			commonButton09.addHotkey(Keyboard.Z);
			commonButton09.label = "Common 9";
			commonButton09.x = 312;
			commonButton09.y = 708;
			addChild(commonButton09);
			
			commonButton10.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton10.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton10.soundPath = "sounds/ui/Click.mp3";
			commonButton10.hotkeyText = "(X)";
			commonButton10.addHotkey(Keyboard.X);
			commonButton10.label = "Common 10";
			commonButton10.x = 456;
			commonButton10.y = 708;
			addChild(commonButton10);
			
			commonButton11.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton11.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton11.soundPath = "sounds/ui/Click.mp3";
			commonButton11.hotkeyText = "(C)";
			commonButton11.addHotkey(Keyboard.C);
			commonButton11.label = "Common 11";
			commonButton11.x = 598;
			commonButton11.y = 708;
			addChild(commonButton11);
			
			commonButton12.released = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonReleased.png") as Bitmap;
			commonButton12.pressed = GameEngine.fileResourceManager.getResource("images/gui/RegularButtonPressed.png") as Bitmap;
			commonButton12.soundPath = "sounds/ui/Click.mp3";
			commonButton12.hotkeyText = "(V)";
			commonButton12.addHotkey(Keyboard.V);
			commonButton12.label = "Common 12";
			commonButton12.x = 742;
			commonButton12.y = 708;
			addChild(commonButton12);
			
			systemButton01.released = GameEngine.fileResourceManager.getResource("images/gui/TopGrayButtonReleased.png") as Bitmap;
			systemButton01.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopGrayButtonPressed.png") as Bitmap;
			systemButton01.soundPath = "sounds/ui/Click.mp3";
			systemButton01.label = "System 1";
			systemButton01.x = 28;
			systemButton01.y = 16;
			addChild(systemButton01);
			
			systemButton02.released = GameEngine.fileResourceManager.getResource("images/gui/TopGrayButtonReleased.png") as Bitmap;
			systemButton02.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopGrayButtonPressed.png") as Bitmap;
			systemButton02.soundPath = "sounds/ui/Click.mp3";
			systemButton02.label = "System 2";
			systemButton02.x = 200;
			systemButton02.y = 16;
			addChild(systemButton02);
			
			systemButton03.released = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			systemButton03.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonPressed.png") as Bitmap;
			systemButton03.soundPath = "sounds/ui/Click.mp3";
			systemButton03.label = "System 3";
			systemButton03.x = 708;
			systemButton03.y = 16;
			addChild(systemButton03);
			
			systemButton04.released = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			systemButton04.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonPressed.png") as Bitmap;
			systemButton04.soundPath = "sounds/ui/Click.mp3";
			systemButton04.label = "System 4";
			systemButton04.x = 808;
			systemButton04.y = 16;
			addChild(systemButton04);
			
			systemButton05.released = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			systemButton05.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonPressed.png") as Bitmap;
			systemButton05.soundPath = "sounds/ui/Click.mp3";
			systemButton05.label = "System 5";
			systemButton05.x = 908;
			systemButton05.y = 16;
			addChild(systemButton05);
			
			buttons[SYSTEM_BUTTON_01_ID] = systemButton01;
			buttons[SYSTEM_BUTTON_02_ID] = systemButton02;
			buttons[SYSTEM_BUTTON_03_ID] = systemButton03;
			buttons[SYSTEM_BUTTON_04_ID] = systemButton04;
			buttons[SYSTEM_BUTTON_05_ID] = systemButton05;
			buttons[COMMON_BUTTON_01_ID] = commonButton01;
			buttons[COMMON_BUTTON_01_ID] = commonButton01;
			buttons[COMMON_BUTTON_02_ID] = commonButton02;
			buttons[COMMON_BUTTON_03_ID] = commonButton03;
			buttons[COMMON_BUTTON_04_ID] = commonButton04;
			buttons[COMMON_BUTTON_05_ID] = commonButton05;
			buttons[COMMON_BUTTON_06_ID] = commonButton06;
			buttons[COMMON_BUTTON_07_ID] = commonButton07;
			buttons[COMMON_BUTTON_08_ID] = commonButton08;
			buttons[COMMON_BUTTON_09_ID] = commonButton09;
			buttons[COMMON_BUTTON_10_ID] = commonButton10;
			buttons[COMMON_BUTTON_11_ID] = commonButton11;
			buttons[COMMON_BUTTON_12_ID] = commonButton12;
			buttons[YES_BUTTON_ID] = yesButton;
			buttons[NO_BUTTON_ID] = noButton;
			
			//  More settup for the zoom.
			//  first i assign the mask its dimentions and its temporary image.
			//  During the actual program, once placed in the right possition, this
			//  will be the image displayed. The reason i add the image is so that i have a 
			//  clipping mask the size of the display window. It could have been anything,
			//  but for simplisity sake i did this.
			zoomMask.released = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			zoomMask.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			zoomMask.x = TOP_AUX_WINDOW_X;
			zoomMask.y = TOP_AUX_WINDOW_Y;
			zoomMask.width = TOP_AUX_WINDOW_WIDTH;
			zoomMask.height = TOP_AUX_WINDOW_HEIGHT;
			
			//  Here I assign the zoom button its dimentions and its temporary image.
			//  remove the // if you want to test it.
			//  I had to comment out the images or else it would block the image behind it.
			//  In future code, when this is placed and hooked up that image will be the.
			//  proper image instead and i can redo some of the code here.
			
			//zoomButton.released = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			//zoomButton.pressed = GameEngine.fileResourceManager.getResource("images/gui/TopBrownButtonReleased.png") as Bitmap;
			zoomButton.x = TOP_AUX_WINDOW_X;
			zoomButton.y = TOP_AUX_WINDOW_Y;
			zoomButton.width = TOP_AUX_WINDOW_WIDTH;
			zoomButton.height = TOP_AUX_WINDOW_HEIGHT;
			
			//  Here i apply the mask to make sure the image doesn't display outside
			//  of its borders.
			zoomButton.mask = zoomMask;
			
			//  These are the event listeners i add, the first two are limited
			//  to clicking within the image display and call for their respective
			//  actions.
			zoomButton.addEventListener(MouseEvent.MOUSE_WHEEL, scrollHandeler);
			zoomButton.addEventListener(MouseEvent.MOUSE_DOWN, dragImage);
			
			//  These two listeners listen to the stage. This is not the most pretty
			//  way to do this, although it is one of the only ones that will always work.
			//  The first one lets you release the dragging of the image anywhere on the screen
			//  The second one releases the drag if you should move your mouse outside the window.
			addEventListener(MouseEvent.MOUSE_UP, stopImageDrag);
			addEventListener(MouseEvent.MOUSE_OUT, stopImageDrag);
			
			//  These two are used to handle the scaling up and down of the image.
			_scaleX = zoomButton.scaleX;
			_scaleY = zoomButton.scaleY;
			addChild(zoomButton);
		}
		
		//  as the name implies, this handles the scrolling and by extention, the scaling
		//  of the image.
		private function scrollHandeler(event:MouseEvent):void
		{
			//  delta is the direction of the scrolling(up/down)
			//  if down, its negative, if up it is positive    (dang that bad english right there!)
			//  I have limited the scale to 8 which is about 4x normal size
			if (event.delta > 0 && zoomButton.scaleX-1 < _scaleX+8 && zoomButton.scaleY-1 < _scaleY+8)
			{
				zoomButton.scaleX += 1;
				zoomButton.scaleY += 1; 
				_zoomAmount += 1;
			}
			//  same as before except opposite, this one makes sure the image is not scaled to small.
			else if(event.delta <0 && zoomButton.scaleX-1 > _scaleX && zoomButton.scaleY-1 > _scaleY)
			{
				zoomButton.scaleX -= 1;
				zoomButton.scaleY -= 1; 
				_zoomAmount -= 1;
				
				//  down here i do a small check to see if the image has somehow jumped out of its bountries.
				//  aka moved to the side, displaying whats under. If it has, i offset it so it hides whats beneath.
				if (zoomButton.x > TOP_AUX_WINDOW_X)
				{
					zoomButton.x = TOP_AUX_WINDOW_X;
				}
				if (zoomButton.x+zoomButton.width < TOP_AUX_WINDOW_X+TOP_AUX_WINDOW_WIDTH)
				{
					zoomButton.x +=((TOP_AUX_WINDOW_X + TOP_AUX_WINDOW_WIDTH) - (zoomButton.x + zoomButton.width));
				}
				if (zoomButton.y > TOP_AUX_WINDOW_Y)
				{
					zoomButton.y = TOP_AUX_WINDOW_Y;
				}
				if (zoomButton.y+zoomButton.height < TOP_AUX_WINDOW_Y+TOP_AUX_WINDOW_HEIGHT)
				{
					zoomButton.y += ((TOP_AUX_WINDOW_Y + TOP_AUX_WINDOW_HEIGHT) - (zoomButton.y + zoomButton.height));
				}
				
			}
		}
		
		//  This is the function for moving the image. I use the standard "start_drag" for its simplicity.
		private function dragImage(event:MouseEvent):void
		{
			//  First i create a rectangle which will be the restrictions of the drag.
			//  If i hadn't done this or done it incorrectly, you could drag the image away from the
			//  display area.
			var rect:Rectangle = new Rectangle();
			rect.x = TOP_AUX_WINDOW_X  - (zoomButton.width - TOP_AUX_WINDOW_WIDTH);
			rect.y = TOP_AUX_WINDOW_Y  - (zoomButton.height - TOP_AUX_WINDOW_HEIGHT);
			rect.width = zoomButton.width - TOP_AUX_WINDOW_WIDTH;
			rect.height = zoomButton.height - TOP_AUX_WINDOW_HEIGHT;
			zoomButton.startDrag(false,rect);
		}
		
		//  This is the function for stopping the drag, it is super simple.
		//  However with how the functionality is programmed right now, this function
		//  will be run a lot. Everytime the user clicks, this function gets called.
		//  Its ugly, but if needed i can always fix it.
		private function stopImageDrag(event:MouseEvent):void
		{
			zoomButton.stopDrag();
		}
		
		private function blink(e:Event):void
		{
			_blinkState = !_blinkState;
			for(var id:Object in buttons)
			{
				if(buttons[id].blink != null)
				{
					if(buttons[id].blink())
					{
						buttons[id].setBlinkState(_blinkState);
					}
					else
					{
						buttons[id].setBlinkState(false);
					}
				}
			}
		}
	
	}

}