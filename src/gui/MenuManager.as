package gui 
{
	import enums.MenuType;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class MenuManager 
	{
		private var _menus:Dictionary;
		private var _gameUI:Game;
		
		private var _mainScreenMenuId:String;
		private var _topAuxMenuId:String;
		private var _bottomAuxMenuId:String;
		
		public function MenuManager(game:Game) 
		{
			_gameUI = game;
			_menus = new Dictionary();
		}
		
		public function registerMenu(menu:Menu):void {
			if (menu.id == null || menu.id.length == 0 )
				throw new Error("Cannot add a menu with a null or empty id.");
			
			if (menu.id in _menus)
				throw new Error("A menu with the id \"" + menu.id + "\" has already been added to the menu manager.");
				
			_menus[menu.id] = menu;
			
			switch(menu.type.text)
			{
				case MenuType.MainScreen.text:
					menu.x = Game.MAIN_WINDOW_X;
					menu.y = Game.MAIN_WINDOW_Y;
					break;
					
				case MenuType.TopAuxScreen.text:
					menu.x = Game.TOP_AUX_WINDOW_X;
					menu.y = Game.TOP_AUX_WINDOW_Y;
					break;
					
				case MenuType.BottomAuxScreen.text:
					menu.x = Game.BOTTOM_AUX_WINDOW_X;
					menu.y = Game.BOTTOM_AUX_WINDOW_Y;
					break;
					
				default:
					break;
			}
			
			_gameUI.addChild(menu)
			menu.visible = false;
		}
		
		public function getMenuById(id:String):Menu
		{
			if (id == null || id.length == 0)
				return null;
			
			return _menus[id];
		}
		
		public function set mainScreenMenu(id:String):void
		{
			if (id == null || id.length == 0) return;
			var newMenu:Menu = _menus[id] as Menu;
			if (newMenu == null || newMenu.type != MenuType.MainScreen) return;
			if(_mainScreenMenuId != null && _menus[_mainScreenMenuId] != null)
			{
				var oldMenu:Menu = _menus[_mainScreenMenuId] as Menu;
				oldMenu.visible = false;
				oldMenu.onUnload();
			}
			newMenu.onLoad();
			newMenu.visible = true;
			_mainScreenMenuId = id;
		}
		
		public function get mainScreenMenu():String 
		{
			return _mainScreenMenuId;
		}
		
		public function set topAuxMenu(id:String):void
		{
			if(_topAuxMenuId != null && _menus[_topAuxMenuId] != null)
				_menus[_topAuxMenuId].visible = false;
				
			if (id == null || id.length == 0)
				return;
				
			var newMenu:Menu = _menus[id] as Menu;
			if (newMenu == null || newMenu.type != MenuType.TopAuxScreen)
				return;
			
			newMenu.onLoad();
			newMenu.visible = true;
			_topAuxMenuId = id;
		}
		
		public function get topAuxMenu():String 
		{
			return _topAuxMenuId;
		}
		
		public function set bottomAuxMenu(id:String):void
		{
			if(_bottomAuxMenuId != null && _menus[_bottomAuxMenuId] != null)
				_menus[_bottomAuxMenuId].visible = false;
			
			if (id == null || id.length == 0)
				return;
				
			var newMenu:Menu = _menus[id] as Menu;
			if (newMenu == null || newMenu.type != MenuType.BottomAuxScreen)
				return;
			
			newMenu.onLoad();
			newMenu.visible = true;
			_bottomAuxMenuId = id;
		}
		
		public function get bottomAuxMenu():String 
		{
			return _bottomAuxMenuId;
		}
	}
}