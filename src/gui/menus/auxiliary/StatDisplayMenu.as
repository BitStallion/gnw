package gui.menus.auxiliary
{
	import enums.CharacterStat;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import gui.Menu;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class StatDisplayMenu extends Menu
	{
		public static const ID:String = "Stat_Display";
		
		private var _stat:CharacterStat;
		private var _statImages:Dictionary;
		
		public function StatDisplayMenu()
		{
			super(ID, MenuType.TopAuxScreen);
			
			_statImages = new Dictionary();
			
			_statImages[CharacterStat.agility.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/AgilityIcon.png") as Bitmap;
			_statImages[CharacterStat.charisma.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/CharismaIcon.png") as Bitmap;
			_statImages[CharacterStat.dexterity.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/DexterityIcon.png") as Bitmap;
			_statImages[CharacterStat.endurance.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/EnduranceIcon.png") as Bitmap;
			_statImages[CharacterStat.intelligence.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/IntelligenceIcon.png") as Bitmap;
			_statImages[CharacterStat.strength.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/StrengthIcon.png") as Bitmap;
			_statImages[CharacterStat.willpower.text] = GameEngine.fileResourceManager.getResource("images/character/creation/stats/WillpowerIcon.png") as Bitmap;
			
			for each(var image:Bitmap in _statImages)
			{
				addChild(image);
				image.visible = false;
			}
		}
		
		public function get stat():CharacterStat
		{
			return _stat;
		}
		
		public function set stat(value:CharacterStat):void
		{
			if(_stat != null)
				(_statImages[_stat.text] as Bitmap).visible = false;
			
			_stat = value;
			
			if(_stat != null)
				(_statImages[_stat.text] as Bitmap).visible = true;
		}
	
	}

}