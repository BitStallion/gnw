package gui.menus.auxiliary 
{
	import data.Entity;
	import enums.MenuType;
	import gui.Menu;
	import flash.text.*;
	import gui.Game;
	
	/**
	 * Bottom auxilliary menu used to display item stats.
	 * 
	 * @author GnW Team
	 */
	public class ItemStatsMenu extends Menu 
	{
		public static const ID:String = "Item_Stats";
		
		private var textBox:TextField;
		private var _item:Entity;
		
		public function ItemStatsMenu() 
		{
			super(ID, MenuType.BottomAuxScreen);
			
			textBox = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			textBox.wordWrap = true;
			textBox.embedFonts = true;
			textBox.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.antiAliasType = AntiAliasType.ADVANCED;
			textBox.multiline = true;
			addChild(textBox);
			
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".bold {font-weight: bold;} \n .italic {font-style: italic;} \n .cost {font-size: 20;}");
			textBox.styleSheet = style;
		}
		
		public function get item():Entity 
		{
			return _item;
		}
		
		public function set item(value:Entity):void 
		{
			_item = value;
			generateText();
		}
		
		private function generateText():void
		{
			var text:String = "";
			
			// TODO Add other stats once items are converted to entities.
			text += "<span class='cost'>COST: " + item.variables["cost"] + "</span>\n\n";;
			text += "NAME: " + item.variables["name"] + "\n";
			
			// Waiting until combat system is changed to something more reasonable
			//if(item.hasTag("weapon"))
				//text += "DAMAGE: " + item.variables[EntityComponent.DAMAGE] + "\n";
			if (item.hasTag("armor"))
				text += "ARMOR: " + item.variables["armor"] + "\n";
			
			text += "DESC: " + item.variables["description"];
			
			textBox.htmlText = text;
		}
		
	}

}