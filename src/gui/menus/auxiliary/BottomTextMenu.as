package gui.menus.auxiliary 
{
	import enums.MenuType;
	import flash.display.Sprite;
	import flash.text.*;
	import gui.Game;
	import gui.Menu;
	import Assets;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class BottomTextMenu extends Menu 
	{
		public static const ID:String = "Bottom_Text";
		
		private var _text:String = "";
		
		private var textBox:TextField;
		
		public function BottomTextMenu() 
		{
			super(ID, MenuType.BottomAuxScreen);
			
			textBox = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			textBox.wordWrap = true;
			textBox.embedFonts = true;
			textBox.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.antiAliasType = AntiAliasType.ADVANCED;
			textBox.multiline = true;
			addChild(textBox);
		}
		
		override public function onLoad():void 
		{
			text = "";
		}
		
		public function get text():String 
		{
			return _text;
		}
		
		public function set text(value:String):void 
		{
			_text = value;
			textBox.text = value;
		}
	}
}