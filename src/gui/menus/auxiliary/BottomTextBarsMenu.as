package gui.menus.auxiliary 
{
	import enums.MenuType;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import gui.controls.Bar;
	import gui.Game;
	import gui.Menu;
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class BottomTextBarsMenu extends Menu
	{
		public static const ID:String = "Bottom_Text_Bars";	

		private var _bars:Vector.<Bar> = new Vector.<Bar>();
		private var _hGap:uint = 6;
		private var _text:String = "";
		private var _textField:TextField;
		
		public function BottomTextBarsMenu() 
		{
			super(ID, MenuType.BottomAuxScreen);
			_textField = new TextField();
			_textField.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			_textField.wordWrap = true;
			_textField.embedFonts = true;
			_textField.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			_textField.autoSize = TextFieldAutoSize.LEFT;
			_textField.antiAliasType = AntiAliasType.ADVANCED;
			_textField.multiline = true;
			addChild(_textField);
		}
		
		override public function onLoad():void 
		{
			//text = "";
		}
		
		public function get text():String 
		{
			return _text;
		}
		
		public function set text(value:String):void 
		{
			_text = value;
			_textField.text = value;
			resetLayout();
		}
		
		public function addBars(data:Object):void
		{
			var dataArray: Array = data as Array;
			for each (var dataItem:Object in dataArray)
			{
				var dataItemArray:Array = dataItem as Array;
				var bar:Bar = new Bar(Game.BOTTOM_AUX_WINDOW_WIDTH, dataItemArray[0], dataItemArray[1], dataItemArray[2]);
				
				if (dataItemArray.length > 3 && dataItemArray[3])
					bar.showMax = true;
				
				_bars.push(bar);
				addChild(bar);
			}
			resetLayout();
		}
		
		public function clearBars():void
		{
			for each (var bar:Bar in _bars)
			{
				removeChild(bar);
			}
			_bars = new Vector.<Bar>();
		}
		
		private function resetLayout():void
		{
			var H:uint = _textField.height;
			for (var i:uint = 0; i < _bars.length; i++)
			{
				_bars[i].y = H;
				H += _bars[i].height + 6;
			}
		}
		
	}

}