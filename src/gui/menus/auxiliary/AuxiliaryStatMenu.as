package gui.menus.auxiliary 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECDynamicNumber;
	import data.EntityComponents.ECExperience;
	import enums.MenuType;
	import flash.text.*;
	import flash.utils.Dictionary;
	import gui.controls.Bar;
	import gui.Menu;
	import gui.Game;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class AuxiliaryStatMenu extends Menu 
	{
		public static const ID:String = "Aux_Stat_Menu";
		
		private var _bars:Dictionary = new Dictionary();
		private var _nameField:TextField;
		private var _lvlField:TextField;
		private var _character:Entity;
		
		public function AuxiliaryStatMenu() 
		{
			super(ID, MenuType.BottomAuxScreen);
			
			_nameField = new TextField();
			_nameField.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			_nameField.wordWrap = true;
			_nameField.embedFonts = true;
			_nameField.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			_nameField.autoSize = TextFieldAutoSize.LEFT;
			_nameField.antiAliasType = AntiAliasType.ADVANCED;
			addChild(_nameField);
			
			_lvlField = new TextField();
			_lvlField.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			_lvlField.wordWrap = true;
			_lvlField.embedFonts = true;
			_lvlField.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			_lvlField.autoSize = TextFieldAutoSize.LEFT;
			_lvlField.antiAliasType = AntiAliasType.ADVANCED;
			_lvlField.multiline = true;
			_lvlField.x = Game.BOTTOM_AUX_WINDOW_WIDTH - 50;
			addChild(_lvlField);
			
			var dataArray:Array = new Array ("Juice", "Lust", "Vigor", "Spirit", "XP");
			var H:uint = 25;
			for each (var item:String in dataArray)
			{
				var bar:Bar = new Bar(Game.BOTTOM_AUX_WINDOW_WIDTH, item, 0, 100);
				_bars[item] = bar;
				bar.y = H;
				H += bar.height + 6;
				addChild(bar);
			}
		}
		
		override public function onLoad():void 
		{
			update();
		}
		
		public function set character(value:Entity):void 
		{
			_character = value;
			update();
		}
		
		public function update():void
		{
			if (_character == null) return;
			
			var exp:ECExperience = _character.getComponent(EntityComponent.EXPERIENCE) as ECExperience;
			var dynNum:ECDynamicNumber;
			
			_nameField.text = _character.variables["name"];
			_lvlField.text = "Lvl " + _character.variables["level"];
			
			dynNum = _character.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			(_bars["Juice"] as Bar).virtualLength = dynNum.value;
			(_bars["Juice"] as Bar).maxVirtualLength = dynNum.max;
			(_bars["Juice"] as Bar).showMax = true;
			
			dynNum = _character.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			(_bars["Lust"] as Bar).virtualLength = dynNum.value;
			(_bars["Lust"] as Bar).maxVirtualLength = dynNum.max;
			(_bars["Lust"] as Bar).showMax = true;
			
			dynNum = _character.getComponent(EntityComponent.VIGOR) as ECDynamicNumber;
			(_bars["Vigor"] as Bar).virtualLength = dynNum.value;
			(_bars["Vigor"] as Bar).maxVirtualLength = dynNum.max;
			(_bars["Vigor"] as Bar).showMax = true;
			
			dynNum = _character.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
			(_bars["Spirit"] as Bar).virtualLength = dynNum.value;
			(_bars["Spirit"] as Bar).maxVirtualLength = dynNum.max;
			(_bars["Spirit"] as Bar).showMax = true;
			
			(_bars["XP"] as Bar).virtualLength = exp.experience;
			(_bars["XP"] as Bar).maxVirtualLength = exp.experienceNextLevel;
			(_bars["XP"] as Bar).showMax = true;
		}
		
	}

}