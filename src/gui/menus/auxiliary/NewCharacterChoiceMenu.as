package gui.menus.auxiliary 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECAttributes;
	import enums.*;
	import flash.display.Sprite;
	import flash.text.*;
	import gui.Menu;
	import gui.Game;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class NewCharacterChoiceMenu extends Menu 
	{
		public static const ID:String = "New_Character_Choice";
		
		private var cursor:Sprite;
		
		private var _name:String;
		private var _gender:Gender;
		private var _race:Race;
		private var _body:BodyType;
		
		private var textBox:TextField;
		
		public function NewCharacterChoiceMenu() 
		{
			super(ID, MenuType.BottomAuxScreen);
			
			textBox = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 20, Colors.LASER_GREEN);
			textBox.wordWrap = true;
			textBox.embedFonts = true;
			textBox.width = Game.BOTTOM_AUX_WINDOW_WIDTH;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.antiAliasType = AntiAliasType.ADVANCED;
			textBox.multiline = true;
			textBox.x = 10;
			textBox.y = 10;
			addChild(textBox);
		}
		
		
		override public function onLoad():void 
		{
			textBox.text = 	"";
		}
		
		public function clear():void
		{
			textBox.text = "";
		}
		
		public function set addName(value:String):void 
		{
			_name = value;
			textBox.appendText("NAME:\t" + _name + "\n");
		}
		
		public function set addRace(value:Race):void 
		{
			_race = value;
			textBox.appendText("RACE:\t" + (_race != null ? _race.text : "") + "\n");
		}
		
		public function set addGender(value:Gender):void 
		{
			_gender = value;
			textBox.appendText("GENDER:\t" + (_gender != null ? _gender.text : "") + "\n");
		}
		
		public function set addBody(value:BodyType):void 
		{
			_body = value;
			textBox.appendText("BODY:\t" + (_body != null ? _body.text : "") + "\n");
		}
		
		public function addStats(char:Entity):void
		{
			for each(var stat:Enum in Utils.EnumList(CharacterStat))
			{
				var charStat:CharacterStat = stat as CharacterStat;
				
				textBox.appendText("\n" + stat.text.substr(0, 3).toUpperCase() + ":\t\t" + (char.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes)[charStat.text]);
			}
		}
	}

}