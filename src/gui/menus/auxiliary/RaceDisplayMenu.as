package gui.menus.auxiliary 
{
	import enums.Race;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import gui.Menu;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class RaceDisplayMenu extends Menu 
	{
		public static const ID:String = "Race_Display";
		
		private var _race:Race;
		private var _raceImages:Dictionary;
		
		public function RaceDisplayMenu() 
		{
			super(ID, MenuType.TopAuxScreen);
			
			_raceImages = new Dictionary();
			
			_raceImages["Blank"] = GameEngine.fileResourceManager.getResource("images/character/creation/races/BlankRace.png") as Bitmap;
			_raceImages[Race.Dwarf.text] = GameEngine.fileResourceManager.getResource("images/character/creation/races/DwarfRace.png") as Bitmap;
			_raceImages[Race.Elf.text] = GameEngine.fileResourceManager.getResource("images/character/creation/races/ElfRace.png") as Bitmap;
			_raceImages[Race.Goblin.text] = GameEngine.fileResourceManager.getResource("images/character/creation/races/GoblinRace.png") as Bitmap;
			_raceImages[Race.Human.text] = GameEngine.fileResourceManager.getResource("images/character/creation/races/HumanRace.png") as Bitmap;
			_raceImages[Race.Ork.text] = GameEngine.fileResourceManager.getResource("images/character/creation/races/OrkRace.png") as Bitmap;
			
			for each (var image:Bitmap in _raceImages) {
				addChild(image);
				image.visible = false;
			}
			
			(_raceImages["Blank"] as Bitmap).visible = true;
		}
		
		override public function onLoad():void 
		{
			race = null;
		}
		
		public function get race():Race 
		{
			return _race;
		}
		
		public function set race(value:Race):void 
		{
			if (_race == null)
				(_raceImages["Blank"] as Bitmap).visible = false;
			else
				(_raceImages[_race.text] as Bitmap).visible = false;
				
			_race = value;
			
			if (_race == null)
				(_raceImages["Blank"] as Bitmap).visible = true;
			else
				(_raceImages[_race.text] as Bitmap).visible = true;
		}
		
	}

}