package gui.menus.auxiliary
{
	import enums.BodyType;
	import enums.Gender;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import gui.Menu;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class BodyTypeDisplayMenu extends Menu
	{
		public static const ID:String = "Body_Type_Display";
		
		private var _bodyType:BodyType;
		private var _bodyTypeImages:Dictionary;
		
		public function BodyTypeDisplayMenu()
		{
			super(ID, MenuType.TopAuxScreen);
			
			_bodyTypeImages = new Dictionary();
			
			_bodyTypeImages[BodyType.Chubby.text] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/ChubbyBodyType.png") as Bitmap;
			_bodyTypeImages[BodyType.Husky.text] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/HuskyBodyType.png") as Bitmap;
			_bodyTypeImages[BodyType.Muscular.text] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/MuscularBodyType.png") as Bitmap;
			_bodyTypeImages[BodyType.Normal.text] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/NormalBodyType.png") as Bitmap;
			_bodyTypeImages[BodyType.Thin.text] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/ThinBodyType.png") as Bitmap;
			_bodyTypeImages["None"] = GameEngine.fileResourceManager.getResource("images/character/creation/bodyTypes/UnknownBodyType.png") as Bitmap;
			
			for each(var image:Bitmap in _bodyTypeImages)
			{
				addChild(image);
				image.visible = false;
			}
			
			(_bodyTypeImages["None"] as Bitmap).visible = true;
		}
		
		public function get bodyType():BodyType
		{
			return _bodyType;
		}
		
		public function set bodyType(value:BodyType):void
		{
			if(_bodyType == null)
				(_bodyTypeImages["None"] as Bitmap).visible = false;
			else
				(_bodyTypeImages[_bodyType.text] as Bitmap).visible = false;
			
			_bodyType = value;
			
			if(_bodyType == null)
				(_bodyTypeImages["None"] as Bitmap).visible = true;
			else
				(_bodyTypeImages[_bodyType.text] as Bitmap).visible = true;
		}
	
	}

}