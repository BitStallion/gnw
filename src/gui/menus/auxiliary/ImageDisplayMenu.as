package gui.menus.auxiliary 
{
	import enums.Gender;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.utils.Dictionary;
	import gui.Menu;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ImageDisplayMenu extends Menu 
	{
		public static const ID:String = "Image_Display";
		
		private var _image:Bitmap;
		
		public function ImageDisplayMenu() 
		{
			super(ID, MenuType.TopAuxScreen);
		}
		
		override public function onLoad():void 
		{
			image = null;
		}
		
		public function get image():Bitmap
		{
			return _image;
		}
		
		public function set image(value:Bitmap):void 
		{
			if(_image != null)
				removeChild(_image);
			
			_image = value;
			
			if(_image != null)
				addChild(_image);
		}
		
	}

}