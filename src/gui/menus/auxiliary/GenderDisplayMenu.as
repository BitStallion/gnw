package gui.menus.auxiliary
{
	import enums.Gender;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import gui.Menu;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class GenderDisplayMenu extends Menu
	{
		public static const ID:String = "Gender_Display";
		
		private var _gender:Gender;
		private var _genderImages:Dictionary;
		
		public function GenderDisplayMenu()
		{
			super(ID, MenuType.TopAuxScreen);
			
			_genderImages = new Dictionary();
			
			_genderImages["Neuter"] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoNeuter.png") as Bitmap;
			_genderImages[Gender.Female.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoFemale.png") as Bitmap;
			_genderImages[Gender.Femboy.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoFemboy.png") as Bitmap;
			_genderImages[Gender.Shemale.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoFuta.png") as Bitmap;
			_genderImages[Gender.Herm.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoHerm.png") as Bitmap;
			_genderImages[Gender.Male.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoMale.png") as Bitmap;
			_genderImages[Gender.Cuntboy.text] = GameEngine.fileResourceManager.getResource("images/character/creation/EmbryoCuntboy.png") as Bitmap;
			
			for each(var image:Bitmap in _genderImages)
			{
				addChild(image);
				image.visible = false;
			}
			
			(_genderImages["Neuter"] as Bitmap).visible = true;
		}
		
		public function get gender():Gender
		{
			return _gender;
		}
		
		public function set gender(value:Gender):void
		{
			if(_gender == null)
				(_genderImages["Neuter"] as Bitmap).visible = false;
			else
				(_genderImages[_gender.text] as Bitmap).visible = false;
			
			_gender = value;
			
			if(_gender == null)
				(_genderImages["Neuter"] as Bitmap).visible = true;
			else
				(_genderImages[_gender.text] as Bitmap).visible = true;
		}
	
	}

}