package gui.menus
{
	import combat.ability.special.UseItemAbility;
	import combat.CombatEvent;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECInventory;
	import data.entityEvents.EntityEvent;
	import enums.*;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import gui.controls.ConfigurableButton;
	import gui.menus.auxiliary.*;
	import gui.*;
	import gui.menus.components.*;
	import gui.PropertyChangedEvent;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class InventoryMenu extends Menu
	{
		public static const ID:String = "Inventory_menu";
		
		private const allMode:String = "all";
		private const equipmentMode:String = "equipable";
		private const consumableMode:String = "consumable";
		private const questItemMode:String = "questItem";
		
		private var description:TextField;
		private var itemSelector:ItemList;
		
		private var _character:Entity;
		private var _selectedItem:Entity;
		private var _currentMode:String;
		
		private var _armorSlot:EquipmentSlotBar;
		 private var _weaponSlot:EquipmentSlotBar;
		
		public function InventoryMenu()
		{
			super(ID, MenuType.MainScreen);
			
			description = new TextField();
			description.defaultTextFormat = new TextFormat("Bombard", 20, Colors.LASER_GREEN);
			description.wordWrap = true;
			description.embedFonts = true;
			description.width = Game.MAIN_WINDOW_WIDTH-310;
			description.autoSize = TextFieldAutoSize.LEFT;
			description.antiAliasType = AntiAliasType.ADVANCED;
			description.multiline = true;
			description.x = 300;
			description.y = 20;
			addChild(description);
			
			_armorSlot = new EquipmentSlotBar("Armor");
			_armorSlot.x = 300;
			_armorSlot.y = 400;
			addChild(_armorSlot);
			
			_weaponSlot = new EquipmentSlotBar("Weapon");
			_weaponSlot.x = 300;
			_weaponSlot.y = 300;
			addChild(_weaponSlot);
			
			itemSelector = new ItemList(265, Game.MAIN_WINDOW_HEIGHT - 20);
			itemSelector.x = 10;
			itemSelector.y = 10;
			addChild(itemSelector);
			
			itemSelector.addEventListener(ItemList.ItemChanged, itemChanged);
		}
		
		private function itemChanged(e:PropertyChangedEvent):void
		{
			selectedItem = e.newValue as Entity;
		}
		
		override public function onLoad():void
		{
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = AuxiliaryStatMenu.ID;
			
			if(GameEngine.world != null)
			{
				(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = GameEngine.world.player;
			}
			
			setMode(allMode)(null);
			updateEquipmentSlots();
		}
		
		private function setMode(mode:String):Function
		{
			return function(e:Event):void
			{
				_currentMode = mode;
				
				var inv:ECInventory = _character.getComponent(EntityComponent.INVENTORY) as ECInventory;
				
				switch(mode)
				{
					case allMode: 
					{
						if(_character != null)
							itemSelector.items = inv.allEntities();
						break;
					}
					case equipmentMode: 
					{
						if(_character != null)
							itemSelector.items = inv.allEntitiesWithTag(equipmentMode);
						break;
					}
					case consumableMode: 
					{
						if(_character != null)
							itemSelector.items = inv.allEntitiesWithTag(consumableMode);
						break;
					}
					case questItemMode: 
					{
						if(_character != null)
							itemSelector.items = inv.allEntitiesWithTag(questItemMode);
						break;
					}
				}
				
				if(selectedItem == null || (mode != allMode && !selectedItem.hasTag(mode)))
				{
					selectedItem = null;
				}
				else
				{
					itemSelector.selectedItem = selectedItem;
				}
			};
		}
		
		private function equipSelected(event:Event):void
		{
			(_character.getComponent(EntityComponent.INVENTORY) as ECInventory).equipEntity(_selectedItem);
			_character.applyEffects();
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
			updateEquipmentSlots();
			refreshItemList();
		}
		
		private function discardSelected(event:Event):void
		{
			var inv:ECInventory = _character.getComponent(EntityComponent.INVENTORY) as ECInventory;
			inv.removeEntity(_selectedItem);
			_selectedItem.destroy();
			_character.applyEffects();
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
			updateEquipmentSlots();
			selectedItem = null;
			refreshItemList();
		}
		
		private function unequipSelected(event:Event):void
		{
			(_character.getComponent(EntityComponent.INVENTORY) as ECInventory).unequipEntity(_selectedItem);
			_character.applyEffects();
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
			updateEquipmentSlots();
			refreshItemList();
		}
		
		private function useSelected(event:Event):void
		{
			if(_selectedItem==null||_character==null)
				return;
			
			if(_selectedItem.hasTag(consumableMode))
			{
				if(GameEngine.currentBattle == null)
				{
					var useEvent:EntityEvent = new EntityEvent(EntityEvent.onUse);
					useEvent.target = _character;
					_selectedItem.dispatchEvent(useEvent);
					_character.applyEffects();
					(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
					refreshItemList();
				}
				else
				{
					var e:CombatEvent = new CombatEvent(CombatEvent.SELECTED);
					var ablility:UseItemAbility = new UseItemAbility(_selectedItem);
					ablility.owner = _character;
					e.selected = ablility;
					GameEngine.endCurrentState();
					GameEngine.currentBattle.selectedEventHandler(e);
				}
			}
		}
		
		private function refreshItemList():void
		{
			if(_character!=null)
			{
				var inv:ECInventory = _character.getComponent(EntityComponent.INVENTORY) as ECInventory;
				switch(_currentMode)
				{
					case allMode: 
					{
						itemSelector.items = inv.allEntities();
						break;
					}
					case equipmentMode: 
					{
						itemSelector.items = inv.allEntitiesWithTag(equipmentMode);
						break;
					}
					case consumableMode: 
					{
						itemSelector.items = inv.allEntitiesWithTag(consumableMode);
						break;
					}
					case questItemMode: 
					{
						itemSelector.items = inv.allEntitiesWithTag(questItemMode);
						break;
					}
				}
				if(selectedItem == null || !inv.containsEntity(selectedItem)) 
				{
					selectedItem = null;
				}
				else
				{
					itemSelector.selectedItem = selectedItem;
				}
			}
		}
		
		private function updateEquipmentSlots():void
		{
			if (_character != null)
			{
				var inv:ECInventory = _character.getComponent(EntityComponent.INVENTORY) as ECInventory;
				_armorSlot.item = inv.getEntityInSlot(EquipmentSlot.armor.text);
				_weaponSlot.item = inv.getEntityInSlot(EquipmentSlot.weapon.text);
			}
		}
		
		public function set character(value:Entity):void
		{
			_character = value;
			updateEquipmentSlots();
			refreshItemList();
		}
		
		public function get selectedItem():Entity
		{
			return _selectedItem;
		}
		
		public function set selectedItem(value:Entity):void
		{
			_selectedItem = value;
			description.text = _selectedItem ? _selectedItem.variables["description"] : "";
			GameEngine.gui.hideCommonButtons();
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "All Items", setMode(allMode));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Equipment", setMode(equipmentMode));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Consumables", setMode(consumableMode));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_04_ID, "Quest Items", setMode(questItemMode));
			if(_selectedItem != null)
			{
				(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = GameEngine.fileResourceManager.getResource(value.variables["image"]) as Bitmap;
				var nextButtonID:int = Game.COMMON_BUTTON_05_ID;
				if(GameEngine.currentBattle == null && value.hasTag(equipmentMode))
				{
					if(value.variables["equipedSlot"] == null)
					{
						GameEngine.gui.setAndShow(nextButtonID, "Equip Item", equipSelected);
						nextButtonID++;
					}
					else
					{
						GameEngine.gui.setAndShow(nextButtonID, "Unequip Item", unequipSelected);
						nextButtonID++;
					}
				}
				if(value.hasTag(consumableMode))
				{
					GameEngine.gui.setAndShow(nextButtonID, "Use Item", useSelected);
					nextButtonID++;
				}
				if(GameEngine.currentBattle == null && !value.hasTag(questItemMode))
				{
					GameEngine.gui.setAndShow(Game.COMMON_BUTTON_12_ID, "Discard Item", discardSelected);
					nextButtonID++;
				}
			}
			else
			{
				(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = null;
			}
		}
	}

}