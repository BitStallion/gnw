package gui.menus
{
	import data.Action;
	import data.ParsingDataContextBuilder;
	import enums.MenuType;
	import enums.TriggerType;
	import events.Command;
	import events.OldEventManager;
	import events.OldGameEvent;
	import events.Link;
	import events.Scene;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import gui.Game;
	import gui.Menu;
	import gui.controls.ScrollBar;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import gui.menus.auxiliary.ImageDisplayMenu;
	import parsing.parse;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class OldEventMenu extends Menu
	{
		public static const ID:String = "OldEvent_Menu";
		
		private var scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		
		private var _text:String = "";
		private var _eventManager:OldEventManager;
		private var _textBox:TextField;
		private var content:Sprite;
		
		public function OldEventMenu(eventManager:OldEventManager)
		{
			super(ID, MenuType.MainScreen);
			
			_eventManager = eventManager;
			
			content = new Sprite();
			
			_textBox = new TextField();
			_textBox.defaultTextFormat = new TextFormat("Calibri", 18, Colors.LASER_GREEN);
			_textBox.wordWrap = true;
			_textBox.x = 5;
			_textBox.width = Game.MAIN_WINDOW_WIDTH - 21;
			_textBox.autoSize = TextFieldAutoSize.LEFT;
			_textBox.multiline = true;
			_textBox.antiAliasType = AntiAliasType.ADVANCED;
			content.addChild(_textBox);
			
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".bold {font-weight: bold;} \n .italic {font-style: italic;}");
			_textBox.styleSheet = style;
			
			content.graphics.beginFill(0, 0);
			content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			content.graphics.endFill();
			addChild(content);
			
			scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			scrollBar.container = new Sprite();
			scrollBar.container.graphics.beginFill(0);
			scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			scrollBar.container.graphics.endFill();
			scrollBar.container.visible = false;
			addChild(scrollBar.container);
			addChild(scrollBar);
			scrollBar.content = content;
		}
		
		override public function onLoad():void
		{
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			if(GameEngine.world != null)
			{
				(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = GameEngine.world.player;
			}
			else
			{
				GameEngine.menuManager.bottomAuxMenu = null;
			}
			
			text = "";
			
			if(_eventManager.currentScene != null)
				loadScene(_eventManager.currentScene);
		}
		
		/**
		 * The text that is displayed in the main view.
		 */
		public function get text():String
		{
			return _text;
		}
		
		/**
		 * The text that is displayed in the main view.
		 */
		public function set text(value:String):void
		{
			_text = value;
			_textBox.htmlText = value;
			scrollBar.update();
			scrollBar.resetScroll();
		}
		
		public function startEventById(id:String):void
		{
			var event:OldGameEvent = _eventManager.getEventById(id);
			startEvent(event);
		}
		
		public function startEvent(event:OldGameEvent):void
		{
			if(event == null)
				throw new Error("Argument exception: Cannot start null event.");
			
			loadScene(event.startingScene);
		}
		
		/**
		 * Loads the scene specified into the event menu. Parsing of the event text is done here.
		 *
		 * @param	scene	The scene to be loaded.
		 */
		private function loadScene(scene:Scene):void
		{
			text = parse(scene.text, GameEngine.getCurrentParsingContext());
			if(scene.imageName != null)
			{
				(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = GameEngine.fileResourceManager.getResource(scene.imageName) as Bitmap;
			}
			setupButtons(scene);
		}
		
		private function setupButtons(scene:Scene):void
		{
			GameEngine.gui.hideCommonButtons();
			GameEngine.gui.hideYesNoButtons();
			
			var commands:Vector.<Command> = scene.commands;
			
			var i:int;
			
			for(i = 0; i < commands.length; i++)
			{
				GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID + i, commands[i].text, commands[i].invokeActionsForEvent);
			}
			
			var links:Vector.<Link> = scene.validLinks(GameEngine.getCurrentDataContext());
			
			if(links.length == 0 && commands.length == 0)
				GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", endEvent);
			else if(links.length == 1)
				GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID + commands.length, links[0].text || "Next", gotoScene(links[0]));
			else
			{
				for(i = 0; i < links.length; i++)
				{
					GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID + i + commands.length, links[i].text, gotoScene(links[i]));
				}
			}
			
			if(scene.exitText != null && scene.exitText.length > 0)
				GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, scene.exitText, endEvent);
		}
		
		private function endEvent(e:Event):void
		{
			_eventManager.currentScene = null;
			GameEngine.endCurrentState();
		}
		
		private function gotoScene(link:Link):Function
		{
			return function(e:Event):void
			{
				var event:OldGameEvent;
				var scene:Scene;
				if(link.eventId == null || link.eventId.length == 0)
				{
					var nextScene:Scene = _eventManager.currentEvent.getSceneById(link.sceneId);
					_eventManager.currentScene = nextScene;
					loadScene(nextScene);
				}
				else if(link.sceneId == null || link.sceneId.length == 0)
				{
					event = _eventManager.getEventById(link.eventId);
					if(event != null)
					{
						scene = event.startingScene;
						_eventManager.currentScene = scene;
						loadScene(scene);
					}
				}
				else
				{
					event = _eventManager.getEventById(link.eventId);
					if(event != null)
					{
						scene = event.getSceneById(link.sceneId);
						_eventManager.currentScene = scene;
						loadScene(scene);
					}
				}
			}
		}
	
	}
}