package gui.menus 
{
	import data.Entity;
	import data.character.Ass;
	import data.EntityComponent;
	import data.EntityComponents.ECAttributes;
	import data.ParsingDataContextBuilder;
	import data.time.DateTime;
	import enums.*;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.*;
	import flash.utils.Dictionary;
	import gui.Menu;
	import gui.Game;
	import parsing.parse;
	import system.GameWorld;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ParseTestMenu extends Menu 
	{
		public static const ID:String = "Parse_Test_Menu";
		
		private var unparsedtext:TextField;
		private var parsedtext:TextField;
		
		public function ParseTestMenu() 
		{
			super(ID, MenuType.MainScreen);
		
			unparsedtext = new TextField();
			unparsedtext.defaultTextFormat = new TextFormat("Calibri", 18, Colors.LASER_GREEN);
			unparsedtext.wordWrap = true;
			unparsedtext.x = 5;
			unparsedtext.y = 5;
			unparsedtext.width = Game.MAIN_WINDOW_WIDTH - 10;
			unparsedtext.height = Game.MAIN_WINDOW_HEIGHT / 2 - 10;
			unparsedtext.multiline = true;
			unparsedtext.antiAliasType = AntiAliasType.ADVANCED;
			unparsedtext.border = true;
			unparsedtext.borderColor = 0xffffff;
			unparsedtext.type = TextFieldType.INPUT;
			addChild(unparsedtext);
			
			parsedtext = new TextField();
			parsedtext.defaultTextFormat = new TextFormat("Calibri", 18, Colors.LASER_GREEN);
			parsedtext.wordWrap = true;
			parsedtext.x = 5;
			parsedtext.y = Game.MAIN_WINDOW_HEIGHT / 2 + 5;
			parsedtext.width = Game.MAIN_WINDOW_WIDTH - 10;
			parsedtext.height = Game.MAIN_WINDOW_HEIGHT / 2 - 10;
			parsedtext.multiline = true;
			parsedtext.antiAliasType = AntiAliasType.ADVANCED;
			parsedtext.border = true;
			parsedtext.borderColor = 0xffffff;
			addChild(parsedtext);
			
		}
		
		override public function onLoad():void 
		{
			GameEngine.menuManager.bottomAuxMenu = null;
			GameEngine.menuManager.topAuxMenu = null;
			
			GameEngine.gui.hideAll();
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Parse", parseText);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", toDebugMenu);
		}
		
		private function parseText(e:Event):void
		{
			var context:Object = constructDataContext();
			
			parsedtext.text = parse(unparsedtext.text, context);
		}
		
		private function constructDataContext():Object
		{
			var testWorld:GameWorld = new GameWorld();
			testWorld.player = GameEngine.dataManager.createCharacterFromTemplate("player", Race.Human, Gender.Herm, BodyType.Normal);
			testWorld.player.variables["name"] = "Test Player";
			var status:ECAttributes = testWorld.player.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
			for each(var stat:CharacterStat in Utils.EnumList(CharacterStat))
			{
				status[stat.text] = Math.floor(Math.random() * 10) + 5;
			}
			testWorld.gameTime = new DateTime(1980, 3, 14);
			testWorld.variables = new Dictionary();
			
			var builder:ParsingDataContextBuilder = new ParsingDataContextBuilder(testWorld);
			
			return builder.buildContext();
		}
		
		private function toDebugMenu(e:Event):void {
			GameEngine.menuManager.mainScreenMenu = DebugMenu.ID;
		}
		
	}

}