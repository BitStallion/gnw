package gui.menus
{
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import gui.Game;
	import gui.Menu;
	import flash.text.*;
	import system.GlobalConstants;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class TitleMenu extends Menu
	{
		public static const ID:String = "Title_Menu";
		
		private var _titleBoards:Vector.<Bitmap>;
		private var _banner:Bitmap;
		
		public function TitleMenu()
		{
			super(ID, MenuType.MainScreen);
			
			_banner = GameEngine.fileResourceManager.getResource("images/title/TitleBanner.png") as Bitmap;
			_banner.x = (Game.MAIN_WINDOW_WIDTH - _banner.width) / 2;
			_banner.y = 5;
			addChild(_banner);
			
			addTitleBoards();
		}
		
		private function addTitleBoards():void
		{
			_titleBoards = new Vector.<Bitmap>();
			var list:Array = GameEngine.fileResourceManager.getAllStartingWith("images/title/board/");
			for(var i:int = 0; i < list.length; i++)
			{
				_titleBoards.push(GameEngine.fileResourceManager.getResource(list[i]));
			}
			
			selectTitleBoard();
		}
		
		private function selectTitleBoard():void
		{
			var rand:int = Math.floor(_titleBoards.length * Math.random());
			
			_titleBoards[rand].x = (Game.MAIN_WINDOW_WIDTH - _titleBoards[rand].width) / 2;
			_titleBoards[rand].y = _banner.height + (Game.MAIN_WINDOW_HEIGHT - _banner.height - _titleBoards[rand].height) / 2
			addChild(_titleBoards[rand]);
		}
		
		public override function onLoad():void
		{
			selectTitleBoard();
			
			GameEngine.menuManager.bottomAuxMenu = null;
			GameEngine.menuManager.topAuxMenu = null;
		}
	}
}