package gui.menus 
{
	import enums.MenuType;
	import flash.events.Event;
	import flash.text.*;
	import gui.*;
	import gui.controls.Slider;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class OptionsMenu extends Menu
	{
		public static const ID:String = "Options_Menu"
		
		private var _soundFXSlider:Slider;
		private var _musicSlider:Slider;
		
		private var _soundLevel:Number;
		private var _musicLevel:Number;
		
		private var soundLabel:TextField;
		private var musicLabel:TextField;
		
		private var soundVolume:TextField;
		private var musicVolume:TextField;
		
		public function OptionsMenu() 
		{
			super(ID, MenuType.MainScreen);
			
			soundLabel = setupTextfield(30);
			soundLabel.text = "Sound Volume";
			soundLabel.x = 25;
			soundLabel.y = 50;
			addChild(soundLabel);
			
			musicLabel = setupTextfield(30);
			musicLabel.text = "Music Volume"
			musicLabel.x = 25;
			musicLabel.y = 100;
			addChild(musicLabel);
			
			soundVolume = setupTextfield(30);
			soundVolume.text = "100";
			soundVolume.x = 580;
			soundVolume.y = 50;
			addChild(soundVolume);
			
			musicVolume = setupTextfield(30);
			musicVolume.text = "100"
			musicVolume.x = 580;
			musicVolume.y = 100;
			addChild(musicVolume);
			
			_soundFXSlider = new Slider(200, 0, 100, 9);
			_soundFXSlider.x = 300;
			_soundFXSlider.y = 55;
			addChild(_soundFXSlider);
			
			_soundFXSlider.soundPath = "sounds/ui/LowBeep.mp3";
			_soundFXSlider.addEventListener(PropertyChangedEvent.Changed, onSoundFXChange);
			
			_musicSlider = new Slider(200, 0, 100, 9);
			_musicSlider.x = 300;
			_musicSlider.y = 105;
			addChild(_musicSlider);
			
			_musicSlider.soundPath = "sounds/ui/LowBeep.mp3";
			_musicSlider.addEventListener(PropertyChangedEvent.Changed, onMusicChange);
		}
		
		private function setupTextfield(size:uint):TextField
		{
			var textField:TextField = new TextField();
			textField.defaultTextFormat = new TextFormat("Bombard", size, Colors.LASER_GREEN);
			textField.embedFonts = true;
			textField.autoSize = TextFieldAutoSize.LEFT;
			textField.antiAliasType = AntiAliasType.ADVANCED;
			textField.selectable = false;
			return textField;
		}
		
		override public function onLoad():void 
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Save", function(e:Event):void
			{
				GameEngine.soundManager.soundFXLevel = _soundFXSlider.value/100;
				GameEngine.endCurrentState();
			});
			GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", function(e:Event):void
			{
				GameEngine.soundManager.soundFXLevel = _soundLevel / 100;
				GameEngine.endCurrentState();
			});
			
			_soundLevel = GameEngine.soundManager.soundFXLevel * 100;
			_soundFXSlider.value = _soundLevel;
			
			_musicLevel = GameEngine.soundManager.musicLevel * 100;
			_musicSlider.value = _musicLevel;
		}
		
		private function onSoundFXChange(e:Event):void {
			GameEngine.soundManager.soundFXLevel = _soundFXSlider.value / 100;
			soundVolume.text = _soundFXSlider.value.toString();
			GameEngine.gui.show(Game.YES_BUTTON_ID);
		}
		
		private function onMusicChange(e:Event):void {
			GameEngine.soundManager.musicLevel = _musicSlider.value / 100;
			musicVolume.text = _musicSlider.value.toString();
			GameEngine.gui.show(Game.YES_BUTTON_ID);
		}
	}

}