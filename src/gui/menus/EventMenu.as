package gui.menus 
{
	import enums.MenuType;
	import events.EventManager;
	import events.GameEvent;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.AntiAliasType;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import gui.controls.ScrollBar;
	import gui.Game;
	import gui.Menu;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import gui.menus.auxiliary.ImageDisplayMenu;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class EventMenu extends Menu 
	{
		public static const ID:String = "Event_Menu";
		private var scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		private var _text:String = "";
		private var _eventManager:EventManager;
		private var _textBox:TextField;
		private var content:Sprite;
		
		public function EventMenu(eventManager:EventManager) 
		{
			super(ID, MenuType.MainScreen);
			_eventManager = eventManager;
			content = new Sprite();
			
			_textBox = new TextField();
			_textBox.defaultTextFormat = new TextFormat("Calibri", 18, Colors.LASER_GREEN);
			_textBox.wordWrap = true;
			_textBox.x = 5;
			_textBox.width = Game.MAIN_WINDOW_WIDTH - 21;
			_textBox.autoSize = TextFieldAutoSize.LEFT;
			_textBox.multiline = true;
			_textBox.antiAliasType = AntiAliasType.ADVANCED;
			content.addChild(_textBox);
			
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".bold {font-weight: bold;} \n .italic {font-style: italic;}");
			_textBox.styleSheet = style;
			
			content.graphics.beginFill(0, 0);
			content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			content.graphics.endFill();
			addChild(content);
			
			scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			scrollBar.container = new Sprite();
			scrollBar.container.graphics.beginFill(0);
			scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			scrollBar.container.graphics.endFill();
			scrollBar.container.visible = false;
			addChild(scrollBar.container);
			addChild(scrollBar);
			scrollBar.content = content;
		}
		
		override public function onLoad():void
		{
			GameEngine.gui.hideCommonButtons();
			GameEngine.gui.hideYesNoButtons();
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			if(GameEngine.world != null)
			{
				(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = GameEngine.world.player;
			}
			else
			{
				GameEngine.menuManager.bottomAuxMenu = null;
			}
			text = "";
		}
		
		/**
		 * The text that is displayed in the main view.
		 */
		public function get text():String
		{
			return _text;
		}
		
		/**
		 * The text that is displayed in the main view.
		 */
		public function set text(value:String):void
		{
			_text = value;
			_textBox.htmlText = value;
			scrollBar.update();
			scrollBar.resetScroll();
		}
		
		public function runEventFunction(fun:Function):void
		{
			fun();
			var needNext:Boolean = true;
			for(var i:int = Game.COMMON_BUTTON_01_ID; i <= Game.COMMON_BUTTON_12_ID; i++)
			{
				if(GameEngine.gui.isVisible(i))
				{
					needNext = false;
					break;
				}
			}
			if(needNext)
			{
				if(GameEngine.gui.isVisible(Game.YES_BUTTON_ID))
				{
					needNext = false;
				}
				else if(GameEngine.gui.isVisible(Game.NO_BUTTON_ID))
				{
					needNext = false;
				}
			}
			if(needNext) GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", function(e:Event):void{GameEngine.endCurrentState()});
		}
		
		public function endEvent():void
		{
			GameEngine.endCurrentState();
		}
	}
}