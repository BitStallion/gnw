package gui.menus 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECMapData;
	import data.entityEvents.EntityEvent;
	import enums.MenuType;
	import events.OldGameEvent;
	import events.Scene;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	import gui.Game;
	import gui.Menu;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import gui.menus.auxiliary.ImageDisplayMenu;
	import gui.menus.components.DoubleBufferedBitmap;
	import map.Map;
	import map.MapChunk;
	import map.MapManager;
	import data.Camera;
	import map.Tile;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class MapMenu extends Menu 
	{
		public static const ID:String = "Map_menu"
		private static const _maxPathDistance:uint = 20;
		private static const _pathedMapWidth:uint = _maxPathDistance * 2 + 1;
		private static const _mapInteractEvent:String = "map-interact-event"
		
		private var _mapManager:MapManager;
		private var _mapDisplay:DoubleBufferedBitmap;
		private var _cam:Camera;
		private var _greenBox:Bitmap;
		private var _redBox:Bitmap;
		private var _lastTime:uint;
		private var _mousePos:Point;
		private var _mousePath:Vector.<Point>;
		private var _mouseLBDown:Boolean = false;
		private var _up:Boolean = false;
		private var _down:Boolean = false;
		private var _left:Boolean = false;
		private var _right:Boolean = false;
		private var _pathedMap:Vector.<Boolean> = new Vector.<Boolean>();
		private var _currColor:Vector3D;
		private var _targColor:Vector3D;
		
		public function MapMenu(mapManager:MapManager) 
		{
			super(ID, MenuType.MainScreen);
			//width = Game.MAIN_WINDOW_WIDTH;
			//height = Game.MAIN_WINDOW_HEIGHT;
			_mapManager = mapManager;
			_mapDisplay = new DoubleBufferedBitmap(new BitmapData(Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT, false, 0), new BitmapData(Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT, false, 0));
			addChild(_mapDisplay);
			_cam = new Camera(0.5, 0.5, Game.MAIN_WINDOW_WIDTH / 32, Game.MAIN_WINDOW_HEIGHT / 32);
			_greenBox = GameEngine.fileResourceManager.getImage("images/map/GreenBox.png");
			_redBox = GameEngine.fileResourceManager.getImage("images/map/RedBox.png");
			addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
			addEventListener(MouseEvent.MOUSE_UP, mouseUp);
			addEventListener(MouseEvent.ROLL_OVER, mouseRollOver);
			addEventListener(MouseEvent.ROLL_OUT, mouseRollOut);
			for(var i:uint = 0; i < (_pathedMapWidth * _pathedMapWidth); i++)
			{
				_pathedMap.push(false);
			}
			_currColor = new Vector3D(Math.random() * 256, Math.random() * 256, Math.random() * 256);
			_targColor = new Vector3D(Math.random() * 256, Math.random() * 256, Math.random() * 256);
		}
		
		override public function onLoad():void 
		{
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = AuxiliaryStatMenu.ID;
			
			if(GameEngine.world != null)
			{
				(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = GameEngine.world.player;
				GameEngine.world.player.addEventListener(EntityEvent.enterTile, enterTile);
			}
			addEventListener(Event.ENTER_FRAME, enterFrame);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, keyUp);
			setupGUI();
			_lastTime = getTimer();
		}
		
		override public function onUnload():void 
		{
			if(GameEngine.world != null)
			{
				GameEngine.world.player.removeEventListener(EntityEvent.enterTile, enterTile);
			}
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDown);
			stage.removeEventListener(KeyboardEvent.KEY_UP, keyUp);
		}
		
		private function keyDown(e:KeyboardEvent):void
		{
			switch(e.keyCode)
			{
				case Keyboard.UP:
				{
					_up = true;
					break;
				}
				case Keyboard.DOWN:
				{
					_down = true;
					break;
				}
				case Keyboard.LEFT:
				{
					_left = true;
					break;
				}
				case Keyboard.RIGHT:
				{
					_right = true;
					break;
				}
			}
		}
		
		private function keyUp(e:KeyboardEvent):void
		{
			switch(e.keyCode)
			{
				case Keyboard.UP:
				{
					_up = false;
					break;
				}
				case Keyboard.DOWN:
				{
					_down = false;
					break;
				}
				case Keyboard.LEFT:
				{
					_left = false;
					break;
				}
				case Keyboard.RIGHT:
				{
					_right = false;
					break;
				}
			}
		}
		
		private function mouseDown(e:MouseEvent):void
		{
			_mouseLBDown = true;
		}
		
		private function mouseUp(e:MouseEvent):void
		{
			_mouseLBDown = false;
		}
		
		private function mouseRollOver(e:MouseEvent):void
		{
			_mouseLBDown = e.buttonDown;
		}
		
		private function mouseRollOut(e:MouseEvent):void
		{
			_mouseLBDown = false;
		}
		
		private function moveNorth(e:Event):void
		{
			if(GameEngine.world)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var vec:Point = new Point(mapData.targetX - mapData.x, mapData.targetY - mapData.y);
				if(vec.length <= 0.03125)
				{
					try
					{
						mapData.setTarget(mapData.targetX, mapData.targetY - 1);
					}
					catch(e:GnWError)
					{
					}
					mapData.aiPath = null;
				}
			}
		}
		
		private function moveSouth(e:Event):void
		{
			if(GameEngine.world)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var vec:Point = new Point(mapData.targetX - mapData.x, mapData.targetY - mapData.y);
				if(vec.length <= 0.03125)
				{
					try
					{
						mapData.setTarget(mapData.targetX, mapData.targetY + 1);
					}
					catch(e:GnWError)
					{
					}
					mapData.aiPath = null;
				}
			}
		}
		
		private function moveEast(e:Event):void
		{
			if(GameEngine.world)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var vec:Point = new Point(mapData.targetX - mapData.x, mapData.targetY - mapData.y);
				if(vec.length <= 0.03125)
				{
					try
					{
						mapData.setTarget(mapData.targetX + 1, mapData.targetY);
					}
					catch(e:GnWError)
					{
					}
					mapData.aiPath = null;
				}
			}
		}
		
		private function moveWest(e:Event):void
		{
			if(GameEngine.world)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var vec:Point = new Point(mapData.targetX - mapData.x, mapData.targetY - mapData.y);
				if(vec.length <= 0.03125)
				{
					try
					{
						mapData.setTarget(mapData.targetX - 1, mapData.targetY);
					}
					catch(e:GnWError)
					{
					}
					mapData.aiPath = null;
				}
			}
		}
		
		private function examine(e:Event):void
		{
			var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
			var props:Dictionary = mapData.map.getTileProperties("Region", mapData.x, mapData.y, mapData.z, "examine");
			GameEngine.startEvent(props["event"].toString());
		}
		
		private function enterTile(e:Event):void
		{
			if(GameEngine.world != null)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var isSafe:Boolean = false;
				for each(var tileID:uint in mapData.map.getTileIDs(mapData.x, mapData.y, mapData.z))
				{
					var tile:Tile = GameEngine.mapManager.getTile(tileID);
					if (tile.isSafe)
					{
						isSafe = true;
						break;
					}
				}
				var eventData:Dictionary = mapData.map.getTileProperties("Region", mapData.x, mapData.y, mapData.z, "EventStory");
				var newEventData:Dictionary = new Dictionary();
				var key:String;
				var weights:Number = 0;
				for (key in eventData)
				{
					if (key == "nil" || GameEngine.testEvent(key))
					{
						weights += Number(eventData[key]);
						newEventData[key] = Number(eventData[key]);
					}
				}
				var randNum:Number = Math.random() * weights;
				for (key in newEventData)
				{
					if (randNum - newEventData[key] <= 0)
					{
						if (key != "nil") GameEngine.startEvent(key);
						return;
					}
				}
				if (!isSafe)
				{
					eventData = mapData.map.getTileProperties("Region", mapData.x, mapData.y, mapData.z, "EventRE");
					newEventData = new Dictionary();
					weights = 0;
					for (key in eventData)
					{
						if (key == "nil" || GameEngine.testEvent(key))
						{
							weights += Number(eventData[key]);
							newEventData[key] = Number(eventData[key]);
						}
					}
					randNum = Math.random() * weights;
					for (key in newEventData)
					{
						if (randNum - newEventData[key] <= 0)
						{
							GameEngine.startEvent(key);
							return;
						}
					}
				}
				setupGUI();
			}
		}
		
		private function setupGUI():void
		{
			if(GameEngine.world != null)
			{
				var mapData:ECMapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				var directions:uint = mapData.map.canMoveInDirections(mapData.x, mapData.y, mapData.z);
				GameEngine.gui.hideCommonButtons();
				if((directions & Tile.north) > 0) GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "North", moveNorth);
				if((directions & Tile.west) > 0) GameEngine.gui.setAndShow(Game.COMMON_BUTTON_05_ID, "West", moveWest);
				if((directions & Tile.south) > 0) GameEngine.gui.setAndShow(Game.COMMON_BUTTON_06_ID, "South", moveSouth);
				if((directions & Tile.east) > 0) GameEngine.gui.setAndShow(Game.COMMON_BUTTON_07_ID, "East", moveEast);
				if(mapData.map.getTileProperties("Region", mapData.x, mapData.y, mapData.z, "examine").hasOwnProperty("event")) GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Examine", examine);
				var chunk:MapChunk = mapData.map.getChunk(mapData.x >> MapChunk.chunkXShiftBits, mapData.y >> MapChunk.chunkYShiftBits, mapData.z);
				if(chunk != null)
				{
					var nextButton:int = Game.COMMON_BUTTON_09_ID;
					for each(var layer:String in chunk.layers)
					{
						var tileActions:Dictionary = GameEngine.mapManager.getTile(chunk.getTileID(layer, mapData.x, mapData.y)).actions();
						if(tileActions != null)
						{
							for(var name:String in tileActions)
							{
								GameEngine.gui.setAndShow(nextButton++, name, action(tileActions[name] as Function, chunk, layer, mapData.x, mapData.y));
								if(nextButton > Game.COMMON_BUTTON_12_ID) break;
							}
							if(nextButton > Game.COMMON_BUTTON_12_ID) break;
						}
					}
				}
			}
		}
		
		private function action(act:Function, chunk:MapChunk, layer:String, x:int, y:int):Function
		{
			return function(e:Event):void
			{
				act(chunk, layer, x, y);
			}
		}
		
		public function enterFrame(e:Event):void
		{
			var mapData:ECMapData = null;
			var deltaTime:Number = (getTimer() - _lastTime) / 1000;
			_lastTime = getTimer();
			var vec:Point;
			if(GameEngine.world != null)
			{
				mapData = GameEngine.world.player.getComponent(EntityComponent.POSITION) as ECMapData;
				if(mapData.targetX == mapData.x && mapData.targetY == mapData.y)
				{
					try
					{
						if(_up)
						{
							mapData.setTarget(mapData.targetX, mapData.targetY - 1);
							mapData.aiPath = null;
						}
						else if(_down)
						{
							mapData.setTarget(mapData.targetX, mapData.targetY + 1);
							mapData.aiPath = null;
						}
						else if(_left)
						{
							mapData.setTarget(mapData.targetX - 1, mapData.targetY);
							mapData.aiPath = null;
						}
						else if(_right)
						{
							mapData.setTarget(mapData.targetX + 1, mapData.targetY);
							mapData.aiPath = null;
						}
					}
					catch(e:GnWError)
					{
						mapData.aiPath = null;
					}
				}
				mapData.move(deltaTime);
				_cam.center = new Point(mapData.x + 0.5, mapData.y + 0.5);
			}
			else
			{
				_cam.x = mouseX / 8;
				_cam.y = mouseY / 8;
			}
			if(mapData != null)
			{
				var drawBuffer:BitmapData = _mapDisplay.drawBuffer;
				if(_currColor.equals(_targColor)) _targColor = new Vector3D(Math.random() * 256, Math.random() * 256, Math.random() * 256);
				var vec3d:Vector3D = _targColor.subtract(_currColor);
				if(vec3d.length > 200 * deltaTime)
				{
					vec3d.scaleBy((200 * deltaTime) / _targColor.length);
					_currColor.incrementBy(vec3d);
				}
				else
				{
					_currColor = _targColor;
				}
				var color:uint = (uint(_currColor.x) << 16) | (uint(_currColor.y) << 8) | (uint(_currColor.z));
				drawBuffer.fillRect(new Rectangle(0, 0, drawBuffer.width, drawBuffer.height), color);
				var chunkStartX:int = Math.floor(_cam.x) >> MapChunk.chunkXShiftBits;
				var chunkStartY:int = Math.floor(_cam.y) >> MapChunk.chunkYShiftBits;
				var chunkEndX:int = Math.ceil(_cam.right) >> MapChunk.chunkXShiftBits;
				var chunkEndY:int = Math.ceil(_cam.bottom) >> MapChunk.chunkYShiftBits;
				for(var y:int = chunkStartY; y <= chunkEndY; y++)
				{
					for(var x:int = chunkStartX; x <= chunkEndX; x++)
					{
						try
						{
							var chunk:MapChunk = mapData.map.getChunk(x, y, 0);
							drawBuffer.copyPixels(chunk.chunkImage, MapChunk.chunkImageRect, new Point(((x << MapChunk.chunkXShiftBits) - _cam.x) * 32, ((y << MapChunk.chunkYShiftBits) - _cam.y) * 32));
						}
						catch(e:GnWError)
						{
						}
					}
				}
				if(mouseX >= 0 && mouseY >= 0 && mouseX < drawBuffer.width && mouseY < drawBuffer.height)
				{
					//if(mapData.x == mapData.targetX && mapData.y == mapData.targetY)
					{
						var currentMousePos:Point = new Point(Math.floor(_cam.x + (mouseX / 32)), Math.floor(_cam.y + (mouseY / 32)));
						if(_mousePos == null || _mousePath == null || !_mousePos.equals(currentMousePos))
						{
							_mousePos = currentMousePos;
							_mousePath = findPath(mapData.map, new Point(mapData.targetX, mapData.targetY), _mousePos, mapData.z);
						}
						if(_mousePath.length > 0)
						{
							for each(var p:Point in _mousePath)
							{
								drawBuffer.copyPixels(_greenBox.bitmapData, _greenBox.bitmapData.rect, new Point((p.x - _cam.x) * 32, (p.y - _cam.y) * 32), null, null, true);
							}
							if(!_mousePath[_mousePath.length - 1].equals(_mousePos))
							{
								drawBuffer.copyPixels(_redBox.bitmapData, _redBox.bitmapData.rect, new Point((_mousePos.x - _cam.x) * 32, (_mousePos.y - _cam.y) * 32), null, null, true);
							}
						}
					}
					if(_mouseLBDown)
					{
						mapData.aiPath = _mousePath.slice();
					}
				}
				drawBuffer.copyPixels(mapData.icon, mapData.iconRect, new Point((mapData.x - _cam.x) * 32, (mapData.y - _cam.y) * 32), null, null, true);
			}
			_mapDisplay.swap();
		}
		
		private function indexOfPoint(vec:Vector.<Point>, p:Point):int
		{
			for(var i:uint = 0; i < vec.length; i++)
			{
				if(vec[i].equals(p)) return i;
			}
			return -1;
		}
		
		private function findPath(map1:Map, start:Point, end:Point, z:int):Vector.<Point>
		{
			var paths:Vector.<Vector.<Point>> = new Vector.<Vector.<Point>>();
			var closestPath:Vector.<Point> = new Vector.<Point>();
			var closestPathDistance:Number = start.subtract(end).length;
			var distance:Number;
			closestPath.push(start);
			paths.push(closestPath);
			var newPath:Vector.<Point>;
			var p:Point;
			var mapIndex:uint;
			for(mapIndex = 0; mapIndex < (_pathedMapWidth * _pathedMapWidth); mapIndex++)
			{
				_pathedMap[mapIndex] = false;
			}
			var pathedUpperLeft:Point = new Point(start.x - _maxPathDistance, start.y - _maxPathDistance);
			var pMap:Point = start.subtract(pathedUpperLeft);
			mapIndex = pMap.y * _pathedMapWidth + pMap.x;
			_pathedMap[mapIndex] = true;
			while(paths.length > 0)
			{
				var newPaths:Vector.<Vector.<Point>> = new Vector.<Vector.<Point>>();
				for each(var path:Vector.<Point> in paths)
				{
					if(path.length < _maxPathDistance + 1)
					{
						var lastPoint:Point = path[path.length - 1];
						var orgPoint:Point = null;
						var directions:uint = map1.canMoveInDirections(lastPoint.x, lastPoint.y, z);
						if((directions & Tile.north) > 0)
						{
							p = new Point(lastPoint.x, lastPoint.y - 1);
							pMap = p.subtract(pathedUpperLeft);
							mapIndex = pMap.y * _pathedMapWidth + pMap.x;
							if(indexOfPoint(path, p) < 0 && _pathedMap[mapIndex] == false)
							{
								_pathedMap[mapIndex] = true;
								if(orgPoint == null && closestPath != path)
								{
									orgPoint =  p;
								}
								else
								{
									newPath = path.slice();
									newPath.push(p);
									newPaths.push(newPath);
									distance = p.subtract(end).length;
									if(distance < closestPathDistance)
									{
										closestPath = newPath;
										closestPathDistance = distance;
										if(distance == 0)
										{
											return closestPath;
										}
									}
								}
							}
						}
						if((directions & Tile.south) > 0)
						{
							p = new Point(lastPoint.x, lastPoint.y + 1);
							pMap = p.subtract(pathedUpperLeft);
							mapIndex = pMap.y * _pathedMapWidth + pMap.x;
							if(indexOfPoint(path, p) < 0 && _pathedMap[mapIndex] == false)
							{
								_pathedMap[mapIndex] = true;
								if(orgPoint == null && closestPath != path)
								{
									orgPoint =  p;
								}
								else
								{
									newPath = path.slice();
									newPath.push(p);
									newPaths.push(newPath);
									distance = p.subtract(end).length;
									if(distance < closestPathDistance)
									{
										closestPath = newPath;
										closestPathDistance = distance;
										if(distance == 0)
										{
											return closestPath;
										}
									}
								}
							}
						}
						if((directions & Tile.east) > 0)
						{
							p = new Point(lastPoint.x + 1, lastPoint.y);
							pMap = p.subtract(pathedUpperLeft);
							mapIndex = pMap.y * _pathedMapWidth + pMap.x;
							if(indexOfPoint(path, p) < 0 && _pathedMap[mapIndex] == false)
							{
								_pathedMap[mapIndex] = true;
								if(orgPoint == null && closestPath != path)
								{
									orgPoint =  p;
								}
								else
								{
									newPath = path.slice();
									newPath.push(p);
									newPaths.push(newPath);
									distance = p.subtract(end).length;
									if(distance < closestPathDistance)
									{
										closestPath = newPath;
										closestPathDistance = distance;
										if(distance == 0)
										{
											return closestPath;
										}
									}
								}
							}
						}
						if((directions & Tile.west) > 0)
						{
							p = new Point(lastPoint.x - 1, lastPoint.y);
							pMap = p.subtract(pathedUpperLeft);
							mapIndex = pMap.y * _pathedMapWidth + pMap.x;
							if(indexOfPoint(path, p) < 0 && _pathedMap[mapIndex] == false)
							{
								_pathedMap[mapIndex] = true;
								if(orgPoint == null && closestPath != path)
								{
									orgPoint =  p;
								}
								else
								{
									newPath = path.slice();
									newPath.push(p);
									newPaths.push(newPath);
									distance = p.subtract(end).length;
									if(distance < closestPathDistance)
									{
										closestPath = newPath;
										closestPathDistance = distance;
										if(distance == 0)
										{
											return closestPath;
										}
									}
								}
							}
						}
						if(orgPoint != null)
						{
							path.push(orgPoint);
							newPaths.push(path);
							distance = orgPoint.subtract(end).length;
							if(distance < closestPathDistance)
							{
								closestPath = path;
								closestPathDistance = distance;
								if(distance == 0)
								{
									return closestPath;
								}
							}
						}
					}
				}
				paths = newPaths;
			}
			return closestPath;
		}
	}
}