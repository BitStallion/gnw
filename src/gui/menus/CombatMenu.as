package gui.menus
{
	import combat.*;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import gui.controls.ScrollBar;
	import gui.Game;
	import gui.Menu;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import gui.menus.auxiliary.ImageDisplayMenu;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class CombatMenu extends Menu
	{
		public static const ID:String = "Combat_Menu";
		
		private var _scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		private var _text:String = "";
		private var _textBox:TextField;
		private var _content:Sprite;
		
		public var combatFinishedCallback:Function;
		
		public function CombatMenu()
		{
			super(ID, MenuType.MainScreen);
			
			_content = new Sprite();
			
			_textBox = new TextField();
			_textBox.defaultTextFormat = new TextFormat("Calibri", 18, Colors.LASER_GREEN);
			_textBox.wordWrap = true;
			_textBox.x = 5;
			_textBox.width = Game.MAIN_WINDOW_WIDTH - 21;
			_textBox.autoSize = TextFieldAutoSize.LEFT;
			_textBox.multiline = true;
			_textBox.antiAliasType = AntiAliasType.ADVANCED;
			_content.addChild(_textBox);
			
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".bold {font-weight: bold;} \n .italic {font-style: italic;} \n .enemy {color: #FF0000;}");
			_textBox.styleSheet = style;
			
			_content.graphics.beginFill(0, 0);
			_content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			_content.graphics.endFill();
			addChild(_content);
			
			_scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			_scrollBar.container = new Sprite();
			_scrollBar.container.graphics.beginFill(0);
			_scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			_scrollBar.container.graphics.endFill();
			_scrollBar.container.visible = false;
			addChild(_scrollBar.container);
			addChild(_scrollBar);
			_scrollBar.content = _content;
			
			//addEventListener(CombatEvent.APPEND_TO_LOG, appendToLog);
			//addEventListener(CombatEvent.CLEAR_LOG, clearLog);
			addEventListener(CombatEvent.SELECT, select);
			addEventListener(CombatEvent.SHOW_STATS, showStats);
			addEventListener(CombatEvent.COMBAT_FINISHED, combatFinished);
		}
		
		private function clearLog():void 
		{
			text = "";
		}
		
		private function select(e:CombatEvent):void
		{
			var i:uint;
			var selectable:IDescriptable;
			GameEngine.gui.hideCommonButtons();
			for each (selectable in e.selectables)
			{
				if (selectable.buttonId > -1)
				{
					GameEngine.gui.setAndShow(selectable.buttonId, selectable.buttonDescription, createSelectableCallback(selectable));
				}
				else
				{
					GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID + i, selectable.buttonDescription, createSelectableCallback(selectable));
					i++;
				}
			}
		}
		
		private function showStats(e:CombatEvent):void
		{
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
		}
		
		override public function onLoad():void
		{
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = AuxiliaryStatMenu.ID;
			GameEngine.gui.hideAll();
			for(var i:int; i < GameEngine.currentBattle.entities.length; i++)
			{
				if((GameEngine.currentBattle.entities[i].getComponent(EntityComponent.COMBAT) as ECCombat).allegiance == Allegiance.Enemy && GameEngine.currentBattle.entities[i].variables["image"] != null)
				{
					var image:Bitmap = GameEngine.fileResourceManager.getResource(GameEngine.currentBattle.entities[i].variables["image"]) as Bitmap;
					(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = image;
					break;
				}
			}
			GameEngine.currentBattle.combat();
		}
		
		public function combatPrep():void
		{
			text = "";
			GameEngine.logManager.logFunction = appendToLog;
			GameEngine.logManager.clearFunction = clearLog;
		}
		
		private function combatFinished(e:CombatEvent):void
		{
			GameEngine.logManager.logFunction = null;
			GameEngine.logManager.clearFunction = null;
			if (combatFinishedCallback != null)
			{
				combatFinishedCallback(e);
			}
		}
		
		private function appendToLog(str:String):void
		{
			if(text == "")
			{
				text = str;
			}
			else
			{
				text += "\n" + str;
			}
			_scrollBar.toEnd();
		}
		
		public function get text():String
		{
			return _text;
		}
		
		public function set text(value:String):void
		{
			_text = value;
			_textBox.htmlText = value;
			_scrollBar.update();
			_scrollBar.resetScroll();
		}
		
		private function createSelectableCallback(selectable:IDescriptable):Function
		{
			return function(e:Event):void
			{
				GameEngine.gui.hideAll();
				var event:CombatEvent = new CombatEvent(CombatEvent.SELECTED);
				event.selected = selectable;
				GameEngine.currentBattle.selectedEventHandler(event);
			}
		}
	
	}

}