package gui.menus 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECAttributes;
	import enums.MenuType;
	import flash.events.Event;
	import gui.Game;
	import gui.Menu;
	import gui.menus.components.StatPointSpender;
	import gui.PropertyChangedEvent;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class CharacterMenu extends Menu 
	{
		public static const ID:String = "Character_Menu";
		private var _character:Entity;
		private var attributes:StatPointSpender;
		
		public function CharacterMenu() 
		{
			super(ID, MenuType.MainScreen);
			attributes = new StatPointSpender();
			attributes.x = 10;
			attributes.y = 10;
			addChild(attributes);
			
			//attributes.addEventListener(Event.CHANGE, StatpointUpdate);
			//attributes.addEventListener(PropertyChangedEvent.UIPropertyChanged, statChanged);
		}
		
		override public function onLoad():void
		{
			//GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = null;
			GameEngine.gui.hideAll();
			var attrib:ECAttributes = _character.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
			if(attrib["free"] > 0)
			{
				_character.startTransaction();
				GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Accept", function(e:Event):void
				{
					_character.commitTransaction();
					GameEngine.endCurrentState();
				});
			}
			attributes.startStatAssignment(attrib);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", function(e:Event):void
			{
				_character.rollbackTransaction();
				GameEngine.endCurrentState();
			});
		}
		
		public function set character(value:Entity):void
		{
			_character = value;
		}
	}
}