package gui.menus 
{
	import enums.MenuType;
	import flash.display.Sprite;
	import flash.events.Event;
	import gui.Menu;
	import gui.Game; 
	import gui.menus.components.navigation.DungeonNavScreen;
	import gui.menus.auxiliary.*;
	import gui.PropertyChangedEvent;
	import navigation.*;
	import navigation.location.*;
	
	/**
	 * Used to move the character around the map.
	 * 
	 * 
	 * @author GnW Team
	 */
	public class NavigationMenu extends Menu 
	{
		public static const ID:String = "Navigation_menu"
		
		private var _navManager:NavigationManager;
		
		private var dungeonMap:DungeonNavScreen;
		
		public function NavigationMenu(navManager:NavigationManager) 
		{
			super(ID, MenuType.MainScreen);
			
			_navManager = navManager;
			_navManager.addEventListener(NavigationManager.LOCATION_CHANGED_EVENT, onLocationChange);
			
			dungeonMap = new DungeonNavScreen(Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			addChild(dungeonMap);
		}
		
		override public function onLoad():void 
		{
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = AuxiliaryStatMenu.ID;
			
			if(GameEngine.world != null)
				(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = GameEngine.world.player;
			
			var local:Location = _navManager.currentLocation;
			
			showLocation(local);
			
			
			//Starts playing a new music track when the menu is loaded up.
			GameEngine.soundManager.playAndLoopMusic("sounds/music/Cave1.mp3");		
		}
		
		private function onLocationChange(event:PropertyChangedEvent):void
		{
			showLocation(event.newValue as Location);
		}
		
		private function showLocation(local:Location):void
		{
			if (local is Map)
				showMap(local as Map);
			else if (local is Dungeon)
				dungeonMap.showDungeon(local as Dungeon);
			else if (local is Store)
				showStore(local as Store);
		}
		
		private function showMap(map:Map):void 
		{
			
		}
		
		private function showStore(store:Store):void 
		{
			
		}
	}
}