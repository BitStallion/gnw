package gui.menus 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECInventory;
	import data.EntityComponents.ECLootTable;
	import enums.MenuType;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import gui.Game;
	import gui.Menu;
	import gui.menus.auxiliary.*;
	import gui.menus.components.*;
	import gui.PropertyChangedEvent;
	import flash.text.*;
	
	/**
	 * This menu is used by the player to sell or buy items from vendors.
	 * @author GnW Team
	 */
	public class BarterMenu extends Menu 
	{
		public static const ID:String = "Barter_Menu"
		
		private var _player:Entity;
		private var _seller:Entity;
		
		private var _selectedItem:Entity;
		private var playerItemList:ItemList;
		private var sellerItemList:ItemList;
		private var playerName:TextField;
		private var sellerName:TextField;
		
		private var playerMoney:TextField;
		private var sellerMoney:TextField;
		private var exchangeMoney:TextField;
		
		private var _playerMoneyValue:int;
		private var _sellerMoneyValue:int;
		private var _exchangeValue:int;
		
		private var sellArrow:Sprite;
		
		public function BarterMenu() 
		{
			super(ID, MenuType.MainScreen);
			
			playerName = setupTextfield(30);
			playerName.x = Game.MAIN_WINDOW_WIDTH - 285;
			playerName.y = 15;
			addChild(playerName);
			
			playerItemList = new ItemList(265, Game.MAIN_WINDOW_HEIGHT - 120);
			playerItemList.x = Game.MAIN_WINDOW_WIDTH - 290;
			playerItemList.y = 60;
			addChild(playerItemList);
			
			playerItemList.addEventListener(ItemList.ItemChanged, playerItemSelected);
			
			sellerName = setupTextfield(30);
			sellerName.x = 25;
			sellerName.y = 15;
			addChild(sellerName);
			
			sellerItemList = new ItemList(265, Game.MAIN_WINDOW_HEIGHT - 120);
			sellerItemList.x = 20;
			sellerItemList.y = 60;
			addChild(sellerItemList);
			
			sellerItemList.addEventListener(ItemList.ItemChanged, sellerItemSelected);
			
			playerMoney = setupTextfield(30);
			playerMoney.autoSize = TextFieldAutoSize.RIGHT;
			playerMoney.x = Game.MAIN_WINDOW_WIDTH - 35;
			playerMoney.y = Game.MAIN_WINDOW_HEIGHT - 50;
			addChild(playerMoney);
			
			sellerMoney = setupTextfield(30);
			sellerMoney.x = 25;
			sellerMoney.y = Game.MAIN_WINDOW_HEIGHT - 50;
			addChild(sellerMoney);
			
			exchangeMoney = setupTextfield(30);
			exchangeMoney.autoSize = TextFieldAutoSize.CENTER;
			exchangeMoney.x = Game.MAIN_WINDOW_WIDTH / 2 - 10;
			exchangeMoney.y = Game.MAIN_WINDOW_HEIGHT - 50;
			addChild(exchangeMoney);
			
			_playerMoneyValue = 0;
			_sellerMoneyValue = 0;
			_exchangeValue = 0;
			
			sellArrow = new Sprite();
			var image:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/SellArrow.png") as Bitmap
			image.x = - (image.width / 2);
			sellArrow.addChild(image);
			sellArrow.x = (Game.MAIN_WINDOW_WIDTH / 2) - 5;
			sellArrow.y = Game.MAIN_WINDOW_HEIGHT - 70;
			addChild(sellArrow);
		}
		
		private function setupTextfield(size:uint):TextField
		{
			var textField:TextField = new TextField();
			textField.defaultTextFormat = new TextFormat("Bombard", size, Colors.LASER_GREEN);
			textField.embedFonts = true;
			textField.autoSize = TextFieldAutoSize.LEFT;
			textField.antiAliasType = AntiAliasType.ADVANCED;
			textField.selectable = false;
			return textField;
		}
		
		override public function onLoad():void 
		{
			GameEngine.gui.hideAll();
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Accept", confirmTransaction);
			GameEngine.gui.hide(Game.YES_BUTTON_ID);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Cancel", cancelTransaction);
			
			GameEngine.menuManager.topAuxMenu = ImageDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = ItemStatsMenu.ID;
			
			initData();
		}
		
		private function initData():void
		{
			playerItemList.items = (_player.getComponent(EntityComponent.INVENTORY) as ECInventory).allTradableEntities();
			playerMoneyValue =_player.variables["money"];
			
			sellerItemList.items = (_seller.getComponent(EntityComponent.INVENTORY) as ECInventory).allTradableEntities();
			sellerMoneyValue = _seller.variables["money"];
			
			playerName.text = _player.variables["name"] as String;
			sellerName.text = _seller.variables["name"] as String;
			
			exchangeValue = 0;
			exchangeMoney.textColor = Colors.LASER_GREEN;
		}
		
		private function playerItemSelected(e:PropertyChangedEvent):void 
		{
			_selectedItem = e.newValue as Entity;
			
			sellerItemList.clearSelected();
			setPlayerButtons();
			
			(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = GameEngine.fileResourceManager.getResource(_selectedItem.variables["image"]) as Bitmap;
			(GameEngine.menuManager.getMenuById(ItemStatsMenu.ID) as ItemStatsMenu).item = _selectedItem;
		}
		
		private function sellerItemSelected(e:PropertyChangedEvent):void
		{
			_selectedItem = e.newValue as Entity;
			
			playerItemList.clearSelected();
			setSellerButtons();
			
			(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = GameEngine.fileResourceManager.getResource(_selectedItem.variables["image"]) as Bitmap;
			(GameEngine.menuManager.getMenuById(ItemStatsMenu.ID) as ItemStatsMenu).item = _selectedItem;
		}
		
		private function setSellerButtons():void {
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Buy", buyItem(1));
			//GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Buy 5", buyItem(5));
			//GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Buy 10", buyItem(10));
		}
		
		private function setPlayerButtons():void {
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Sell", sellItem(1));
			//GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Sell 5", sellItem(5));
			//GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Sell 10", sellItem(10));
		}
		
		private function checkIfCanComplete():void {
			if (_exchangeValue > _sellerMoneyValue || Math.abs(_exchangeValue) > _playerMoneyValue)
			{
				exchangeMoney.textColor = Colors.NEON_RED;
				sellArrow.alpha = 0.4;
				GameEngine.gui.hide(Game.YES_BUTTON_ID);
			}
			else {
				exchangeMoney.textColor = Colors.LASER_GREEN;
				sellArrow.alpha = 1;
				GameEngine.gui.show(Game.YES_BUTTON_ID);
			}
		}
		
		private function sellItem(number:uint):Function {
			return function(e:Event):void {
				if (_selectedItem == null)
					return;
				
				GameEngine.gui.show(Game.YES_BUTTON_ID);
				
				var playerOwned:Boolean = !playerItemList.itemIsNoted(_selectedItem);
				
				playerItemList.removeItem(_selectedItem);
				sellerItemList.addItem(_selectedItem, playerOwned);
				
				(_player.getComponent(EntityComponent.INVENTORY) as ECInventory).removeEntity(_selectedItem);
				(_seller.getComponent(EntityComponent.INVENTORY) as ECInventory).addEntity(_selectedItem);
				
				exchangeValue = _exchangeValue + parseInt(_selectedItem.variables["cost"]);
				
				_selectedItem = null;
				
				checkIfCanComplete();
			}
		}
		
		private function buyItem(number:uint):Function {
			return function(e:Event):void {
				if (_selectedItem == null)
					return;
				
				GameEngine.gui.show(Game.YES_BUTTON_ID);
				
				var sellerOwned:Boolean = !sellerItemList.itemIsNoted(_selectedItem);
				
				sellerItemList.removeItem(_selectedItem);
				playerItemList.addItem(_selectedItem, sellerOwned);
				
				(_seller.getComponent(EntityComponent.INVENTORY) as ECInventory).removeEntity(_selectedItem);
				(_player.getComponent(EntityComponent.INVENTORY) as ECInventory).addEntity(_selectedItem);
				
				exchangeValue = _exchangeValue - parseInt(_selectedItem.variables["cost"]);
				
				_selectedItem = null;
				
				checkIfCanComplete();
			}
		}
		
		private function set playerMoneyValue(value:int):void 
		{
			_playerMoneyValue = value;
			playerMoney.text = value.toString();
		}
		
		private function set sellerMoneyValue(value:int):void 
		{
			_sellerMoneyValue = value;
			sellerMoney.text = value.toString();
		}
		
		private function set exchangeValue(value:int):void 
		{
			_exchangeValue = value;
			sellArrow.scaleX = value >= 0 ? 1 : -1;
			sellArrow.visible = value != 0;
			exchangeMoney.text = Math.abs(value).toString();
		}
		
		/**
		 * Sets up the barter menu. Should be called prior to the menu being selected.
		 * 
		 * @param	player
		 * @param	seller
		 */
		public function setup(player:Entity, seller:Entity):void
		{
			_player = player;
			_seller	= seller;
			
			(_player.getComponent(EntityComponent.INVENTORY) as ECInventory).startTransaction();
			
			var inv:ECInventory = _seller.getComponent(EntityComponent.INVENTORY) as ECInventory;
			inv.startTransaction();
			
			var table:ECLootTable = _seller.getComponent(EntityComponent.STORE) as ECLootTable;
			if (table.lastGenerated == null || (GameEngine.world != null && table.lastGenerated > GameEngine.world.gameTime.copy().addDays(3))) {
				inv.clear();
				inv.addEntities(table.generateItems());
				_seller.variables["money"] = table.generateMoney();
			}
		}
		
		public function cancelTransaction(e:Event):void
		{
			(_player.getComponent(EntityComponent.INVENTORY) as ECInventory).rollbackTransaction();
			(_seller.getComponent(EntityComponent.INVENTORY) as ECInventory).rollbackTransaction();
			
			initData();
			GameEngine.endCurrentState();
		}
		
		public function confirmTransaction(e:Event):void
		{
			_player.variables["money"] = _playerMoneyValue + _exchangeValue;
			_seller.variables["money"] = _sellerMoneyValue - _exchangeValue;
			
			(_player.getComponent(EntityComponent.INVENTORY) as ECInventory).commitTransaction();
			(_seller.getComponent(EntityComponent.INVENTORY) as ECInventory).commitTransaction();
			
			initData();
			GameEngine.endCurrentState();
		}
	}
}