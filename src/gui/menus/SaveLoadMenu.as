package gui.menus 
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import gui.Menu;
	import enums.MenuType;
	import flash.events.Event;
	import gui.controls.ConfigurableButton;
	import gui.Game;
	import gui.Menu;
	import gui.menus.components.SaveFileBar;
	import gui.controls.ScrollBar;
	import system.SaveManager;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class SaveLoadMenu extends Menu 
	{
		public static const ID:String = "Save_Load_Menu";
		
		private static var _saveBars:Vector.<SaveFileBar>;
		
		private var _saveMode:Boolean = false;
		private var _saveManager:SaveManager;
		
		private var scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		private var content:Sprite = new Sprite();
		
		public function SaveLoadMenu(saveManager:SaveManager) 
		{
			super(ID, MenuType.MainScreen);
			
			_saveManager = saveManager;
			_saveBars = new Vector.<SaveFileBar>();
			
			for (var i:int = 0; i < _saveManager.slotCount; i++)
			{
				_saveBars[i] = new SaveFileBar(_saveManager.loadFromSlot(i));
				_saveBars[i].x = (Game.MAIN_WINDOW_WIDTH - SaveFileBar.BAR_WIDTH) / 2;
				_saveBars[i].y = 10 + i * (SaveFileBar.BAR_HEIGHT + 6);
				content.addChild(_saveBars[i]);
			}
			
			content.graphics.beginFill(0, 0);
			content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH / 2, 10 + _saveManager.slotCount * (SaveFileBar.BAR_HEIGHT + 6));
			content.graphics.endFill();			
			addChild(content);
			
			scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			scrollBar.container = new Sprite();
			scrollBar.container.graphics.beginFill(0);
			scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			scrollBar.container.graphics.endFill();
			scrollBar.container.visible = false;
			addChild(scrollBar.container);
			addChild(scrollBar);
			scrollBar.content = content;
			if (scrollBar.visible)
			{
				for each (var bar:SaveFileBar in _saveBars)
				{
					bar.width -= 16;
				}
			}
		}
		
		override public function onLoad():void {
			GameEngine.menuManager.bottomAuxMenu = null;
			GameEngine.menuManager.topAuxMenu = null;
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", returnToTitle);
		}
		
		private function returnToTitle(e:Event):void {
			GameEngine.menuManager.mainScreenMenu = TitleMenu.ID;
		}
		
		public function get saveMode():Boolean 
		{
			return _saveMode;
		}
		
		public function set saveMode(value:Boolean):void 
		{
			_saveMode = value;
			
			for each (var saveBar:SaveFileBar in _saveBars)
			{
				saveBar.saveMode = value;
			}
		}
		
	}

}