package gui.menus
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECAttributes;
	import enums.*;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import flash.utils.Dictionary;
	import gui.Menu;
	import gui.Game;
	import gui.PropertyChangedEvent;
	import gui.menus.auxiliary.*;
	import gui.menus.components.GenderSelector;
	import gui.menus.components.StatPointSpender;
	import system.GlobalConstants;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class NewGameMenu extends Menu
	{
		public static const ID:String = "New_Game_Menu";
		
		private var _character:Entity;
		
		private var screens:Vector.<Sprite>;
		
		private var _genderChoice:Gender;
		private var _raceChoice:Race;
		private var _bodyChoice:BodyType;
		private var nameTextField:TextField;
		private var textbox:TextField;
		
		private var stats:StatPointSpender;
		private var genderSelector:GenderSelector;
		
		public function NewGameMenu()
		{
			super(ID, MenuType.MainScreen);
			
			screens = new Vector.<Sprite>();
			createRaceScreen();
			createGenderScreen();
			createBodySetupScreen();
			createStatsScreen();
			createNameScreen();
			
			for each(var screen:Sprite in screens)
				addChild(screen);
			
			textbox = new TextField();
			textbox.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			textbox.wordWrap = true;
			textbox.embedFonts = true;
			textbox.width = Game.MAIN_WINDOW_WIDTH - 100;
			textbox.autoSize = TextFieldAutoSize.LEFT;
			textbox.antiAliasType = AntiAliasType.ADVANCED;
			textbox.multiline = true;
			textbox.x = (Game.MAIN_WINDOW_WIDTH - textbox.width) / 2;
			textbox.y = 400;
			textbox.text = "Testing";
			addChild(textbox);
		}
		
		private function createRaceScreen():void
		{
			screens[0] = new Sprite();
			
			var textBox:TextField = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 30, Colors.LASER_GREEN);
			textBox.embedFonts = true;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.multiline = false;
			textBox.text = "Select your race";
			textBox.x = (Game.MAIN_WINDOW_WIDTH - textBox.width) / 2;
			textBox.y = (Game.MAIN_WINDOW_HEIGHT - textBox.height) / 2;
			screens[0].addChild(textBox);
		}
		
		private function createGenderScreen():void
		{
			screens[1] = new Sprite();
			genderSelector = new GenderSelector();
			genderSelector.x = (Game.MAIN_WINDOW_WIDTH - genderSelector.width) / 2;
			genderSelector.y = (Game.MAIN_WINDOW_HEIGHT - genderSelector.height) / 2 - 50;
			screens[1].addChild(genderSelector);
		}
		
		private function createBodySetupScreen():void
		{
			screens[2] = new Sprite();
			
			var textBox:TextField = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 30, Colors.LASER_GREEN);
			textBox.embedFonts = true;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.multiline = false;
			textBox.text = "Select your body type";
			textBox.x = (Game.MAIN_WINDOW_WIDTH - textBox.width) / 2;
			textBox.y = (Game.MAIN_WINDOW_HEIGHT - textBox.height) / 2;
			screens[2].addChild(textBox);
		}
		
		private function createStatsScreen():void
		{
			screens[3] = new Sprite();
			
			stats = new StatPointSpender();
			stats.x = (Game.MAIN_WINDOW_WIDTH - stats.width) / 2;
			stats.y = (Game.MAIN_WINDOW_HEIGHT - stats.height) / 2 - 50;
			screens[3].addChild(stats);
			
			stats.addEventListener(Event.CHANGE, StatpointUpdate);
			stats.addEventListener(PropertyChangedEvent.Changed, statChanged);
		}
		
		private function statChanged(e:PropertyChangedEvent):void
		{
			var stat:CharacterStat = e.newValue as CharacterStat;
			
			var text:String = "";
			
			switch(stat)
			{
				case CharacterStat.strength: 
					text = Assets.STRENGTH_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.endurance: 
					text = Assets.ENDURANCE_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.agility: 
					text = Assets.AGILITY_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.charisma: 
					text = Assets.CHARISMA_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.dexterity: 
					text = Assets.DEXTERITY_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.intelligence: 
					text = Assets.INTELLIGENCE_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.willpower: 
					text = Assets.WILLPOWER_FLAVOUR_TEXT;
					break;
				
				default: 
					break;
			}
			
			textbox.text = text;
			(GameEngine.menuManager.getMenuById(StatDisplayMenu.ID) as StatDisplayMenu).stat = stat;
		}
		
		private function StatpointUpdate(e:Event):void
		{
			//if(stats.freePoints == 0)
				GameEngine.gui.show(Game.YES_BUTTON_ID);
			/*else
				GameEngine.gui.hide(Game.YES_BUTTON_ID);*/
		}
		
		private function createNameScreen():void
		{
			screens[4] = new Sprite();
			
			var groupContainer:Sprite = new Sprite();
			
			var textBox:TextField = new TextField();
			textBox.defaultTextFormat = new TextFormat("Bombard", 20, Colors.LASER_GREEN);
			textBox.embedFonts = true;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.multiline = false;
			textBox.text = "What do you want your name to be?";
			groupContainer.addChild(textBox);
			
			var textBorder:Sprite = new Sprite();
			textBorder.graphics.beginFill(0, 0);
			textBorder.graphics.lineStyle(3, Colors.LASER_GREEN);
			textBorder.graphics.drawRect(-3, textBox.height + 12, 356, textBox.height + 6);
			textBorder.graphics.endFill();
			groupContainer.addChild(textBorder);
			
			nameTextField = new TextField();
			nameTextField.defaultTextFormat = new TextFormat("Bombard", 30, Colors.LASER_GREEN);
			nameTextField.embedFonts = true;
			nameTextField.multiline = false;
			nameTextField.width = 350;
			nameTextField.height = 40;
			nameTextField.maxChars = 22;
			nameTextField.type = TextFieldType.INPUT;
			nameTextField.y = textBox.height + 10;
			nameTextField.addEventListener(Event.CHANGE, onNameChange);
			groupContainer.addChild(nameTextField);
			
			groupContainer.x = (Game.MAIN_WINDOW_WIDTH - groupContainer.width) / 2;
			groupContainer.y = (Game.MAIN_WINDOW_HEIGHT - groupContainer.height) / 2;
			
			screens[4].addChild(groupContainer);
		}
		
		private function onNameChange(e:Event):void
		{
			nameTextField.text.length > 0 ? GameEngine.gui.show(Game.YES_BUTTON_ID) : GameEngine.gui.hide(Game.YES_BUTTON_ID);
		}
		
		override public function onLoad():void
		{
			_genderChoice = null;
			_raceChoice = null;
			_bodyChoice = null;
			
			GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_01_ID, "Back", returnToTitle);
			GameEngine.menuManager.bottomAuxMenu = NewCharacterChoiceMenu.ID;
			
			setUpNameScreen();
			setRace(null)(null);
			showScreen(4);
			textbox.text = "";
		}
		
		private function setUpNameScreen():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.show(Game.SYSTEM_BUTTON_01_ID);
			
			GameEngine.menuManager.topAuxMenu = null;
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", goToRace);
			if(nameTextField.text.length == 0)
				GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			textbox.text = "";
			
			stage.focus = nameTextField;
		}
		
		private function setUpRaceScreen():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.show(Game.SYSTEM_BUTTON_01_ID);
			
			GameEngine.menuManager.topAuxMenu = RaceDisplayMenu.ID;
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Human", setRace(Race.Human));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Dwarf", setRace(Race.Dwarf));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Elf", setRace(Race.Elf));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_04_ID, "Ork", setRace(Race.Ork));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_05_ID, "Goblin", setRace(Race.Goblin));
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", goToGender);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", goToName);
			
			if(_raceChoice == null)
				GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			setRace(_raceChoice)(null);
		}
		
		private function setUpGenderScreen():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.show(Game.SYSTEM_BUTTON_01_ID);
			
			GameEngine.menuManager.topAuxMenu = GenderDisplayMenu.ID;
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Male", setGender(Gender.Male));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Shemale", setGender(Gender.Shemale));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Female", setGender(Gender.Female));
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_05_ID, "Femboy", setGender(Gender.Femboy));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_06_ID, "Herm", setGender(Gender.Herm));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_07_ID, "Cuntboy", setGender(Gender.Cuntboy));
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", goToBody);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", goToRace);
			
			if(_genderChoice == null)
				GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			setGender(_genderChoice)(null);
			textbox.text = "";
		}
		
		private function setUpBodyScreen():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.show(Game.SYSTEM_BUTTON_01_ID);
			
			GameEngine.menuManager.topAuxMenu = BodyTypeDisplayMenu.ID;
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "Thin", setBodyType(BodyType.Thin));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Normal", setBodyType(BodyType.Normal));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Chubby", setBodyType(BodyType.Chubby));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_04_ID, "Muscular", setBodyType(BodyType.Muscular));
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_05_ID, "Husky", setBodyType(BodyType.Husky));
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Continue", goToStats);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", goToGender);
			GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			setBodyType(_bodyChoice)(null);
		}
		
		private function setUpStatsScreen():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.show(Game.SYSTEM_BUTTON_01_ID);
			
			GameEngine.menuManager.topAuxMenu = StatDisplayMenu.ID;
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Start", completeCharacter);
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", goToBody);
			GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			_character = GameEngine.dataManager.createCharacterFromTemplate(GlobalConstants.PlayerId, _raceChoice, _genderChoice, _bodyChoice);
			
			var statList:Vector.<Enum> = Utils.EnumList(CharacterStat);
			var status:ECAttributes = _character.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
			for each(var stat:Enum in statList)
			{
				var charStat:CharacterStat = stat as CharacterStat;
				
				status[charStat.text + "Base"] = GlobalConstants.StatStartPoints;
			}
			status["free"] = GlobalConstants.NewCharacterFreeStatPoints;
			_character.applyEffects();
			
			stats.startStatAssignment(status);
			textbox.text = "";
		}
		
		private function showScreen(value:int):void
		{
			for(var i:int = 0; i < screens.length; i++)
			{
				screens[i].visible = (i == value);
			}
		}
		
		private function setGender(gender:Gender):Function
		{
			return function(e:Event):void
			{
				(GameEngine.menuManager.getMenuById(GenderDisplayMenu.ID) as GenderDisplayMenu).gender = gender;
				
				_genderChoice = gender;
				
				var text:String = "";
				
				GameEngine.gui.show(Game.YES_BUTTON_ID);
				
				switch(gender)
				{
					case Gender.Female: 
						text = Assets.FEMALE_FLAVOUR_TEXT;
						break;
					
					case Gender.Cuntboy: 
						text = Assets.CUNTBOY_FLAVOUR_TEXT;
						break;
					
					case Gender.Male: 
						text = Assets.MALE_FLAVOUR_TEXT;
						break;
					
					case Gender.Femboy: 
						text = Assets.FEMBOY_FLAVOUR_TEXT;
						break;
					
					case Gender.Herm: 
						text = Assets.HERM_FLAVOUR_TEXT;
						break;
					
					case Gender.Shemale: 
						text = Assets.SHEMALE_FLAVOUR_TEXT;
						break;
					
					default: 
						GameEngine.gui.hide(Game.YES_BUTTON_ID);
						break;
				}
				
				genderSelector.selectGender(gender);
				
				textbox.text = text;
			};
		}
		
		private function setRace(race:Race):Function
		{
			return function(e:Event):void
			{
				(GameEngine.menuManager.getMenuById(RaceDisplayMenu.ID) as RaceDisplayMenu).race = race;
				
				_raceChoice = race;
				
				var text:String = "";
				
				GameEngine.gui.show(Game.YES_BUTTON_ID);
				switch(race)
				{
					case Race.Human: 
						text = Assets.HUMAN_FLAVOUR_TEXT;
						break;
					
					case Race.Dwarf: 
						text = Assets.DWARF_FLAVOUR_TEXT;
						break;
					
					case Race.Elf: 
						text = Assets.ELF_FLAVOUR_TEXT;
						break;
					
					case Race.Ork: 
						text = Assets.ORK_FLAVOUR_TEXT;
						break;
					
					case Race.Goblin: 
						text = Assets.GOBLIN_FLAVOUR_TEXT;
						break;
					
					default: 
						GameEngine.gui.hide(Game.YES_BUTTON_ID);
						break;
				}
				
				textbox.text = text;
			};
		}
		
		private function setBodyType(body:BodyType):Function
		{
			return function(e:Event):void
			{
				(GameEngine.menuManager.getMenuById(BodyTypeDisplayMenu.ID) as BodyTypeDisplayMenu).bodyType = body;
				
				_bodyChoice = body;
				
				var text:String = "";
				
				GameEngine.gui.show(Game.YES_BUTTON_ID);
				switch(body)
				{
					case BodyType.Thin: 
						text = Assets.THIN_FLAVOUR_TEXT;
						break;
					
					case BodyType.Normal: 
						text = Assets.NORMAL_FLAVOUR_TEXT;
						break;
					
					case BodyType.Chubby: 
						text = Assets.CHUBBY_FLAVOUR_TEXT;
						break;
					
					case BodyType.Muscular: 
						text = Assets.MUSCULAR_FLAVOUR_TEXT;
						break;
					
					case BodyType.Husky: 
						text = Assets.HUSKY_FLAVOUR_TEXT;
						break;
					
					default: 
						GameEngine.gui.hide(Game.YES_BUTTON_ID);
						break;
				}
				
				textbox.text = text;
			};
		}
		
		private function returnToTitle(e:Event):void
		{
			GameEngine.menuManager.mainScreenMenu = TitleMenu.ID;
		}
		
		private function goToGender(e:Event):void
		{
			var ngMenu:NewCharacterChoiceMenu = GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu;
			ngMenu.clear();
			ngMenu.addName = nameTextField.text;
			ngMenu.addRace = _raceChoice;
			setUpGenderScreen();
			showScreen(1);
		}
		
		private function goToRace(e:Event):void
		{
			var ngMenu:NewCharacterChoiceMenu = GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu;
			ngMenu.clear();
			ngMenu.addName = nameTextField.text;
			setUpRaceScreen();
			showScreen(0);
		}
		
		private function goToBody(e:Event):void
		{
			var ngMenu:NewCharacterChoiceMenu = GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu;
			ngMenu.clear();
			ngMenu.addName = nameTextField.text;
			ngMenu.addRace = _raceChoice;
			ngMenu.addGender = _genderChoice;
			setUpBodyScreen();
			showScreen(2);
		}
		
		private function goToStats(e:Event):void
		{
			var ngMenu:NewCharacterChoiceMenu = GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu;
			ngMenu.clear();
			ngMenu.addName = nameTextField.text;
			ngMenu.addRace = _raceChoice;
			ngMenu.addGender = _genderChoice;
			ngMenu.addBody = _bodyChoice;
			setUpStatsScreen();
			showScreen(3);
		}
		
		private function goToName(e:Event):void
		{
			var ngMenu:NewCharacterChoiceMenu = GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu;
			ngMenu.clear();
			setUpNameScreen();
			showScreen(4);
		}
		
		private function gotoSummary(e:Event):void
		{
			(GameEngine.menuManager.getMenuById(NewCharacterChoiceMenu.ID) as NewCharacterChoiceMenu).addStats(_character);
		}
		
		private function completeCharacter(e:Event):void
		{
			_character.variables["name"] = nameTextField.text;
			//stats.applyStats();
			GameEngine.createNewCharacter(_character);
		}
	}
}