package gui.menus
{
	import combat.ability.XmlBasicAttack;
	import combat.Allegiance;
	import combat.CombatEvent;
	import combat.CombatSystem;
	import combat.Random;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECArray;
	import data.EntityComponents.ECBuffHandler;
	import data.EntityComponents.ECInventory;
	import data.EntityComponents.ECDynamicNumber;
	import data.EntityComponents.ECExperience;
	import data.EntityComponents.ECAttributes;
	import data.EntityComponents.ECLootTable;
	import data.time.DateTime;
	import enums.*;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import gui.controls.ScrollBar;
	import gui.Game;
	import gui.Menu;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import system.GlobalConstants;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DebugMenu extends Menu
	{
		public static const ID:String = "Debug_Menu";
		
		private var scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		private var content:Sprite;
		private var _textBox:TextField;
		
		public function DebugMenu()
		{
			super(ID, MenuType.MainScreen);
			
			content = new Sprite();
			
			_textBox = new TextField();
			_textBox.defaultTextFormat = new TextFormat("Bombard", 18, Colors.LASER_GREEN);
			_textBox.wordWrap = true;
			_textBox.embedFonts = true;
			_textBox.x = 5;
			_textBox.width = Game.MAIN_WINDOW_WIDTH - 21;
			_textBox.autoSize = TextFieldAutoSize.LEFT;
			_textBox.multiline = true;
			content.addChild(_textBox);
			
			setText();
			
			content.graphics.beginFill(0, 0);
			content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			content.graphics.endFill();
			addChild(content);
			
			scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			scrollBar.container = new Sprite();
			scrollBar.container.graphics.beginFill(0);
			scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			scrollBar.container.graphics.endFill();
			scrollBar.container.visible = false;
			addChild(scrollBar.container);
			addChild(scrollBar);
			scrollBar.content = content;
		}
		
		private function setText():void
		{
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".header {font-size: 30pt; margin-top: 10px;}")
			
			_textBox.styleSheet = style;
			
			_textBox.htmlText = "<p class='header'>Nav Test</p>" + "<p>Test the navigation system.</p>" + "<br/>" + "<p class='header'>Combat Test</p>" + "<p>Test the combat system.</p>" + "<br/>" + "<p class='header'>Parsing Test</p>" + "<p>Test the parsing system and parsable text.</p>";
		}
		
		override public function onLoad():void
		{
			GameEngine.gui.hideAll();
			GameEngine.gui.setAndShow(Game.NO_BUTTON_ID, "Back", backToTitle);
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID, "New Sys Tests", toNewSysTest);
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_02_ID, "Combat Test", toCombatTest);
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_03_ID, "Parsing Test", toParsingTest);
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_04_ID, "Inv. Test", toInventoryTest);
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_05_ID, "Time+", advanceTime);
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_06_ID, "Time-", revertTime);
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_07_ID, "Barter Test", toBarterTest);
			
			GameEngine.gui.setAndShow(Game.COMMON_BUTTON_09_ID, "Quick Start", toQuickStart);
		}
		
		private function randomPlayer():Entity
		{
			var rand:Random = new Random();
			
			var genders:Vector.<Enum> = Utils.EnumList(Gender);
			var gender:Gender = Gender.Herm;//genders[rand.getNextInt(genders.length) - 1] as Gender;
			
			var races:Vector.<Enum> = Utils.EnumList(Race);
			var race:Race = races[rand.getNextInt(races.length) - 1] as Race;
			
			var bodies:Vector.<Enum> = Utils.EnumList(BodyType);
			var body:BodyType = bodies[rand.getNextInt(bodies.length) - 1] as BodyType;
			
			var character:Entity = GameEngine.dataManager.createCharacterFromTemplate(GlobalConstants.PlayerId, race, gender, body);
			character.variables["name"] = "Test Character";
			
			var status:ECAttributes = character.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes;
			for each (var stat:CharacterStat in Utils.EnumList(CharacterStat))
			{
				if(stat.order > CharacterStat.willpower.order) break;
				status[stat.text + "Base"] = 20;
			}
			
			return character;
		}
		
		private function toQuickStart(e:Event):void
		{
			GameEngine.createNewCharacter(randomPlayer());
		}
		
		private function backToTitle(e:Event):void
		{
			GameEngine.endCurrentState();
		}
		
		private function toNewSysTest(e:Event):void
		{
			//GameEngine.gotoLocation("water_purification_plant", "Main Level", "wpp_entrance");
			GameEngine.newNavSys = true;
			GameEngine.newEventSys = true;
			GameEngine.createNewCharacter(randomPlayer());
			//GameEngine.startEvent("new-game-preamble");
		}
		
		private function toCombatTest(e:Event):void
		{
			var menu:CombatMenu = GameEngine.menuManager.getMenuById(CombatMenu.ID) as CombatMenu;
			GameEngine.currentBattleDebug = new CombatSystem(menu);
			GameEngine.currentBattle.context = GameEngine.getCurrentParsingContext();
			menu.combatFinishedCallback = function(e:CombatEvent):void
			{
				GameEngine.menuManager.bottomAuxMenu = null;
				GameEngine.menuManager.topAuxMenu = null;
				GameEngine.gui.hideAll();
				GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Return", function(e:Event):void
					{
						GameEngine.menuManager.mainScreenMenu = DebugMenu.ID
					});
			}
			menu.combatPrep();
			var ent:Entity = randomPlayer();
			var exp:ECExperience = ent.getComponent(EntityComponent.EXPERIENCE) as ECExperience;
			exp.experience = 0;
			exp.checkNextLevel();
			
			var dynNum:ECDynamicNumber = ent.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			dynNum.value = dynNum.max;
			dynNum = ent.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
			dynNum.value = dynNum.max;
			dynNum = ent.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			dynNum.value = 0;
			
			var attacks:ECArray = ent.getComponent(EntityComponent.ATTACKS) as ECArray;
			//attacks.dataBase.push("LaserSwitchblade");
			attacks.dataBase.push("Tick22");
			//attacks.dataBase.push("Pinky");
			/*var player:Participant = new EntityParticipant(ent);
			player.registerAbility(GameEngine.combatDataManager.getAttack("LaserSwitchblade"), true);
			player.registerAbility(GameEngine.combatDataManager.getAttack("Tick22"));
			player.registerAbility(GameEngine.combatDataManager.getAttack("Pinky"));*/
			
			var buffs:ECBuffHandler = ent.getComponent(EntityComponent.BUFFS) as ECBuffHandler;
			buffs.addEntity(GameEngine.dataManager.createBuffFromTemplate("TestBuff"));
			ent.applyEffects();
			
			GameEngine.currentBattle.addEntity(ent, Allegiance.Player);
			GameEngine.currentBattle.addEntity(GameEngine.dataManager.createNPCFromTemplate("TestDickRat"));
			//referee.register(XmlParticipant.Create("Fungal"));
			//referee.register(XmlParticipant.Create("GavrilOgtar(Ogre)"));
			//referee.register(XmlParticipant.Create("GrapeSodaSlime"));
			//referee.register(XmlParticipant.Create("PipeCleaner"));
			//referee.register(XmlParticipant.Create("Vagabunny"));
			
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).character = ent;
			GameEngine.menuManager.mainScreenMenu = CombatMenu.ID;
		}
		
		private function toParsingTest(e:Event):void
		{
			GameEngine.menuManager.mainScreenMenu = ParseTestMenu.ID;
		}
		
		private function toInventoryTest(e:Event):void
		{
			var character:Entity = GameEngine.dataManager.createCharacterFromTemplate(GlobalConstants.PlayerId, Race.Human, Gender.Herm, BodyType.Normal);
			
			var inv:ECInventory = character.getComponent(EntityComponent.INVENTORY) as ECInventory;
			
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("Tick22Pistol"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("PinkyBatton"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("DerringPistol"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("LaserSwitchblade"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("NoviceSlaveArmor"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("BasicClothes"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("RaggedArmor"));
			
			(GameEngine.menuManager.getMenuById(InventoryMenu.ID) as InventoryMenu).character = character;
			GameEngine.menuManager.mainScreenMenu = InventoryMenu.ID;
		}
		
		private var time:DateTime = new DateTime(0, 1, 1, 18);
		
		private function advanceTime(e:Event):void
		{
			time.addMinutes(6);
			GameEngine.gui.setMenuTime(time.decimalHours);
			trace(time);
		}
		
		private function revertTime(e:Event):void
		{
			time.addMinutes(-6);
			GameEngine.gui.setMenuTime(time.decimalHours);
			trace(time);
		}
		
		private function toBarterTest(e:Event):void
		{
			var player:Entity = GameEngine.dataManager.createCharacterFromTemplate(GlobalConstants.PlayerId, Race.Human, Gender.Herm, BodyType.Normal);
			player.variables["name"] = "Test Player";
			var inv:ECInventory = player.getComponent(EntityComponent.INVENTORY) as ECInventory;
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("Tick22Pistol"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("PinkyBatton"));
			inv.addEntity(GameEngine.dataManager.createItemFromTemplate("NoviceSlaveArmor"));
			
			player.variables["money"] = 200;
			
			var seller:Entity = GameEngine.dataManager.createNPCFromTemplate("Spade");
			
			seller.variables["money"] = 100;
			
			(GameEngine.menuManager.getMenuById(BarterMenu.ID) as BarterMenu).setup(player, seller);
			GameEngine.menuManager.mainScreenMenu = BarterMenu.ID;
		}
	}

}