package gui.menus.components 
{
	import data.Entity;
	import flash.display.Sprite;
	import flash.text.*;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class EquipmentSlotBar extends Sprite 
	{
		private var _item:Entity;
		
		private var _BG:Sprite;
		private var _itemField:TextField;
		
		public function EquipmentSlotBar(slotName:String) 
		{
			super();
			
			_item = item;
			
			_BG = new Sprite();
			_BG.graphics.beginFill(Colors.CARBON);
			_BG.graphics.lineStyle(3, Colors.LASER_GREEN);
			_BG.graphics.drawRect(0, 0, 300, 60);
			_BG.graphics.moveTo(0, 20);
			_BG.graphics.lineTo(300, 20);
			_BG.graphics.endFill();
			addChild(_BG);
			
			var nameField:TextField = new TextField();
			nameField.autoSize = TextFieldAutoSize.LEFT;
			nameField.defaultTextFormat = new TextFormat("Bombard", 16, Colors.LASER_GREEN);
			nameField.embedFonts = true;
			nameField.mouseEnabled = false;
			nameField.x = 10;
			nameField.y = 0;
			nameField.text = slotName;
			addChild(nameField);
			
			_itemField = new TextField();
			_itemField.autoSize = TextFieldAutoSize.LEFT;
			_itemField.defaultTextFormat = new TextFormat("Bombard", 30, Colors.LASER_GREEN);
			_itemField.embedFonts = true;
			_itemField.mouseEnabled = false;
			_itemField.x = 15;
			_itemField.y = 25;
			addChild(_itemField);
			
			_itemField.text = "Empty"
		}
		
		public function get item():Entity 
		{
			return _item;
		}
		
		public function set item(value:Entity):void 
		{
			_item = value;
			_itemField.text = item != null ? item.variables["name"] : "Empty";
		}
		
	}

}