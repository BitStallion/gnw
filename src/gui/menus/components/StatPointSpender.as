package gui.menus.components
{
	import data.Entity;
	import data.EntityComponents.ECAttributes;
	import flash.display.Sprite;
	import enums.Utils;
	import enums.CharacterStat;
	import enums.Enum;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import gui.PropertyChangedEvent;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class StatPointSpender extends Sprite
	{
		private var _BG:Sprite;
		private var _stats:ECAttributes;
		private var _statChanges:Dictionary;
		private var _statsRemaining:TextField;
		
		public function StatPointSpender()
		{
			super();
			_BG = new Sprite();
			addChild(_BG);
			
			_statChanges = new Dictionary();
			
			var statList:Vector.<Enum> = Utils.EnumList(CharacterStat);
			
			var prev:Sprite = new Sprite();
			for each(var stat:Enum in statList)
			{
				var charStat:CharacterStat = stat as CharacterStat;
				if(charStat.order > CharacterStat.willpower.order) break;
				
				var statChanger:StatPointSelector = new StatPointSelector(charStat);
				
				statChanger.x = 5;
				statChanger.y = prev.y + prev.height + 5;
				addChild(statChanger);
				
				_statChanges[charStat.text] = statChanger;
				
				statChanger.minusButton.addEventListener(MouseEvent.CLICK, decStat(charStat));
				statChanger.plusButton.addEventListener(MouseEvent.CLICK, incStat(charStat));
				statChanger.addEventListener(MouseEvent.MOUSE_OVER, mouseOverChanger(charStat, this));
				
				prev = statChanger;
			}
			
			_statsRemaining = new TextField();
			_statsRemaining.defaultTextFormat = new TextFormat("Bombard", 28, Colors.LASER_GREEN);
			_statsRemaining.embedFonts = true;
			_statsRemaining.autoSize = TextFieldAutoSize.LEFT;
			_statsRemaining.multiline = false;
			_statsRemaining.text = "Available: 0";
			_statsRemaining.selectable = false;
			_statsRemaining.x = (width-_statsRemaining.width) / 2;
			_statsRemaining.y = prev.y + prev.height + 5;
			addChild(_statsRemaining);
			
			_BG.graphics.beginFill(Colors.CARBON);
			_BG.graphics.lineStyle(3, Colors.LASER_GREEN);
			_BG.graphics.drawRect(0, 0, width + 10, height + 10);
			_BG.graphics.moveTo(0, height - _statsRemaining.height - 7);
			_BG.graphics.lineTo(width - 5, height - _statsRemaining.height - 7);
			_BG.graphics.endFill();
		}
		
		private function mouseOverChanger(stat:CharacterStat, parent:Sprite):Function
		{
			return function(e:MouseEvent):void
			{
				parent.dispatchEvent(new PropertyChangedEvent(PropertyChangedEvent.Changed, null, stat));
			};
		}
		
		public function startStatAssignment(stats:ECAttributes):void
		{
			_stats = stats;
			var statList:Vector.<Enum> = Utils.EnumList(CharacterStat);
			for each(var stat:Enum in statList)
			{
				var charStat:CharacterStat = stat as CharacterStat;
				if(charStat.order > CharacterStat.willpower.order) break;
				
				var statPointer:StatPointSelector = (_statChanges[stat.text] as StatPointSelector);
				statPointer.startValue = stats[charStat.text] || 0;
				statPointer.change = 0;
			}
			
			updatePoints();
		}
		
		public function get freePoints():uint
		{
			return Math.floor(_stats["free"]);
		}
		
		private function incStat(stat:CharacterStat):Function
		{
			return function(e:Event):void
			{
				var selector:StatPointSelector = (_statChanges[stat.text] as StatPointSelector);
				
				if(selector.sum < 100 && freePoints >= 1)
				{
					selector.change++;
					_stats["free"]--;
					_stats[stat.text + "Base"]++;
					_stats.entity.applyEffects();
					selector.currentValue = _stats[stat.text]
					updatePoints();
				}
			};
		}
		
		private function decStat(stat:CharacterStat):Function
		{
			return function(e:Event):void
			{
				var selector:StatPointSelector = (_statChanges[stat.text] as StatPointSelector);
				
				if(selector.change > 0)
				{
					selector.change--;
					_stats["free"]++;
					_stats[stat.text + "Base"]--;
					_stats.entity.applyEffects();
					selector.currentValue = _stats[stat.text]
					updatePoints();
				}
			};
		}
		
		private function updatePoints():void
		{
			_statsRemaining.text = "Available: " + freePoints;
			
			var statList:Vector.<Enum> = Utils.EnumList(CharacterStat);
			for each(var stat:Enum in statList)
			{
				if(stat.order > CharacterStat.willpower.order) break;
				var selector:StatPointSelector = (_statChanges[stat.text] as StatPointSelector);
				
				selector.plusButton.visible = freePoints > 0;
			}
			
			dispatchEvent(new Event(Event.CHANGE));
		}
	}

}