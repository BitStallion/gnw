package gui.menus.components
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import gui.Game;
	import gui.controls.ConfigurableButton;
	import system.SaveFile;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class SaveFileBar extends Sprite
	{
		public static const BAR_WIDTH:int = 652;
		public static const BAR_HEIGHT:int = 60;
		
		private var loadButton:ConfigurableButton = new ConfigurableButton(Colors.LASER_GREEN);
		private var saveButton:ConfigurableButton = new ConfigurableButton(Colors.LASER_GREEN);
		private var _saveFile:SaveFile;
		private var _saveMode:Boolean;
		private var nameField:TextField;
		private var dateField:TextField;
		
		public function SaveFileBar(saveFile:SaveFile)
		{
			super();
			
			_saveFile = saveFile;
			
			graphics.beginFill(Colors.CARBON);
			graphics.lineStyle(3, Colors.LASER_GREEN);
			graphics.drawRect(0, 0, BAR_WIDTH, BAR_HEIGHT);
			graphics.endFill();
			
			nameField = new TextField();
			nameField.autoSize = TextFieldAutoSize.LEFT;
			nameField.defaultTextFormat = new TextFormat("Bombard", 30, Colors.LASER_GREEN);
			nameField.embedFonts = true;
			nameField.mouseEnabled = false;
			nameField.x = 10;
			nameField.y = 12;
			addChild(nameField);
			
			dateField = new TextField();
			dateField.autoSize = TextFieldAutoSize.LEFT;
			dateField.defaultTextFormat = new TextFormat("Bombard", 20, Colors.LASER_GREEN);
			dateField.embedFonts = true;
			dateField.mouseEnabled = false;
			dateField.x = 340;
			dateField.y = 30;
			addChild(dateField);
			
			loadButton.released = GameEngine.fileResourceManager.getResource(Game.SCREEN_BUTTON_RELEASED_IMAGE) as Bitmap;
			loadButton.pressed = GameEngine.fileResourceManager.getResource(Game.SCREEN_BUTTON_PRESSED_IMAGE) as Bitmap;
			loadButton.label = "Load Save";
			loadButton.x = BAR_WIDTH - loadButton.width - 5;
			loadButton.y = (BAR_HEIGHT - loadButton.height) / 2;
			addChild(loadButton);
			
			saveButton.released = GameEngine.fileResourceManager.getResource(Game.SCREEN_BUTTON_RELEASED_IMAGE) as Bitmap;
			saveButton.pressed = GameEngine.fileResourceManager.getResource(Game.SCREEN_BUTTON_PRESSED_IMAGE) as Bitmap;
			saveButton.label = "Save Game";
			saveButton.x = BAR_WIDTH - loadButton.width - 5;
			saveButton.y = (BAR_HEIGHT - loadButton.height) / 2;
			addChild(saveButton);
			
			updateInfo();
		}
		
		public function updateInfo():void
		{
			if(_saveFile == null)
			{
				nameField.text = "Empty";
				dateField.visible = false;
				loadButton.visible = false;
				saveButton.visible = _saveMode;
				saveButton.label = "Save Game";
				return;
			}
			
			nameField.text = _saveFile.characterName;
			dateField.text = Assets.GNW_DATE_FORMAT.format(_saveFile.dateSaved);
			loadButton.visible = !_saveMode;
			saveButton.visible = _saveMode;
			saveButton.label = "Overwrite";
			dateField.visible = true;
		}
		
		public function get saveMode():Boolean
		{
			return _saveMode;
		}
		
		public function set saveMode(value:Boolean):void
		{
			_saveMode = value;
			updateInfo();
		}
	
	}

}