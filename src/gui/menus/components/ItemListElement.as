package gui.menus.components 
{
	import data.Entity;
	import flash.display.Sprite;
	import flash.text.*;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ItemListElement extends Sprite 
	{
		private var _item:Entity;
		private var _selected:Boolean;
		
		private var _unselectedBG:Sprite;
		private var _selectedBG:Sprite;
		private var _notedIcon:Sprite;
		
		private var _noted:Boolean;
		
		public function ItemListElement(item:Entity, width:Number) 
		{
			super();
			
			_item = item;
			
			_unselectedBG = new Sprite();
			_unselectedBG.graphics.beginFill(Colors.CARBON);
			_unselectedBG.graphics.lineStyle(3, Colors.LASER_GREEN);
			_unselectedBG.graphics.drawRect(0, 0, width, 30);
			_unselectedBG.graphics.endFill();
			addChild(_unselectedBG);
			
			_selectedBG = new Sprite();
			_selectedBG.graphics.beginFill(Colors.MEDIUM_GREEN);
			_selectedBG.graphics.lineStyle(3, Colors.LASER_GREEN);
			_selectedBG.graphics.drawRect(0, 0, width, 30);
			_selectedBG.graphics.endFill();
			addChild(_selectedBG);
			
			var nameField:TextField = new TextField();
			nameField.autoSize = TextFieldAutoSize.LEFT;
			nameField.defaultTextFormat = new TextFormat("Bombard", 20, Colors.LASER_GREEN);
			nameField.embedFonts = true;
			nameField.mouseEnabled = false;
			nameField.x = 10;
			nameField.y = 3;
			nameField.text = item.variables["name"];
			addChild(nameField);
			
			_notedIcon = new Sprite();
			_notedIcon.graphics.beginFill(Colors.LASER_GREEN);
			_notedIcon.graphics.drawRect(0, 0, 10, 10);
			_notedIcon.graphics.endFill();
			_notedIcon.x = width - 20;
			_notedIcon.y = 10;
			addChild(_notedIcon);
			
			selected = false;
			noted = false;
		}
		
		public function get item():Entity 
		{
			return _item;
		}
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			_selected = value;
			_unselectedBG.visible = !_selected;
			_selectedBG.visible = _selected;
		}
		
		public function get noted():Boolean 
		{
			return _noted;
		}
		
		public function set noted(value:Boolean):void 
		{
			_noted = value;
			_notedIcon.visible = value;
		}
		
	}

}