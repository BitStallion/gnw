package gui.menus.components.navigation 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import gui.PropertyChangedEvent;
	import navigation.Corridor;
	import navigation.Room;
	import navigation.location.Dungeon;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class CorridorGraphic extends Sprite 
	{
		private var _corridor:Corridor;
		private var _isAvailable:Boolean;
		
		private var available:Sprite;
		private var unavailable:Sprite;
		
		public function CorridorGraphic(corridor:Corridor) 
		{
			super();
			
			_corridor = corridor;
			
			init();
		}
		
		private function init():void
		{
			var ent:Room = _corridor.entryRoom;
			var ext:Room = _corridor.exitRoom;
			
			var wid:int = 0;
			var len:int = 0;
			var x:int = 0;
			var y:int = 0;
			
			if (ent.col + ent.colspan <= ext.col ) {		// On right side
				wid = RoomGraphic.roomSize(ent.col + ent.colspan + 1 - ext.col);
				x = RoomGraphic.roomLocation(ent.col) + RoomGraphic.roomSize(ent.colspan);
				y = rowDiff(ent, ext);
			} else if (ent.col >= ext.col + ext.colspan ) { 	// On left side
				wid = RoomGraphic.roomSize(ext.col + ext.colspan - ent.col);
				x = RoomGraphic.roomLocation(ent.col);
				y = rowDiff(ent, ext);
			} else if (ent.row > ext.row) {		// On top
				len = RoomGraphic.roomSize(ext.row + ext.rowspan - ent.row);
				x = colDiff(ent, ext);
				y = RoomGraphic.roomLocation(ent.row);
			} else {		// On bottom
				len = RoomGraphic.roomSize(ent.row + ent.rowspan + 1 - ext.row);
				x = colDiff(ent, ext);
				y = RoomGraphic.roomLocation(ent.row) + RoomGraphic.roomSize(ent.rowspan);
			}
			
			available = new Sprite();
			available.graphics.lineStyle(6, Colors.LASER_GREEN);
			available.graphics.lineTo(wid, len);
			addChild(available);
			
			unavailable = new Sprite();
			unavailable.graphics.lineStyle(6, Colors.MEDIUM_GREEN);
			unavailable.graphics.lineTo(wid, len);
			addChild(unavailable);
			
			this.x = x;
			this.y = y;
			
			isAvailable = false;
		}
		
		private function colDiff(ent:Room, ext:Room):Number
		{
			return RoomGraphic.roomSize(ent.colspan) > RoomGraphic.roomSize(ext.colspan) 
					? RoomGraphic.roomLocation(ext.col) +  RoomGraphic.roomSize(ext.colspan) / 2
					: RoomGraphic.roomLocation(ent.col) +  RoomGraphic.roomSize(ent.colspan) / 2;
		}
		
		private function rowDiff(ent:Room, ext:Room):Number
		{
			return RoomGraphic.roomSize(ent.rowspan) > RoomGraphic.roomSize(ext.rowspan) 
					? RoomGraphic.roomLocation(ext.row) +  RoomGraphic.roomSize(ext.rowspan) / 2
					: RoomGraphic.roomLocation(ent.row) +  RoomGraphic.roomSize(ent.rowspan) / 2;
		}
		
		public function get isAvailable():Boolean 
		{
			return _isAvailable;
		}
		
		public function set isAvailable(value:Boolean):void 
		{
			_isAvailable = value;
			available.visible = _isAvailable;
			unavailable.visible = !_isAvailable;
		}
	}

}