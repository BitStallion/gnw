package gui.menus.components.navigation
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	import flash.utils.Dictionary;
	import gui.Game;
	import gui.menus.auxiliary.AuxiliaryStatMenu;
	import gui.PropertyChangedEvent;
	import gui.menus.auxiliary.ImageDisplayMenu;
	import navigation.*;
	import navigation.location.Dungeon;
	import system.GlobalConstants;
	import events.Command;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class DungeonNavScreen extends Sprite
	{
		public static const BaseRoomSize:Number = 12;
		
		private var dungeonMap:Sprite;
		private var dungeonMask:Sprite;
		private var titleBox:TextField;
		private var textbox:TextField;
		private var _currentDungeon:Dungeon;
		
		private var _roomMap:Dictionary;
		private var _corridorMap:Dictionary;
		
		/**
		 * This determines if the map will follow the player through the dunegeon or if the view will be fixed.
		 * Normally, the view is fixed if the dungeon is small enough to fit completely in the screen, but if the
		 * dungeon is too big, the view will follow the player.
		 */
		private var _followMode:Boolean;
		
		private const border:uint = 20;
		private const textRegion:int = 200;
		
		public function DungeonNavScreen(width:uint, height:uint)
		{
			super();
			
			_roomMap = new Dictionary();
			_corridorMap = new Dictionary();
			
			titleBox = new TextField();
			titleBox.defaultTextFormat = new TextFormat(GlobalConstants.MenuFont, 26, Colors.LASER_GREEN);
			titleBox.embedFonts = true;
			titleBox.width = width - 2 * border;
			titleBox.autoSize = TextFieldAutoSize.CENTER;
			titleBox.antiAliasType = AntiAliasType.ADVANCED;
			titleBox.x = width / 2;
			titleBox.y = border;
			addChild(titleBox);
			
			textbox = new TextField();
			textbox.defaultTextFormat = new TextFormat(GlobalConstants.MenuFont, 16, Colors.LASER_GREEN);
			textbox.wordWrap = true;
			textbox.embedFonts = true;
			textbox.width = width - 2 * border;
			textbox.autoSize = TextFieldAutoSize.LEFT;
			textbox.antiAliasType = AntiAliasType.ADVANCED;
			textbox.multiline = true;
			textbox.x = border;
			textbox.y = border + 32;
			addChild(textbox);
			
			dungeonMap = new Sprite();
			dungeonMap.y = textRegion;
			addChild(dungeonMap);
			
			dungeonMask = new Sprite();
			dungeonMask.graphics.beginFill(0x000000, 0);
			dungeonMask.graphics.drawRect(border, border + textRegion, width - (2 * border), height - (2 * border) - textRegion);
			dungeonMask.graphics.endFill();
			addChild(dungeonMask);
			dungeonMap.mask = dungeonMask;
		}
		
		public function showDungeon(dungeon:Dungeon):void
		{
			clearDungeon();
			_currentDungeon = dungeon;
			
			if(dungeon.currentRoom == null)
				return;
			
			_currentDungeon.addEventListener(Dungeon.ROOM_CHANGED, onRoomChange);
			
			var currentFloor:Floor = dungeon.currentRoom.floor;
			
			var corridors:Vector.<Corridor> = currentFloor.allCorridors();
			for each(var corridor:Corridor in corridors)
			{
				drawCorridor(corridor);
			}
			
			var rooms:Vector.<Room> = currentFloor.allRooms();
			for each(var room:Room in rooms)
			{
				drawRoom(room, room == dungeon.currentRoom);
			}
			
			setupRoom(dungeon.currentRoom);
			
			checkFollowMode();
			
			if(!_followMode)
				centerMap();
			else
				centerMapOnRoom(_currentDungeon.currentRoom);
		}
		
		private function clearDungeon():void
		{
			_roomMap = new Dictionary();
			_corridorMap = new Dictionary();
			
			var numChildren:int = dungeonMap.numChildren;
			
			for(var i:int = 0; i < numChildren; i++)
			{
				dungeonMap.removeChildAt(0);
			}
			
			if(_currentDungeon != null)
				_currentDungeon.removeEventListener(Dungeon.ROOM_CHANGED, onRoomChange);
		}
		
		private function checkFollowMode():void
		{
			_followMode = dungeonMap.width > dungeonMask.width || dungeonMap.height > dungeonMask.height;
		}
		
		private function onRoomChange(event:PropertyChangedEvent):void
		{
			var room:Room = event.newValue as Room;
			setupRoom(room);
			GameEngine.world.player.advanceTurn();
			(GameEngine.menuManager.getMenuById(AuxiliaryStatMenu.ID) as AuxiliaryStatMenu).update();
		}
		
		private function setupRoom(room:Room):void
		{
			resetRoomsAndCorridors();
			
			if(_roomMap[room] == null)
				return;
			
			(_roomMap[room] as RoomGraphic).isCurrent = true;
			
			for each(var corridor:Corridor in room.corridors)
			{
				(_corridorMap[corridor] as CorridorGraphic).isAvailable = true;
				
				var otherRoom:Room = corridor.getOtherRoom(room);
				(_roomMap[otherRoom] as RoomGraphic).isSelectable = true;
			}
			
			var imageName:String = room.imageName || room.floor.image;
			var image:Bitmap = GameEngine.fileResourceManager.getResource(imageName) as Bitmap;
			
			(GameEngine.menuManager.getMenuById(ImageDisplayMenu.ID) as ImageDisplayMenu).image = image;
			
			setupNavButtons(room);
			
			title = room.floor.location.name + " - " + room.floor.level
			text = room.text;
			
			if(_followMode)
				centerMapOnRoom(room);
		}
		
		/**
		 * Resets the highlighting on all room and corridor graphics, and disables corridor graphics from being selected.
		 */
		private function resetRoomsAndCorridors():void
		{
			var floor:Floor = _currentDungeon.currentRoom.floor;
			
			for each(var room:Room in floor.allRooms())
			{
				var roomGraph:RoomGraphic = (_roomMap[room] as RoomGraphic);
				if(roomGraph == null)
					continue;
				roomGraph.isCurrent = false;
				roomGraph.isSelectable = false;
			}
			
			for each(var corridor:Corridor in floor.allCorridors())
			{
				var corrGraph:CorridorGraphic = _corridorMap[corridor] as CorridorGraphic;
				if(corrGraph == null)
					continue;
				
				corrGraph.isAvailable = false;
			}
		}
		
		private function setupNavButtons(room:Room):void
		{
			GameEngine.gui.hideCommonButtons();
			GameEngine.gui.hideYesNoButtons();
			
			if(room.isEntrance)
				GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Leave", leaveDungeon(room.door));
			
			var commands:Vector.<Command> = room.commands;
			
			for(var i:int = 0; i < commands.length; i++)
			{
				GameEngine.gui.setAndShow(Game.COMMON_BUTTON_01_ID + i, commands[i].text, commands[i].invokeActionsForEvent);
			}
		}
		
		private function leaveDungeon(door:IDoor):Function
		{
			return function(e:Event):void
			{
				GameEngine.navigationManager.enterDoor(door);
			};
		}
		
		/**
		 * Adds a room graphic to the map area based on the room.
		 *
		 * @param	room
		 * @param	isCurrent
		 */
		private function drawRoom(room:Room, isCurrent:Boolean = false):void
		{
			var roomSprite:RoomGraphic = new RoomGraphic(room, isCurrent);
			dungeonMap.addChild(roomSprite);
			_roomMap[room] = roomSprite;
		}
		
		private function drawCorridor(corridor:Corridor):void
		{
			var corrSprite:CorridorGraphic = new CorridorGraphic(corridor);
			dungeonMap.addChild(corrSprite);
			_corridorMap[corridor] = corrSprite;
		}
		
		private function centerMap():void
		{
			dungeonMap.x = (this.width - dungeonMap.width) / 2;
			dungeonMap.y = (this.height - dungeonMap.height + textRegion) / 2;
		}
		
		private function centerMapOnRoom(room:Room):void
		{
			var roomX:Number = RoomGraphic.roomLocation(room.col) + RoomGraphic.roomSize(room.colspan) / 2;
			var roomY:Number = RoomGraphic.roomLocation(room.row) + RoomGraphic.roomSize(room.rowspan) / 2;
			
			dungeonMap.x = (dungeonMask.width / 2) - roomX;
			dungeonMap.y = (dungeonMask.height / 2) + textRegion + 20 - roomY;
		}
		
		public function set title(value:String):void
		{
			titleBox.text = value;
		}
		
		public function set text(value:String):void
		{
			textbox.text = value;
		}
	}

}