package gui.menus.components.navigation 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import gui.PropertyChangedEvent;
	import navigation.Room;
	import navigation.location.Dungeon;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class RoomGraphic extends Sprite 
	{
		private var _room:Room;
		private var _isCurrent:Boolean;
		private var _isSelectable:Boolean;
		
		private var unselected:Sprite;
		private var selected:Sprite;
		private var mouseOver:Sprite;
		
		public function RoomGraphic(room:Room, isCurrent:Boolean = false) 
		{
			super();
			
			_room = room;
			_isCurrent = isCurrent;
			
			init();
		}
		
		private function init():void
		{
			unselected = new Sprite();
			unselected.graphics.beginFill(_room.isEntrance ? Colors.DARK_BLUE :Colors.DARK_GREEN);
			unselected.graphics.lineStyle(3, _room.isEntrance ? Colors.LASER_BLUE :Colors.LASER_GREEN);
			unselected.graphics.drawRect(0, 0, roomSize(_room.colspan), roomSize(_room.rowspan));
			unselected.graphics.endFill();
			addChild(unselected);
			
			selected = new Sprite();
			selected.graphics.beginFill(_room.isEntrance ? Colors.LASER_BLUE :Colors.LASER_GREEN);
			selected.graphics.lineStyle(3, _room.isEntrance ? Colors.LASER_BLUE :Colors.LASER_GREEN);
			selected.graphics.drawRect(0, 0, roomSize(_room.colspan), roomSize(_room.rowspan));
			selected.graphics.endFill();
			addChild(selected);
			
			mouseOver = new Sprite();
			mouseOver.graphics.beginFill(_room.isEntrance ? Colors.MEDIUM_BLUE :Colors.MEDIUM_GREEN);
			mouseOver.graphics.lineStyle(3, _room.isEntrance ? Colors.LASER_BLUE :Colors.LASER_GREEN);
			mouseOver.graphics.drawRect(0, 0, roomSize(_room.colspan), roomSize(_room.rowspan));
			mouseOver.graphics.endFill();
			addChild(mouseOver);
			
			isCurrent = _isCurrent;
			
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			addEventListener(MouseEvent.CLICK, onMouseClick);
			
			this.x = _room.col * DungeonNavScreen.BaseRoomSize * 2;
			this.y = _room.row * DungeonNavScreen.BaseRoomSize * 2;
		}
		
		private function onMouseClick(e:MouseEvent):void 
		{
			if(_isSelectable)
				_room.floor.location.currentRoom = this._room;
		}
		
		private function onMouseOut(e:Event):void 
		{
			if (!_isCurrent && _isSelectable)
				mouseOver.visible = false;
		}
		
		private function onMouseOver(e:MouseEvent):void 
		{
			if (!_isCurrent && _isSelectable)
				mouseOver.visible = true;
		}
		
		public static function roomSize(span:int):Number
		{
			return DungeonNavScreen.BaseRoomSize + (span - 1) * 2 * DungeonNavScreen.BaseRoomSize;
		}
		
		public static function roomLocation(ord:int):Number
		{
			return ord * DungeonNavScreen.BaseRoomSize * 2;
		}
		
		public function get isCurrent():Boolean 
		{
			return _isCurrent;
		}
		
		public function set isCurrent(value:Boolean):void 
		{
			_isCurrent = value;
			
			selected.visible = _isCurrent;
			unselected.visible = !_isCurrent;
			mouseOver.visible = false;
		}
		
		public function get isSelectable():Boolean 
		{
			return _isSelectable;
		}
		
		public function set isSelectable(value:Boolean):void 
		{
			_isSelectable = value;
		}
		
	}

}