package gui.menus.components 
{
	import data.Entity;
	import enums.EquipmentSlot;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.*;
	import gui.controls.ScrollBar;
	import gui.PropertyChangedEvent;
	import gui.Game;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ItemList extends Sprite 
	{
		public static const ItemChanged:String = "Item_changed";
		
		private var _itemBars:Vector.<ItemListElement>;
		
		private var _scrollBar:ScrollBar;
		private var _content:Sprite;
		private var _border:Sprite;
		
		private var _selectedItem:ItemListElement;
		
		public function ItemList(width:Number, height:Number) 
		{
			super();
			
			_border = new Sprite();
			_border.graphics.beginFill(0, 0);
			_border.graphics.lineStyle(3, Colors.LASER_GREEN);
			_border.graphics.drawRect(0, 0, width, height);
			_border.graphics.endFill();
			addChild(_border);
			
			_itemBars = new Vector.<ItemListElement>();
			
			_content = new Sprite()
			_content.graphics.beginFill(0, 0);
			_content.graphics.drawRect(0, 0, width, height-10);
			_content.graphics.endFill();			
			addChild(_content);
			
			_scrollBar = new ScrollBar(height-6);
			_scrollBar.x = width - 16;
			_scrollBar.y = 3;
			_scrollBar.container = new Sprite();
			_scrollBar.container.graphics.beginFill(0);
			_scrollBar.container.graphics.drawRect(0, 4, width, height-10);
			_scrollBar.container.graphics.endFill();
			_scrollBar.container.visible = false;
			addChild(_scrollBar.container);
			addChild(_scrollBar);
			_scrollBar.content = _content;
		}
		
		public function set selectedItem(item:Entity):void
		{
			if(_selectedItem != null && _selectedItem.item == item) return;
			if (_selectedItem != null) _selectedItem.selected = false;
			var oldItem:ItemListElement = _selectedItem;
			for each(var itemEntry:ItemListElement in _itemBars)
			{
				if(itemEntry.item == item)
				{
					_selectedItem = itemEntry;
					_selectedItem.selected = true;
					dispatchEvent(new PropertyChangedEvent(ItemChanged, oldItem != null ? oldItem.item : null, _selectedItem.item));
					return;
				}
			}
		}
		
		public function get selectedItem():Entity
		{
			return _selectedItem != null ? _selectedItem.item : null;
		}
		
		public function clearSelected():void 
		{
			if (_selectedItem != null)
			{
				_selectedItem.selected = false;
				_selectedItem = null;
			}
		}
		
		public function addItem(item:Entity, noted:Boolean):void
		{
			if (item == null)
				return;
			
			var bar:ItemListElement = new ItemListElement(item, _scrollBar.container.width - 28);
			bar.x = 6;
			bar.y = _itemBars.length * 36 + 6;
			_itemBars.push(bar);
			bar.addEventListener(MouseEvent.MOUSE_DOWN, selectItem);
			_content.addChild(bar);
			
			bar.noted = noted;
		}
		
		public function removeItem(item:Entity):void
		{
			if (item == null)
				return;
			
			var found:Boolean = false;
			for each(var bar:ItemListElement in _itemBars.slice())
			{
				if (item == bar.item && !found)
				{
					_content.removeChild(bar);
					bar.removeEventListener(MouseEvent.CLICK, selectItem);
					_itemBars.splice(_itemBars.indexOf(bar),1);
					found = true;
					continue;
				}
				
				if (found)
				{
					bar.y = _itemBars.indexOf(bar) * 36 + 6;
				}
			}
		}
		
		public function containtsItem(item:Entity):Boolean 
		{
			for each(var bar:ItemListElement in _itemBars)
			{
				if(bar.item == item)
					return true;
			}
			
			return false;
		}
		
		public function itemIsNoted(item:Entity):Boolean 
		{
			for each(var bar:ItemListElement in _itemBars)
			{
				if(bar.item == item && bar.noted)
					return true;
			}
			
			return false;
		}
		
		public function set items(value:Vector.<Entity>):void 
		{
			for each(var bar:ItemListElement in _itemBars)
			{
				_content.removeChild(bar);
				bar.removeEventListener(MouseEvent.CLICK, selectItem);
			}
			
			_itemBars = new Vector.<ItemListElement>();
			_selectedItem = null;
			
			for each (var item:Entity in value)
			{
				addItem(item, item.variables["equipedSlot"] != null);
			}
			
			_scrollBar.resetScroll();	
		}
		
		public function get items():Vector.<Entity>
		{
			var items:Vector.<Entity> = new Vector.<Entity>();
			
			for each(var bar:ItemListElement in _itemBars)
			{
				items.push(bar.item);
			}
			
			return items;
		}
		
		private function selectItem(e:MouseEvent):void 
		{
			var element:ItemListElement = e.currentTarget as ItemListElement;
			
			if (element != null)
			{
				if (_selectedItem != null)
					_selectedItem.selected = false;
				
				var oldItem:ItemListElement = _selectedItem;
				
				_selectedItem = element;
				element.selected = true;
				
				dispatchEvent(new PropertyChangedEvent(ItemChanged, oldItem != null ? oldItem.item : null, _selectedItem.item));
			}
			
		}
		
	}

}