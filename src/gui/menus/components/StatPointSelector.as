package gui.menus.components
{
	import enums.CharacterStat;
	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class StatPointSelector extends Sprite
	{
		private var _change:int;
		private var _stat:CharacterStat;
		private var _startValue:uint;
		private var _currentValue:uint;
		
		private var statValue:TextField;
		
		private var _minusButton:SimpleButton;
		private var _plusButton:SimpleButton;
		
		public function StatPointSelector(stat:CharacterStat)
		{
			super();
			
			_stat = stat;
			
			init();
		}
		
		public function init():void
		{
			var name:String = _stat.text;
			name = name.charAt(0).toUpperCase() + name.substring(1);
			var statName:TextField = new TextField();
			statName.defaultTextFormat = new TextFormat("Bombard", 28, Colors.LASER_GREEN);
			statName.embedFonts = true;
			statName.autoSize = TextFieldAutoSize.LEFT;
			statName.multiline = false;
			statName.text = name;
			statName.selectable = false;
			addChild(statName);
			
			statValue = new TextField();
			statValue.defaultTextFormat = new TextFormat("Bombard", 28, Colors.LASER_GREEN);
			statValue.embedFonts = true;
			statValue.autoSize = TextFieldAutoSize.LEFT;
			statValue.multiline = false;
			statValue.selectable = false;
			statValue.x = 150;
			addChild(statValue);
			
			_minusButton = new SimpleButton();
			var minusIcon:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/MinusButton.png") as Bitmap;
			_minusButton.upState = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/MinusButtonOver.png") as Bitmap;
			_minusButton.downState = minusIcon;
			_minusButton.hitTestState = minusIcon;
			_minusButton.overState = minusIcon;
			
			_minusButton.x = 250;
			_minusButton.y = 5;
			_minusButton.useHandCursor = false;
			addChild(_minusButton);
			
			_plusButton = new SimpleButton();
			var plusIcon:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/PlusButton.png") as Bitmap;
			_plusButton.upState = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/PlusButtonOver.png") as Bitmap;
			_plusButton.downState = plusIcon;
			_plusButton.hitTestState = plusIcon;
			_plusButton.overState = plusIcon;
			
			_plusButton.x = _minusButton.x + 30;
			_plusButton.y = 5;
			_plusButton.useHandCursor = false;
			addChild(_plusButton);
			
			var bounds:Rectangle = getRect(this);
			
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, bounds.width, bounds.height);
			graphics.endFill();
		}
		
		public function get startValue():uint
		{
			return _startValue;
		}
		
		public function set startValue(value:uint):void
		{
			_startValue = value;
			_currentValue = value;
			showValueSum();
			
			_minusButton.visible = _change > 0;
			_plusButton.visible = sum < 100;
		}
		
		public function get currentValue():uint
		{
			return _currentValue;
		}
		
		public function set currentValue(value:uint):void
		{
			_currentValue = value;
			showValueSum();
			
			_minusButton.visible = _change > 0;
			_plusButton.visible = sum < 100;
		}
		
		public function get minusButton():SimpleButton
		{
			return _minusButton;
		}
		
		public function get plusButton():SimpleButton
		{
			return _plusButton;
		}
		
		public function get change():int
		{
			return _change;
		}
		
		public function get sum():int
		{
			return _change + _startValue;
		}
		
		public function set change(value:int):void
		{
			_change = value;
			showValueSum();
			
			_minusButton.visible = _change > 0;
			_plusButton.visible = sum < 100;
		}
		
		private function showValueSum():void
		{
			if(_startValue != _currentValue)
			{
				statValue.text = _startValue.toString() + " => " + _currentValue.toString();
			}
			else
			{
				statValue.text = (_startValue + _change).toString();
			}
		}
	}

}