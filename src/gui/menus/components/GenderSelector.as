package gui.menus.components
{
	import enums.Gender;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import gui.Game;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class GenderSelector extends Sprite
	{
		private var genderImageHeight:Number;
		private var genderImageWidth:Number;
		private var genderSelectorBox:Sprite;
		
		public function GenderSelector()
		{
			super();
			init();
		}
		
		private function init():void
		{
			var male:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/MaleGender.png") as Bitmap;
			
			genderImageHeight = male.height;
			genderImageWidth = male.width;
			
			var point:Point = genderToPosition(Gender.Male);
			male.x = point.x;
			male.y = point.y;
			addChild(male);
			
			point = genderToPosition(Gender.Shemale);
			var futa:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/ShemaleGender.png") as Bitmap;
			futa.x = point.x;
			futa.y = point.y;
			addChild(futa);
			
			point = genderToPosition(Gender.Female);
			var female:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/FemaleGender.png") as Bitmap;
			female.x = point.x;
			female.y = point.y;
			addChild(female);
			
			point = genderToPosition(Gender.Femboy);
			var femboy:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/FemboyGender.png") as Bitmap;
			femboy.x = point.x;
			femboy.y = point.y;
			addChild(femboy);
			
			point = genderToPosition(Gender.Herm);
			var herm:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/HermGender.png") as Bitmap;
			herm.x = point.x;
			herm.y = point.y;
			addChild(herm);
			
			point = genderToPosition(Gender.Cuntboy);
			var cuntboy:Bitmap = GameEngine.fileResourceManager.getResource("images/character/creation/gender/CuntboyGender.png") as Bitmap;
			cuntboy.x = point.x;
			cuntboy.y = point.y;
			addChild(cuntboy);
			
			genderSelectorBox = new Sprite();
			genderSelectorBox.graphics.beginFill(0, 0);
			genderSelectorBox.graphics.lineStyle(3, Colors.LASER_GREEN);
			genderSelectorBox.graphics.drawRect(-3, 0, genderImageWidth + 6, genderImageHeight);
			genderSelectorBox.graphics.endFill();
			addChild(genderSelectorBox);
			genderSelectorBox.visible = false;
		}
		
		private function genderToPosition(gender:Gender):Point
		{
			var point:Point = new Point();
			
			switch(gender)
			{
				case Gender.Male: 
					point.x = 3;
					point.y = 0;
					break;
				
				case Gender.Shemale: 
					point.x = genderImageWidth + 9;
					point.y = 0;
					break;
				
				case Gender.Female: 
					point.x = 2 * genderImageWidth + 15;
					point.y = 0;
					break;
				
				case Gender.Femboy: 
					point.x = 3;
					point.y = genderImageHeight;
					break;
				
				case Gender.Herm: 
					point.x = genderImageWidth + 9;
					point.y = genderImageHeight;
					break;
				
				case Gender.Cuntboy: 
					point.x = 2 * genderImageWidth + 15;
					point.y = genderImageHeight;
					break;
				
				default: 
					break;
			}
			
			return point;
		}
		
		public function selectGender(gender:Gender):void
		{
			genderSelectorBox.visible = true;
			
			switch(gender)
			{
				case Gender.Male: 
				case Gender.Shemale: 
				case Gender.Female: 
				case Gender.Femboy: 
				case Gender.Herm: 
				case Gender.Cuntboy: 
					var point:Point = genderToPosition(gender);
					genderSelectorBox.x = point.x;
					genderSelectorBox.y = point.y - 10;
					break;
				
				default: 
					genderSelectorBox.visible = false;
					break;
			}
		}
	
	}

}