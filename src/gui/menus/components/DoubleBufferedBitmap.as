package gui.menus.components 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class DoubleBufferedBitmap extends Bitmap 
	{
		private var _buffer0:BitmapData; //An array isn't used to gain a tiny bit of resource efficiency
		private var _buffer1:BitmapData;
		private var _bufferIndex:uint;
		
		public function DoubleBufferedBitmap(buffer0:BitmapData = null, buffer1:BitmapData = null, pixelSnapping:String = "auto", smoothing:Boolean = false) 
		{
			_buffer0 = buffer0;
			_buffer1 = buffer1;
			_bufferIndex = 0;
			super(buffer0, pixelSnapping, smoothing);
		}
		
		public function set buffer0(buffer:BitmapData):void
		{
			_buffer0 = buffer;
		}
		
		public function get buffer0():BitmapData
		{
			return _buffer0;
		}
		
		public function set buffer1(buffer:BitmapData):void
		{
			_buffer1 = buffer;
		}
		
		public function get buffer1():BitmapData
		{
			return _buffer1;
		}
		
		public function get drawBuffer():BitmapData
		{
			if(_bufferIndex != 0) return _buffer0;
			return _buffer1;
		}
		
		public function swap():void
		{
			if(_bufferIndex != 0)
			{
				_bufferIndex = 0;
				bitmapData = _buffer0;
			}
			else
			{
				_bufferIndex = 1;
				bitmapData = _buffer1;
			}
		}
	}
}