package gui.menus 
{
	import enums.MenuType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import gui.Menu;
	import gui.controls.ScrollBar;
	import gui.Game;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class CreditMenu extends Menu 
	{
		public static const ID:String = "Credit_Menu";
		
		private var scrollBar:ScrollBar = new ScrollBar(Game.MAIN_WINDOW_HEIGHT);
		private var content:Sprite;
		private var _textBox:TextField;
		
		public function CreditMenu() 
		{
			super(ID, MenuType.MainScreen);
			
			content = new Sprite();
			
			_textBox = new TextField();
			_textBox.defaultTextFormat = new TextFormat("Bombard", 18, Colors.LASER_GREEN);
			_textBox.wordWrap = true;
			_textBox.embedFonts = true;
			_textBox.x = 5;
			_textBox.width = Game.MAIN_WINDOW_WIDTH - 21;
			_textBox.autoSize = TextFieldAutoSize.LEFT;
			_textBox.multiline = true;
			content.addChild(_textBox);
			
			setText();
			
			content.graphics.beginFill(0, 0);
			content.graphics.drawRect(0, 0, Game.MAIN_WINDOW_WIDTH, Game.MAIN_WINDOW_HEIGHT);
			content.graphics.endFill();			
			addChild(content);
			
			scrollBar.x = Game.MAIN_WINDOW_WIDTH - 16;
			scrollBar.container = new Sprite();
			scrollBar.container.graphics.beginFill(0);
			scrollBar.container.graphics.drawRect(0, 0, ScrollBar.REGULAR_CONTAINER_WIDTH, ScrollBar.REGULAR_CONTAINER_HEIGHT);
			scrollBar.container.graphics.endFill();
			scrollBar.container.visible = false;
			addChild(scrollBar.container);
			addChild(scrollBar);
			scrollBar.content = content;
		}
		
		private function setText():void {
			var style:StyleSheet = new StyleSheet();
			style.parseCSS(".header {font-size: 30pt; margin-top: 10px;}")
			
			_textBox.styleSheet = style;
			
			_textBox.htmlText = 
								"<p class='header'>Art</p>" + 
								"<ul><li>Pixelfetish</li></ul>" +
								"<br/>" +
								"<p class='header'>Code</p>" +  
								"<ul>" +
									"<li>BitStallion</li>" +
									"<li>H.coder</li>" +
									"<li>Mihail</li>" +
									"<li>TheWrongHands</li>" +
								"</ul>" +
								"<br/>" +
								"<p class='header'>Game Design</p>" +  
								"<ul>" +
									"<li>Zeus Kabob</li>" +
								"</ul>" +
								"<br/>" + 
								"<p class='header'>World Design</p>" +  
								"<ul>" +
									"<li>Xmancometh</li>" +
								"</ul>" +
								"<br/>" + 
								"<p class='header'>Writing</p>" + 
								"<ul>" +
									"<li>Allthingsgerman</li>" +
									"<li>Duplicity</li>" +
									"<li>Floyd Fowler</li>" +
									"<li>Interlopersf</li>" +
									"<li>Mario Diamanti</li>" +
									"<li>Perry.smith545</li>" +
									"<li>Sunny</li>" +
									"<li>Tamharin Nim Peddicord</li>" +
									"<li>TARINunit9</li>" +
								"</ul>" +
								"<br/>";
		}
		
	}

}