package gui.menus 
{
	import enums.MenuType;
	import gui.Menu;
	import data.Entity;
	import gui.menus.components.StatPointSpender;
	import gui.PropertyChangedEvent;
	import enums.*;
	import flash.text.*;
	import gui.menus.auxiliary.*;
	import gui.Game;
	import flash.events.Event;
	
	/**
	 * Menu to assign statpoints and eventually perks to the player (or other characters) when they level up.
	 * @author GnW Team
	 */
	public class LevelUpMenu extends Menu 
	{
		public static const ID:String = "Level_Up_Menu";
		
		private var stats:StatPointSpender;
		private var textbox:TextField;
		private var _character:Entity;
		
		public function LevelUpMenu() 
		{
			super(ID, MenuType.MainScreen);
			
			stats = new StatPointSpender();
			stats.x = (Game.MAIN_WINDOW_WIDTH - stats.width) / 2;
			stats.y = (Game.MAIN_WINDOW_HEIGHT - stats.height) / 2 - 50;
			addChild(stats);
			
			stats.addEventListener(Event.CHANGE, statpointUpdate);
			stats.addEventListener(PropertyChangedEvent.Changed, statChanged);
			
			textbox = new TextField();
			textbox.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			textbox.wordWrap = true;
			textbox.embedFonts = true;
			textbox.width = Game.MAIN_WINDOW_WIDTH - 100;
			textbox.autoSize = TextFieldAutoSize.LEFT;
			textbox.antiAliasType = AntiAliasType.ADVANCED;
			textbox.multiline = true;
			textbox.x = (Game.MAIN_WINDOW_WIDTH - textbox.width) / 2;
			textbox.y = 400;
			textbox.text = "Testing";
			addChild(textbox);
		}
		
		override public function onLoad():void 
		{
			GameEngine.gui.hideAll();
			
			GameEngine.menuManager.topAuxMenu = StatDisplayMenu.ID;
			GameEngine.menuManager.bottomAuxMenu = null;
			
			GameEngine.gui.setAndShow(Game.YES_BUTTON_ID, "Start", complete);
			GameEngine.gui.hide(Game.YES_BUTTON_ID);
			
			textbox.text = "";
		}
		
		private function statChanged(e:PropertyChangedEvent):void
		{
			var stat:CharacterStat = e.newValue as CharacterStat;
			
			var text:String = "";
			
			switch(stat)
			{
				case CharacterStat.strength: 
					text = Assets.STRENGTH_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.endurance: 
					text = Assets.ENDURANCE_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.agility: 
					text = Assets.AGILITY_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.charisma: 
					text = Assets.CHARISMA_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.dexterity: 
					text = Assets.DEXTERITY_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.intelligence: 
					text = Assets.INTELLIGENCE_FLAVOUR_TEXT;
					break;
				
				case CharacterStat.willpower: 
					text = Assets.WILLPOWER_FLAVOUR_TEXT;
					break;
				
				default: 
					break;
			}
			
			textbox.text = text;
			(GameEngine.menuManager.getMenuById(StatDisplayMenu.ID) as StatDisplayMenu).stat = stat;
		}
		
		private function statpointUpdate(e:Event):void
		{
			if(stats.freePoints == 0)
				GameEngine.gui.show(Game.YES_BUTTON_ID);
			else
				GameEngine.gui.hide(Game.YES_BUTTON_ID);
		}
		
		private function complete(e:Event):void
		{
			stats.applyStats();
		}
		
		/**
		 * Sets up the level up menu. Should be called prior to the menu being selected.
		 * 
		 * @param	Entity character	The character that the player will assign stats to.
		 * @param	uint points			The number of stat points for the player to assign.
		 */
		public function setup(character:Entity, points:uint):void
		{
			stats.startStatAssignment(character, points);
		}
		
	}

}