package gui.controls {
	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import gui.controls.ConfigurableButton;
	import gui.PropertyChangedEvent;
	import utility.Util;
	
	/**
	 * A graduated linear slider for integer values.
	 * @author GnW Team
	 */
	public class Slider extends Sprite 
	{
		private var _leftButton:SimpleButton;
		private var _rightButton:SimpleButton;
		private var _sliderNub:Sprite;
		private var _sliderBar:Sprite;
		
		private var _width:Number;
		private var _low:int;
		private var _high:int;
		private var _inc:uint;
		private var _diff:int;
		
		private var _soundPath:String;
		
		private var _value:Number;
		private var _grad:uint;
		
		/**
		 * Creates a new slider UI component.
		 * 
		 * @param	width	The width of the slider. This is only the width of the slidable area.
		 * @param	low		The number on the low end of the slider.
		 * @param	high	The number on the high end of the slider.
		 * @param	inc		Number of separations on the slider
		 */
		public function Slider(width:Number, low:int, high:int, inc:uint) 
		{
			super();
			
			_width = width;
			_low = low;
			_high = high;
			_inc = inc;
			
			_diff = Math.abs(_high - _low) / (_inc + 1);
			
			_leftButton = new SimpleButton();
			var leftIcon:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/MinusButton.png") as Bitmap;
			_leftButton.upState = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/MinusButtonOver.png") as Bitmap;
			_leftButton.downState = leftIcon;
			_leftButton.hitTestState = leftIcon;
			_leftButton.overState = leftIcon;
			_leftButton.addEventListener(MouseEvent.CLICK, decrementSlider);
			
			_leftButton.x = 0;
			_leftButton.y = 0;
			_leftButton.useHandCursor = false;
			addChild(_leftButton);
			
			_rightButton = new SimpleButton();
			var rightIcon:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/PlusButton.png") as Bitmap;
			_rightButton.upState = GameEngine.fileResourceManager.getResource("images/gui/minibuttons/PlusButtonOver.png") as Bitmap;
			_rightButton.downState = rightIcon;
			_rightButton.hitTestState = rightIcon;
			_rightButton.overState = rightIcon;
			_rightButton.addEventListener(MouseEvent.CLICK, incrementSlider);
			
			_rightButton.x = _leftButton.width + 10 + width;
			_rightButton.y = 0;
			_rightButton.useHandCursor = false;
			addChild(_rightButton);
			
			_sliderBar = new Sprite();
			drawSliderBar();
			_sliderBar.x = _leftButton.width + 5;
			addChild(_sliderBar)
			
			_sliderNub = new Sprite();
			_sliderNub.graphics.beginFill(Colors.LASER_GREEN);
			_sliderNub.graphics.drawRect( -6, -6, 12, 12);
			_sliderNub.graphics.endFill();
			_sliderBar.addChild(_sliderNub);
			setSlider(0, false);
			
			_sliderNub.addEventListener(MouseEvent.MOUSE_DOWN, nubClicked);
		}
		
		private function incrementSlider(e:Event):void
		{
			setSlider(_grad + 1, true);
		}
		
		private function decrementSlider(e:Event):void
		{
			setSlider(_grad - 1, true);
		}
		
		private function nubClicked(e:Event):void 
		{
			addEventListener(Event.ENTER_FRAME, enterFrame);
			stage.addEventListener(MouseEvent.MOUSE_UP, nubReleased);
		}
		
		private function nubReleased(e:Event):void 
		{
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			stage.removeEventListener(MouseEvent.MOUSE_UP, nubReleased);
		}
		
		/**
		 * Used when the mouse has clicked and held on the slider nub to react to changes in mouse position.
		 * @param	e
		 */
		private function enterFrame(e:Event):void 
		{
			var graduation:Number = graduationX(1);
			var mouse:Number = _sliderBar.mouseX;
			
			if (mouse < graduation / 2 )
				setSlider(0, true);
			else if (mouse > _width - (graduation / 2))
				setSlider(_inc + 1, true);
			else {
				var grad:int = 1;
				while ( mouse > (graduationX(grad) + (graduation / 2)))
				{
					grad++;
				}
				setSlider(grad, true);
			}
		}
		
		/**
		 * Draws the slider bar and it's graduations.
		 */
		private function drawSliderBar():void
		{
			_sliderBar.graphics.clear();
			_sliderBar.graphics.lineStyle(2, Colors.LASER_GREEN);
			
			_sliderBar.graphics.moveTo(0, 0);
			_sliderBar.graphics.lineTo(0, 26);
			
			_sliderBar.graphics.moveTo(_width, 0);
			_sliderBar.graphics.lineTo(_width, 26);
			
			_sliderBar.graphics.moveTo(0, 13);
			_sliderBar.graphics.lineTo(_width, 13);
			
			for (var i:int = 0; i < _inc; i++) {
				var val:Number = graduationX(i + 1);
				_sliderBar.graphics.moveTo(val, 6);
				_sliderBar.graphics.lineTo(val, 20);
			}
		}
		
		private function graduationX(grad:int):Number
		{
			var dist:Number = _width / (_inc + 1);
			return dist * grad;
		}
		
		private function graduationValue(grad:int):Number
		{
			return _diff * grad + _low;
		}
		
		private function setSlider(grad:int, playSound:Boolean):void
		{
			grad = Util.clampInt(0, _inc + 1, grad);
			
			if (grad == _grad)
				return;
			
			_grad = grad;
			
			_sliderNub.y = 13;
			_sliderNub.x = graduationX(_grad)
			
			if(_soundPath && playSound)
				GameEngine.soundManager.playSoundFX(_soundPath);
			
			var oldValue:Number = _value;
			_value = graduationValue(_grad);
			
			dispatchEvent(new PropertyChangedEvent(PropertyChangedEvent.Changed, oldValue, _value));
		}
		
		public function get value():Number 
		{
			return _value;
		}
		
		public function set value(value:Number):void 
		{
			value = Util.clampNumber(_low, _high, value);
			
			var grad:int = 0;
			while ( value > graduationValue(grad))
			{
				grad++;
			}
			
			setSlider(grad, false);
		}
		
		public function set soundPath(value:String):void 
		{
			_soundPath = value;
		}
	}

}