package gui.controls 
{
	import flash.display.Sprite;
	import flash.text.AntiAliasType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class Bar extends Sprite
	{
		private static const OUTLINE_COLOR:uint = 0x00FF00;
		private static const FULL_COLOR:uint = 0x1A7F00;
		private static const EMPTY_COLOR:uint = 0x0D4100;		
		
		private const _barHeight:uint = 15;
		private const _hGap:uint = 6;
		private var _maxVirtualLength:uint;
		private var _textField:TextField = new TextField();
		private var _title:String;
		private var _virtualLength:uint;
		private var _width:uint;
		private var _showMax:Boolean;
		
		private var _barEmpty:Sprite;
		private var _barFill:Sprite;
		private var _barEnd:Sprite;
		
		public function Bar(width:uint, title:String, maxVirtualLength:uint, virtualLength:uint) 
		{
			super();
			
			_maxVirtualLength = maxVirtualLength;
			_title = title;
			_virtualLength = virtualLength;
			_width = width;
			
			_textField.x = _hGap;
			_textField.width = _width - 2 * _hGap;
			_textField.autoSize = TextFieldAutoSize.LEFT;
			_textField.defaultTextFormat = new TextFormat("Bombard", 17, Colors.LASER_GREEN);
			_textField.antiAliasType = AntiAliasType.ADVANCED;
			_textField.embedFonts = true;
			
			graphics.beginFill(0, 0);
			graphics.drawRect(0, 0, _width, 1);
			graphics.endFill();
			
			_barEmpty = new Sprite();
			_barEmpty.graphics.beginFill(EMPTY_COLOR);
			_barEmpty.graphics.drawRect(0, _textField.height, _width - 2 * _hGap, _barHeight);
			_barEmpty.graphics.endFill();
			_barEmpty.x = _hGap;
			addChild(_barEmpty);
			
			_barFill = new Sprite();
			_barFill.graphics.beginFill(FULL_COLOR);
			_barFill.graphics.drawRect(0, _textField.height, _width - 2 * _hGap, _barHeight);
			_barFill.graphics.endFill();
			_barFill.x = _hGap;
			addChild(_barFill);
			
			_barEnd = new Sprite();
			_barEnd.graphics.beginFill(OUTLINE_COLOR);
			_barEnd.graphics.drawRect(0, _textField.height, 1, _barHeight);
			_barEnd.graphics.endFill();
			addChild(_barEnd);
			
			addChild(_textField);
			
			update();
		}
		
		private function update():void
		{	
			var w:uint = Math.round((_width - 2 * _hGap) * _virtualLength / _maxVirtualLength);
			
			_barFill.width = w;
			_barEnd.x = _hGap + w;
			
			_textField.text = _title + ": " + virtualLength + (_showMax ? "/" + _maxVirtualLength : "");
		}
		
		public function get virtualLength():uint 
		{
			return _virtualLength;
		}
		
		public function set virtualLength(value:uint):void 
		{			
			_virtualLength = value;
			update();
		}
		
		public function get maxVirtualLength():uint 
		{
			return _maxVirtualLength;
		}
		
		public function set maxVirtualLength(value:uint):void 
		{			
			_maxVirtualLength = value;
			update();
		}
		
		public function get showMax():Boolean 
		{
			return _showMax;
		}
		
		public function set showMax(value:Boolean):void 
		{
			_showMax = value;
			update();
		}
		
	}

}