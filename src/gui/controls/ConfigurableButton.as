package gui.controls
{
	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import Assets;
	import flash.text.TextFormatAlign;
	
	/**
	 * A wrapper for a SimpleButton with overlaid text.
	 * @author GnW Team
	 */
	public class ConfigurableButton extends Sprite 
	{
		private var simpleButton:SimpleButton = new SimpleButton()
		private var textField:TextField = new TextField();
		
		private var _pressed:Bitmap;
		private var _released:Bitmap;
		private var _hotKeys:Vector.<uint>;
		private var _hotKeyText:String;
		private var _label:String;
		private var _action:Function;
		private var _blink:Function;
		private var _soundPath:String;
		
		/**
		 * Creates a new simple button.
		 * @param	textColor	Sets the color of the label text. Defaults to black.
		 */
		public function ConfigurableButton(textColor:uint = 0x000000, font:String = "Bombard", fontSize:uint = 20) 
		{
			super();
			simpleButton.useHandCursor = false;
			_hotKeys = new Vector.<uint>();
			_hotKeyText = null;
			textField.autoSize = TextFieldAutoSize.CENTER;
			textField.defaultTextFormat = new TextFormat(font, fontSize, textColor, null, null, null, null, null, TextFormatAlign.CENTER);
			textField.embedFonts = true;
			textField.mouseEnabled = false;
			addChild(simpleButton);
			addChild(textField);
			simpleButton.addEventListener(MouseEvent.CLICK, simpleButton_click);
			simpleButton.addEventListener(MouseEvent.MOUSE_DOWN, simpleButton_down);
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keydown);
		}
		
		private function simpleButton_click(e:MouseEvent):void 
		{
			e.stopPropagation();
			var mouseEvent:MouseEvent = new MouseEvent(MouseEvent.CLICK);
			
			dispatchEvent(mouseEvent);
		}
		
		private function simpleButton_down(e:MouseEvent):void 
		{
			if(_soundPath)
				GameEngine.soundManager.playSoundFX(_soundPath);
		}
		
		private function keydown(e:KeyboardEvent):void 
		{
			if(visible && !(stage != null && stage.focus is TextField) && _hotKeys.indexOf(e.keyCode) >= 0)
			{
				var mouseEvent:MouseEvent = new MouseEvent(MouseEvent.CLICK);
				dispatchEvent(mouseEvent);
			}
		}
		
		/**
		 * The image that the button will display when pressed.
		 */
		public function get pressed():Bitmap 
		{
			return _pressed;
		}
		
		/**
		 * The image that the button will display when pressed.
		 */
		public function set pressed(value:Bitmap):void 
		{
			simpleButton.downState = value;
			_pressed = value;
		}
		
		/**
		 * The image that the button will display when not being pressed.
		 */
		public function get released():Bitmap 
		{			
			return _released;
		}
		
		/**
		 * The image that the button will display when not being pressed.
		 */
		public function set released(value:Bitmap):void 
		{
			simpleButton.hitTestState = value;
			simpleButton.overState = value;
			simpleButton.upState = value;
			_released = value;
		}
		
		private function adjustButtonText():void
		{
			var label:String = _label || "";
			if(_hotKeyText != null)
			{
				if (_hotKeyText.substring(-1, -1) != "\n")
				{
					textField.text = _hotKeyText + label;
				}
				else
				{
					textField.text = _hotKeyText + " " + label;
				}
			}
			else
			{
				textField.text = label;
			}
			textField.x = (simpleButton.width - textField.width) / 2;
			textField.y = (simpleButton.height - textField.height) / 2;
		}
		
		public function set hotkeyText(text:String):void
		{
			_hotKeyText = text;
			adjustButtonText();
		}
		
		public function get hotkeyText():String
		{
			return _hotKeyText;
		}
		
		public function addHotkey(keycode:uint):void
		{
			if(_hotKeys.indexOf(keycode) < 0)_hotKeys.push(keycode);
		}
		
		public function removeHotkey(keycode:uint):void
		{
			var index:uint = _hotKeys.indexOf(keycode);
			if(index >= 0) _hotKeys = _hotKeys.splice(index, 1);
		}
		
		/**
		 * The text that will be overlaid on the button.
		 */
		public function get label():String 
		{
			return _label;
		}
		
		/**
		 * The text that will be overlaid on the button.
		 */
		public function set label(value:String):void 
		{
			_label = value;
			adjustButtonText();
		}
		
		/**
		 * The function that will be called when the button is clicked. Must be a valid event listener.
		 */
		public function get action():Function 
		{
			return _action;
		}
		
		/**
		 * The function that will be called when the button is clicked. Must be a valid event listener.
		 */
		public function set action(value:Function):void
		{
			if (_action != null)
				removeEventListener(MouseEvent.CLICK, _action);
			_action = value;
			if (_action != null)
				addEventListener(MouseEvent.CLICK, _action);
		}
		
		public function get blink():Function
		{
			return _blink;
		}
		
		public function set blink(value:Function):void
		{
			_blink = value;
		}
		
		public function setBlinkState(value:Boolean):void
		{
			if(value)
			{
				simpleButton.hitTestState = _pressed;
				simpleButton.overState = _pressed;
				simpleButton.upState = _pressed;
			}
			else
			{
				simpleButton.hitTestState = _released;
				simpleButton.overState = _released;
				simpleButton.upState = _released;
			}
		}
		
		/**
		 * The file resource path to the sound object that will play when the button is clicked.
		 */
		public function get soundPath():String 
		{
			return _soundPath;
		}
		
		/**
		 * The file resource path to the sound object that will play when the button is clicked.
		 */
		public function set soundPath(value:String):void 
		{
			_soundPath = value;
		}
		
		public function addGlow(glow:GlowFilter):void
		{
			textField.filters = [glow];
		}
	}
}