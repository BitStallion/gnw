package gui.controls
{
	import flash.display.Bitmap;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class ScrollBar extends Sprite
	{
		public static const REGULAR_CONTAINER_X:int = 320;
		public static const REGULAR_CONTAINER_Y:int = 64;
		public static const REGULAR_CONTAINER_WIDTH:int = 656;
		public static const REGULAR_CONTAINER_HEIGHT:int = 544;
		public static const REGULAR_X:int = 978;
		public static const REGULAR_Y:int = 66;
		
		private var _container:Sprite;
		private var _content:Sprite;
		
		private var incButton:SimpleButton = new SimpleButton();
		private var decButton:SimpleButton = new SimpleButton();
		
		private var thumb:Sprite = new Sprite();
		private var scrollImage:Sprite;
		private var placeholder:Sprite = new Sprite;
		
		private var dragY:int;
		
		public function ScrollBar(height:Number)
		{
			super();
			visible = false;
			
			scrollImage = new Sprite();
			scrollImage.graphics.beginFill(Colors.DARK_GREEN);
			scrollImage.graphics.drawRect(0, 0, 12, height);
			scrollImage.graphics.endFill();
			addChild(scrollImage);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addChild(incButton);
			addChild(decButton);
			
			var incImage:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/IncButton.png") as Bitmap;
			var decImage:Bitmap = GameEngine.fileResourceManager.getResource("images/gui/DecButton.png") as Bitmap;
			
			thumb.addChild(GameEngine.fileResourceManager.getResource("images/gui/Thumb.png") as Bitmap);
			
			incButton.downState = incImage;
			incButton.hitTestState = incImage;
			incButton.overState = incImage;
			incButton.upState = incImage;
			incButton.useHandCursor = false;
			
			decButton.downState = decImage;
			decButton.hitTestState = decImage;
			decButton.overState = decImage;
			decButton.upState = decImage;
			decButton.useHandCursor = false;
			
			incButton.y = scrollImage.height - incButton.height;
			
			placeholder.y = decButton.height;
			placeholder.graphics.beginFill(0, 0);
			placeholder.graphics.lineStyle(0, 0, 0);
			placeholder.graphics.drawRect(0, 0, scrollImage.width, scrollImage.height - incButton.height - decButton.height);
			placeholder.graphics.endFill();
			
			addChild(placeholder);
			addChild(thumb);
			
			incButton.addEventListener(MouseEvent.MOUSE_DOWN, incButton_mouseDown);
			decButton.addEventListener(MouseEvent.MOUSE_DOWN, decButton_mouseDown);
			placeholder.addEventListener(MouseEvent.CLICK, placeholder_click);
			thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumb_mouseDown);			
			update();
		}
		
		private function content_mouseWheel(e:MouseEvent):void
		{
			moveContentRelative(e.delta * content.height / 100);
			update();
		}
		
		private function thumb_mouseDown(e:MouseEvent):void
		{
			e.stopPropagation();
			dragY = e.stageY - thumb.localToGlobal(new Point).y;
			stage.addEventListener(MouseEvent.MOUSE_UP, stage_mouseUp);
			addEventListener(Event.ENTER_FRAME, enterFrame);
		}
		
		private function enterFrame(e:Event):void
		{
			var p1:Point = placeholder.localToGlobal(new Point);
			var y1:int = p1.y + dragY;
			
			var p2:Point = placeholder.localToGlobal(new Point(0, placeholder.height));
			var y2:int = p2.y - thumb.height + dragY;
			
			if (stage.mouseY < y1)
			{
				thumb.y = 0;
			}
			else if (stage.mouseY > y1 && stage.mouseY < y2)
			{
				thumb.y = thumb.parent.globalToLocal(new Point(0, stage.mouseY)).y - dragY;
			}
			else
			{
				thumb.y = thumb.parent.height;
			}
			
			moveContent(container.y - (content.height * (thumb.y - decButton.height) / placeholder.height));
			update();
		}
		
		private function placeholder_click(e:MouseEvent):void
		{
			if (e.localY > thumb.y)
			{
				moveContentRelative(-content.height / 20);
			}
			else if (e.localY < thumb.y)
			{
				moveContentRelative(content.height / 20);
			}
			update();
		}
		
		private function decButton_mouseDown(e:MouseEvent):void
		{
			e.stopPropagation();
			stage.addEventListener(MouseEvent.MOUSE_UP, stage_mouseUp);
			addEventListener(Event.ENTER_FRAME, decTick);
		}
		
		private function decTick(e:Event):void
		{
			trace("decTick");
			moveContentRelative(content.height / 100);
			update();
		}
		
		private function incButton_mouseDown(e:MouseEvent):void
		{
			e.stopPropagation();
			stage.addEventListener(MouseEvent.MOUSE_UP, stage_mouseUp);
			addEventListener(Event.ENTER_FRAME, incTick);
		}
		
		private function stage_mouseUp(e:MouseEvent):void
		{
			removeEventListener(Event.ENTER_FRAME, incTick);
			removeEventListener(Event.ENTER_FRAME, decTick);
			removeEventListener(Event.ENTER_FRAME, enterFrame);
			thumb.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, stage_mouseUp);
		}
		
		private function incTick(e:Event):void
		{
			trace("incTick");
			moveContentRelative(-content.height / 100);
			update();
		}
		
		public function update():void
		{
			if (container == null || content == null || container.height >= content.height)
			{
				visible = false;
				if (content)
				{
					content.removeEventListener(MouseEvent.MOUSE_WHEEL, content_mouseWheel);
					removeEventListener(MouseEvent.MOUSE_WHEEL, content_mouseWheel);
				}
				return;
			}
			visible = true;
			if (content)
			{
				content.addEventListener(MouseEvent.MOUSE_WHEEL, content_mouseWheel);
				addEventListener(MouseEvent.MOUSE_WHEEL, content_mouseWheel);
			}			
			content.mask = container;
			
			
			thumb.height = placeholder.height * container.height / content.height;
			thumb.y = decButton.height + placeholder.height * (container.y - content.y) / content.height;
			
			thumb.scaleX = 1;
		}
		
		private function moveContent(y:int):void
		{
			content.y = y;
			if (content.y > container.y)
			{
				content.y = container.y;
			}
			if (content.y + content.height < container.y + container.height)
			{
				content.y = container.y + container.height - content.height;
			}
		}
		
		private function moveContentRelative(dy:int):void
		{
			moveContent(content.y + dy);
		}
		
		public function get container():Sprite
		{
			return _container;
		}
		
		public function set container(value:Sprite):void
		{
			_container = value;
			update();
		}
		
		public function get content():Sprite
		{
			return _content;
		}
		
		public function set content(value:Sprite):void
		{
			_content = value;
			update();
		}
		
		public function resetScroll():void
		{
			moveContent(0);
			update();
		}
		
		public function toEnd():void
		{
			moveContent(-content.height);
			update();
		}
	
	}

}