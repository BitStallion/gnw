package gui 
{
	
	/**
	 * Class is deisned to control and mange the cursor.
	 * @author BlueLight
	 */
	
	import flash.ui.Mouse;
	import flash.ui.MouseCursorData;
	import flash.display.BitmapData;
	import flash.geom.Point;
	public class CursorManger 
	{
		public static const cursor_file:String = "images/gui/cursor/cursor.png";
		//public static const cursor_file = "images/gui/DecButton.png";
		public static const CURSOR:String = "GNW_Cursor";
		
		
		public function CursorManger() 
		{
			initCursor();		
		}
		
		private function initCursor():void
		{
			var cursorBitmapData:BitmapData = GameEngine.fileResourceManager.getImage(cursor_file).bitmapData; //Loads pixels cursor image.
			
			var cursorVector:Vector.<BitmapData> = new Vector.<BitmapData>();
			cursorVector[0] = cursorBitmapData;//adds our image as the first image of an animation. If we stored more images in vector, and gave the cursor a FPS we could make an animation
			
			var cursorData:MouseCursorData = new MouseCursorData();
			//cursorData.hotSpot = new Point(0, 0); // click location // not really needed. Default is 0,0
			cursorData.data = cursorVector; 
				
			
			Mouse.registerCursor(CURSOR, cursorData); //Telling the computer how to use our cursor.
			Mouse.cursor = CURSOR;// tell the computer to use the cursor that uses the string CURSOR which is our cursor.
			
		}
		
	}

}