package gui 
{
	import enums.MenuType;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Menu extends Sprite 
	{
		protected var _id:String;
		protected var _type:MenuType;
		
		public function Menu(id:String, type:MenuType) 
		{
			super();
			_id = id;
			_type = type;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get type():MenuType 
		{
			return _type;
		}
		
		/**
		 * Used to perform any actions that need to be done when the menu is loaded.
		 * Subclasses should override this method to use this functionality.
		 */
		public function onLoad():void
		{
		}
		
		public function onUnload():void
		{
		}
	}
}