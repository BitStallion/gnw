package gui 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class PropertyChangedEvent extends Event 
	{
		private var _newValue:Object;
		private var _oldValue:Object;
		
		public static const Changed:String = "UI_Prop_Change"
		
		public function PropertyChangedEvent(type:String, oldValue:Object, newValue:Object, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
			_oldValue = oldValue;
			_newValue = newValue;
		} 
		
		public override function clone():Event 
		{ 
			return new PropertyChangedEvent(type, oldValue, newValue, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("PropertyChangedEvent", "oldValue", "newValue", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
		public function get newValue():Object 
		{
			return _newValue;
		}
		
		public function get oldValue():Object 
		{
			return _oldValue;
		}
		
	}
	
}