package events 
{
	/**
	 * Used to link one scene to other scenes. These are usually used as dialogue choices and are presented at the end of a scene.
	 * 
	 * @author GnW Team
	 */
	public class Link 
	{
		private var _text:String;
		private var _sceneId:String;
		private var _eventId:String;
		
		/**
		 * Creates a new link.
		 * 	
		 * @param	text		The text that should appear in the dialogue choice.
		 * @param	targetId	The ID of the scene that link targets.	
		 */
		public function Link(text:String = null, sceneId:String = null, eventId:String = null) 
		{
			_text = text;
			_sceneId = sceneId;
			_eventId = eventId;
		}
		
		/**
		 * The text that should be displayed for the player when choosing a link to select.
		 */
		public function get text():String
		{
			return _text;
		}
		
		/**
		 * The ID of the scene that the link targets. If blank or null,
		 * assume the starting scene of the event.
		 */
		public function get sceneId():String
		{
			return _sceneId;
		}
		
		/**
		 * The ID of the event that the scene is part of. If blank or null,
		 * assume the same event as the link owner.
		 */
		public function get eventId():String 
		{
			return _eventId;
		}
	}

}