package events 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class EventManager 
	{
		private var _events:Dictionary;
		private var _currentEvent:GameEvent;
		
		public function EventManager() 
		{
			_events = new Dictionary();
		}
		
		public function set currentEvent(event:GameEvent):void
		{
			_currentEvent = event;
		}
		
		public function get currentEvent():GameEvent
		{
			return _currentEvent;
		}
		
		/**
		 * Adds a new event to the manager event library. All events added must have unique IDs.
		 *
		 * @param	event	The event to add.
		 */
		public function addEvent(event:GameEvent):void
		{
			_events[event.id] = event;
		}
		
		/**
		 * Returns an event with a matching ID.
		 *
		 * @param	id	The ID of the event to search for.
		 * @return	Returns either the event with a matching ID or null if no event with that ID can be found.
		 */
		public function getEventById(id:String):GameEvent
		{
			if(id == null)
				return null;
			
			return _events[id];
		}
		
		/**
		 * Returns a list of events with matching IDs. If no ids match, an empty list is returned.
		 *
		 * @param	ids	The IDs of the events to search for.
		 * @return	Returns a list containing all events that have IDs matching those in the id list.
		 */
		public function getEventsByIds(ids:Vector.<String>):Vector.<GameEvent>
		{
			var eventList:Vector.<GameEvent> = new Vector.<GameEvent>();
			
			for each(var id:String in ids)
			{
				var event:GameEvent = getEventById(id);
				
				if(event != null)
					eventList.push(event);
			}
			
			return eventList;
		}
	}
}