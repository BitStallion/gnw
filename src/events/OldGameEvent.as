package events 
{
	import combat.Random;
	import flash.utils.Dictionary;
	import navigation.Location;
	import system.GlobalConstants;
	import mx.utils.StringUtil;
	import utility.GnWError;
	
	/**
	 * GameEvents store all information that in game events need to work. Each event is composed of Scenes, Conditions and Actors,
	 * and also has a globally unique ID, a name/title, a location that it takes place in, and a probability weighting that is used 
	 * in random selections of events. 
	 * @author ...
	 */
	public class OldGameEvent 
	{
		private var _actors:Dictionary; // List of characters referenced by the scenes
		private var _scenes:Dictionary; // List of scenes that make up the event
		private var _conditions:Vector.<Condition>; // List of condtions that need to pass for the game event to be available.
		private var _id:String;		// Unique id for the event;
		
		private var _name:String;
		private var _startingSceneIds:Array;
		private var _location:String
		private var _prob:Number;
		
		/**
		 * Creates a new GameEvent.
		 * 
		 * @param	id			A unique id for the event.
		 * @param	name		A non-unique name for the event.
		 * @param	location	The location that the event takes place in.
		 * @param	prob		The probabability weighting of the event being selected when randomly chosen. Default value is 1.0.
		 * @param	startingSceneId		The id of the starting scene for the event. If none is specified, the event with the id "start" is selected.
		 */
		public function OldGameEvent(id:String, name:String, location:String, prob:Number = 1.0, startingSceneId:String = null) 
		{
			_actors = new Dictionary();
			_scenes = new Dictionary();
			_conditions = new Vector.<Condition>();
			
			_id = id;
			_name = name;
			_location = location;
			_prob = prob;
			
			if (startingSceneId != null && startingSceneId.length > 0)
			{
				_startingSceneIds = startingSceneId.split(",");
				
				for (var i:int = 0; i < _startingSceneIds.length; i++ )
					_startingSceneIds[i] = StringUtil.trim(_startingSceneIds[i]);
			}
		}
		
		/**
		 * Adds a scene to the event. All scenes in the event must have unique IDs.
		 * 
		 * @param	scene	The scene to add.
		 */
		public function addScene(scene:Scene):void
		{
			if (scene.id == null || scene.id.length == 0 )
				throw new GnWError("Cannot add a scene with a null or empty id.");
			
			if (scene.id in _scenes)
				throw new GnWError("A scene with the id \"" + scene.id + "\" has already been added to the event\"" + _id + "\".");
			
			_scenes[scene.id] = scene;
			scene.setEvent(this);
		}
		
		/**
		 * Adds a new actor to the event. Each actor added must have a unique ID.
		 * 
		 * @param	actor	The actor to add.
		 */
		public function addActor(actor:Actor):void
		{
			if (actor.id == null || actor.id.length == 0 )
				throw new GnWError("Cannot add an actor with a null or empty id.");
			
			if (actor.id in _actors)
				throw new GnWError("An actor with the id \"" + actor.id + "\" has already been added to the event\"" + _id + "\".");
			
			_actors[actor.id] = actor;
		}
		
		/**
		 * Adds a new condition to the event. 
		 * 
		 * @param	condition	The condition to add.
		 */
		public function addCondition(condition:Condition):void
		{
			_conditions.push(condition);
		}
		
		/**
		 * The ID of the event.
		 */
		public function get id():String
		{
			return _id;
		}
		
		/**
		 * The name of the event. Can also be the title.
		 */
		public function get name():String
		{
			return _name;
		}
		
		/**
		 * The location that the event takes place in.
		 */
		public function get location():String 
		{
			return _location;
		}
		
		/**
		 * The probability weighting of the event.
		 */
		public function get prob():Number 
		{
			return _prob;
		}
		
		/**
		 * The scene that the event should start from.
		 */
		public function get startingScene():Scene
		{
			if (_startingSceneIds != null && _startingSceneIds.length > 0)
			{
				if (_startingSceneIds.length == 1)
					return getSceneById(_startingSceneIds[0])
				else
				{
					var scenes:Array = new Array();
					
					for each(var id:String in _startingSceneIds)
					{
						var scene:Scene =  getSceneById(id);
						if (scene.evalConditions(GameEngine.getCurrentDataContext()))
							scenes.push(scene);
					}
					
					var rand:int = new Random().getNextInt(scenes.length);
					return scenes[rand-1];
				}
			}
			
			return getSceneById(GlobalConstants.StartingSceneId);
		}
		
		/**
		 * Returns a scene from the event that matches the passed ID.
		 * 
		 * @param	id	The ID of the scene to be returned.
		 * @return	Returns either the scene with the matching ID, or null if no scene has a matching ID.
		 */
		public function getSceneById(id:String):Scene
		{
			if (id == null || id.length == 0)
				return null;
				
			return _scenes[id];
		}
		
		/**
		 * The list of actors used in the event.
		 */
		public function get actors():Vector.<Actor>
		{
			var vec:Vector.<Actor> = new Vector.<Actor>();
			
			for each (var actor:Actor in _actors)
				vec.push(actor);
			
			return vec;
		}
		
		public function get startingSceneIds():Array 
		{
			return _startingSceneIds;
		}
		
		/**
		 * Evaluates all conditions for the GameEvent and returns the result.
		 * 
		 * @return If all conditions pass, true is returned. If any condition fails, false is returned.
		 */
		public function evalConditions(dataContext:Object):Boolean
		{
			if (_conditions.length == 0 )
				return true;
				
			for (var i:int = 0; i < _conditions.length; i++)
			{
				if (!_conditions[i].evaluate(dataContext))
					return false;
			}
			
			return true;
		}
	}
}