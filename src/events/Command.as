package events
{
	import enums.TriggerType;
	import data.Action;
	import data.IActionInvoker;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Command implements IActionInvoker
	{
		private var _actions:Vector.<Action>;
		private var _text:String;
		
		public function Command(text:String)
		{
			_actions = new Vector.<Action>();
			_text = text;
		}
		
		public function get text():String
		{
			return _text;
		}
		
		/**
		 * Adds a new Action to the command.
		 *
		 * @param	action	The action to add to the command.
		 */
		public function addAction(action:Action):void
		{
			_actions.push(action);
		}
		
		public function invokeActions(type:TriggerType):void
		{
			for each(var action:Action in _actions)
			{
				GameEngine.actionManager.queueAction(action);
			}
		}
		
		public function invokeActionsForEvent(e:Event):void
		{
			invokeActions(null);
		}
	
	}

}