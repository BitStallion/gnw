package events 
{
	import data.Action;
	import data.IActionInvoker;
	import enums.ActionType;
	import enums.TriggerType;
	/**
	 * Scenes contain the actual text displayed in events and can be linked together to form dialogue trees. Scenes also have conditions 
	 * that should be met for a scene to become available for selection from another scene.
	 * 
	 * @author GnW Team
	 */
	public class Scene implements IActionInvoker
	{
		private var _links:Vector.<Link>;
		private var _conditions:Vector.<Condition>;
		private var _actions:Vector.<Action>;
		private var _commands:Vector.<Command>;
		private var _event:OldGameEvent;
		private var _imageName:String;
		
		private var _id:String;
		private var _text:String;
		
		public var exitText:String;
		
		/**
		 * Creates a new scene.
		 * 
		 * @param	id	The ID of the scene. This only needs to be unique within the event that contains it.
		 */
		public function Scene(id:String, text:String) 
		{
			_links = new Vector.<Link>();
			_conditions = new Vector.<Condition>();
			_actions = new Vector.<Action>();
			_commands = new Vector.<Command>();
			
			_id = id;
			_text = text;
		}
		
		internal function setEvent(value:OldGameEvent):void
		{
			_event = value;
		}
		
		public function get event():OldGameEvent
		{
			return _event;
		}
		
		/**
		 * Adds a new link to the scene.
		 * 
		 * @param	link	The link to add.
		 */
		public function addLink(link:Link):void 
		{
			_links.push(link);
		}
		
		
		/**
		 * All links to other scenes the that scene contains.
		 */
		public function get links():Vector.<Link>
		{
			var linkList:Vector.<Link> = new Vector.<Link>();
			
			for each (var link:Link in _links)
				linkList.push(link);
			
			return linkList;
		}
		
		/**
		 * Tests the conditions of each scene that this scene links to and returns only the set of links pointing
		 * to scenes that pass evaluation using the provided data context.
		 * 
		 * @param	dataContext		The data context that the scene conditions will evaluate against.
		 * @return	A set of links that pass evalution.	
		 */
		public function validLinks(dataContext:Object):Vector.<Link>
		{
			var linkList:Vector.<Link> = new Vector.<Link>();
			
			for each (var link:Link in _links)
			{
				var linkEvent:OldGameEvent = GameEngine.oldEventManager.getEventById(link.eventId);
				
				if (event == null)
					linkEvent = event;
				
				var scene:Scene = linkEvent.getSceneById(link.sceneId) || linkEvent.startingScene;
				
				if (scene != null && scene.evalConditions(dataContext))
					linkList.push(link);
			}
			
			return linkList;
		}
		
		/**
		 * Adds a new condition to the scene.
		 * 
		 * @param	condition	The condition to add to the scene.
		 */
		public function addCondition(condition:Condition):void
		{
			_conditions.push(condition);
		}
		
		/**
		 * Adds a new Action to the scene.
		 * 
		 * @param	action
		 */
		public function addAction(action:Action):void
		{
			_actions.push(action);
		}
		
		/**
		 * Adds a new Command to the scene.
		 * 
		 * @param	command
		 */
		public function addCommand(command:Command):void
		{
			_commands.push(command);
		}
		
		/**
		 * All commands owned by the scene.
		 */
		public function get commands():Vector.<Command>
		{
			var commandList:Vector.<Command> = new Vector.<Command>();
			
			for each (var command:Command in _commands)
				commandList.push(command);
			
			return commandList;
		}
		
		/**
		 * The ID of the scene. Must be unique within the event.
		 */
		public function get id():String
		{
			return _id;
		}
		
		public function set text(text:String):void
		{
			_text = text;
		}
		
		/**
		 * The scene's unparsed text.
		 */
		public function get text():String
		{
			return _text;
		}
		
		public function set imageName(value:String):void 
		{
			_imageName = value;
		}
		
		public function get imageName():String 
		{
			return _imageName;
		}
		
		/**
		 * Evaluates all conditions for the Scene and returns the result.
		 * 
		 * @param	dataContext		The data context obeject used in the evaluation.
		 * @return	If all conditions pass, true is returned. If any condition fails, false is returned.
		 */
		public function evalConditions(dataContext:Object):Boolean
		{
			if (_conditions.length == 0 )
				return true;
				
			for (var i:int = 0; i < _conditions.length; i++)
			{
				if (!_conditions[i].evaluate(dataContext))
					return false;
			}
			
			return true;
		}
		
		public function invokeActions(trigger:TriggerType):void
		{
			for each (var action:Action in _actions)
			{
				if (action.trigger == trigger )
					GameEngine.actionManager.queueAction(action);
			}
		}
		
	}
}