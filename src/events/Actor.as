package events 
{
	import enums.CharacterType;
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Actor 
	{
		private var _name:String;
		private var _id:String;
		private var _type:CharacterType;
		
		public function Actor(_name:String, id:String, type:CharacterType = null) 
		{
			_id = id;
			_name = name;
			
			_type = type == null ? CharacterType.Templated : type;
		}
		
		public function get name():String 
		{
			return _name;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get type():CharacterType 
		{
			return _type;
		}
	}
}