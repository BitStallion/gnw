package events 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class GameEvent 
	{
		private var _id:String;
		private var _name:String;
		private var _check:Function;
		private var _event:Function;
		
		public function GameEvent(id:String, name:String, check:Function, event:Function)
		{
			_id = id;
			_name = name;
			_check = check;
			_event = event;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function get check():Function
		{
			return _check || function():Boolean
			{
				return true;
			};
		}
		
		public function get event():Function
		{
			return _event;
		}
	}
}