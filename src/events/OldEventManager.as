package events
{
	import data.Action;
	import data.IXMLDataLoader;
	import enums.*;
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import navigation.NavigationManager;
	import parsing.parse;
	import navigation.Location
	import utility.GnWError;
	
	/**
	 * Manages and stores game events. This system can return a specific event using its ID, or it can return a random event based on
	 * the event's location and whether or not the event's conditions have been met.
	 *
	 * @author GnW Team
	 */
	public class OldEventManager implements IXMLDataLoader
	{
		private var _eventList:Dictionary;
		
		private var _currentScene:Scene;
		
		/**
		 * Creates a new event Manager.
		 *
		 * @param	navigationManager	The navigation manager is used to look up location data from location IDs stored in events.
		 */
		public function OldEventManager()
		{
		}
		
		public function startLoading():void
		{
			_eventList = new Dictionary();
		}
		
		public function canHandleXMLType(name:String):Boolean
		{
			return name == "oldevents";
		}
		
		public function readXMLData(file:String, xml:XML):void
		{
			if(xml.name() == "oldevents")
			{
				loadEventsFromXml(xml);
			}
		}
		
		public function get currentScene():Scene
		{
			return _currentScene;
		}
		
		public function set currentScene(value:Scene):void
		{
			if(_currentScene != null)
				_currentScene.invokeActions(TriggerType.OnExit);
			
			_currentScene = value;
			
			if(_currentScene != null)
				_currentScene.invokeActions(TriggerType.OnEnter);
		}
		
		public function get currentEvent():OldGameEvent
		{
			if(_currentScene == null)
				return null;
			
			return _currentScene.event;
		}
		
		/**
		 * Adds a new event to the manager event library. All events added must have unique IDs.
		 *
		 * @param	event	The event to add.
		 */
		public function addEvent(event:OldGameEvent):void
		{
			if(event.id == null || event.id.length == 0)
				throw new GnWError("Cannot add an event with a null or empty id.");
			
			if(event.id in _eventList)
				throw new GnWError("An event with the id \"" + event.id + "\" has already been added to the event manager.");
			
			_eventList[event.id] = event;
		}
		
		/**
		 * Returns an event with a matching ID.
		 *
		 * @param	id	The ID of the event to search for.
		 * @return	Returns either the event with a matching ID or null if no event with that ID can be found.
		 */
		public function getEventById(id:String):OldGameEvent
		{
			if(id == null)
				return null;
			
			return _eventList[id];
		}
		
		/**
		 * Returns a list of events with matching IDs. If no ids match, an empty list is returned.
		 *
		 * @param	ids	The IDs of the events to search for.
		 * @return	Returns a list containing all events that have IDs matching those in the id list.
		 */
		public function getEventsByIds(ids:Vector.<String>):Vector.<OldGameEvent>
		{
			var eventList:Vector.<OldGameEvent> = new Vector.<OldGameEvent>();
			
			for each(var id:String in ids)
			{
				var event:OldGameEvent = getEventById(id);
				
				if(event != null)
					eventList.push(event);
			}
			
			return eventList;
		}
		
		/**
		 * Returns a scene from the event and scene ids.
		 *
		 * @param	eventId	The ID of the event that the scene belongs to.
		 * @param 	sceneId	The ID of the scene to search for.
		 * @return	Returns either the scene with a matching IDs or null if no scene can be found.
		 */
		public function getSceneById(eventId:String, sceneId:String):Scene
		{
			var event:OldGameEvent = getEventById(eventId);
			if(event != null)
				return event.getSceneById(sceneId);
			
			return null;
		}
		
		/**
		 * Returns a random event that take place in the location passed, and has its other conditions met.
		 *
		 * @param	location 	The location that the returned event should take place in.
		 * @return	A randomly chosen event with a matching location.
		 */
		public function getEvent(location:Location):OldGameEvent
		{
			var availableEvents:Vector.<OldGameEvent> = getEventsAtLocation(location);
			
			return getRandomValidEvent(availableEvents);
		}
		
		/**
		 * Gets a random event from a set of events. Takes into account the events' probability weightings.
		 *
		 * @param	eventSet	The set of events to choose from.
		 * @return	An event chosen from the list passed in.
		 */
		private function getRandomEvent(eventSet:Vector.<OldGameEvent>):OldGameEvent
		{
			var rand:Number = Math.random();
			var weight:Number = 0;
			var sum:Number = 0;
			
			for each(var event:OldGameEvent in eventSet)
			{
				weight += event.prob;
			}
			
			rand *= weight;
			
			for(var i:int = 0; i < eventSet.length; i++)
			{
				if((eventSet[i].prob + sum) >= rand)
					return eventSet[i];
				else
					sum += eventSet[i].prob;
			}
			
			return null; // Should never be hit
		}
		
		/**
		 * Gets a random event from a set of events. Takes into account the events' probability weightings.
		 *
		 * @param	eventSet	The set of events to choose from.
		 * @return	An event chosen from the list passed in.
		 */
		public function getRandomValidEvent(eventSet:Vector.<OldGameEvent>):OldGameEvent
		{
			var context:Object = GameEngine.getCurrentDataContext();
			
			if(eventSet.length == 0)
				return null;
			
			if(eventSet.length == 1)
			{
				var evt:OldGameEvent = eventSet[0];
				return evt.evalConditions(context) ? evt : null;
			}
			
			var validEvents:Vector.<OldGameEvent> = new Vector.<OldGameEvent>();
			
			for each(var event:OldGameEvent in eventSet)
			{
				if(event.evalConditions(context))
					validEvents.push(event);
			}
			
			if(validEvents.length == 0)
				return null;
			
			if(validEvents.length == 1)
				return validEvents[0];
			
			return getRandomEvent(validEvents);
		}
		
		/**
		 * Returns a list containing all events from the specified location.
		 *
		 * @param	location	The location from which the events should be retrieved.
		 * @return	A list of events from the location.
		 */
		private function getEventsAtLocation(location:Location):Vector.<OldGameEvent>
		{
			var evList:Vector.<OldGameEvent> = new Vector.<OldGameEvent>();
			
			for each(var event:OldGameEvent in _eventList)
			{
				if(event.location == location.id)
					evList.push(event);
			}
			
			return evList;
		}
		
		/**
		 * Parses the event xml document and converts the xml into data classes, then adds the events to the event library.
		 */
		private function loadEventsFromXml(eventSet:XML):void
		{
			for each(var eventData:XML in eventSet.event)
			{
				if(eventData.location == null)
					throw new GnWError("Event " + eventData.@id + " is missing its location. Events must have a location.");
				
				var event:OldGameEvent = new OldGameEvent(eventData.@id.toString(), eventData.@name.toString(), eventData.location[0].@id.toXMLString(), eventData.@prob.toString(), eventData.@startSceneId.toString());
				
				for each(var actorData:XML in eventData.actor)
				{
					var type:CharacterType = Utils.Parse(CharacterType, actorData.@type.toString()) as CharacterType;
					
					var actor:Actor = new Actor(actorData.@name.toString(), actorData.@id.toString(), type);
					
					event.addActor(actor);
				}
				
				for each(var conditionData:XML in eventData.condition)
				{
					var cond:Condition = new Condition(conditionData.@target, conditionData.@attr, conditionData.@cond, conditionData.@value);
					event.addCondition(cond);
				}
				
				for each(var sceneData:XML in eventData.scene)
				{
					event.addScene(extractScene(sceneData, event.id));
				}
				
				addEvent(event);
			}
		}
		
		private function extractScene(sceneData:XML, eventId:String):Scene
		{
			var whiteSpace:RegExp = /(\s+)/gi;
			var embeddedNewline:RegExp = /(\\n)/gi;
			var embeddedTab:RegExp = /(\\t)/gi;
			
			var boldOpen:RegExp = /(\[b\])/gi;
			var boldClose:RegExp = /(\[\/b\])/gi;
			var italicOpen:RegExp = /(\[i\])/gi;
			var italicClose:RegExp = /(\[\/i\])/gi;
			
			var text:String = sceneData.text[0].toString();
			text = text.replace(whiteSpace, ' ');
			text = text.replace(embeddedNewline, '\n');
			text = text.replace(embeddedTab, '\t');
			
			text = text.replace(boldOpen, "<span class='bold'>");
			text = text.replace(boldClose, "</span>");
			text = text.replace(italicOpen, "<span class='italic'>");
			text = text.replace(italicClose, "</span>");
			
			var scene:Scene = new Scene(sceneData.@id.toXMLString(), text);
			
			var actionData:XML;
			var action:Action;
			
			scene.imageName = sceneData.@image.toXMLString();
			var exit:XML = sceneData.exit[0];
			if(exit != null)
				scene.exitText = exit.@text;
			
			for each(var linkData:XML in sceneData.link)
			{
				var link:Link = new Link(linkData.@text.toString(), linkData.@sceneId.toString(), linkData.@eventId.toString() || eventId);
				scene.addLink(link);
			}
			
			for each(actionData in sceneData.action)
			{
				action = Action.buildAction(actionData.@type, actionData.@trigger, actionData.value);
				scene.addAction(action);
			}
			
			for each(var conditionData:XML in sceneData.condition)
			{
				var cond:Condition = new Condition(conditionData.@target, conditionData.@attr, conditionData.@cond, conditionData.@value);
				scene.addCondition(cond);
			}
			
			for each(var commandData:XML in sceneData.command)
			{
				var command:Command = new Command(commandData.@text);
				
				for each(actionData in commandData.action)
				{
					action = Action.buildAction(actionData.@type, TriggerType.OnInteraction.text, actionData.value);
					command.addAction(action);
				}
				
				scene.addCommand(command);
			}
			
			return scene;
		}
	}
}