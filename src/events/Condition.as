package events 
{
	/**
	 * Conditions are used to determine the eligibility of GameEvents or Scenes for use. Conditions take in a target object that
	 * is to have one of its attributes tested against, and then evaluate the result of the condition based on that object.
	 * 
	 * @author GnW Team
	 */
	public class Condition 
	{
		public static const GREATER_THAN:String = "gt";
		public static const LESS_THAN:String = "lt";
		public static const GREATER_THAN_EQUAL_TO:String = "gte";
		public static const LESS_THAN_EQUAL_TO:String = "lte";
		public static const EQUAL:String = "eq";
		public static const HAS:String = "has";
		public static const HASNOT:String = "hasnot";
		
		private var _target:String;
		private var _attr:String;
		private var _cond:String;
		private var _value:*;
		
		/**
		 * Creates a new Condition object. 
		 * 
		 * @param	target	The target that will have it's attributes tested against the condition.
		 * @param	attr	The attriubute of the target that will be tested
		 * @param	cond	The condition that will be used for evaluation. Must be one of the constants defined in Condition.
		 * @param	value	The value that the target's attribute will be evaluated against.
		 */
		public function Condition(target:String, attr:String, cond:String, value:* ) 
		{
			_target = target;
			_attr = attr;
			_cond = cond;
			_value = value;
		}
		
		/**
		 * Evaluates the condition and returns a boolean.
		 * 
		 * @return Returns true if the condition passes, false if it fails.
		 */
		public function evaluate(dataContext:Object):Boolean
		{
			if (_attr == null)
				return false;
			
			var target:Object = dataContext[_target];
			if (target == null)
				return false;
			
			var targetValue:* = seekAttr(target, _attr);
			
			switch(_cond)
			{
				case EQUAL:
					return targetValue != null && targetValue == _value;
					
				case GREATER_THAN:
					return targetValue != null && targetValue > _value;
					
				case LESS_THAN:
					return targetValue != null && targetValue < _value;
					
				case GREATER_THAN_EQUAL_TO:
					return targetValue != null && targetValue >= _value;
					
				case LESS_THAN_EQUAL_TO:
					return targetValue != null && targetValue <= _value;
					
				case HAS:
					return targetValue != null;
					
				case HASNOT:
					return targetValue == null;
					
				default:
					return false;
			}
		}
		
		private function seekAttr(target:Object, attr:String):*
		{
			if (attr == null || attr.length == 0)
				return target;
			
			var list:Array = attr.split(".");
			var value:Object = target;
			for (var i:int = 0; i < list.length; i++)
			{
				value = value[list[i]]
				
				if (value == null)
					break;
			}
			
			return value;
		}
		
	}

}