package
{
	import combat.*;
	import combat.ability.XmlBasicAttack;
	import combat.participant.*;
	import data.*;
	import data.EntityComponents.ECAttributes;
	import data.EntityComponents.ECDynamicNumber;
	import data.EntityComponents.ECExperience;
	import data.EntityComponents.ECInventory;
	import data.time.DateTime;
	import enums.*;
	import events.*;
	import flash.events.Event;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import gui.*;
	import gui.menus.auxiliary.*;
	import gui.menus.*;
	import lua.LuaEngine;
	import map.MapManager;
	import navigation.location.Dungeon;
	import navigation.*;
	import parsing.parse;
	import system.*;
	
	/**
	 * This is the main class of the game. It is responsible for initializing all of the managers, menus and states in the game.
	 * Additionally, it also provides a set of helpers for common tasks in the game.
	 * 
	 * @author GnW Team
	 */
	public class GameEngine
	{
		private static var _descriptor:Descriptor;
		private static var _logManager:LogManager;
		private static var _fileResourceManager:FileResourceManager;
		private static var _dataManager:DataManager;
		private static var _eventManager:EventManager;
		private static var _oldEventManager:OldEventManager;
		private static var _menuManager:MenuManager;
		private static var _navigationManager:NavigationManager;
		private static var _mapManager:MapManager;
		private static var _saveManager:SaveManager;
		private static var _stateManager:StateManager;
		private static var _soundManager:SoundManager;
		private static var _actionManager:ActionManager;
		private static var _combatDataManager:CombatDataManager;
		private static var _cursorManger:CursorManger;
		private static var _gui:Game;
		private static var _world:GameWorld;
		private static var _currentBattle:CombatSystem;
		private static var _lua:LuaEngine;
		
		private static var _versionTextBox:TextField;
		public static var newNavSys:Boolean = false;
		public static var newEventSys:Boolean = false;
		
		
		public function GameEngine()
		{
		}
		
		/**
		 * Initializes the game. Must be called in the Main start method
		 */
		public static function init():void
		{
			_fileResourceManager = new FileResourceManager();
			_descriptor = new Descriptor();
			_fileResourceManager.addXMLDataLoader(_descriptor);
			_dataManager = new DataManager();
			_fileResourceManager.addXMLDataLoader(_dataManager);
			_navigationManager = new NavigationManager();
			_fileResourceManager.addXMLDataLoader(_navigationManager);
			_mapManager = new MapManager();
			_fileResourceManager.addXMLDataLoader(_mapManager);
			_oldEventManager = new OldEventManager();
			_fileResourceManager.addXMLDataLoader(_oldEventManager);
			_combatDataManager = new CombatDataManager();
			_fileResourceManager.addXMLDataLoader(_combatDataManager);
			_fileResourceManager.loadXMLData();
			_logManager = new LogManager();
			_gui = new Game();
			_eventManager = new EventManager();
			_menuManager = new MenuManager(_gui);
			_saveManager = new SaveManager();
			_actionManager = new ActionManager();
			_soundManager = new SoundManager();
			
			_currentBattle = null;
			_menuManager.registerMenu(new CreditMenu());
			_menuManager.registerMenu(new DebugMenu());
			_menuManager.registerMenu(new EventMenu(_eventManager));
			_menuManager.registerMenu(new OldEventMenu(_oldEventManager));
			_menuManager.registerMenu(new InventoryMenu());
			_menuManager.registerMenu(new NavigationMenu(_navigationManager));
			_menuManager.registerMenu(new MapMenu(_mapManager));
			_menuManager.registerMenu(new NewGameMenu());
			_menuManager.registerMenu(new ParseTestMenu());
			_menuManager.registerMenu(new SaveLoadMenu(_saveManager));
			_menuManager.registerMenu(new TitleMenu());
			_menuManager.registerMenu(new CombatMenu());
			_menuManager.registerMenu(new BarterMenu());
			_menuManager.registerMenu(new CharacterMenu());
			_menuManager.registerMenu(new OptionsMenu());
			
			_menuManager.registerMenu(new BodyTypeDisplayMenu());
			_menuManager.registerMenu(new GenderDisplayMenu());
			_menuManager.registerMenu(new RaceDisplayMenu());
			_menuManager.registerMenu(new StatDisplayMenu());
			_menuManager.registerMenu(new ImageDisplayMenu());
			
			_menuManager.registerMenu(new AuxiliaryStatMenu());
			_menuManager.registerMenu(new BottomTextMenu());
			_menuManager.registerMenu(new BottomTextBarsMenu());
			_menuManager.registerMenu(new NewCharacterChoiceMenu());
			_menuManager.registerMenu(new ItemStatsMenu());
			
			_lua = new LuaEngine();
			
			setupStateManager();
			_gui.addEventListener(Event.EXIT_FRAME, onExitFrame);
			
			_versionTextBox = new TextField();
			_versionTextBox.defaultTextFormat = new TextFormat("Bombard", 14, Colors.LASER_GREEN);
			_versionTextBox.wordWrap = false;
			_versionTextBox.embedFonts = true;
			_versionTextBox.autoSize = TextFieldAutoSize.LEFT;
			_versionTextBox.multiline = false;
			_versionTextBox.text = GlobalConstants.GameVersion;
			_versionTextBox.x = 1014 - _versionTextBox.width;
			_versionTextBox.y = 765 - _versionTextBox.height;
			_gui.addChild(_versionTextBox);
			
			
			
			//Starts game on the title screen 
			_stateManager.rootState = GameState.title;
			_stateManager.refreshState();
			
			_cursorManger = new CursorManger();
			
			for each(var file:String in _fileResourceManager.getAllStartingWith("lua/auto/"))
			{
				if(file.substring(file.length - 4) == ".lua")
				{
					_lua.doFile(file);
				}
			}
			trace("");
	
		}
		
		/**
		 * Used by the gui to make sure that states only change between frames.
		 * @param	e
		 */
		static private function onExitFrame(e:Event):void
		{
			_actionManager.invokeActions();
			
			if (_flushState)
			{
				_flushState = false;
				_stateManager.refreshState();
			}
		}
		static private var _flushState:Boolean = false;
		
		/**
		 * Initializes the state manager and the games states.
		 */
		static private function setupStateManager():void
		{
			_stateManager = new StateManager();
			
			_stateManager.addState(new State(GameState.title, function():void
				{
					_menuManager.mainScreenMenu = TitleMenu.ID;
					_stateManager.rootState = GameState.title;
					
					GameEngine.gui.hideAll();
					
					GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_01_ID, "Start", startNewGame);
					GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_02_ID, "Load", gotoSaveLoad);
					
					GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_03_ID, "Credits", gotoCredits);
					GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_04_ID, "Debug", gotoDebug);
					GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_05_ID, "Options", gotoOptions);
				}));
			
			_stateManager.addState(new State(GameState.charCreation, function():void
				{
					_menuManager.mainScreenMenu = NewGameMenu.ID;
					_stateManager.rootState = GameState.title;
				}));
			
			_stateManager.addState(new State(GameState.combat, function():void
				{
					_menuManager.mainScreenMenu = CombatMenu.ID;
				}));
			
			_stateManager.addState(new State(GameState.event, function():void
				{
					_gui.hideAll();
					if(!newEventSys)
					{
						_menuManager.mainScreenMenu = OldEventMenu.ID;
					}
					else
					{
						_menuManager.mainScreenMenu = EventMenu.ID;
					}
				}));
			
			_stateManager.addState(new State(GameState.inventory, function():void
				{
					(_menuManager.getMenuById(InventoryMenu.ID) as InventoryMenu).character = _world.player;
					_menuManager.mainScreenMenu = InventoryMenu.ID;
					_gui.setAndShow(Game.NO_BUTTON_ID, "Back", function(e:Event):void
					{
						endCurrentState();
					});
				}));
			_stateManager.addState(new State(GameState.navigation, function():void
				{
					_gui.hideAll();
					if(world)
					{
						_gui.setAndShow(Game.SYSTEM_BUTTON_03_ID, "Inventory", gotoInventory);
						_gui.setAndShow(Game.SYSTEM_BUTTON_04_ID, "Character", gotoCharacter, function():Boolean
						{
							return (_world.player.getComponent(EntityComponent.ATTRIBUTES) as ECAttributes)["free"] > 0;
						});
						GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_05_ID, "Options", gotoOptions);
					}
					if(newNavSys)
					{
						_menuManager.mainScreenMenu = MapMenu.ID;
					}
					else
					{
						_menuManager.mainScreenMenu = NavigationMenu.ID;
					}
					_stateManager.rootState = GameState.navigation;
				}));
			_stateManager.addState(new State(GameState.character, function():void
				{
					(_menuManager.getMenuById(CharacterMenu.ID) as CharacterMenu).character = _world.player;
					_menuManager.mainScreenMenu = CharacterMenu.ID;
					_stateManager.rootState = GameState.navigation;
				}));
			_stateManager.addState(new State(GameState.options, function():void
				{
					_menuManager.mainScreenMenu = OptionsMenu.ID;
				}));
				
			_stateManager.addState(new State(GameState.saveLoad, function():void
				{
					_menuManager.mainScreenMenu = SaveLoadMenu.ID;
				}));
				
			_stateManager.addState(new State(GameState.debug, function():void
				{
					_menuManager.mainScreenMenu = DebugMenu.ID;
				}));
				
			_stateManager.addState(new State(GameState.credits, function():void
				{
					_menuManager.mainScreenMenu = CreditMenu.ID;
					GameEngine.gui.hideAll();
					_gui.setAndShow(Game.NO_BUTTON_ID, "Back", function(e:Event):void
					{
						endCurrentState();
					});
				}));
			_stateManager.addState(new State(GameState.trade, function():void
				{
					_menuManager.mainScreenMenu = BarterMenu.ID;
				}));
		}
		
		/*******************************************
		*Getter functions for the managers*
		********************************************/
		public static function get descriptor():Descriptor
		{
			return _descriptor;
		}
		
		public static function get logManager():LogManager
		{
			return _logManager;
		}
		
		public static function get eventManager():EventManager
		{
			return _eventManager;
		}
		
		public static function get oldEventManager():OldEventManager
		{
			return _oldEventManager;
		}
		
		static public function get gui():Game
		{
			return _gui;
		}
		
		static public function get menuManager():MenuManager
		{
			return _menuManager;
		}

		static public function get world():GameWorld
		{
			return _world;
		}
		
		static public function get navigationManager():NavigationManager
		{
			return _navigationManager;
		}
		
		static public function get mapManager():MapManager
		{
			return _mapManager;
		}
		
		static public function get dataManager():DataManager
		{
			return _dataManager;
		}
		
		static public function get actionManager():ActionManager
		{
			return _actionManager;
		}
		
		static public function get combatDataManager():CombatDataManager
		{
			return _combatDataManager;
		}
		
		static public function get fileResourceManager():FileResourceManager
		{
			return _fileResourceManager;
		}
		
		static public function get soundManager():SoundManager 
		{
			return _soundManager;
		}
		
		static public function get currentBattle():CombatSystem
		{
			return _currentBattle;
		}
		
		static public function set currentBattleDebug(battle:CombatSystem):void
		{
			_currentBattle = battle;
		}
		
		static public function get lua():LuaEngine
		{
			return _lua;
		}
		
		public static function testEvent(id:String):Boolean
		{
			var event:GameEvent = _eventManager.getEventById(id);
			if(event == null) return false;
			return event.check();
		}
		
		/**
		 * Starts the event specified by the ID.
		 *
		 * @param	id	The ID of the event.
		 */
		public static function startEvent(id:String):void
		{
			if(!newEventSys)
			{
				var oldEvent:OldGameEvent = _oldEventManager.getEventById(id);
				
				if(oldEvent == null)
					return;
				
				if(!oldEvent.evalConditions(getCurrentDataContext()))
					return;
				
				_oldEventManager.currentScene = oldEvent.startingScene;
				_stateManager.pushState(GameState.event);
			}
			else
			{
				var event:GameEvent = _eventManager.getEventById(id);
				
				if(event == null)
					return;
				
				if(!event.check())
					return;
				
				_stateManager.pushState(GameState.event);
				(_menuManager.getMenuById(EventMenu.ID) as EventMenu).runEventFunction(event.event);
			}
		}
		
		/**
		 * Immediately changes the game state to navigation and sets the player's position according to the passed parameters.
		 * 
		 * @param	locationId	The unique id of the location
		 * @param	level		The level to go to in the location.
		 * @param	roomId		The room to go to in the level.
		 */
		public static function gotoLocation(locationId:String, level:String = null, roomId:String = null):void
		{
			var location:Location = _navigationManager.getLocationById(locationId);
			
			if(location == null)
				return;
			
			_navigationManager.currentLocation = location;
			
			if(location is Dungeon && (level != null || roomId != null))
			{
				var dungeon:Dungeon = location as Dungeon;
				
				dungeon.currentRoom = dungeon.getRoom(level, roomId);
			}
			
			_stateManager.rootState = GameState.navigation;
			_stateManager.clearState();
		}
		
		public static function startTrade(characterId:String):void
		{
			var merchant:Entity = _dataManager.getCharacterById(characterId);
			if (merchant == null){
				merchant = GameEngine.dataManager.createNPCFromTemplate(characterId);
				_dataManager.addCharacter(merchant);
			}
			
			var menu:BarterMenu = (_menuManager.getMenuById(BarterMenu.ID) as BarterMenu);
			menu.setup(world.player, merchant);
			
			_stateManager.pushState(GameState.trade);
		}
		
		public static function newCombat(onRound:Function = null, onWin:Function = null, onLose:Function = null, onDraw:Function = null):void
		{
			var menu:CombatMenu = GameEngine.menuManager.getMenuById(CombatMenu.ID) as CombatMenu;
			_currentBattle = new CombatSystem(menu);
			menu.combatFinishedCallback = function(e:CombatEvent):void
			{
				switch(e.combatStatus)
				{
					//if the player wins, this will display the random victory event
					case CombatStatus.Win:
					{
						if(onWin != null)
						{
							_stateManager.setState(GameState.event);
							onWin();
						}
						else
						{
							endCurrentState();
						}
						break;
					}
					
					//if the player loses, the defeat event will be shown.
					case CombatStatus.Lose:
					{
						if(onLose != null)
						{
							_stateManager.setState(GameState.event);
							onLose();
						}
						else
						{
							endCurrentState();
						}
						break;
					}
					
					
					case CombatStatus.Draw:
					{
						if(onDraw != null)
						{
							_stateManager.setState(GameState.event);
							onDraw();
						}
						else
						{
							endCurrentState();
						}
						break;
					}
				}
				_currentBattle = null;
			}
			menu.combatPrep();
		}
		
		public static function startCombat():void
		{
			_stateManager.setState(GameState.combat);
		}
		
		/**
		* This method sets up combat by taking in possible events and organizes the information. 
		* It will select 1 possibly combatant set, start, win, and lose event each out of a provided to it.
		* 
		* @param	combatantIds	
		* @param	startEventIds List of event IDs for combat start events. One will be selected and played at random.
		* @param	winEventIds List of event IDs for player wins. One will be selected and played at random.
		* @param	loseEventIds List of event IDs for player losses. One will be selected and played at random.
		*/
		public static function oldStartCombat(combatantIds:Vector.<String>, startEventIds:Vector.<String>, winEventIds:Vector.<String>, loseEventIds:Vector.<String>):void
		{
			//variable initialize
			var startEvents:Vector.<OldGameEvent> = _oldEventManager.getEventsByIds(startEventIds);
			var winEvents:Vector.<OldGameEvent> = _oldEventManager.getEventsByIds(winEventIds);
			var loseEvents:Vector.<OldGameEvent> = _oldEventManager.getEventsByIds(loseEventIds);
			var event:OldGameEvent;
			
			var menu:CombatMenu = GameEngine.menuManager.getMenuById(CombatMenu.ID) as CombatMenu;
			_currentBattle = new CombatSystem(menu);
			_currentBattle.context = GameEngine.getCurrentParsingContext();
			menu.combatFinishedCallback = function(e:CombatEvent):void
			{
				switch(e.combatStatus)
				{
					//if the player wins, this will display the random victory event
					case CombatStatus.Win: 
						event = _oldEventManager.getRandomValidEvent(winEvents);
						_oldEventManager.currentScene = event.startingScene;
						
						_stateManager.setState(GameState.event);
						break;
					
					//if the player loses, the defeat event will be shown.
					case CombatStatus.Lose: 
						event = _oldEventManager.getRandomValidEvent(loseEvents);
						_oldEventManager.currentScene = event.startingScene;
						
						_stateManager.setState(GameState.event);
						break;
					
					
					case CombatStatus.Draw: 
						break;
				}
				_currentBattle = null;
			}
			menu.combatPrep();
			
			//var player:Participant = new EntityParticipant(world.player);
			
			/*var eqipment:Equipment = (world.player.getComponent(EntityComponent.INVENTORY) as ECInventory).GetItemInSlot(EquipmentSlot.Weapon);
			
			if(eqipment == null)
				player.registerAbility(_combatDataManager.getAttack("Punch"), true);
			else
				player.registerAbility(_combatDataManager.getAttack(eqipment.basicAttack), true);*/
			
			_currentBattle.addEntity(world.player, Allegiance.Player); // adds the player object to the combat system, on the player side.
			
			//add all the entities in the event, into the combat system
			for each(var id:String in combatantIds)
			{
				//creates Entity from Id's template and adds it to Combat system, defaulting to Enemy Aliance every time.
				currentBattle.addEntity(dataManager.createNPCFromTemplate(id));
			}
			
			//If there are events before combat starts, we select a random one, and then enter combat.
			if(startEvents.length > 0)
			{
				event = _oldEventManager.getRandomValidEvent(startEvents); 
				_oldEventManager.currentScene = event.startingScene;
				
				_stateManager.pushState(GameState.combat, false);
				_stateManager.pushState(GameState.event);
			}
			else  //if there are no startEvents, go strait into combat.
			{
				_stateManager.pushState(GameState.combat);
			}
		}
		
		/**
		 * Sets the state that the game will occupy the next time the frame changes.
		 * @param	state	The name of the state Enum.
		 */
		public static function setNextState(state:String):void
		{
			var st:GameState = Utils.Parse(GameState, state) as GameState;
			
			_stateManager.setState(st, false);
			_flushState = true;
		}
		
		/**
		 * Ends the current state and returns the game to the current default state.
		 */
		public static function endCurrentState():void
		{
			_stateManager.popState();
		}
		
		/**
		 * Sets the game to the inventory state and sets up a return button.
		 * @param	e
		 */
		static public function gotoInventory(e:Event):void
		{
			_world.player.rollbackTransaction();
			_stateManager.pushState(GameState.inventory);

		}
		
		static private function gotoNavigation(e:Event):void
		{
			_stateManager.pushState(GameState.navigation);
		}
		
		static private function gotoOptions(e:Event):void
		{
			_stateManager.pushState(GameState.options);
		}
		
		static private function gotoSaveLoad(e:Event):void
		{
			_stateManager.pushState(GameState.saveLoad);
		}
		
		static private function gotoCharacter(e:Event):void
		{
			_world.player.rollbackTransaction();
			_stateManager.pushState(GameState.character);
		}
		
		static private function gotoDebug(e:Event):void
		{
			_stateManager.pushState(GameState.debug);
		}
		
		static private function gotoCredits(e:Event):void
		{
			_stateManager.pushState(GameState.credits);
		}
		
		static private function startNewGame(e:Event):void
		{
			startEvent("new-game-preamble");
		}
		
		/**
		 * Use this method to start a game with an existing character and world.
		 *
		 * @param	world
		 */
		static public function loadGame(world:GameWorld):void
		{
			_world = world;
			_stateManager.pushState(GameState.navigation);
		}
		
		/**
		 * Gets the data context for the current game world.
		 * @return
		 */
		public static function getCurrentDataContext():Object
		{
			return new DataContextBuilder(_world).buildContext();
		}
		
		/**
		 * Gets the parsing context for the current world.
		 * @return
		 */
		public static function getCurrentParsingContext():Object
		{
			return new ParsingDataContextBuilder(_world).buildContext();
		}
		
		/**
		 * Use this method to start a game with a new character.
		 *
		 * @param	player	This character will be the player's character when they start.
		 */
		static public function createNewCharacter(player:Entity):void
		{
			_world = new GameWorld();
			_world.player = player;
			_world.gameTime = new DateTime(2980, 5, 14);
			_world.variables = new Dictionary();
			
			var temp:Entity = dataManager.createItemFromTemplate("Tick22Pistol");
			var inv:ECInventory = player.getComponent(EntityComponent.INVENTORY) as ECInventory;
			inv.addEntity(temp);
			inv.equipEntity(temp);
			
			temp = dataManager.createItemFromTemplate("BasicClothes");
			inv.addEntity(temp);
			inv.equipEntity(temp);
			
			temp = dataManager.createItemFromTemplate("TestRegenPot");
			inv.addEntity(temp);
			temp = dataManager.createItemFromTemplate("TestDrug");
			inv.addEntity(temp);
			temp = dataManager.createItemFromTemplate("TestDrug");
			inv.addEntity(temp);
			
			var exp:ECExperience = player.getComponent(EntityComponent.EXPERIENCE) as ECExperience;
			exp.experience = 0;
			exp.checkNextLevel();
			
			var juice:ECDynamicNumber = player.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			var lust:ECDynamicNumber = player.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			lust.maxBase = 100;
			var vigor:ECDynamicNumber = player.getComponent(EntityComponent.VIGOR) as ECDynamicNumber;
			vigor.regenBase = 10;
			var spirit:ECDynamicNumber = player.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
			
			player.applyEffects();
			
			juice.value = juice.max;
			lust.value = 0;
			vigor.value = vigor.max;
			spirit.value = spirit.max;
			
			dataManager.addCharacter(player);
			
			_stateManager.rootState = GameState.navigation;
			_stateManager.clearState(false);
			
			_lua.gnwLib["vars"] = world.variables;
			_lua.gnwLib["player"] = world.player;
			
			startEvent(GlobalConstants.NewGameStartEventId);
		}
	}
}