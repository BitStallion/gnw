package system 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class SaveManager 
	{
		private var _saveList:Vector.<SaveFile>;
		
		public function SaveManager() 
		{
			_saveList = new Vector.<SaveFile>(12, true);
			
			// TODO remove this code after we're done testing.
			saveToSlot(new SaveFile(new GameWorld()),0);
		}
		
		public function loadFromSlot(slot:uint):SaveFile
		{
			return _saveList[slot];
		}
		
		public function saveToSlot(save:SaveFile, slot:uint):void
		{
			save.dateSaved = new Date();
			_saveList[slot] = save;
		}
		
		public function get slotCount():int {
			return _saveList.length;
		}
		
	}

}