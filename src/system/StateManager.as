package system
{
	import enums.GameState;
	import flash.utils.Dictionary;
	import utility.GnWError;
	
	/**
	 * The State Manager is responsible for maintaining game state. The states here should tie in to
	 * specific screens and should be used to ensure that all necessary actions and setup are complete
	 * before a state is entered and after it is left.
	 *
	 * @author GnW Team
	 */
	public class StateManager
	{
		private var _stateMap:Dictionary;
		
		private var _stateStack:Vector.<GameState>;
		
		public function StateManager()
		{
			_stateMap = new Dictionary();
			_stateStack = new Vector.<GameState>();
		}
		
		/**
		 * Adds a new state to the state manager.
		 * @param	state	The state to add.
		 */
		public function addState(state:State):void
		{
			if(state==null)
				throw new Error("Cannot add a null state to the state manager.");
			
			if(state.gameState==null)
				throw new Error("Cannot add a state with no GameState to the state manager.");
			
			_stateMap[state.gameState] = state;
		}
		
		/**
		 * Runs the onExit function of the current state, goes to the next lower state on the stack and runs
		 * the onEnter of the that state. If the current state is the root state, this function does nothing.
		 * 
		 * @param	trigger 	A flag to determine whether or not the onExit and onEnter functions are called.
		 */
		public function popState(trigger:Boolean = true):void {
			if (_stateStack.length == 1)
				return;
			
			var state:State = _stateMap[currentState];
			if(state != null)
				state.onExit();
			
			_stateStack.pop();
			
			state = _stateMap[currentState];
			if(state != null && trigger)
				state.onEnter();
		}
		
		/**
		 * Sets the current state to the value passed and immediately transitions to the new state. The onExit
		 * function of the current state is run followed by the onEnter function of the new state.
		 *
		 * @param	value	The new state to enter.
		 * @param	trigger 	A flag to determine whether or not the onEnter function of the new state is called.
		 */
		public function pushState(value:GameState, trigger:Boolean = true):void
		{
			if (value == currentState)
			{
				refreshState();
				return;
			}
			
			var state:State = _stateMap[currentState];
			if(state != null)
				state.onExit();
			
			_stateStack.push(value);
			
			state = _stateMap[currentState];
			if(state != null && trigger)
				state.onEnter();
		}
		
		/**
		 * Replaces the current state with the value passed and immediately transitions to the new state. The onExit
		 * function of the current state is run followed by the onEnter function of the new state.
		 *
		 * @param	value	The new state to enter.
		 * @param	trigger 	A flag to determine whether or not the onEnter function of the new state is called.
		 */
		public function setState(value:GameState, trigger:Boolean = true):void {
			if (_stateStack.length == 1)
			{
				pushState(value, trigger);
				return;
			}
			
			var state:State = _stateMap[currentState];
			if(state != null)
				state.onExit();
			
			_stateStack[_stateStack.length-1] = value;
			
			state = _stateMap[currentState];
			if(state != null && trigger)
				state.onEnter();
		}
		
		/**
		 * Sets the root state of the state stack.
		 * @param	value	The GameState of the 
		 */
		public function set rootState(value:GameState):void
		{
			var state:State = _stateMap[value];
			
			if (state == null)
				throw new GnWError("Cannot set state " + value+" as root state because it cannot be found in the state manager.");
			
			_stateStack[0] = value;
		}
		
		/**
		 * Removes all states from the stack except the root state and transitions to the root state.
		 * 
		 * @param	triggers	A flag to determine whether or not the onEnter function of the new state is called.
		 */
		public function clearState(trigger:Boolean = true):void {
			var state:State = _stateMap[currentState];
			if(state != null)
				state.onExit();
			
			_stateStack.splice(1, int.MAX_VALUE);
			
			state = _stateMap[currentState];
			if(state != null && trigger)
				state.onEnter();
		}
		
		/**
		 * Runs the entry function of the current state.
		 */
		public function refreshState():void {
			var state:State = _stateMap[currentState];
			if(state != null)
				state.onEnter();
		}
		
		/**
		 * The next state the state manager will transition to.
		 */
		public function get nextState():GameState
		{
			if (_stateStack.length == 0)
				return null;
			
			if (_stateStack.length == 1)
				return currentState;
			
			return _stateStack[_stateStack.length-2]
		}
		
		/**
		 * The current state occupied by the state manager.
		 */
		public function get currentState():GameState
		{
			if (_stateStack.length == 0)
				return null;
			
			return _stateStack[_stateStack.length-1];
		}
	}
}