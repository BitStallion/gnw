package system 
{
	import data.IXMLSerializable;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import utility.GnWError;
	import utility.Util;
	/**
	 * Used to play sounds and music from anywhere in the application.
	 * 
	 * @author GnW Team
	 */
	public class SoundManager implements IXMLSerializable
	{
		private var _musicLevel:Number;
		private var _soundFXLevel:Number;
		private var _mute:Boolean;
		
		private var _currentMusic:SoundChannel;
		
		public function SoundManager() 
		{
			_musicLevel = 1;
			_soundFXLevel = 1;
			_mute = false;
		}
		
		/**
		 * This gets the file, checks to make sure it's not null and plays it.
		 * @param	path The location of the desired soundeffect 
		 */
		public function playSoundFX(path:String):void {
			var sound:Sound = GameEngine.fileResourceManager.getSound(path);
			if (sound == null)
				throw new GnWError("Incorrect path specified. No sound object found at " + path);
				
			if (_mute)
				return;
			
			sound.play(0, 0, new SoundTransform(_soundFXLevel));
		}
		
		/**
		 * This gets the Music, checks to make sure it's not null and plays in a nearly infinie loop. 
		 * @param	path The location of the desired music 
		 */
		public function playAndLoopMusic(path:String):void {
			var sound:Sound = GameEngine.fileResourceManager.getSound(path);
			if (sound == null)
				throw new GnWError("Incorrect path specified. No sound object found at " + path);
			
			stopMusic();
			
			_currentMusic = sound.play(0, int.MAX_VALUE, new SoundTransform( _mute ? 0 :_musicLevel));
		}
		
		/**
		 * stops the current music and removes the sound file
		 */
		public function stopMusic():void {
			if (_currentMusic == null)
				return;
				
			_currentMusic.stop();
			_currentMusic = null;
		}
		
		/**
		 * Mutes the currently playing music and prevents future sound effects from playing.
		 * */
		public function mute():void {
			_mute = true;
			_currentMusic.soundTransform = new SoundTransform(0)
		}
		
		/**
		 * Unmutes the current music and allows future sound effects to be played.
		 */
		public function unmute():void {
			_mute = false;
			_currentMusic.soundTransform = new SoundTransform(_musicLevel)
		}
		
		public function get musicLevel():Number 
		{
			return _musicLevel;
		}
		
		/**
		 * sets the music level between 0 and 1. If it is to low or high, it sets it to the closest value in the range
		 */
		public function set musicLevel(value:Number):void 
		{
			_musicLevel = Util.clampNumber(0, 1, value);
		}
		
		public function get soundFXLevel():Number 
		{
			return _soundFXLevel;
		}
		
		/**
		 * sets the soundFX level between 0 and 1. If it is to low or high, it sets it to the closest value in the range
		 */
		public function set soundFXLevel(value:Number):void 
		{
			_soundFXLevel = Util.clampNumber(0, 1, value);
		}
		
		/**
		 * Used to save the sound settings.
		 * @return	Xml object containing the current levels of music and sound fx as well as 
		 * whether or not the game is muted.
		 */
		public function toXML():XML 
		{
			var xml:XML = new XML("<soundsettings></soundsettings>");
			xml.@["music"] = _musicLevel;
			xml.@["soundfx"] = _soundFXLevel;
			xml.@["mute"] =_mute;
			return xml;
		}
		
		public function fromXML(xml:XML):void 
		{
			_musicLevel = xml.@["music"];
			_soundFXLevel = xml.@["soundfx"];
			_mute = Boolean(xml.@["mute"].toLowerCase() === 'true');
		}
	}
}