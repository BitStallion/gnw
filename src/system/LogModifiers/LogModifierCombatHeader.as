package system.LogModifiers 
{
	import combat.Allegiance;
	import system.LogModifier;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class LogModifierCombatHeader extends LogModifier 
	{
		public var allegiance:Allegiance;
		
		public function LogModifierCombatHeader() 
		{
			super();
			allegiance = Allegiance.Noncombatant;
		}
		
		override public function modifyLog(log:String):String
		{
			var label:String = "<span>[SYSTEM] ";
			if(allegiance == Allegiance.Ally)
			{
				label = "<span>[ALLY] "
			}
			else if(allegiance == Allegiance.Enemy)
			{
				label = "<span class='enemy'>[ENEMY] "
			}
			else if(allegiance == Allegiance.Player)
			{
				label = "<span>[PLAYER] "
			}
			return label + log + "</span>";
		}
	}
}