package system 
{
	import parsing.parse;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class LogManager 
	{
		private var _logFunction:Function;
		private var _clearFunction:Function;
		private var _contextStack:Vector.<Object>;
		private var _modifierStack:Vector.<LogModifier>;
		
		public function LogManager() 
		{
			_logFunction = null;
			_clearFunction = null;
			_contextStack = new Vector.<Object>();
			_modifierStack = new Vector.<LogModifier>();
		}
		
		public function set logFunction(fun:Function):void
		{
			_logFunction = fun;
		}
		
		public function get logFunction():Function
		{
			return _logFunction;
		}
		
		public function appendToLog(log:String):void
		{
			if(_logFunction == null) return;
			var curContex:Object = {};
			for each(var contex:Object in _contextStack)
			{
				for(var key:Object in contex)
				{
					curContex[key] = contex[key];
				}
			}
			var str:String = parse(log, curContex);
			for(var index:int = _modifierStack.length - 1; index >= 0; index--)
			{
				str = _modifierStack[index].modifyLog(str);
			}
			_logFunction(str);
		}
		
		public function set clearFunction(fun:Function):void
		{
			_clearFunction = fun;
		}
		
		public function get clearFunction():Function
		{
			return _clearFunction;
		}
		
		public function clear():void
		{
			if(_clearFunction != null) _clearFunction();
		}
		
		public function pushContext(contex:Object):void
		{
			_contextStack.push(contex);
		}
		
		public function popContext():void
		{
			_contextStack.pop();
		}
		
		public function clearContextStack():void
		{
			_contextStack = new Vector.<Object>();
		}
		
		public function pushModifier(mod:LogModifier):void
		{
			_modifierStack.push(mod);
		}
		
		public function popModifier():void
		{
			_modifierStack.pop();
		}
		
		public function clearModifierStack():void
		{
			_modifierStack = new Vector.<LogModifier>();
		}
	}
}