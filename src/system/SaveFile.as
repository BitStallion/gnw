package system 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class SaveFile 
	{
		public var dateSaved:Date;
		
		private var _gameWorld:GameWorld;
		
		public function SaveFile(world:GameWorld) 
		{
			_gameWorld = world;
		}
		
		public function get gameWorld():GameWorld 
		{
			return _gameWorld;
		}
		
		public function get characterName():String {
			return  _gameWorld.player != null ? _gameWorld.player.variables["name"] : "";
		}
	}
}