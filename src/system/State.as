package system 
{
	import enums.GameState;
	/**
	 * States are used to manage high level game functionality. The game can only exist in a
	 * single state at a time, so this class is used to package the setup and shutdown procedures of each
	 * state as they are transitioned to and from.
	 * @author GnW Team
	 */
	public class State 
	{
		private var _gameState:GameState;
		private var _onEnter:Function;
		private var _onExit:Function;
		
		public function State(gameState:GameState, onEnter:Function = null, onExit:Function = null) 
		{
			_gameState = gameState;
			
			_onEnter = onEnter || function():void {};
			_onExit = onExit || function():void {};
		}
		
		/**
		 * The game state associated with this state.
		 */
		public function get gameState():GameState 
		{
			return _gameState;
		}
		
		/**
		 * This function is called when the stated is entered.
		 */
		public function get onEnter():Function 
		{
			return _onEnter;
		}
		
		/**
		 * This function is called when the state is exited.
		 */
		public function get onExit():Function 
		{
			return _onExit;
		}
		
	}

}