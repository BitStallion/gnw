package system
{
	import data.Entity;
	import data.time.DateTime;
	import flash.utils.Dictionary;
	import system.SaveFile;
	/**
	 * This is single class that we need to save and load through serialization.
	 * All characters, flags, etc, should be members of this class
	 * no methods, just data in variables.
	 * @author ...
	 */
	public class GameWorld 
	{
		public var player:Entity;
		public var gameTime:DateTime;
		public var variables:Dictionary;
		
		public function GameWorld() 
		{
			
		}
		
	}

}