package system 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class GlobalConstants 
	{
		public static const GameVersion:String = Version.currentVersion;
		
		// Player
		public static const NewCharacterFreeStatPoints:int = 10;
		public static const StatStartPoints:int = 10;
		public static const StatPointsPerLevel:int = 7;
		public static const PlayerId:String = "player";
		
		public static const DayStart:Number = 6;
		public static const DayEnd:Number = 18;
		public static const NightStart:Number = 20;
		public static const NightEnd:Number = 4;
		
		public static const StartingSceneId:String = "start";
		public static const NewGameStartEventId:String = "new-game-start";
		
		// UI
		public static const MenuFont:String = "Bombard";

	}

}