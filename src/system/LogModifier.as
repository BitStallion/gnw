package system 
{
	/**
	 * This allows headers to be added for logs
	 * 
	 * @author TheWrongHands
	 */
	public class LogModifier 
	{
		
		public function LogModifier() 
		{
		}
		
		public function modifyLog(log:String):String
		{
			return log;
		}
	}
}