package  
{
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class Version 
	{
		public static const stage:String = "Alpha";
		public static const major:uint = 0;
		public static const minor:uint = 1;
		public static const build:uint = 0;
		[Embed(source = "../.git/refs/heads/master", mimeType="application/octet-stream")]
		private static const CommitHash:Class;
		
		public function Version() 
		{
		}
		
		public static function get revision():String
		{
			var hash:ByteArray = new CommitHash();
			return hash.readUTFBytes(8).toUpperCase();
		}
		
		public static function get currentVersion():String
		{
			var version:String = stage + " " + major + "." + minor + "." + build + "." + revision;
			CONFIG::debug
			{
				version += " DEBUG";
			}
			return version;
		}
	}
}