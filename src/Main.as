package 
{
	import aze.motion.easing.Linear;
	import data.time.DateTime;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.filters.GlowFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.net.navigateToURL;
	import Assets;
	import aze.motion.eaze;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.*;
	import gui.controls.ConfigurableButton;
	import utility.Util;

	/**
	 * Main function of the game. Preloader is cleared and game engine is initialized here. 
	 * 
	 * @author GnW Team
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite 
	{
		private var font:String = Assets.BOMBARD_FONT;
		private var warningScreen:Sprite;
		private var recruitmentScreen:Sprite;
		private var blackout:Sprite;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			// centers and scales the game in the browser (probably a better place for this)
			stage.align = "";
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			GameEngine.init(); //initialize game engine.
			addChild(GameEngine.gui);
			
			CONFIG::release
			{
				GameEngine.gui.visible = false;
				setupWarning(); //Was a bit annoying, so i made it only get compiled into the release version
				setupRecruitment();
			}
			
			blackout = new Sprite();
			blackout.graphics.beginFill(0);
			blackout.graphics.drawRect(0, 0, stage.width, stage.height);
			blackout.graphics.endFill();
			addChild(blackout);
			
			startFadeIn();
		}
		
		/**
		 * Sets up the warning screen.
		 */
		private function setupWarning():void {
			warningScreen = new Sprite();
			var bitmap:Bitmap = GameEngine.fileResourceManager.getImage("images/title/Age Warning.png");
			warningScreen.addChild(bitmap)
			
			var textBox:TextField = new TextField();
			textBox.defaultTextFormat = new TextFormat("Arial", 13, 0);
			textBox.wordWrap = true;
			textBox.multiline = true;
			textBox.x = 95;
			textBox.y = 365;
			textBox.width = 830;
			textBox.selectable = false;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.text = Assets.WARNING_MESSAGE;
			warningScreen.addChild(textBox)
			
			var button:Sprite = new Sprite();
			button.graphics.beginFill(0, 0);
			button.graphics.drawRect(0, 0, 30, 70);
			button.graphics.endFill();
			button.x = 630;
			button.y = 275;
			button.addEventListener(MouseEvent.CLICK, onWarningClick);
			warningScreen.addChild(button);
			
			addChild(warningScreen);
		}
		
		/**
		 * Sets up the recruitment screen.
		 */
		private function setupRecruitment():void {
			recruitmentScreen = new Sprite();
			var bitmap:Bitmap = GameEngine.fileResourceManager.getResource("images/title/Recruitment Poster.png") as Bitmap;
			recruitmentScreen.addChild(bitmap)
			
			var textBox:TextField = new TextField();
			textBox.defaultTextFormat = new TextFormat("Arial", 14, 0);
			textBox.wordWrap = true;
			textBox.multiline = true;
			textBox.x = 25;
			textBox.y = 550;
			textBox.width = 850;
			textBox.selectable = false;
			textBox.autoSize = TextFieldAutoSize.LEFT;
			textBox.text = Assets.RECRUIT_MESSAGE;
			recruitmentScreen.addChild(textBox)
			
			var button:Sprite = new Sprite();
			button.graphics.beginFill(0, 0);
			button.graphics.drawRect(0, 0, 400, 200);
			button.graphics.endFill();
			button.x = 285;
			button.y = 150;
			button.addEventListener(MouseEvent.CLICK, onButtClick);
			recruitmentScreen.addChild(button);
			
			var forumButton:ConfigurableButton = new ConfigurableButton(0xffffff, "Creampuff", 36);
			forumButton.released = GameEngine.fileResourceManager.getImage("images/gui/buttons/PillButton.png");
			forumButton.pressed = GameEngine.fileResourceManager.getImage("images/gui/buttons/PillButton.png");
			forumButton.label = "Forum";
			var outline:GlowFilter = new GlowFilter(0x000000, 1.0, 2.0, 2.0, 10);
			outline.quality = BitmapFilterQuality.MEDIUM;
			forumButton.addGlow(outline);
			forumButton.x = 66;
			forumButton.y = 194;
			forumButton.action = onForumButtonClick;
			recruitmentScreen.addChild(forumButton);
			
			addChild(recruitmentScreen);
			recruitmentScreen.visible = false;
		}
		
		/**
		 * Event hander for warning screen exclaimation mark click.
		 * @param	e
		 */
		private function onWarningClick(e:Event):void {
			var child:DisplayObject = warningScreen.getChildAt(2);
			child.removeEventListener(MouseEvent.CLICK, onWarningClick);
			startFadeOut(gotoRecuitment);
		}
		
		/**
		 * Event hander for recruitment screen butt click.
		 * @param	e
		 */
		private function onButtClick(e:Event):void {
			var child:DisplayObject = recruitmentScreen.getChildAt(2);
			child.removeEventListener(MouseEvent.CLICK, onButtClick);
			
			child = recruitmentScreen.getChildAt(3);
			(child as ConfigurableButton).action = null;
			
			startFadeOut(gotoTitle);
		}
		
		private function onForumButtonClick(e:Event):void {
			var request:URLRequest = new URLRequest("http://www.legendofkrystal.com/forum/viewforum.php?f=37&cache=1");
			navigateToURL(request, "_blank");
		}
		
		private function gotoRecuitment():void {
			removeChild(warningScreen);
			recruitmentScreen.visible = true;
			startFadeIn();
		}
		
		private function gotoTitle():void {
			removeChild(recruitmentScreen);
			GameEngine.gui.visible = true;
			startFadeIn();
		}
		
		/**
		 * Fades the screen to black over 0.5 seconds.
		 * @param	func	Callback for when fadeout completes.
		 */
		private function startFadeOut(func:Function = null):void {
			eaze(blackout).to(0.5, { alpha: 1 } ).easing(Linear.easeNone).onComplete(func);
		}
		
		/**
		 * Fades the screen from black over 0.5 seconds.
		 * @param	func	Callback for when fadein completes.
		 */
		private function startFadeIn(func:Function = null):void {
			eaze(blackout).to(0.5, { alpha: 0 } ).easing(Linear.easeNone).onComplete(func);
		}
	}
}