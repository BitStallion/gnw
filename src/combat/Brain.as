package combat 
{
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class Brain extends EventDispatcher
	{
		protected var _owner:Entity;
		
		protected var _ability:Ability;
		protected var _target:Entity;
		
		public function Brain(owner:Entity) 
		{
			_owner = owner;
		}
		
		public function operate():Boolean
		{
			return true;
		}
		
		public function set ability(ability:Ability):void 
		{
			_ability = ability;
		}
		
		public function get ability():Ability 
		{
			return _ability;
		}
		
		public function get target():Entity 
		{
			return _target;
		}
		
		public function clearAbility():void
		{
			_ability = null;
			_target = null;
		}
		
		protected function getAvailableAbilities(category:String):Vector.<Ability>
		{
			var result:Vector.<Ability> = new Vector.<Ability>();
			var ability:Ability;
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			for each (ability in combatComp.abilities)
			{
				if (ability.isAvailable() && (category == "" || ability.category == category))
				{
					result.push(ability);
				}
			}
			return result;
		}
		
		protected function getAvailableAbilitiesAsDescriptables(category:String):Vector.<IDescriptable>
		{
			var result:Vector.<IDescriptable> = new Vector.<IDescriptable>();
			var ability:Ability;
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			for each (ability in combatComp.abilities)
			{
				if (ability.isAvailable() && (category == "" || ability.category == category))
				{
					result.push(ability);
				}
			}
			return result;
		}
		
		protected function getAvailableTargets(ability:Ability):Vector.<Entity> 
		{
			var result:Vector.<Entity> = new Vector.<Entity>();
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			for each(var ent:Entity in GameEngine.currentBattle.entities)
			{
				if (ability.isValidTarget(ent))
				{
					result.push(ent);
				}
			}
			return result;
		}
		
		protected function getAvailableTargetsAsDescriptables(ability:Ability):Vector.<IDescriptable> 
		{
			var result:Vector.<IDescriptable> = new Vector.<IDescriptable>();
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			for each(var ent:Entity in GameEngine.currentBattle.entities)
			{
				if (ability.isValidTarget(ent))
				{
					result.push(ent.getComponent(EntityComponent.COMBAT));
				}
			}
			return result;
		}
		
	}

}