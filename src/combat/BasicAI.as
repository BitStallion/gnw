package combat 
{
	import combat.ability.special.WaitAbility;
	import data.Entity;
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class BasicAI 
	{
		public var owner:Entity;
		
		public function BasicAI() 
		{
			
		}
		
		public function chooseAbility(availableAbilities:Vector.<Ability>):Ability 
		{
			if (availableAbilities && availableAbilities.length > 0)
			{
				return availableAbilities[0];
			}
			else
			{
				return new WaitAbility(this.owner);
			}
		}
		
		public function chooseTarget(availableTargets:Vector.<Entity>):Entity 
		{
			return availableTargets[0];
		}
	}
}