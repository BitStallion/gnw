package combat
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class CombatEvent extends Event
	{
		public static const ACT:String = "combat-event:act";
		public static const APPEND_TO_LOG:String = "combat-event:append-to-log";
		public static const CLEAR_LOG:String = "combat-event:clear-log";
		public static const COMBAT_FINISHED:String = "combat-event:combat-finished";
		public static const NEXT:String = "combat-event:next";
		public static const NEXT_ROUND:String = "combat-event:next-round";
		public static const OPERATE_FINISHED:String = "combat-event:operate-finished";
		public static const SELECT:String = "combat-event:select";
		public static const SELECTED:String = "combat-event:selected";
		public static const SHOW_STATS:String = "combat-event:show-stats";
		
		public var combatStatus:CombatStatus;
		public var data:Object;
		public var selectables:Vector.<IDescriptable>;
		public var selected:IDescriptable;
		public var sourceAllegiance:Allegiance;
		public var text:String;
		
		public function CombatEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
		
		}
	
	}

}