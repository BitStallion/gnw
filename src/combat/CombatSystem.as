package combat 
{
	import combat.ability.special.NextRoundAbility;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import data.EntityComponents.ECExperience;
	import flash.events.EventDispatcher;
	import parsing.parse;
	import system.LogModifiers.LogModifierCombatHeader;
	import utility.GnWError;
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class CombatSystem 
	{
		private var _currentEntIndex:int;
		private var _status:CombatStatus;
		private var _random:Random;
		private var _roundMode:uint;
		private var _roundCount:uint;
		public var logCombatHeader:LogModifierCombatHeader;
		public var context:Object;
		public var gui:EventDispatcher;
		public var entities:Vector.<Entity>;
		public var cheatWin:Boolean;
		
		public function CombatSystem(gui:EventDispatcher) 
		{
			_currentEntIndex = 0;
			_status = CombatStatus.NotStarted;
			_random = new Random(777);
			_roundMode = 0;
			_roundCount = 0;
			logCombatHeader = new LogModifierCombatHeader();
			this.gui = gui;
			entities = new Vector.<Entity>();
			cheatWin = false;
		}
		
		public function addEntity(ent:Entity, allegiance:Allegiance = null):void
		{
			if(entities.indexOf(ent) < 0)
			{
				var combatComp:ECCombat;
				try
				{
					combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
					combatComp.reset();
				}
				catch(e:GnWError)
				{
					combatComp = new ECCombat();
					ent.setComponent(EntityComponent.COMBAT, combatComp);
				}
				entities.push(ent);
				combatComp.allegiance = allegiance ? allegiance : Allegiance.Enemy;
				GameEngine.logManager.appendToLog("A challenger appears: " + combatComp.description + ".");
				ent.applyEffects();
			}
		}
		
		public function get random():Random
		{
			return _random;
		}
		
		public function combat():void
		{
			var ent:Entity;
			var combatComp:ECCombat;
			var allies:uint;
			var enemies:uint;
			/* This function needs to be able to handle a situation both where no human interaction is required (function doesn't need to return and recover)
			 * and where human interaction is required (function has to return and recover) while avoiding looping function calls to prevent a possible stack overflow.
			 * A simple infinite loop and switch statement seems to do the job nicely.
			 */
			while(true)
			{
				switch(_roundMode)
				{
					case 0:
					{
						GameEngine.logManager.clearModifierStack();
						GameEngine.logManager.pushModifier(logCombatHeader);
						GameEngine.logManager.pushContext(context);
						_roundMode = 1;
						break;
					}
					case 1:
					{
						logCombatHeader.allegiance = Allegiance.Noncombatant;
						if(_status == CombatStatus.NotStarted)
						{
							_status = CombatStatus.InProcess;
							GameEngine.logManager.appendToLog("The combat begins!");
						}
						else
						{
							_roundCount++;
							GameEngine.logManager.appendToLog("New round (#" + _roundCount + ").");
						}
						gui.dispatchEvent(new CombatEvent(CombatEvent.SHOW_STATS));
						_roundMode = 2;
						break;
					}
					case 2:
					{
						while(_currentEntIndex < entities.length)
						{
							combatComp = entities[_currentEntIndex].getComponent(EntityComponent.COMBAT) as ECCombat;
							if(combatComp.brain.ability == null)
							{
								if(!combatComp.determineAbility()) return;
								_currentEntIndex++;
							}
							else
							{
								_currentEntIndex++;
							}
						}
						if(!GameEngine.newEventSys) context["enemy_is_alone"] = isEnemyAlone();
						for each(ent in entities)
						{
							combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
							logCombatHeader.allegiance = combatComp.allegiance;
							combatComp.preRound();
						}
						for each(ent in entities)
						{
							combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
							logCombatHeader.allegiance = combatComp.allegiance;
							combatComp.round();
						}
						for each(ent in entities)
						{
							combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
							logCombatHeader.allegiance = combatComp.allegiance;
							combatComp.postRound();
							if(combatComp.active) ent.advanceTurn();
						}
						allies = 0;
						enemies = 0;
						for each(ent in entities)
						{
							combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
							if(combatComp.active)
							{
								if(combatComp.allegiance == Allegiance.Player || combatComp.allegiance == Allegiance.Ally)
								{
									allies++;
								}
								else if(combatComp.allegiance == Allegiance.Enemy)
								{
									enemies++;
								}
							}
						}
						if(cheatWin)
						{
							gui.dispatchEvent(new CombatEvent(CombatEvent.SHOW_STATS));
							_status = CombatStatus.Win;
							logCombatHeader.allegiance = Allegiance.Player;
							GameEngine.logManager.appendToLog("Player cheated.");
							finished();
							return;
						}
						else if(allies == 0 && enemies == 0)
						{
							gui.dispatchEvent(new CombatEvent(CombatEvent.SHOW_STATS));
							_status = CombatStatus.Draw;
							logCombatHeader.allegiance = Allegiance.Noncombatant;
							GameEngine.logManager.appendToLog("Draw.");
							finished();
							return;
						}
						else if(enemies == 0)
						{
							gui.dispatchEvent(new CombatEvent(CombatEvent.SHOW_STATS));
							_status = CombatStatus.Win;
							logCombatHeader.allegiance = Allegiance.Player;
							GameEngine.logManager.appendToLog("Player wins.");
							var expTotal:uint = 0;
							for each(ent in entities)
							{
								combatComp = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
								if(combatComp.allegiance == Allegiance.Enemy)
								{
									var level:uint = (ent.variables["level"] || GameEngine.world.player.variables["level"]) - GameEngine.world.player.variables["level"];
									var exp:uint = ent.variables["exp"] || 0;
									if(level >= 0)
									{
										expTotal += exp * (1 + level * 0.25);
									}
									else if(level >= -3)
									{
										switch(level)
										{
											case -1:
											{
												expTotal += exp * 0.75;
												break;
											}
											case -2:
											{
												expTotal += exp * 0.5;
												break;
											}
											case -3:
											{
												expTotal += exp * 0.25;
												break;
											}
										}
									}
								}
							}
							if(expTotal > 0)
							{
								(GameEngine.world.player.getComponent(EntityComponent.EXPERIENCE) as ECExperience).experience += expTotal;
							}
							finished();
							return;
						}
						else if(allies == 0)
						{
							gui.dispatchEvent(new CombatEvent(CombatEvent.SHOW_STATS));
							_status = CombatStatus.Lose;
							logCombatHeader.allegiance = Allegiance.Enemy;
							GameEngine.logManager.appendToLog("Enemy wins.");
							finished();
							return;
						}
						_currentEntIndex = 0;
						_roundMode = 1;
						break;
					}
				}
			}
		}
		
		private function finished():void
		{
			GameEngine.logManager.clearModifierStack();
			GameEngine.logManager.clearContextStack();
			var event:CombatEvent = new CombatEvent(CombatEvent.COMBAT_FINISHED);
			event.combatStatus = _status;
			gui.dispatchEvent(event);
		}
		
		private function isEnemyAlone():Boolean
		{
			var c:int = 0;
			for each(var ent:Entity in entities)
			{
				var combatComp:ECCombat = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
				if(combatComp.active && combatComp.allegiance == Allegiance.Enemy)
				{
					c++;
				}
				if(c > 1)
				{
					return false;
				}
			}
			return true;
		}
		
		public function surrender():void
		{
			
			logCombatHeader.allegiance = Allegiance.Player;
			GameEngine.logManager.appendToLog("Player surrender.");
			for each(var ent:Entity in entities)
			{
				var combatComp:ECCombat = ent.getComponent(EntityComponent.COMBAT) as ECCombat;
				if(combatComp.allegiance == Allegiance.Player || combatComp.allegiance == Allegiance.Ally)
				{
					combatComp.surrender();
				}
			}
		}
		
		public function selectedEventHandler(e:CombatEvent):void
		{
			if(e.selected is NextRoundAbility)
			{
				//gui.dispatchEvent(new CombatEvent(CombatEvent.CLEAR_LOG));
				combat();
				return;
			}
			
			if(_currentEntIndex >= 0 && _currentEntIndex < entities.length)
			{
				var event:CombatEvent = new CombatEvent(CombatEvent.SELECTED);
				event.selected = e.selected;
				(entities[_currentEntIndex].getComponent(EntityComponent.COMBAT) as ECCombat).dispatcher.dispatchEvent(event);
			}
		}
		
		private function mix(... arguments:*):Object
		{
			var i:int;
			var j:String;
			var newObj:Object = {};
			for(i = 0; i < arguments.length; i++)
			{
				for(j in arguments[i])
				{
					if(arguments[i].hasOwnProperty(j))
					{
						newObj[j] = arguments[i][j];
					}
				}
			}
			return newObj;
		}
	}
}