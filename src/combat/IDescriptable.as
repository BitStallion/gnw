package combat 
{
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public interface IDescriptable 
	{
		function get buttonDescription():String;
		function get description():String;
		function get shortDescription():String;		
		function get buttonId():int;
	}
	
}