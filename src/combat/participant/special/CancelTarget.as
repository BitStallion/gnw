package combat.participant.special 
{
	import combat.IDescriptable;
	import gui.Game;
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class CancelTarget implements IDescriptable
	{
		private var _category:String;
		protected var _buttonDescription:String;
		protected var _buttonId:int = -1;
		protected var _description:String;
		protected var _shortDescription:String;
		
		public function CancelTarget(category:String) 
		{
			super();
			_buttonId = Game.NO_BUTTON_ID;
			_category = category;
			_buttonDescription = _description = _shortDescription = "Cancel"
		}
		
		public function get category():String 
		{
			return _category;
		}
		
		public function get buttonDescription():String
		{
			return _buttonDescription;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function get shortDescription():String
		{
			return _shortDescription;
		}
		
		public function get buttonId():int
		{
			return _buttonId;
		}
	}

}