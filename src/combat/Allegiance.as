package combat
{
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class Allegiance
	{
		public static const Player:Allegiance = new Allegiance();
		public static const Ally:Allegiance = new Allegiance();
		public static const Enemy:Allegiance = new Allegiance();
		public static const Noncombatant:Allegiance = new Allegiance();
		
		public function Allegiance()
		{
		}
		
		public function IsLike(allegiance:Allegiance):Boolean
		{
			switch (this)
			{
				case Player: 
				case Ally: 
					return allegiance == Player || allegiance == Ally;
				case Enemy: 
					return allegiance == Enemy;
				case Noncombatant: 
					return false;
				default: 
					throw new ArgumentError("Unknown allegiance.");
			}
		}
		
		public function IsOpposite(allegiance:Allegiance):Boolean
		{
			switch (this)
			{
				case Player: 
				case Ally: 
					return allegiance == Enemy;
				case Enemy: 
					return allegiance == Player || allegiance == Ally;
				case Noncombatant: 
					return false;
				default: 
					throw new ArgumentError("Unknown allegiance.");
			}
		}
	}

}