package combat
{
	import combat.ability.special.WaitAbility;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class RandomAI extends BasicAI
	{
		public function RandomAI()
		{
			super();
		
		}
		
		override public function chooseAbility(availableAbilities:Vector.<Ability>):Ability
		{
			if (availableAbilities && availableAbilities.length > 0)
			{
				var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
				var roll:int = GameEngine.currentBattle.random.getNextInt(availableAbilities.length);
				return availableAbilities[roll - 1];
			}
			else
			{
				return new WaitAbility(this.owner);
			}
		}
		
		override public function chooseTarget(availableTargets:Vector.<Entity>):Entity
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var roll:int = GameEngine.currentBattle.random.getNextInt(availableTargets.length);
			return availableTargets[roll - 1];
		}
	}

}