package combat.brain 
{
	import combat.BasicAI;
	import combat.Brain;
	import combat.CombatEvent;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class ComputerBrain extends Brain 
	{
		private var _ai:BasicAI;
		
		public function ComputerBrain(owner:Entity)
		{
			super(owner);
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			_ai = combatComp.ai;
		}
		
		override public function operate():Boolean
		{
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			_ability = _ai.chooseAbility(getAvailableAbilities(""));
			if(_ability.isTargetable())
			{
				_target = _ai.chooseTarget(getAvailableTargets(_ability));
			}
			else
			{
				_target = null;
			}
			return true;
		}
	}
}