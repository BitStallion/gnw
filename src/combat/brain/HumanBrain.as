package combat.brain
{
	import adobe.utils.CustomActions;
	import combat.Ability;
	import combat.ability.special.AttackAbility;
	import combat.ability.special.CancelAbility;
	import combat.ability.special.CategoryAbility;
	import combat.ability.special.DefendAbility;
	import combat.ability.special.EscapeAbility;
	import combat.ability.special.WinAbility;
	import combat.ability.special.SurrenderAbility;
	import combat.Brain;
	import combat.CombatEvent;
	import combat.IDescriptable;
	import combat.participant.special.CancelTarget;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import data.EntityComponents.ECDynamicNumber;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class HumanBrain extends Brain
	{
		private var _categoryAbility:CategoryAbility;
		
		public function HumanBrain(owner:Entity)
		{
			super(owner);
		
		}
		
		override public function operate():Boolean
		{
			_ability = null;
			_target = null;
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			
			if (combatComp.isStunned())
			{
				GameEngine.logManager.appendToLog(combatComp.description + " stunned and cannot act.");
				return true;
			}
			else
			{
				chooseFirstAbility();
			}
			return false;
		}
		
		private function chooseFirstAbility():void
		{
			_categoryAbility = null;
			var abilities:Vector.<IDescriptable> = new Vector.<IDescriptable>();
			
			abilities.push(new AttackAbility(_owner));
			abilities.push(new CategoryAbility("melee", "Melee T.", Game.COMMON_BUTTON_02_ID));
			abilities.push(new CategoryAbility("lust", "Lust T.", Game.COMMON_BUTTON_03_ID));
			abilities.push(new CategoryAbility("item", "Item", Game.COMMON_BUTTON_04_ID));
			abilities.push(new DefendAbility(_owner));
			abilities.push(new CategoryAbility("ranged", "Ranged T.", Game.COMMON_BUTTON_06_ID));
			abilities.push(new CategoryAbility("magic", "Magic T.", Game.COMMON_BUTTON_07_ID));
			abilities.push(new EscapeAbility(_owner));
			abilities.push(new WinAbility(_owner));
			abilities.push(new SurrenderAbility(_owner));
			
			var event:CombatEvent = new CombatEvent(CombatEvent.SELECT);
			event.selectables = abilities;
			
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			GameEngine.currentBattle.gui.dispatchEvent(event);
			GameEngine.gui.setAndShow(Game.SYSTEM_BUTTON_03_ID, "Inventory", GameEngine.gotoInventory);
			combatComp.dispatcher.addEventListener(CombatEvent.SELECTED, firstAbilitySelectedEventHandler);
		}
		
		private function firstAbilitySelectedEventHandler(e:CombatEvent):void
		{
			(_owner.getComponent(EntityComponent.COMBAT) as ECCombat).dispatcher.removeEventListener(CombatEvent.SELECTED, firstAbilitySelectedEventHandler);
			var ability:Ability = e.selected as Ability;
			if (ability is CategoryAbility)
			{
				_categoryAbility = ability as CategoryAbility;
				chooseSecondAbility((ability as CategoryAbility).category);
			}
			else
			{
				_ability = ability;
				chooseTarget();
			}
		}
		
		private function chooseSecondAbility(category:String):void
		{
			var abilities:Vector.<IDescriptable> = getAvailableAbilitiesAsDescriptables(category);
			abilities.push(new CancelAbility());
			var event:CombatEvent = new CombatEvent(CombatEvent.SELECT);
			event.selectables = abilities;
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			GameEngine.currentBattle.gui.dispatchEvent(event);
			combatComp.dispatcher.addEventListener(CombatEvent.SELECTED, secondAbilitySelectedEventHandler);
		}
		
		private function secondAbilitySelectedEventHandler(e:CombatEvent):void
		{
			(_owner.getComponent(EntityComponent.COMBAT) as ECCombat).dispatcher.removeEventListener(CombatEvent.SELECTED, secondAbilitySelectedEventHandler);
			var ability:Ability = e.selected as Ability;
			if (ability is CancelAbility)
			{
				chooseFirstAbility();
			}
			else
			{
				_ability = ability;
				chooseTarget();
			}
		}
		
		private function chooseTarget():void
		{
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			if (!_ability.isTargetable())
			{
				_target = null;
				GameEngine.currentBattle.combat();
				return;
			}
			var availableTargets:Vector.<IDescriptable> = getAvailableTargetsAsDescriptables(_ability); // TODO Find a less crazy way to do this - Hands
			if(availableTargets.length == 1)
			{
				_target = (availableTargets[0] as EntityComponent).entity;
				GameEngine.currentBattle.combat();
				return;
			}
			availableTargets.push(new CancelTarget(""));
			var event:CombatEvent = new CombatEvent(CombatEvent.SELECT);
			event.selectables = availableTargets;
			GameEngine.currentBattle.gui.dispatchEvent(event);
			combatComp.dispatcher.addEventListener(CombatEvent.SELECTED, targetSelectedEventHandler);
		}
		
		private function targetSelectedEventHandler(e:CombatEvent):void
		{
			var combatComp:ECCombat = _owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			combatComp.dispatcher.removeEventListener(CombatEvent.SELECTED, targetSelectedEventHandler);
			if (e.selected is CancelTarget)
			{
				if (_categoryAbility)
				{
					chooseSecondAbility((_categoryAbility as CategoryAbility).category);
				}
				else
				{
					chooseFirstAbility();
				}
			}
			else
			{
				var abilityTarget:Entity = (e.selected as EntityComponent).entity;
				_target = abilityTarget;
				GameEngine.currentBattle.combat();
			}
		}
	
	}

}