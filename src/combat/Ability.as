package combat
{
	import data.Entity;
	import data.EntityComponents.ECCombat;
	import utility.GnWError;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class Ability implements IDescriptable
	{
		public var owner:Entity;
		
		protected var _buttonDescription:String;
		protected var _description:String;
		protected var _shortDescription:String;		
		protected var _buttonId:int = -1;
		
		protected var _category:String = "";
		
		public function Ability()
		{
		
		}
		
		public function isAvailable():Boolean
		{
			if (!inner_isAvailable())
			{
				return false;
			}
			
			for each (var participant:Entity in GameEngine.currentBattle.entities)
			{
				if (isValidTarget(participant))
				{
					return true;
				}
			}
			return false;
		}
		
		protected function inner_isAvailable():Boolean
		{
			return true;
		}
		
		public function isTargetable():Boolean
		{
			throw new GnWError("Not implemented");
		}
		
		public function isValidTarget(target:Entity):Boolean
		{
			throw new GnWError("Not implemented");
		}
		
		public function preRound():void
		{
		}
		
		public function useOn(target:Entity):void
		{
			throw new GnWError("Not implemented");
		}
		
		public function postRound():void
		{
		}
		
		public function get category():String
		{
			return _category;
		}		
		
		/* BEGIN OF INTERFACE combat.IDescriptable */
		
		public function get buttonDescription():String
		{
			return _buttonDescription;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function get shortDescription():String
		{
			return _shortDescription;
		}
		
		public function get buttonId():int
		{
			return _buttonId;
		}
		
		/* END OF INTERFACE combat.IDescriptable */
	
	}

}