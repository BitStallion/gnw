package combat.ability.special
{
	import combat.Ability;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class CategoryAbility extends Ability
	{
		
		public function CategoryAbility(category:String, description:String, buttonId:int)
		{
			super();
			_buttonId = buttonId;
			_category = category;
			_buttonDescription = _description = _shortDescription = description;
		}
	}

}