package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECDamageHandler;
	import enums.DamageType;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class WonderingHandsAbility extends Ability 
	{
		
		public function WonderingHandsAbility(owner:Entity)
		{
			super();
			_description = "[owner.name] is unable to stop [owner.hisher] hands from wondering about [owner.hisher] body as [owner.heshe] is distracted by lust filled thoughts.";
			this.owner = owner;
		}
		
		public override function isTargetable():Boolean
		{
			return false;
		}
		
		public override function preRound():void
		{
			var damage:Number = GameEngine.currentBattle.random.getNextInt(6) + 5;
			var damageHand:ECDamageHandler = owner.getComponent(EntityComponent.DAMAGE) as ECDamageHandler;
			damageHand.takeDamage(damage, DamageType.lust, owner, description);
			GameEngine.logManager.popContext();
		}
		
		public override function useOn(target:Entity):void
		{
		}
	}
}