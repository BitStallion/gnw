package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import data.entityEvents.EntityEvent;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class UseItemAbility extends Ability 
	{
		public var ent:Entity;
		
		public function UseItemAbility(ent:Entity)
		{
			super();
			this.ent = ent;
		}
		
		override public function isAvailable():Boolean 
		{
			return true;
		}
		
		override public function isTargetable():Boolean 
		{
			return true;
		}
		
		override public function isValidTarget(target:Entity):Boolean
		{
			var tcombatComp:ECCombat = target.getComponent(EntityComponent.COMBAT) as ECCombat;
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			return (ent.hasTag("useOnAllies") && tcombatComp.active && !combatComp.allegiance.IsOpposite(tcombatComp.allegiance)) || (ent.hasTag("useOnEnemies") && tcombatComp.active && combatComp.allegiance.IsOpposite(tcombatComp.allegiance));
		}
		
		override protected function inner_isAvailable():Boolean
		{
			return true;
		}
		
		override public function useOn(target:Entity):void
		{
			var context:Object = {"owner": owner.toParsingContext(), "target": target.toParsingContext()};
			GameEngine.logManager.pushContext(context);
			GameEngine.logManager.appendToLog("[owner.name] Used " + ent.variables["name"]);
			GameEngine.logManager.popContext();
			var useEvent:EntityEvent = new EntityEvent(EntityEvent.onUse);
			useEvent.target = target;
			ent.dispatchEvent(useEvent);
			target.applyEffects();
		}
	}
}