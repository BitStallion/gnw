package combat.ability.special
{
	import combat.Ability;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class CancelAbility extends Ability
	{
		
		public function CancelAbility()
		{
			super();
			_buttonId = Game.NO_BUTTON_ID;
			_buttonDescription = _description = _shortDescription = "Cancel";
		}
	
	}

}