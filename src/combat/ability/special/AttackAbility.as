package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class AttackAbility extends Ability 
	{
		private var _innerAbility:Ability;
		
		public function AttackAbility(owner:Entity) 
		{
			super();
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			_buttonId = Game.COMMON_BUTTON_01_ID;
			_innerAbility = combatComp.basickAttack;
			_buttonDescription = _description = _shortDescription = "Attack";
			this.owner = owner;
		}
		
		override public function isTargetable():Boolean
		{
			return true;
		}
		
		override public function isValidTarget(target:Entity):Boolean
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var tcombatComp:ECCombat = target.getComponent(EntityComponent.COMBAT) as ECCombat;
			return tcombatComp.active && tcombatComp.allegiance.IsOpposite(combatComp.allegiance);
		}
		
		override protected function inner_isAvailable():Boolean 
		{
			return _innerAbility.isAvailable();
		}
		
		override public function useOn(target:Entity):void 
		{
			_innerAbility.useOn(target);
		}
		
	}

}