package combat.ability.special 
{
	import combat.Ability;
	import gui.Game;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class NextRoundAbility extends Ability 
	{
		
		public function NextRoundAbility() 
		{
			super();
			_buttonId = Game.YES_BUTTON_ID;
			_buttonDescription = _description = _shortDescription = "Next R.";
		}
		
	}

}