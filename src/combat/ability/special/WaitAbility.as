package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class WaitAbility extends Ability 
	{
		
		public function WaitAbility(owner:Entity) 
		{
			super();
			_buttonDescription = _description = _shortDescription = "Wait";
			this.owner = owner;
		}
		
		override public function isAvailable():Boolean 
		{
			return true;
		}
		
		override public function isTargetable():Boolean 
		{
			return false;
		}
		
		override public function useOn(target:Entity):void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			GameEngine.currentBattle.logCombatHeader.allegiance = combatComp.allegiance;
			GameEngine.logManager.appendToLog(combatComp.description + " awaits.");
		}		
		
	}

}