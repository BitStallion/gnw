package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class StunnedAbility extends Ability 
	{
		
		public function StunnedAbility(owner:Entity, msg:String) 
		{
			super();
			_description = msg;
			this.owner = owner;
		}
		
		public override function isTargetable():Boolean
		{
			return false;
		}
		
		public override function preRound():void
		{
			var context:Object = {"owner": owner.toParsingContext()};
			GameEngine.logManager.pushContext(context);
			GameEngine.logManager.appendToLog(description);
			GameEngine.logManager.popContext();
		}
		
		public override function useOn(target:Entity):void
		{
		}
	}
}