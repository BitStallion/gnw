package combat.ability.special
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class DefendAbility extends Ability
	{
		
		public function DefendAbility(owner:Entity)
		{
			super();
			_buttonId = Game.COMMON_BUTTON_05_ID;
			_buttonDescription = _shortDescription = "Defend";
			_description = "[owner.name] took a step back, allowing [#if enemy_is_alone]enemy's attack to miss[#else]enemies' attacks to miss[#endif], before returning to [owner.hisher] position for the next action.";
			this.owner = owner;
		}
		
		public override function isTargetable():Boolean
		{
			return false;
		}
		
		public override function preRound():void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			combatComp.defend();
			var context:Object = { "owner": owner.toParsingContext() };
			GameEngine.logManager.pushContext(context);
			GameEngine.logManager.appendToLog(description);
			GameEngine.logManager.popContext();
		}
		
		public override function useOn(target:Entity):void
		{
		}
	
	}

}