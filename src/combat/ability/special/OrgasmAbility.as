package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import data.EntityComponents.ECDynamicNumber;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class OrgasmAbility extends Ability
	{
		public function OrgasmAbility(owner:Entity)
		{
			super();
			_description = "[owner.name] falls to the ground wracked with orgasmic spasms.";
			this.owner = owner;
		}
		
		public override function isTargetable():Boolean
		{
			return false;
		}
		
		public override function preRound():void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var lust:ECDynamicNumber = owner.getComponent(EntityComponent.LUST) as ECDynamicNumber;
			var juice:ECDynamicNumber = owner.getComponent(EntityComponent.JUICE) as ECDynamicNumber;
			GameEngine.logManager.pushContext({"owner": owner.toParsingContext(), "target": owner.toParsingContext(), "effect": lust.value});
			GameEngine.logManager.appendToLog(description);
			juice.value -= lust.value;
			lust.value = 0;
			if(juice.value > 0)
			{
				GameEngine.logManager.appendToLog(Assets.TAKE_JUICE_DAMAGE);
			}
			else
			{
				combatComp.alive = false;
				GameEngine.logManager.appendToLog(Assets.JUICE_OUT);
			}
			GameEngine.logManager.popContext();
		}
		
		public override function useOn(target:Entity):void
		{
		}
	}
}