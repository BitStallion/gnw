package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import gui.Game;
	
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class WinAbility extends Ability 
	{
		
		public function WinAbility(owner:Entity)
		{
			super();
			_buttonId = Game.COMMON_BUTTON_11_ID;
			_buttonDescription = _description = _shortDescription = "Just Win";
			this.owner = owner;
		}
		
		override public function isTargetable():Boolean
		{
			return false;
		}
		
		override public function useOn(target:Entity):void 
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			GameEngine.currentBattle.cheatWin = true;
			GameEngine.currentBattle.surrender();
		}
		
	}

}