package combat.ability.special 
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECCombat;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class SurrenderAbility extends Ability 
	{
		
		public function SurrenderAbility(owner:Entity)
		{
			super();
			_buttonId = Game.COMMON_BUTTON_12_ID;
			_buttonDescription = _description = _shortDescription = "Surrender";
			this.owner = owner;
		}
		
		override public function isTargetable():Boolean
		{
			return false;
		}
		
		override public function useOn(target:Entity):void 
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			GameEngine.currentBattle.surrender();
		}
		
	}

}