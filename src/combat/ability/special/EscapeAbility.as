package combat.ability.special
{
	import combat.Ability;
	import data.Entity;
	import gui.Game;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class EscapeAbility extends Ability
	{
		
		public function EscapeAbility(owner:Entity)
		{
			super();
			_buttonId = Game.COMMON_BUTTON_08_ID;
			_buttonDescription = _description = _shortDescription = "Escape";
			this.owner = owner;
		}
		
		override public function isTargetable():Boolean
		{
			return false;
		}
		
		override public function useOn(target:Entity):void
		{
		
		}
	
	}

}