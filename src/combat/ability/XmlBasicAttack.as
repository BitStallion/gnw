package combat.ability
{
	import combat.Ability;
	import data.Entity;
	import data.EntityComponent;
	import data.EntityComponents.ECAttributes;
	import data.EntityComponents.ECCombat;
	import data.EntityComponents.ECDamageHandler;
	import data.EntityComponents.ECDynamicNumber;
	import data.entityEvents.EntityEventGetAttribute;
	import enums.CharacterStat;
	import enums.DamageType;
	import enums.Utils;
	import utility.GnWError;
	import utility.Util;
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class XmlBasicAttack extends Ability
	{
		private var _bab:uint;
		private var _mpCost:uint;
		private var _type:DamageType;
		private var _versus:String;
		private var _x:uint; //times
		private var _y:uint; //max damage
		private var _z:int; //increase damage by
		private var _hitMessage:String;
		private var _missMessage:String;
		
		public function XmlBasicAttack(xml:XML)
		{
			super();
			_shortDescription = xml.shortDescription.text();
			_description = xml.description.text();
			_buttonDescription = xml.buttonDescription != undefined ? xml.buttonDescription.text() : _shortDescription;
			_bab = parseInt(xml.bab.text());
			_mpCost = parseInt(xml.mpCost.text());
			_type = Utils.Parse(DamageType, xml.type.text().toString().toLowerCase()) as DamageType;
			_versus = xml.versus.text().toString().toLowerCase();
			_x = parseInt(xml.effect.@x);
			_y = parseInt(xml.effect.@y);
			_z = parseInt(xml.effect.@z);
			_hitMessage = xml.hitMessage.text();
			_missMessage = xml.missMessage.text();
			
			if(xml.category)
			{
				_category = xml.category.text();
			}
		}
		
		public function get MPCost():int
		{
			return _mpCost;
		}
		
		override public function isTargetable():Boolean
		{
			return true;
		}
		
		override public function isValidTarget(target:Entity):Boolean
		{
			var tcombatComp:ECCombat = target.getComponent(EntityComponent.COMBAT) as ECCombat;
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			return tcombatComp.active && combatComp.allegiance.IsOpposite(tcombatComp.allegiance);
		}
		
		override protected function inner_isAvailable():Boolean
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var magic:ECDynamicNumber = owner.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
			return!combatComp.isStunned() && (_mpCost == 0 || magic.value >= _mpCost);
		}
		
		override public function useOn(target:Entity):void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var magic:ECDynamicNumber = owner.getComponent(EntityComponent.SPIRIT) as ECDynamicNumber;
			var doge:EntityEventGetAttribute = new EntityEventGetAttribute();
			doge.attribute = "hit";
			doge.value = 0.75;
			owner.dispatchEvent(doge);
			doge.attribute = "doge";
			target.dispatchEvent(doge);
			var dex:EntityEventGetAttribute = new EntityEventGetAttribute();
			dex.attribute = CharacterStat.dexterity.text;
			owner.dispatchEvent(dex);
			var agi:EntityEventGetAttribute = new EntityEventGetAttribute();
			agi.attribute = CharacterStat.agility.text;
			target.dispatchEvent(agi);
			var chanceToHit:Number = Util.clampNumber(0.05, 0.95, doge.result + (dex.result - agi.result) / 100);
			magic.value -= _mpCost;
			
			for(var i:int = 0; i < _x; i++)
			{
				if(GameEngine.currentBattle.random.getNext() <= chanceToHit)
				{
					hit(target);
				}
				else
				{
					miss(target);
				}
			}
		}
		
		private function hit(target:Entity):void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var damage:Number = 0;
			damage += GameEngine.currentBattle.random.getNextInt(_y) + _z;
			if(category != "lust")
			{
				var charisma:EntityEventGetAttribute = new EntityEventGetAttribute();
				charisma.attribute = CharacterStat.charisma.text;
				owner.dispatchEvent(charisma);
				damage += charisma.result * 0.05;
			}
			else if(category != "melee")
			{
				var str:EntityEventGetAttribute = new EntityEventGetAttribute();
				str.attribute = CharacterStat.strength.text;
				owner.dispatchEvent(str);
				damage += str.result * 0.05;
			}
			
			var damageHand:ECDamageHandler = target.getComponent(EntityComponent.DAMAGE) as ECDamageHandler;
			damageHand.takeDamage(damage, _type, owner, _hitMessage);
			//applyEffect(target, effect);
			onHit(target);
		}
		
		private function miss(target:Entity):void
		{
			var combatComp:ECCombat = owner.getComponent(EntityComponent.COMBAT) as ECCombat;
			var context:Object = {"owner": owner.toParsingContext(), "target": target.toParsingContext()};
			GameEngine.logManager.pushContext(context);
			GameEngine.logManager.appendToLog(_missMessage);
			GameEngine.logManager.popContext();
		}
		
		protected function onHit(target:Entity):void
		{
		
		}
		
		/*private function getDefence(participant:Entity):int
		{
			switch(_versus)
			{
				case "ac": 
					return participant.getAC();
				case "reflex": 
					return participant.getReflex();
				case "fortitude": 
					return participant.getFortitude();
				case "will": 
					return participant.getWill();
				default: 
					throw new Error("_versus out of range");
			}
		}*/
		
		/*private function applyEffect(participant:Entity, effect:int):void
		{
			switch(_type)
			{
				case EntityComponent.DAMAGE: 
					participant.damage(effect);
					break;
				case "lust": 
					participant.lust(effect);
					break;
				default: 
					throw new Error("_type out of range");
			}
		}*/
	
	}

}