package combat
{
	
	/**
	 * ...
	 * @author Mihail Kamnev
	 */
	public class CombatStatus
	{
		public static const NotStarted:CombatStatus = new CombatStatus();
		public static const InProcess:CombatStatus = new CombatStatus();
		public static const Win:CombatStatus = new CombatStatus();
		public static const Lose:CombatStatus = new CombatStatus();
		public static const Draw:CombatStatus = new CombatStatus();
		
		public function CombatStatus()
		{
		
		}
	
	}

}