package combat 
{
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Kamnev Mihail
	 */
	public class Random 
	{
		private const MAX_RATIO:Number = 1 / uint.MAX_VALUE;
		private var r:uint;
		
		public function Random(seed:uint = 0) 
		{
			r = seed || getTimer();
		}
		
		//returns a random number from 0 to 1
		public function getNext():Number
		{
			/*r ^= (r << 21);
			r ^= (r >>> 35);
			r ^= (r << 4);*/
			return Math.random();// (r * MAX_RATIO);
		}
		
		public function getNextInt(n:uint):uint
		{
			var result:Number = getNext();
			return uint(Math.floor(result * n) + 1);
		}
		
	}

}