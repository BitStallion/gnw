package combat 
{
	import combat.ability.XmlBasicAttack;
	import data.IXMLDataLoader;
	import flash.utils.Dictionary;
	
	/**
	 * This class holds the 
	 * @author TheWrongHands
	 */
	public class CombatDataManager implements IXMLDataLoader 
	{
		private var _participants:Dictionary;
		private var _attacks:Dictionary;
		
		public function CombatDataManager() 
		{
			
		}
		public function startLoading():void 
		{
			_participants = new Dictionary();
			_attacks = new Dictionary();
		}
		
		public function canHandleXMLType(name:String):Boolean 
		{
			return name == "basicEnemies" || name == "basicAttacks";
		}
		
		public function readXMLData(file:String, xml:XML):void 
		{
			if(xml.name() == "basicEnemies")
			{
				for each(var enemy:XML in xml.basicEnemy)
				{
					_participants[enemy.@id.toXMLString()] = enemy;
				}
			}
			else if(xml.name() == "basicAttacks")
			{
				for each(var attack:XML in xml.basicAttack)
				{
					_attacks[attack.@id.toXMLString()] = attack;
				}
			}
		}
		
		public function getParticipantXML(id:String):XML
		{
			return _participants[id];
		}
		
		public function getAttack(id:String):XmlBasicAttack
		{
			return new XmlBasicAttack(_attacks[id] as XML);
		}
	}
}