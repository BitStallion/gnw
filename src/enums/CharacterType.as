package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class CharacterType extends Enum
	{
		{Utils.InitEnumConstants(CharacterType);} // static ctor
		
		public static const Player 		:CharacterType = new CharacterType(0);
		public static const Unique 		:CharacterType = new CharacterType(1);
		public static const Templated 	:CharacterType = new CharacterType(2);
		
		public function CharacterType(order:uint) 
		{
			super(order);
		}
	}
}