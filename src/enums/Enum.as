package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Enum 
	{
		internal var _text :String;
		internal var _order:uint;
		
		public function Enum(order:uint) 
		{
			_order = order;
		}
		
		public function get text():String
		{
			return _text;
		}
		
		public function get order():uint 
		{
			return _order;
		}
		
		public function toString():String
		{
			return _text;
		}
	}

}