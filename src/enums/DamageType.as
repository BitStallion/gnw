package enums 
{
	/**
	 * ...
	 * @author TheWrongHands
	 */
	public class DamageType extends Enum
	{
		{Utils.InitEnumConstants(DamageType);} // static ctor
		
		private static var count:uint = 0;
		public static const lust			:DamageType = new DamageType(count++);
		
		public static const physical		:DamageType = new DamageType(count++);
		public static const piercing		:DamageType = new DamageType(count++, physical);
		public static const slashing		:DamageType = new DamageType(count++, physical);
		public static const crushing		:DamageType = new DamageType(count++, physical);
		
		public static const magic			:DamageType = new DamageType(count++);
		public static const fire			:DamageType = new DamageType(count++, magic);
		public static const water			:DamageType = new DamageType(count++, magic);
		public static const wind			:DamageType = new DamageType(count++, magic);
		public static const earth			:DamageType = new DamageType(count++, magic);
		public static const lightning		:DamageType = new DamageType(count++, magic);
		public static const poison			:DamageType = new DamageType(count++, magic);
		public static const radiation		:DamageType = new DamageType(count++, magic);
		public static const disease			:DamageType = new DamageType(count++, magic);
		
		public var group:DamageType;
		
		public function DamageType(order:uint, group:DamageType = null)
		{
			super(order);
			this.group = group || this;
		}
	}
}