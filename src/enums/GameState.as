package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class GameState extends Enum 
	{
		{Utils.InitEnumConstants(GameState);} // static ctor
		
		private static var nextIndex:uint = 0
		public static const title			:GameState = new GameState(nextIndex++);
		public static const event			:GameState = new GameState(nextIndex++);
		public static const combat			:GameState = new GameState(nextIndex++);
		public static const inventory		:GameState = new GameState(nextIndex++);
		public static const navigation		:GameState = new GameState(nextIndex++);
		public static const character		:GameState = new GameState(nextIndex++);
		public static const charCreation	:GameState = new GameState(nextIndex++);
		public static const debug			:GameState = new GameState(nextIndex++);
		public static const options			:GameState = new GameState(nextIndex++);
		public static const saveLoad		:GameState = new GameState(nextIndex++);
		public static const credits			:GameState = new GameState(nextIndex++);
		public static const trade			:GameState = new GameState(nextIndex++);
		
		public function GameState(order:uint) 
		{
			super(order);
		}
		
	}

}