package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class EventType extends Enum 
	{
		{Utils.InitEnumConstants(EventType); } // static ctor
		
		public static const OneShot 	:EventType = new EventType(0);
		public static const Repeating	:EventType = new EventType(1);
		public static const PostCombat	:EventType = new EventType(2);
		
		public function EventType(order:uint) 
		{
			super(order);
		}
		
	}

}