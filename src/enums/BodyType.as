package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class BodyType extends Enum 
	{
		
		{Utils.InitEnumConstants(BodyType);} // static ctor
		
		public static const Thin		:BodyType = new BodyType(0);
		public static const Normal		:BodyType = new BodyType(1);
		public static const Muscular	:BodyType = new BodyType(2);
		public static const Chubby		:BodyType = new BodyType(3);
		public static const Husky 		:BodyType = new BodyType(4);
		
		public function BodyType(order:uint) 
		{
			super(order);
		}
	}
}