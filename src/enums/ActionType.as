package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class ActionType extends Enum 
	{
		{Utils.InitEnumConstants(ActionType);} // static ctor
		
		public static const event		:ActionType = new ActionType(0);
		public static const setVar		:ActionType = new ActionType(1);
		public static const removeVar	:ActionType = new ActionType(2);
		public static const goTo		:ActionType = new ActionType(3);
		public static const state		:ActionType = new ActionType(4);
		public static const trade		:ActionType = new ActionType(5);
		public static const combat		:ActionType = new ActionType(6);
		
		public function ActionType(order:uint) 
		{
			super(order);
		}
		
	}

}