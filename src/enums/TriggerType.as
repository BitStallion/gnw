package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class TriggerType extends Enum 
	{
		{Utils.InitEnumConstants(TriggerType);} // static ctor
		
		public static const OnEnter			:TriggerType = new TriggerType(0);
		public static const OnExit			:TriggerType = new TriggerType(1);
		public static const OnInteraction	:TriggerType = new TriggerType(2);
		
		public function TriggerType(order:uint) 
		{
			super(order);
		}
		
	}

}