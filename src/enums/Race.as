package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Race extends Enum 
	{
		{Utils.InitEnumConstants(Race); } // static ctor
		
		public static const Human 		:Race = new Race(0);
		public static const Dwarf 		:Race = new Race(1);
		public static const Elf 		:Race = new Race(2);
		public static const Ork 		:Race = new Race(3);
		public static const Goblin 		:Race = new Race(4);
		
		public function Race(order:uint) 
		{
			super(order);
		}
		
	}

}