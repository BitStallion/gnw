package enums 
{
	import flash.utils.describeType;
	
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Utils
	{
		internal static function InitEnumConstants(inType :*):void
		{
			var type:XML = describeType(inType);
			for each (var constant :XML in type.constant)
				inType[constant.@name]._text = constant.@name;
		}
		
		/**
		 * Returns an ordered list containing all Enums of a given type.
		 * 
		 * @param	inType	The type of Enum to the return the list for.
		 * @return	A list containing all Enums of the given type.
		 */
		public static function EnumList(inType :*):Vector.<Enum> {
			var list:Vector.<Enum> = new Vector.<Enum>();
			
			var type:XML = describeType(inType);
			for each (var constant :XML in type.constant)
				list.push(inType[constant.@name]);
				
			list.sort(function compare(x:Enum, y:Enum):Number { return x.order - y.order ; } )
				
			return list;
		}
		
		/**
		 * Tries to parse the name of an Enum of a given type, and returns an Enum with the matching name;
		 * 
		 * @param	inType		The type of Enum to matched.
		 * @param	enumName	The name of the Enum to be matched.
		 * @return	If the name is valid, the matching Enum is returned. If the name is not valid, null is returned.
		 */
		public static function Parse(inType :*, enumName:String):Enum {
			if (enumName == null || enumName.length == 0)
				return null;
			
			enumName = enumName.toLowerCase();
			
			var type:XML = describeType(inType);
			for each (var constant :XML in type.constant)
			{
				if (inType[constant.@name]._text.toLowerCase() == enumName)
					return inType[constant.@name];
			}
				
			return null;
		}
	}
}