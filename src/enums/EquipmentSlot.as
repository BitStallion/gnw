package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class EquipmentSlot extends Enum 
	{
		{Utils.InitEnumConstants(EquipmentSlot);} // static ctor
		
		public static const armor		:EquipmentSlot = new EquipmentSlot(0);
		public static const weapon		:EquipmentSlot = new EquipmentSlot(1);
		
		public function EquipmentSlot(order:uint) 
		{
			super(order);
		}
		
	}

}