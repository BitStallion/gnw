package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class CharacterStat extends Enum 
	{
		{Utils.InitEnumConstants(CharacterStat);} // static ctor
		
		private static var count:uint = 0;
		public static const strength		:CharacterStat = new CharacterStat(count++);
		public static const endurance		:CharacterStat = new CharacterStat(count++);
		public static const dexterity		:CharacterStat = new CharacterStat(count++);
		public static const agility			:CharacterStat = new CharacterStat(count++);
		public static const intelligence	:CharacterStat = new CharacterStat(count++);
		public static const charisma		:CharacterStat = new CharacterStat(count++);
		public static const willpower		:CharacterStat = new CharacterStat(count++);
		
		public function CharacterStat(order:uint) 
		{
			super(order);
		}
		
	}

}