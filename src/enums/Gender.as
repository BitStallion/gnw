package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class Gender extends Enum 
	{
		{Utils.InitEnumConstants(Gender); } // static ctor
		
		public static const Cuntboy	:Gender = new Gender(0);
		public static const Female	:Gender = new Gender(1);
		public static const Femboy	:Gender = new Gender(2);
		public static const Shemale	:Gender = new Gender(3);
		public static const Herm	:Gender = new Gender(4);
		public static const Male	:Gender = new Gender(5);
		
		public function Gender(order:uint) 
		{
			super(order);
		}
	}

}