package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class StairType extends Enum 
	{
		{Utils.InitEnumConstants(StairType); } // static ctor
		
		public static const UpDown		:StairType = new StairType(0);
		public static const DownOnly	:StairType = new StairType(1);
		public static const UpOnly		:StairType = new StairType(2);
		public static const Elevator	:StairType = new StairType(3);
		
		public function StairType(order:uint) 
		{
			super(order);
		}
		
	}

}