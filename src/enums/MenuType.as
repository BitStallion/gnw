package enums 
{
	/**
	 * ...
	 * @author GnW Team
	 */
	public class MenuType extends Enum 
	{
		{Utils.InitEnumConstants(MenuType); } // static ctor
		
		public static const MainScreen 		:MenuType = new MenuType(0);
		public static const TopAuxScreen 	:MenuType = new MenuType(1);
		public static const BottomAuxScreen :MenuType = new MenuType(2);
		
		public function MenuType(order:uint) 
		{
			super(order);
		}
	}
}