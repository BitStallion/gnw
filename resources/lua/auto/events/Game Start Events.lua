gnw.event.addEvent("new-game-preamble", "Preamble", nil, function(data)
	gnw.gui.imageDisplay.setImage("images/events/DeadWorldImage.png")
	local text = {"fuck ya, new system\n\n", "	The Demiurge, master of love and creation, made Eden. This was a perfect paradise. Animals lived in nature, mating with one another in a frenzy of love and sex that never ended. There was no disease or pain, hunger or want. In the centre of that paradise, was the Great Tree of Life. The canopy of the tree reached to the heavens and the leaves emitted a sunlight that covered the earth and the seas. The trunk of the tree acted as a great heart, pumping the wisdom of life; its roots like veins that reached every corner of Eden, giving energy, life, and warmth.\n\n",

	"	Before the Demiurge fell into a long slumber, they gave the beings a gift, a very special one, the gift of freedom, the gift of the decision. The creatures used this gift to grow and evolve, creating large and advanced metropolises where technology controlled everything. Over the years, the creatures left the natural world to live in their crystal cities where they corrupted themselves physically and mentally. They became a selfish and destructive society. Eden began to wilt; its Tree of Life was losing leaves faster and faster. These were the last days of excess and luxury.\n\n",

	"	The greatest war ever seen had begun. In the end, this war brought the end of civilization. Factories worked day and night, promoting rape while creating killer machines. The chimneys of these factories breathed out poison, polluting the word. The spirits that previously maintained balance of the world of Eden disappeared. The light of the Great Tree of Eden was extinguished, never to return.\n\n",

	"	The Demiurge, great creator of all, did nothing but mourn, causing massive floods in the cities. There were no winners, only pain and destruction. The Great Tree of Life stopped pumping its sap and Eden ran dry. All that was left was a sad and grey land of famine; the last leaf of the tree of life fell to the ground...\n\n",

	"	Then, there was only silence.../n/n",
	
	"	From beneath the rubble and ashes of civilization, the few survivors remaining saw that their paradise had changed. It was not the Eden they loved before, but a new one, referred to as Purgatory.\n\n",

	"	Years have passed and the first settlements begin to expand and redevelop to their former glory. Unfortunately, this land of sin and lust is filled with dangerous street gangs that work for chaos and disorder. The forests are infested with mutants, highways and roads are full of rapists, and the forces of law and order are gone. Most cities are empty ruins inhabited by mutants and monsters.\n\n",

	"	This is the lifeless body of Eden: Purgatory."}
	gnw.gui.eventMenu.setText(table.concat(text))
	gnw.gui.buttons.setAndShow(gnw.gui.buttons.YES_BUTTON_ID, "Continue", function()
		gnw.event.startEvent("new-game-baptism")
	end)
end);

gnw.event.addEvent("new-game-baptism", "Baptism", nil, function(data)
	gnw.gui.imageDisplay.setImage("images/events/BaptismImage.png")
	local text = {"	“The flames foretell many things,” says the old great Greenpaw gang shaman.  Beside him sits a little greenpaw girl who looks deeply into the campfire under the powerful shine of a new star in the night sky. “For those who are attuned to the ebb and flow of the world, fire can reveal that which is hidden from more short-sighted and foolish eyes. Watch the flames with me and you may find the way on your own.”\n\n",

	"	“Tonight the great spirits are restless, flames have revealed the truth to me... somewhere within our benighted world, a person has come into existence that will have the power to change the course of fate for every creature living on this changed land.  One day, they will use this power to tilt the fate of Eden down one of many different paths.” The little girl attentively observes the old shaman.\n\n",

	"	“Great shaman, is this new creature the chosen? Are they the saviour of our legends? Is it the one who will will find the hidden path to Eden, and make the world green and alive again?” the girl asks, her voice heavy with anticipation.\n\n",

	"	The old shaman merely sighs, his head sways back and forth like a tree rustling in the wind. “The chosen one has been born, but remember, they could be the land’s salvation, or, they can be the seed that sows the path of ultimate corruption. Remember, it will be up to you to find this creature of destiny, and show him the path towards salvation. And for that, you must be brave, and make yourself strong against the fear and emptiness that surrounds us. You, and they, must survive, for all our sakes…”\n\n",

	"	The girl looks out into the night, watching as the star struggles to be seen through the shimmering, sickening glow that distorts the sky. “Such a beautiful, bright star,” she says, reaching out as though it were tangible.\n\n",

	"	\"The stars are simply a greater flame, far, far away.\" The shaman says wistfully, following her gaze toward the sky. \"They are made not by beast, but by a force we cannot hope to comprehend. They foretell of much greater things, things that we could never see on our own.\" He looks toward the girl, a smile on his wizened face. “Tell me, by what name do you wish to call this one?” She looks to the night sky. The shaman can see the star shining in her eyes, which narrow into a great determination as she finally says, “Their name will be…”"}
	gnw.gui.eventMenu.setText(table.concat(text))
	gnw.gui.buttons.setAndShow(gnw.gui.buttons.YES_BUTTON_ID, "Continue", function()
		gnw.state.setState(gnw.state.charCreation)
	end)
end);

gnw.event.addEvent("new-game-start", "The Adventure Begins", nil, function(data)
	gnw.gui.imageDisplay.setImage("images/events/Underworld.png")
	local text = {"	You don't know where you were born or who your parents were. Like many people in this cruel and lustful world of sin, you ended up in an orphanage.\n You grew up in Marbroll, though as soon you were big enough you were thrown out onto the street like all the others you've known. You spent years doing odd jobs, trying to survive in the dark and filthy underground city Marbroll has become. Marbroll has always been dark and damp thanks to being underground, but since they closed the city it has only gotten worse. Large parts of the town lay in permanent darkness with mold growing everywhere, and only the rich walled off section has reliable light. Although, even they have darker places they dare not travel in.\n\n",
	
	"	You have had a strange mark on the back of your neck for as long as you remember. For all you know it could be a burn, a birthmark or a badly removed tattoo. Someone once told you that gangs often tattooed or branded babies to claim them from birth. Maybe you were disowned and abandoned later on and had the tattoo poorly removed?\n\n",
	
	" Be it a strange ritual or an accident, you have no idea how you got the mark or what it means. Though the strange mark on the back of your neck is the least of your worries. Marbroll is hell and is killing you slowly and painfully. The 'filtered' water is awful, with too much radiation and rust. The air filters don't work most of the time, with many people dying of silicosis. Even those lucky enough to have masks are dying from starvation and dehydration when their body refuses to keep a hold of the water anymore.\n\n",
	
	"	The mines are depleted, with increasingly less material found each shift. While the number of dead workers only increases faster as the greed of the corporations rises. The local economy is nearly dead, and other than the lethal mining work, there are no real jobs, only poverty, misery and corruption. While the poor talk of revolution, the rich have begun to fear losing their power. They have closed the city to stop anyone escaping while the guards abuse the population. This hellhole has become a fascist state to the worst degree. Hatred permeates the atmosphere as the poor and rich seethe in anger. Marbroll is about to drown in it's own blood.\n\n",
	
	"Good thing you have a plan to leave!\n\n",
	
	"	While the city doors are welded shut and guarded, you know parts of the old mines aren't so secure. You heard that there's a conduit that communicates with the old sewers. If you manage to navigate the sewers, you will reach the water treatment plant, which leads directly outside to freedom.\n\n",
	
	" This is your last chance to escape and seek a better life up there in the wastes. With the money you scraped together over the years you've managed to buy a small laser gun and some supplies. You hope you have enough to defend yourself from the vicious and dangerous creatures of the underworld.\n\n",
	
	"	While the miners were working and the officers near your alley were abusing someone in a dark corner, you made your move. Sprinting through the poorly lit tunnels towards the conduit, you threw yourself into it, terrified the officers would find you. The conduit was a massive steel slide seemingly without an end. You don't know how long you took before you finally landed, but it was long enough for the friction to start to burn a bit. With a thud, you land on your rear in the center of a smelly tunnel full of rust, dirt and pipes only illuminated by ancient security lights in patches here and there. Dark water flows slowly by, strange shapes floating along the flow while odd noises come from every direction.\n\n",
	
	"You are not alone!\n\n",
	
	"	With a cheap and old small laser gun in hand you are ready to explore the area. You know you have to find the water purification plant to get outside and be free...\n\n",
	
	"	You are finally going to leave the underworld. Your heart beats with fear and excitement alike, it is time to escape!"}
	gnw.gui.eventMenu.setText(table.concat(text))
	gnw.gui.buttons.setAndShow(gnw.gui.buttons.YES_BUTTON_ID, "Continue", function()
		gnw.goto("underworld_sewers", "Level 3", "new_game_start_room")
	end)
end);