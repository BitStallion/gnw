gnw.event.addEvent("meeting-digit", "Meeting Digit", function()
	return not (as3.tolua(gnw.vars.MetDigit) or false)
end, function()
	gnw.gui.imageDisplay.setImage("images/character/Digit/DigitSmall.png")
	gnw.gui.eventMenu.setText("	Something swift darts through the dark tunnels, a blue, electrical wisp with incredible speed... You quickly unholster your pistol and aim it towards the phenomenon, but it seems to have disappeared. You are left baffled as to what it may have been. Seeing as it has vanished, you stash your pistol away and attempt to relax.")
	gnw.gui.buttons.setAndShow(gnw.gui.buttons.YES_BUTTON_ID, "Continue", function()
		gnw.gui.imageDisplay.setImage("images/character/Digit/DigitNormal.png")
		local text = {"	Suddenly, something presses itself against your pocket. You look down to discover a small levitating creature with a punky looking hair made of electricity with some metal ornaments, no more than a few centimeters in height, rubbing her odd little chin with an enthusiasm you haven�t seen before.\n\n",
		"�Oooooh! You�ve got Sayota 3000 batteries in your PDA! MY FAVORITE!� the small creature clasps her hands together with an undisguised glee. �I can almost taste that scrumptious energy... Heheheheh...�   	You watch in amusement as the little thing�s eyes glaze over as she imagines what you assume to be a tasty treat. She snaps to, ceasing her excess salivation, and replaces the ecstatic grin with a smirk of superiority.\n\n",
		"�AHEM!� she turns her head to the side as she rises to eye level, appearing almost snooty and condescending in nature.\n\n",
		"�You�ve managed to come across me! Aren�t you lucky?� The small creature turns back to you, giving an exaggerated version of what could be her giving you the once-over.\n\n",
		"�Look at you! You barely look as if you�ve ever left your home, much less your egg! But never mind that, your savior is here!�  She places her hands on her curvy hips, assuming a heroic stance before your eyes.\n\n",
		"You�re about to object when she continues on, �I�ve spent a lot of time inside a lot of electronics and tech, and I don�t want to be cocky, but I�m totally the most smarter-est person ever!� You raise your hand to point out that the word smarter-est doesn�t exist, but she continues talking, ever ignorant of your thoughts.\n\n",
		"�I can teach you many things, my pupil! The world and its knowledge is nothing to me! Muahahahahah!� The small creature bursts into what seems to be maniacal laughter, leaving you at a slight loss of words.\n\n",
		"�Who knows? Maybe you�ll be able to escape this place with your life if I help you out a bit...� she checks her non-existent nails, checking for imperfections as you finally point out that you actually DON�T want her help. �Yeah, fine, I�ll help yo�WHAT?!�\n\n",
		"You sigh to yourself, gathering your thoughts as you begin to walk past her, done with the small ordeal.\n\n",
		"�Hey! I SAID HEY! DON�T YOU TURN YOUR BACK TO ME!� You sigh heavily to yourself, and turn to face the small pixie-like creature. She gives you a slightly angry look, and the blue energy running through her takes a slightly reddish hue before returning to its normal color.\n\n",
		"�Let�s face it, you�re weak and pathetic, and definitely look the part! You won�t survive a day without me, which is why I, Digit Dee, will be your guide! I can�t just let you wander off by yourself. My conscience would wreak havoc on my poor soul!� She dramatically places the back of her hand to her head, and faints dramatically, falling flat against an invisible surface at your eye level. She glitches a moment later and is standing proudly before you once again.\n\n",
		"�Well, off we go! Let me just jump into this delicious PDA of yours... so full of precious, scrumptious energy...� She trails off a bit, and the apparently common glazed-over look in her eyes returns for a moment, before she snaps out of it with a squeak. With a quick bow and flourish, Digit Dee transforms into an electric spark and zaps into your PDA before you can object. You shrug to yourself, figuring that she might be useful at some point in the future. For now, she remains inside your device, likely feasting on the energy provided by the batteries inside.\n\n"}
		gnw.gui.eventMenu.setText(table.concat(text))
		gnw.vars.MetDigit = true
		gnw.gui.buttons.setAndShow(gnw.gui.buttons.YES_BUTTON_ID, "Continue", function()
			gnw.state.endEvent()
		end)
	end)
end)