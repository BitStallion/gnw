<?xml version="1.0" encoding="UTF-8"?>
<tileset name="underworld escape tileset" tilewidth="32" tileheight="32" tilecount="105">
 <image source="../../images/tileset/underworld escape tileset.png" width="224" height="480"/>
 <tile id="49">
  <properties>
   <property name="TileType" value="TileSafeFloor"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="64">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="TileType" value="TileGenericFloor"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="TileType" value="TileSafeFloor"/>
  </properties>
 </tile>
</tileset>
