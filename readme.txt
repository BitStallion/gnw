

_______  _______  _        _______  _______    _        _                      _______  _______  _______  _______ 
(  ____ \(  ___  )( (    /|(  ____ \(  ____ \  ( (    /|( )  |\     /||\     /|(  ___  )(  ____ )(  ____ \(  ____ \
| (    \/| (   ) ||  \  ( || (    \/| (    \/  |  \  ( ||/   | )   ( || )   ( || (   ) || (    )|| (    \/| (    \/
| |      | (___) ||   \ | || |      | (_____   |   \ | |     | | _ | || (___) || |   | || (____)|| (__    | (_____ 
| | ____ |  ___  || (\ \) || | ____ (_____  )  | (\ \) |     | |( )| ||  ___  || |   | ||     __)|  __)   (_____  )
| | \_  )| (   ) || | \   || | \_  )      ) |  | | \   |     | || || || (   ) || |   | || (\ (   | (            ) |
| (___) || )   ( || )  \  || (___) |/\____) |  | )  \  |     | () () || )   ( || (___) || ) \ \__| (____/\/\____) |
(_______)|/     \||/    )_)(_______)\_______)  |/    )_)     (_______)|/     \|(_______)|/   \__/(_______/\_______)



resources - All embedded files should go here (images, xml, etc.)
scripts - Python scripts to help automate the build process go here
src
	combat - Classes and systems related to combat.
	data - Used for storing information about characters, events, items and the world.
	enums - All classes in this folder are enums or enum utility classes.
	events - Systems and data objects that deal with events.
	font - All embed fonts used in the game are located here.
	gui - All ui components and screens are located here.
	images - All images used in the game are located here.
	navigation - All classes and systems related to navigating the world.
	parsing - Classes and systems related to parsing.
	system - Classes and systems related to saving/loading.
	xml - All embedded XML files are located here.

Important: As we move forward with the project, we must be sure that we don't use any flash components that limit our ability to
distribute the game on other sites. This means that we won't be able to use things like stage3D or any libraries that take advantage of it.

Table of contents:
	1.0 How to use Git on Windows
		1.1 Required Software
		1.2	Setup Git
		1.3 Commit changes to the repository
		1.4 Useful git commands
	2.0 How to setup the programing environment for Windows
		2.1 Required Software
		2.2 Setup FlashDevelop
		2.3 Useful changes to Windows (Optional)
	3.0	How to create a map for GnW
		3.1 Required Software
		3.2 Creating a new tileset(.tsx files)
		3.3 Creating a new map(.tmx files)

1.0 How to use Git on Windows:
	1.1 Required Software:
		-Git: http://git-scm.com/
	1.2 Setup Git:
		-Install Git
			-Default settings for everything usually work just fine
		-Next find where on your computer you want to keep the Gangs n' Whores project files
		-create a new folder there and name it what ever you like
		-Open the folder you just created
		-Right click on the empty white space and select "Git Bash" from the drop down menu, a shell prompt window should open up
		-Do one of the following:
			-If you have an account with bitbucket.org then use the following command, replacing [Username] with your user name from that site:
				git clone https://[Username]@bitbucket.org/BitStallion/gnw.git
			-If you only wish to access the code without an account, use the following command:
				git clone https://bitbucket.org/BitStallion/gnw.git
		-Once it has finished, close the shell prompt
		-You will notice there is a new folder named "gnw", this contains everything that was in the repository
			any time you wish to interact with git from now on, you will right click on this folder and select "Git Bash" from the drop down menu
		-The rest of this is all optional but recommended
		-Open the git bash again
		-You can set the name that is associated with any commit you make by typing in the following command, replacing [Name] with your user name:
			git config user.name "[Name]"
		-You can set the email address that is associated with any commit you make by typing in the following command, replacing [Email] with your email address:
			git config user.email "[email]"
		-Close the shell again
	
	1.3 Commit changes to the repository
		-Keep all your change notes in a file called "commit.txt" in the same directory as the gnw folder but not in the gnw folder
		-Open the git bash for the repository
		-Type in
			git stash
			git pull --rebase
			git stash apply
				-You may get merge conflict notices, if you do it will have made notes about the conflicts inside the listed files, it should be fairly easy to go in and fix them.
				-After you have corrected the conflicts, continue on to the next step
			git add -A
			git commit -F "../commit.txt"
			git push origin master
			
	1.4 Useful git commands
		git clone [url]
		git status
		git add [file]
		git add -u
		git add -A
		git commit -m "[message]"
		git push origin master
		git stash
		git pull --rebase
		git stash apply

2.0 How to setup the programing environment for Windows:
	2.1 Required Software:
		-Java SE Runtime Environment 7 32bit (jre-7-windows-i586.exe): http://www.oracle.com/technetwork/java/javase/downloads/java-se-jre-7-download-432155.html
		-Microsoft .NET Framework v2.0: http://www.microsoft.com/en-us/download/details.aspx?id=1639
			-Note the best way to get/install this is though windows update if you can
		-Python 3.4.2: https://www.python.org/downloads/release/python-342/
		-FlashDevelop 4.6.4: http://www.flashdevelop.org/

	2.2 Setup FlashDevelop:
		-Install Java, .Net Framework, Python, and FlashDevelop
			-Default settings for everything usually work just fine
		-Run FlashDevelop
		-The first time you run it a window should pop up titled "AppMan", if it dose not or you accidentally close it then you can reopen it by going to:
			Tools->Install Software...
		-Check "Flex SDK" and "Flash Player (SA)"
		-Click Install Items, it make take a few minutes
		-After everything has finished installing (you can tell by the progress bar will have stopped doing stuff and the items you checked before will now have the word installed next to them somewhere) close "AppMan"
		-Next go to:
			Tools->Custom Arguments...
		-Select "Argument: $(DefaultUser)" in the list on the left
		-Change the value on the right to what ever name you wish to go by
		-Click close
		-Close out of FlashDevelop

	2.3 Useful changes to Windows (Optional)
		-Goto Start(the windows button on the taskbar)->Computer
		-Press Alt on your keyboard
		-Goto Tools->Folder Options
		-Goto the View tab
		-Select "Show hidden files, folders, and Drives"
		-Uncheck "Hide empty drives in the Computer folder"
		-Uncheck "Hide extensions for known file types"
		-Click Ok

3.0	How to create a map for GnW:
	3.1 Required Software:
		-Tiled: http://www.mapeditor.org/download.html
	3.2 Creating a new tileset(.tsx files):
		-All tiles must be 32x32 pixels and all tileset images must be a multiple of that
		-Place all tileset images you wish to use in "resources/images/tileset" of the git repository from 1.2
		-Run Tiled
		-File->New...
		-At this point the only setting that is important is the tile size need to be set to 32x32
		-Click OK
		-You have just created a new map, but this is a temporary map that we won't be keeping
		-Next goto Map->New Tileset...
		-If your tileset is all in 1 image then do the following:
			-The name will be filled in automatically so select "Based on Tileset Image" from the drop-down labeled "Type"
			-Click on the "Browse..." button and select the appropriate image in "resources/images/tileset" of the git repository from 1.2
			-Make sure "Use transparent color" is unchecked and that the tile width and height is set to 32x32
			-Click OK
			-On the right you'll see that your new tileset has been added to the middle window
		-If your tileset has the tiles in their own separate image files, then do the following instead:
			-Give your tileset a name
			-Select "Collection of Images" from the drop-down labeled "Type"
			-Click OK
			-On the right you'll see that your new tileset has been added to the middle window
			-Click on the button that looks like a plus sign with the tooltip "Add Tiles" and select the images you would like to add as tiles from "resources/images/tileset"
				-you can select more then one at a time by holding the shift/ctrl key
		-Click on the button that looks like a page with an arrow going away from it with the tooltip "Export Tileset As"
		-Save your tileset in the "resources/xml/tileset" folder of the git repository from 1.2
		-Now that you have your tileset, goto File->Close
	3.3 Creating a new map(.tmx files):
		-Run Tiled
		-File->New...
		-Set Orientation to Orthogonal
		-Tile layer format can be anything but "Base64(gzip compressed)", although "Base64(zlib compressed)" is recommended
		-Tile render order should be "Right Down"
		-Map size can be what ever you want(can be adjusted later, so don't worry about getting it perfect)
		-Tile size should be 32x32
		-Click OK
		-Add the tilesets (created in 3.2) you want to use by going to Map->Add External Tileset...
		-When your finished making your map, save it to the "resources/xml/locations" folder of the git repository from 1.2
