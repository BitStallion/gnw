#!/usr/bin/python

import os
import re

def TraverseDir(Path, FileList, RelBase):
	for File in os.listdir(Path):
		FullPath = os.path.join(Path, File);
		if os.path.isdir(FullPath):
			TraverseDir(FullPath, FileList, RelBase);
		else:
			FileList.append(os.path.relpath(FullPath, RelBase).lower().replace('\\', '/'));
	return;

GnWFilePath = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/..");
ResourceFilePath = os.path.abspath(GnWFilePath + "/resources");
ResourceFileList = [];
ResourceList = [];
TraverseDir(ResourceFilePath, ResourceFileList, ResourceFilePath);
ResourceFileList.sort();
OutputFile = open(GnWFilePath + "/src/ResourceFile.as", "w");
OutputFile.write("package\n");
OutputFile.write("{\n");
OutputFile.write("\timport data.FileResource;\n");
OutputFile.write("\timport flash.utils.Dictionary;\n");
OutputFile.write("\t/**\n");
OutputFile.write("\t * This is an automatically generated file, do not edit directly\n");
OutputFile.write("\t *\n");
OutputFile.write("\t * @author " + os.path.relpath(__file__, GnWFilePath).replace('\\', '/') + "\n");
OutputFile.write("\t */\n");
OutputFile.write("\tpublic class ResourceFile\n");
OutputFile.write("\t{\n");
for File in ResourceFileList:
	Extention = os.path.splitext(File)[1];
	ClassName = re.sub(r"\W", '_', File);#.replace('/', '_').replace('.', '_').replace(' ', '_').replace('-', '_');
	Type = "application/octet-stream";
	if Extention == ".png":
		Type = "image/png";
	if Extention == ".mp3":
		Type = "audio/mpeg";
	ResourceList.append({"File":File, "ClassName":ClassName, "Type":Type})
	OutputFile.write("\t\t[Embed(source = \"../resources/" + File + "\", mimeType=\"" + Type + "\")]\n");
	OutputFile.write("\t\tpublic static const " + ClassName + ":Class;\n");

OutputFile.write("\n");
OutputFile.write("\t\tpublic static function loadResources(resources:Dictionary):void\n");
OutputFile.write("\t\t{\n");
for File in ResourceList:
	OutputFile.write("\t\t\tresources[\"" + File["File"] + "\"] = new FileResource(" + File["ClassName"] + ", \"" + File["Type"] + "\", \"" + File["File"] + "\");\n");
OutputFile.write("\t\t}\n");
OutputFile.write("\n");
OutputFile.write("\t\tpublic function ResourceFile()\n");
OutputFile.write("\t\t{\n");
OutputFile.write("\t\t}\n");
OutputFile.write("\t}\n");
OutputFile.write("}\n");
OutputFile.close();